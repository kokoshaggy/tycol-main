<?php

use yii\db\Migration;

/**
 * Class m180905_153706_create_folder_parent
 */
class m180905_153706_create_folder_parent extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {		
	
		$this->addColumn( '{{%folder}}', 'title', $this->string(40)->notNull()->after('id') );
		$this->addColumn( '{{%folder}}', 'parent_id', $this->integer()->defaultValue(0)->after('id') );
		
		$this->dropColumn('{{%folder}}', 'tyc_ref');
		$this->dropColumn('{{%folder}}', 'type');
		$this->dropColumn('{{%folder}}', 'notes');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "Reverting this migration (new folder structure) is not advisable. Reverting for development ease.";
		
		$this->dropColumn('{{%folder}}', 'title');
		$this->dropColumn('{{%folder}}', 'parent_id');

		$this->addColumn('{{%folder}}', 'type', $this->string()->after('id'));
		$this->addColumn('{{%folder}}', 'tyc_ref', $this->string(16)->unique()->after('id'));
		$this->addColumn('{{%folder}}', 'notes', $this->string()->after('description'));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180905_153706_create_folder_parent cannot be reverted.\n";

        return false;
    }
    */
}
