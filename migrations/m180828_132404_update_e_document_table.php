<?php

use yii\db\Migration;

/**
 * Class m180828_132404_update_e_document_table
 */
class m180828_132404_update_e_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {		
		$this->addColumn('{{%e_document}}', 'creation_date', $this->datetime());
		$this->addColumn('{{%e_document}}', 'last_updated', $this->datetime());
		$this->addColumn('{{%e_document}}', 'deleted', $this->boolean());
		
		// create a one to many table. (This is a little mind boggling. An e-document with multiple locations?)
		$this->createTable("{{%e_doc_file_location}}", [
										'e_document_id' => $this->integer(),
										'file_location' => $this->string(),
										'PRIMARY KEY(e_document_id, file_location)',
        ]);
		
		// creates index for e_document_id
        $this->createIndex(
            'idx-e_doc_file_location-e_document_id',
            "{{%e_doc_file_location}}",
            'e_document_id'
        );

        // add foreign key to table `e_document`
        $this->addForeignKey(
            'EDocumentItem',
            "{{%e_doc_file_location}}",
            'e_document_id',
            "{{%e_document}}",
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "Update e-document table should not be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180828_132404_update_e_document_table cannot be reverted.\n";

        return false;
    }
    */
}
