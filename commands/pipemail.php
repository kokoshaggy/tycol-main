#!/usr/bin/php -q
<?php
namespace app\commands;

use yii\console\Controller;
 
use Yii;
use app\models\Task;
use app\models\Project;
use app\models\Remark;
use yii\console\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;
 
/**
 * Test controller
 */
class PipemailController extends Controller {
	
	function actionIndex(){
		$remark = new Remark;
		$project = new Project;
		$fd = fopen("php://stdin", "r");
		$email = '';
		while (!feof($fd)) {
  			$email .= fread($fd, 1024);
		}
		fclose($fd);
		$lines = explode("\n", $email);
		$Subject='';
		for ($i=0; $i < count($lines); $i++){
    // look out for subject
    		if (preg_match("/Subject:/", $lines[$i], $matches)) {
				list($One,$Subject) = explode("Subject:", $lines[$i]);    
				list($Subject,$Gone) = explode("<", $Subject);
			}
        
    	}
		
		$getProjectId = Project::find()->select('id')->where(['LIKE', 'ref', $Subject])->one();
		
		if(!empty($getProjectId)){
			$remark->remark_type = "project";
			$remark->project_id = $getProjectId;
			$remark->text = $email;
			$remark->remark_date = new Expression('NOW()');
			$remark->tyc_ref = $project->find()->select('tyc_ref')->where(['id'=>$getProjectId])->one;
			
			
		}
	
	
	}
}






