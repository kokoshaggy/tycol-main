<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $name  string user name*/
/* @var $email string user email */
/* @var $body  string the review */
?>
<div class="admin-feedback">
    <p>You Created A New Task</p>

    <blockquote>
        <?=$body; ?>
    </blockquote>
</div>