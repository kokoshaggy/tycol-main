<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Tmproject */
/* @var $form yii\widgets\ActiveForm */
?>
<section>
<div class="new-client-form">

    
	<?php $personsform = ActiveForm::begin(['action' =>['client/create'],'enableClientValidation' => true, 'attributes' => $clientModel->attributes(),'enableAjaxValidation' => false,'id' => 'clientforms']); ?>
	
    <?= $personsform->field($clientModel, 'corporation_id')->dropdownList(ArrayHelper::map($newClient, 'id', 'name'));?>
    
    
    <div class="form-group">
		<p>
			<br>
        	<?= Html::submitButton($clientModel->isNewRecord ? '<span id="clientbuttonText">Make a CLient</span> <img id="clientloader" src="images/45.gif" " /> <span id="clientloader1"><span>' : 'Update', ['class' => $clientModel->isNewRecord ? 'btn btn-danger' : 'btn btn-danger','id'=>'clientsubmit_id']) ?>
		</p>

    </div>

    <?php ActiveForm::end(); ?>

</div>



</section>
<?php 
$getClient = Url::to(['/client/newclient']);
$clientJs = <<<JS

$('#clientforms').on('beforeSubmit', function (e) {
	$('#clientbuttonText').hide();
 	$('#clientloader').show();
    var \$clientform = $(this);
    $.post(\$clientform.attr('action'),\$clientform.serialize())
    .always(function(result){
	
	$(document).find('#clientloader').hide();
   if(result=='sent'){
	   //document.getElementById(name)
	   
	  if($(document).find('#clientsupplier').length){
	  
	  	$.get("$getClient", function(data){
        	$(document).find('.clientRefesh select').prepend('<option selected value="'+data.clientId+'" >'+data.corpName+'</option>');
    	});
	  
	  	
	  	$(document).find('#clientsupplier').modal('toggle');
		
	  
	  } else{
	  
	  	$('#dashboard').modal('toggle');
	 	$.pjax.reload({container:"#person_reload",async: false
		}); 
	  
	  }
	   $(document).find('#clientloader1').html('Sent').show();
	   
    
    }else{
    $(document).find('#clientloader1').html('Sorry an error occured').show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#clientloader').hide();
	$(document).find('#clientloader1').hide();
	$(document).find('#clientbuttonText').show();
	}, 5000);
    return false;
    
    
    
});
JS;
 
$this->registerJs($clientJs);
?>





