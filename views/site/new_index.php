<?php
use app\models\Folder;

use yii\widgets\Pjax;
use app\controllers\ProjectController as project;
use yii\helpers\Html;
use app\models\Person;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\bootstrap\Alert;

$this->title = Yii::t('dashboard', 'dashboard_title');

/* @var $this yii\web\View */

?>

<style>
	#flash {
		display: none;
	}

	#dashboard-content {
		display: grid;
		grid-gap: 50px 10px;
		grid-template-columns: 40% 20% 40%;
		grid-template-areas: 	'folders folders folders'
								'flash flash flash'
								'remarks tasks tasks';
	}
	
	.grid-item {
		
	}
	
	.grid-item.folder {
		grid-area: folders;
	}
	
	.grid-item.flash {
		grid-area: flash;
	}
	
	.grid-item.remark-box {
		grid-area: remarks;
	}
	
	.grid-item.task-box {
		grid-area: tasks;
	}
	
	@media screen and (min-width: 280px) and (max-width: 599px) {
			#dashboard-content {
				grid-gap: 0px 20px;
				grid-template-columns: 100%;
				grid-template-areas: 	'folders'
										'flash'
										'tasks'
										'remarks';
			}
	}
</style>

<!-- Main content -->
<?php $this->beginBlock('folderview'); 
	echo Yii::t('dashboard', 'product_name');
?>
<small><?php echo Yii::t('dashboard', 'to_dashboard'); ?></small>   
<?php $this->endBlock(); ?>

<section id="dashboard-content">
	<div class="grid-item folder">
		<?=$this->render('/folder/latest', ['folders' =>$folders]);?>
	</div>
		
	<div class="grid-item flash"> <!-- **********DIV FOR FLASH MESSAGE********** -->
		<?= Alert::widget([
			   'options' => ['class' => 'alert-info','id'=>'flash'],
			   'body' => Yii::$app->session->getFlash('created_successfully'),
				 ]);?>
	</div> <!-- ********** FLASH MESSAGES DIV CLOSED ********** -->

	<div id="dashboard-remarks" class="grid-item remark-box" > <!-- **********DIV FOR REMARKS*********-->
		<?php echo $this->render('/remark/remarks_feed', ['remarks' => $recent_remarks]); ?>
	</div> <!-- **********REMARKS DIV CLOSED*********-->

	<div id="dashboard-task-reminder" class="grid-item task-box"> <!-- **********DIV FOR TASKS AND REMINDERS -->
		<?php echo $this->render('/task/dashboard_task', ['viewTask' => $viewTask]); echo " Task count " . count($viewTask); ?>
	</div> <!-- **********TASK AND REMINDER DIV COMPLETED********** -->
</section>
      

<?php 
$indexJs = <<<JS

$('#refresh').click(function(){ $.pjax.reload({container:"#content",async: false
}); })

	$('.test3').each(function(){
	$(this).click(function(){
		$('#task'+$(this).data('number')).slideToggle();

		if($(this).hasClass('fa-caret-down')){
				$(this).removeClass('fa-caret-down').addClass('fa-caret-up');
			} else {
				$(this).removeClass('fa-caret-up').addClass('fa-caret-down');
			}
		})
	})
    $('.test1').each(function(){
	$(this).click(function(){
		$('#task2'+$(this).data('number')).slideToggle();

		if($(this).hasClass('fa-caret-down')){
				$(this).removeClass('fa-caret-down').addClass('fa-caret-up');
			} else {
				$(this).removeClass('fa-caret-up').addClass('fa-caret-down');
			}
		})
	})
    
    $('.test').each(function(){
	$(this).click(function(){
		$('#task'+$(this).data('number')).slideToggle();

		if($(this).hasClass('fa-caret-down')){
				$(this).removeClass('fa-caret-down').addClass('fa-caret-up');
			} else {
				$(this).removeClass('fa-caret-up').addClass('fa-caret-down');
			}
		})
	})

	

	$('.client').on('click', function() {
					$(document).find('#sliderwizz1').show();
					$(document).find('#sliderwizz').hide();
					$(document).find('#sliderwizz2').hide();
					$(document).find('#sliderwizz3').hide();
	})
	
	$('.supplier').on('click', function() {
					$(document).find('#sliderwizz2').show();
					$(document).find('#sliderwizz1').hide();
					$(document).find('#sliderwizz3').hide();
					$(document).find('#sliderwizz').hide();
	})
	
	$('.contact').on('click', function() {
					$(document).find('#sliderwizz3').show();
					$(document).find('#sliderwizz2').hide();
					$(document).find('#sliderwizz1').hide();
					$(document).find('#sliderwizz').hide();
	})
	
	$('#activeuser').on('click', function() {
					$(document).find('#sliderwizz').show();
					$(document).find('#sliderwizz3').hide();
					$(document).find('#sliderwizz2').hide();
					$(document).find('#sliderwizz1').hide();
	})
JS;
 
$this->registerJs($indexJs);
?>




