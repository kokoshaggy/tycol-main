<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Tmproject */
/* @var $form yii\widgets\ActiveForm */
?>
<section>
<div class="new-supplier-form">

    
	<?php $supplierform = ActiveForm::begin(['action' =>['supplier/create'],'enableClientValidation' => true, 'attributes' => $supplierModel->attributes(),'enableAjaxValidation' => false,'id' => 'supplierforms']); ?>
	
    <?= $supplierform->field($supplierModel, 'corporation_id')->dropdownList(ArrayHelper::map($newSupplier, 'id', 'name'));?>
    
    
    <div class="form-group">
		<p>
			<br>
        	<?= Html::submitButton($supplierModel->isNewRecord ? '<span id="supplierbuttonText">Make a supplier</span> <img id="supplierloader" src="images/45.gif" " /> <span id="supplierloader1"><span>' : 'Update', ['class' => $supplierModel->isNewRecord ? 'btn btn-danger' : 'btn btn-danger','id'=>'suppliersubmit_id']) ?>
		</p>

    </div>

    <?php ActiveForm::end(); ?>

</div>



</section>
<?php 
$getSupplier = Url::to(['/supplier/newsupplier']);
$supplierJs = <<<JS

$('#supplierforms').on('beforeSubmit', function (e) {
	$('#supplierbuttonText').hide();
 	$('#supplierloader').show();
    var \$supplierform = $(this);
    $.post(\$supplierform.attr('action'),\$supplierform.serialize())
    .always(function(result){
	
	$(document).find('#supplierloader').hide();
   if(result=='sent'){
   
   if($(document).find('#clientsupplier').length){
	  
	  	$.get("$getSupplier", function(data){
        	$(document).find('.supplierRefesh select').prepend('<option selected value="'+data.clientId+'" >'+data.corpName+'</option>');
    	});
	  
	  	
	  	$(document).find('#clientsupplier').modal('toggle');
		
	  
	  } else{
	  
	  	$('#dashboard').modal('toggle');
	 	$.pjax.reload({container:"#person_reload",async: false
		}); 
	  	$(document).find('#supplierloader1').html('Sent').show();
	  }
	   
	   
	   
    
    }else{
    $(document).find('#supplierloader1').html('Sorry an error occured').show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#supplierloader').hide();
	$(document).find('#supplierloader1').hide();
	$(document).find('#supplierbuttonText').show();
	}, 5000);
    return false;
    
    
    
});
JS;
 
$this->registerJs($supplierJs);
?>





