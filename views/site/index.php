<?php
use app\models\Folder;

use yii\widgets\Pjax;
use app\controllers\ProjectController as project;
use yii\helpers\Html;
use app\models\Person;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\bootstrap\Alert;

$this->title = "TycolMain - Dashboard";

/* @var $this yii\web\View */


//$this->title = 'My Yii Application';
?>

<style>
    .dashboardicons{ 
        background:#ccc;
        color: #000;
    }

    .box-black{
    	border:none !important;
    }
	
	.indexFormTitle{
		background:#fff;
		margin-bottom:5px; 
		margin-top:-20px; 
		width:130px; 
		margin-left:10%; 
		text-align:center; 
		font-size:17px;
		color: #d73925;
	}
	
	.indexFormContainer{
		border:#888 1px solid; 
		width:100%;
		padding:5px;
	}
	
	.indexFormContainer:nth-child(odd) {
    margin-top: 20px;
	margin-bottom: 20px;
	
}

	a strong{
		color: black !important;
	}
	.product-description a{
		color: #dd4b39 !important;
	}
	#loader,#loader1,#coploader,#coploader1,#clientloader,#clientloader1,#perloader,#perloader1,#supplierloader,#supplierloader1{
		display:none;
	}
	.box.box-black{
		border-top-color: #000;
	}
	li{
		list-style: none;
	}
	.labelMargin{
		float:right;
	}
	
	.active{
		border-top-color: #DD4B39 !important;
	}
	
	.remark {
		color:#d0ccc6;
		font-weight: bolder;
	}
	
	.inner{
		color:#fdf9f5;
		text-align: center;
		text-decoration: none;

	}

	.small-box-footer{
		color: #D9534F !important;
		font-weight: bold !important;
		font-family: candara !important;
		cursor: default;
	}
	
	.btn-black{
		background:#ccc;
		color:#000;
		font-weight: bold;
	}

	.btn {
		color: #D9534F;
		transition: all 0.5s ease-in-out;
		
	}
	.btn:hover {
		background: #ccc !important;
		
	}
	
	.hideall{
		display: none;
	}
	
	#flash{
		display: none;
	}
	#activeuser{
		border-right:none !important;
	}
	
	.inner:hover h3:before{
		content: "View " !important;
		
	}
	
	.demo1{
		font-family: candara !important;
	}
	button:hover .standby{
		display: inline !important;
	}
	button:hover .main{
		display: none !important;
	}
	.button{
		transition: all 1s ease-in-out !important;
	}
	.button:hover .main{
		display: none !important;
		

	}
	.button:hover .hide{
		display: inline !important;

	}
	.button{
		font-family: candara !important;
		font-weight: bold;
		float: left;
		border: 1px solid #fff;
		width:30%;

	}
	.btn-black:hover .main{
		display: none !important;
	}
	.btn-black:hover .hide{
		display: inline !important;

	}
	.box-title{
		font-family: candara !important;
		font-weight: bold !important;

	}
	.box-black:hover .box-title h3{
		z-index: 100 !important;
		position: absolute !important;
		margin-left: 0px !important;


	}
	.box-black:hover .hi{
		display: inline !important;


	}
	#h3{
		float: left;
		min-width:40%;
		text-align: center;
	}

	.box-black:hover .h{
		display: inline !important;

	}
	.box-title1,
	.box-title2,
	.box-title3,
	.box-title4{
		font-size: 18px;
		z-index: 1000;
		position: absolute;
		display: block;
		animation-duration: 20s;
		animation-timing-function: ease-in-out;
		animation-iteration-count: infinite;
	}

	.box-title1{
		animation-name: anim-1;
	}
	.box-title2{
		animation-name: anim-2;
	}
	.box-title3{
		animation-name: anim-3;
	}
	
	@keyframes anim-1{
		0%, 8.3% { left:-100%; opacity: 0; }
		8.3%, 25% { left:45%; opacity: 1; }
		33.33%, 100% { left:110%; opacity: 0; }
	}
	@keyframes anim-2{
		0%, 33.33% { left:-100%; opacity: 0; }
		41.63%, 58.29% { left:42%; opacity: 1; }
		66.66%, 100% { left:110%; opacity: 0; }
	}
	@keyframes anim-3{
		0%, 66.66% { left:-100%; opacity: 0; }
		74.96%, 91.62% { left:42%; opacity: 1; }
		100% { left:110%; opacity: 0; }
	}
	.his{
		display: none;
	}
	

</style>

<!-- Main content -->
<?php $this->beginBlock('folderview'); ?>
TycolMain
<small> > Dashboard</small>   
<?php $this->endBlock(); ?>
<section class="content">
	
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box dashboardicons">
			<a id = "dashboardicons" href="<?= $goto_folder_page;?>" style = "text-decoration: none">
            <div class="inner">
              <h3 class="demo1">Folders</h3>
              <h3 class = "demo" style="display: none">View</h3>
            

              <p ><h1 style="text-align:center"><?= $total_products; ?></h1></p>
            </div>
            <div class="icon">
              
            </div>
			 </a>
            <span id="newfolder" data-values="<?= Url::to(['/folder/create']) ?>" class="small-box-footer">Create new  </span>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box dashboardicons">
			  <a href="<?= $goto_project_page;?>" style = "text-decoration: none">
            <div class="inner">
              <h3 class="demo1">Projects<sup style="font-size: 20px"></sup></h3>
             
              <p ><h1 style="text-align:center"><?= $total_projects; ?></h1></p>
            </div>
            <div class="icon">
             
            </div>
			</a>
            <span id="newproject" data-values="<?= Url::to(['/project/create']) ?>"  class="small-box-footer">Create new </span>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box dashboardicons">
			  <a href="<?= $goto_invoice_page;?>" style = "text-decoration: none">
            <div class="inner">
              <h3 class="demo1">Invoices</h3>
             

              <p ><h1 style="text-align:center"><?= $total_invoices; ?></h1></p>
            </div>
            <div class="icon">
              
            </div>
			 </a>
            <span id="newinvoice" data-values="<?= Url::to(['/invoice/create']) ?>"  class="small-box-footer">Create new</span>
          </div>
        </div>
        <!-- ./col -->
       <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box dashboardicons">
			  <a href="<?= $goto_order_page;?>" style = "text-decoration: none">
            <div class="inner">
              <h3 class="demo1">Orders</h3>
              

              <p ><h1 style="text-align:center"><?= $total_order; ?></h1></p>
            </div>
            <div class="icon">
             
            </div>
			 </a>
            <span id="neworder" data-values="<?= Url::to(['/order/create']) ?>"  class="small-box-footer">Create new</span>
          </div>
        </div>
        <!-- ./col -->

      </div>
	<div class="row">
		<div class="col-lg-12">
			<?= Alert::widget([
				   'options' => ['class' => 'alert-info','id'=>'flash'],
				   'body' => Yii::$app->session->getFlash('created_successfully'),
					 ]);?>
		</div>
	</div>
<?php Pjax::begin(['id' => 'content']) ?>
    <div class="row">
        <div class="col-lg-6 col-xs-6" >
			<div class="box box-solid">
				<div class="box box-black collapsed-box">
					<div class="box-header with-border" id = "box-header">
						<div  style="border: 1px solid #fff;width:30%;float: left">

							<button type="button" class="btn btn-box-tool btn-black hidde his" data-classs="newremark" id="newremark" style="display: none">View Remarks</button>

						</div>
						<div style="border: 1px solid #fff;width:40%;float: left;text-align: center">
							<h3 class="box-title">Recent Remarks</h3>
						</div>
						<div class="box-tools  pull-right">
							<button  class="btn btn-box-tool" data-widget="collapse" style="background: #fff !important;font-size: 16px" ><i class="fa fa-caret-down"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" style="background: #fff !important"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						
						<?php Pjax::begin(['id' => 'remark_reload']) ?>
							<ul class="products-list product-list-in-box">
								<? foreach($recent_remarks as $k=>$v){?> 
								<li class="item" >
							  
									<div class="product-info">
									<a href="javascript:void(0)" class="product-title"><strong><?= Folder::findOne(['id' => $v['folder_id']])->description ;?></strong> 
									<span style="float:right;" class="label remark"><span style="color:#555; font-family: candara; font-size: 16px"> <?= $v['remark_date'];?></span></span></a>
									<span class="product-description" style="font-family: candara"><?= Project::convert_tycref_tag($v['text']); ?></span>
								</div>
							  
								</li>
								<? }?>
							</ul>
						<?php Pjax::end() ?>
					</div>			  
				</div>
			</div>
			<div class="box box-solid"> 
				<div class="box box-black collapsed-box" id="contactsContainer">

				<div class="box-header with-border"  style="overflow: hidden;height: 40px;">
              
              		<div id = "sliderwiz" style="width: 400px;height: 100%;float: left;">
		             	<p class="box-title1" style="font-family: candara;font-weight: bold;">User</p>
		             	<p class="box-title2" style="font-family: candara;font-weight: bold;">Clients</p>
		             	<p class="box-title3" style="font-family: candara;font-weight: bold;">Suppliers</p>
	             	</div>
	             	<div id = "sliderwizz" style="width: 400px;height: 100%;float: left;display: none;">
		             	<p class="box-title" style="font-family: candara;font-weight: bold;margin-left: 230px">Users</p>
		            </div>
		            <div id = "sliderwizz1" style="width: 400px;height: 100%;float: left;display: none;">
		             	<p class="box-title" style="font-family: candara;font-weight: bold;margin-left: 210px">Clients</p>
		            </div>
		            <div id = "sliderwizz2" style="width: 400px;height: 100%;float: left;display: none;">
		             	<p class="box-title" style="font-family: candara;font-weight: bold;margin-left: 210px">Suppliers</p>
		            </div>
		            <div id = "sliderwizz3" style="width: 400px;height: 100%;float: left;display: none;">
		             	<p class="box-title" style="font-family: candara;font-weight: bold;margin-left: 210px">Contacts</p>
		            </div>

	             	<div class="box-tools  pull-right">
						<button id = "caret"  class="btn btn-box-tool" data-widget="collapse" style="background: #fff !important;font-size: 16px" ><i class="fa fa-caret-down" ></i></button>
						
						<button type="button" class="btn btn-box-tool" data-widget="remove" style="background: #fff !important"><i class="fa fa-times"></i></button>
						
	                
	                
	             </div>
				</div>  
				<div class="box-body">
			   
				<div class="nav-tabs-custom" id = "nav1">
					<ul class="nav nav-tabs pull-right">

						<li class="active" id = "activeuser"><a href="#user" data-toggle="tab">Users</a></li>
						<li><a class="client" href="#client" data-toggle="tab">Clients</a></li>
						<li><a class="supplier" href="#supplier" data-toggle="tab">Suppliers</a></li>
						<li><a class="contact" href="#person" data-toggle="tab">Contact</a></li>
						
					</ul>
					<div class="tab-content">
						
						<div class="tab-pane active" id="user" style="border:none;">
							
								
								<div class="folder-index">

									
									
									<div class="box-body" id="usertable">
										<?php Pjax::begin(['id' => 'user_reload']) ?>
										
										<?=$this->render('/user/dashboarduserlist',['displayAllUsers'=>$displayAllUsers,'personModel'=>$personModel]);?>
										
											<?php Pjax::end() ?>
										
									</div>
		  
								</div>
		
							
		  
						</div>
						
						<div class="tab-pane " id="client" style="border:none">
							
								<?php Pjax::begin(['id' => 'client_reload']) ?>
										
										<?=$this->render('/client/dashboardclientlist',['displayAllClient'=>$clientModel->find()->all(),'corporationModel'=>$corporationModel]);?>
										
											<?php Pjax::end() ?>
								
		
							
		  
						</div>
						<div class="tab-pane " id="supplier" style="border:none">
							
							
							<?php Pjax::begin(['id' => 'supplier_reload']) ?>
										
										<?=$this->render('/supplier/dashboardsupplierlist',['displayAllSupplier'=>$supplierModel->find()->all(),'corporationModel'=>$corporationModel]);?>
										
											<?php Pjax::end() ?>
						</div>
						<div class="tab-pane" id="person" style="border:none">
							
							<?php Pjax::begin(['id' => 'person_reload']) ?>
										
										<?=$this->render('/person/dashboardpersonlist',['displayAllPerson'=>$personModel->find()->all()]);?>
										
											<?php Pjax::end() ?>
								
								
		
							
		  
						</div>
						
						
					</div>
				</div>
			</div>
            <!-- /.tab-content -->
			</div>
			</div> 
        </div>
        <div class="col-md-6">
			<div class="box box-black collapsed-box">
			 	<div class="box-header with-border" style="text-align: center;">              
					<h3 class="box-title" style="font-family: candara;font-weight: bold;">Tasks and Reminders</h3>
					<div class="box-tools  pull-right">
						<button  class="btn btn-box-tool" data-widget="collapse" style="background: #fff !important;font-size: 16px" ><i class="fa fa-caret-down"></i></button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" style="background: #fff !important"><i class="fa fa-times"></i></button>
					</div>
				</div>   
			 	<div class="box-body">
					<div class="nav-tabs-custom" id = "nav1">
						<ul class="nav nav-tabs pull-right" id = "nav">
							<li class="active"><a href="#task" data-toggle="tab" onclick="css()" id = "demo">Tasks</a></li>
							<li><a href="#reminder" data-toggle="tab">Reminders</a></li>              
						</ul>
						<div class="tab-content" style="border:1px solid #ccc">
							<!-- /.tab-pane -->
							<div class="tab-pane active" id="task" style="border-right: none !important; border-left: none !important">
								<div class="nav-tabs-custom" id = "nav1" >
									<ul class="nav nav-tabs " id = "nav">
										<li class="active"><a href="#createtask" data-toggle="tab" >Create A Task</a></li>
										<li><a href="#viewtask" data-toggle="tab">Tasks Assigned to Me</a></li>
										<li><a href="#viewtaskcreated" data-toggle="tab">Tasks I've Assigned to Others</a></li>
									</ul>
									<?php Pjax::begin(['id' => 'task_reload']) ?>
									<div class="tab-content" style="border:1px solid #ccc !important; padding: 10px !important">
										<!-- /.tab-pane -->
										<div class="tab-pane active" id="createtask" style="border-right: none !important;border-left: none !important">
											   <?= $this->render('/task/_dashboardcreatetask', [
											'model' => $reminder,
											'task' => $activeTask,
											'folderTask' => $folderTask]);
										?>
										</div>
										<div class="tab-pane " id="viewtask" style="border:none !important">
											  <?= $this->render('/task/dashboardtask', [
												'viewTask' => $viewTask,
												]);
												?>
										</div>
										<div class="tab-pane " id="viewtaskcreated" style="border:none !important">
										<?= $this->render('/task/dashboardtaskowner', [
											'viewTask' => $viewTask,
											]);
											?>
										</div>
									</div>
									<?php Pjax::end() ?>
									<!-- /.tab-content -->
								</div>
							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="reminder" style="border-right: none !important;border-left: none !important">
								<div class="nav-tabs-custom" id = "nav">
									<ul class="nav nav-tabs " >
										<li class="active"><a href="#newreminder" data-toggle="tab" >New Reminder</a></li>
										<li><a href="#myreminders" data-toggle="tab">My Reminders</a></li>
									</ul>
									<?php Pjax::begin(['id' => 'reminder_reload']) ?>   
									<div class="tab-content" style="border: 1px solid #ccc; border-top:none;">
									  <!-- /.tab-pane -->
										<div class="tab-pane active" id="newreminder" style="border-right: none !important;border-left: none !important; padding: 10px !important">
										
											   <?= $this->render('/reminder/dashboardreminder', [
											'model' => $reminder,
											'task' => $activeTask,]);
												?>
										</div>
										<div class="tab-pane " id="myreminders" style="border:none !important;">
											<div class="box-group" id="accordion">
											<?php $i=1; foreach($userReminders as $key => $value){ ?>
												<div class="panel box box-danger" style="border: none !important;">
													<div class="box-header" style="border-radius:10px;font-family: candara">

														<button  class="fa fa-caret-down test3" data-number="<?=$i;?>" style="float: right; color:#001a23 !important;font-size: 14px !important"></button>
															<span style="float:left;">
																<?= $value['reminder_time']; ?>  

															</span>
															<span id="reminder_time<?= $i?>" style="float:right;"></span>
														

													</div>
													<div id="task<?= $i; ?>" style = "display: none; font-family: candara">
														<div class="box-body">
															<?= $value['notes']; ?>
														</div>
													</div>
												</div>
											<? $i++;} ?>
													 
											</div>			 
										</div>
									</div>
									<?php Pjax::end() ?>
								<!-- /.tab-content -->
								</div>
							</div>
						</div>
					<!-- /.tab-content -->
					</div>
                </div>
			</div>
			<div class="box box-solid ">
				<div class="box box-black collapsed-box">
					<div class="box-header with-border">
						<div class="pull-left button">
							<?=Html::a('View Payments',['payment/index'],['class'=>'btn btn-box-tool btn-black hidde his']);?>
						</div>
						<div id = "h3">
							<h3 class="box-title">Payment</h3>
						</div>
						<div class="box-tools  pull-right">
							<button  class="btn btn-box-tool" data-widget="collapse" style="background: #fff !important;font-size: 16px" ><i class="fa fa-caret-down"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" style="background: #fff !important"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div id = "commonn" >
							<?= $this->render('/payment/dashboardpayment',['model'=>$paymentModel,
																'folder' => $folderModel->find()->all(),
															   'corporation' => $corporationModel->find()->all(),
															   'paymentSource' => $paymentSourceModel->find()->all(),
															   'currencies' => $currenciesModel->find()->all(),
																'valueSettings'=> $value_settings]) ?>
						</div> 
					</div>
				</div>
			</div>
			<div class="box box-solid">
			<div class="box box-black collapsed-box">
				<div class="box-header with-border">
					<div class="pull-left button">
              
					<?=Html::a('View RPOs',['receivedpurchaseorder/index'],['class'=>'btn btn-box-tool btn-black hidde his']);?>
          		</div>
					<div id = "h3">
					<h3 class="box-title">Received Purchase Order</h3>
				</div>
					<div class="box-tools  pull-right">
					<button  class="btn btn-box-tool" data-widget="collapse" style="background: #fff !important;font-size: 16px" ><i class="fa fa-caret-down"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove" style="background: #fff !important"><i class="fa fa-times"></i></button>
                
                
				</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
                    <div id = "commonn" style="margin-top: 20px !important">  
						<?= $this->render('/receivedpurchaseorder/dashboardrpo',['model'=>$rpoModel,
																'folder' => $folderModel->find()->all(),
															   'corporation' => $corporationModel->find()->all(),
															   'value_settings' => $value_settings,
															   'client' => $clientModel->find()->all(),
															   'suppliers' => $supplierModel->find()->all(),
																   'currencies' => $currenciesModel->find()->all(),]) ?>
                        
          			</div>
            </div>
			  
			</div>
		</div>
      		<div class="box box-solid ">
				<div class="box box-black collapsed-box">
				<div class="box-header with-border">
				<div class="pull-left button">
					<?=Html::a('View Correspondences',['correspondence/index'],['class'=>'btn btn-box-tool btn-black hidde his']);?>
				</div>
				<div id = "h3">
					<h3 class="box-title">Correspondence</h3>
				</div>
				<div class="box-tools  pull-right">
					<button  class="btn btn-box-tool" data-widget="collapse" style="background: #fff !important;font-size: 16px" ><i class="fa fa-caret-down"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove" style="background: #fff !important"><i class="fa fa-times"></i></button>
				</div>
            </div>
				<!-- /.box-header -->
				<div class="box-body">
				<div id = "commonn" style="margin-top: 40px !important">
					<?= $this->render('/correspondence/dashboardcorrespondenceform',
													['model'=>$correspondenceModel,
													'folder' => $folderModel->find()->all(),
												   'corporation' => $corporationModel->find()->all(),
												   'value_settings' => $value_settings,
												   'client' => $clientModel->find()->all(),
												   'suppliers' => $supplierModel->find()->all(),
												   'currencies' => $currenciesModel->find()->all(),]) ?>
				</div>	
            </div>
			  
			</div>

				<!-- /.box-body -->
			</div>
        </div>



    </div>
<?php Pjax::end() ?>
</section>
<? 
	Modal::begin([
		'header' =>'<h1 id="headers"></h1>',
		'id' => 'dashboard',
		'size' => 'modal-md',  
		]);
		?>
<div id='formcontent' class='hideall'></div>
<div id='remarkcontent' class='hideall'>
	<div class="folder-create">
		<?= $this->render('../remark/createdashboardremark',
						  ['remarkModel' => $remarkModel,
						   'action' => $action,
						   ]) ?>
	</div>
	
</div>
<div id='clientcontent' class='hideall'>
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs pull-right">
			<li class="active"><a href="#useexisting" data-toggle="tab">Use Existing</a></li>
			<li><a href="#usenew" data-toggle="tab">Create new client</a></li>
		</ul>
		<div class="tab-content">
			<!-- /.tab-pane -->
			<div class="tab-pane active" id="useexisting">
				<div class="box">
					<h1 style="text-align:center;">Use Existing </h1>
					<div class="folder-index">
						<h1></h1>
						<div class="box-body">
							<?= $this->render('client_form',
												  ['newClient' =>$newClient,
												   'clientModel'=>$clientModel,
												   ]) 
								?>
						</div>
					</div>
				</div>
			</div>
			<!-- /.tab-pane -->
			<div class="tab-pane" id="usenew">
				<div class="box">
					<h1 style="text-align:center;"></h1>
					<div class="folder-index">
						<h1>Create new client</h1>
						<div class="box-body">
							 <?=  $this->render('../client/dashboardcreateclient',
								  ['corporation' => $corporationModel,
								   'action' => $action,
								   'personModel' => $personModel,
								   'emailModel' => $emailModel,
								   'telephoneModel' => $telephoneModel,
								   'addressModel' => $addressModel,
								   'hiddenValue' => 'client',
								   'actionid'=>'client',]) ;
							   ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.tab-content -->
	</div>

</div>
<div id='usercontent' class='hideall'>
	<?/*= $this->render('../user/_userform',
					  ['userForm' => $userForm,
					   'action' => $action,]) */?>
</div>
<div id='suppliercontent' class='hideall'>
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs pull-right">
			<li class="active"><a href="#useexistingsup" data-toggle="tab">Use Existing</a></li>
			<li><a href="#usenewsup" data-toggle="tab">Create new supplier</a></li>
		  
		  
		</ul>
		<div class="tab-content">
			<!-- /.tab-pane -->
			<div class="tab-pane active" id="useexistingsup">
				<div class="box">
					<h1 style="text-align:center;">Use Existing </h1>
					<div class="folder-index">
						<h1></h1>
						<div class="box-body">
						   <?= $this->render('supplier_form',
												  ['newSupplier' =>$newSupplier,
												   'supplierModel'=>$supplierModel,
												   ]) 
								?>
						</div>
					</div>
				</div>
			</div>
			<!-- /.tab-pane -->
			<div class="tab-pane" id="usenewsup">
				<div class="box">
					<h1 style="text-align:center;"></h1>
					<div class="folder-index">
						<div class="box-body">
							<h1>Create new supplier</h1>
							<?= $this->render('../supplier/dashboardcreatesupplier',[
							  'corporation1' => $corporationModel,
							  'action' => $action,
							  'personModel' => $personModel,
							  'emailModel' => $emailModel,
							  'telephoneModel' => $telephoneModel,
							  'addressModel' => $addressModel,
							  'hiddenValue' => 'supplier',
							  'actionid'=>'supplier',]) 
								?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.tab-content -->
	</div>
</div>
<div id='personcontent' class='hideall'>
			<?= $this->render('../person/_form',
							  ['action' => 'person/update',
							   'personModel' => $personModel,
							   'emailModel' => $emailModel,
							   'telephoneModel' => $telephoneModel,
							   'addressModel' => $addressModel,]) ?>
		</div>
<?php
		Modal::end();
?>




      

<?php 
$indexJs = <<<JS

$('#refresh').click(function(){ $.pjax.reload({container:"#content",async: false
}); })

	$('.test3').each(function(){
	$(this).click(function(){
		$('#task'+$(this).data('number')).slideToggle();

		if($(this).hasClass('fa-caret-down')){
				$(this).removeClass('fa-caret-down').addClass('fa-caret-up');
			} else {
				$(this).removeClass('fa-caret-up').addClass('fa-caret-down');
			}
		})
	})
    $('.test1').each(function(){
	$(this).click(function(){
		$('#task2'+$(this).data('number')).slideToggle();

		if($(this).hasClass('fa-caret-down')){
				$(this).removeClass('fa-caret-down').addClass('fa-caret-up');
			} else {
				$(this).removeClass('fa-caret-up').addClass('fa-caret-down');
			}
		})
	})
    
    $('.test').each(function(){
	$(this).click(function(){
		$('#task'+$(this).data('number')).slideToggle();

		if($(this).hasClass('fa-caret-down')){
				$(this).removeClass('fa-caret-down').addClass('fa-caret-up');
			} else {
				$(this).removeClass('fa-caret-up').addClass('fa-caret-down');
			}
		})
	})

	

	$('.client').on('click', function(){
					$(document).find('#sliderwizz1').show();
					$(document).find('#sliderwizz').hide();
					$(document).find('#sliderwizz2').hide();
					$(document).find('#sliderwizz3').hide();
	})
	$('.supplier').on('click', function(){
					$(document).find('#sliderwizz2').show();
					$(document).find('#sliderwizz1').hide();
					$(document).find('#sliderwizz3').hide();
					$(document).find('#sliderwizz').hide();
	})
	$('.contact').on('click', function(){
					$(document).find('#sliderwizz3').show();
					$(document).find('#sliderwizz2').hide();
					$(document).find('#sliderwizz1').hide();
					$(document).find('#sliderwizz').hide();
	})
	$('#activeuser').on('click', function(){
					$(document).find('#sliderwizz').show();
					$(document).find('#sliderwizz3').hide();
					$(document).find('#sliderwizz2').hide();
					$(document).find('#sliderwizz1').hide();
	})


				

JS;
 
$this->registerJs($indexJs);
?>