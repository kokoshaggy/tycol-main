<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TmFolder */

$this->title = 'Update Folder: ' . $model->tyc_ref;
$this->params['breadcrumbs'][] = ['label' => 'Folders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tyc_ref, 'url' => ['view', 'id' => $model->tyc_ref]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="folder-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
