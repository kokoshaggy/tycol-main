<style>
    .floatright {
        float: right;
    }
	
	/*.newfolder {
		margin: 10px 10px;
	}*/
	
   .box-body {
		font-family: candara !important;
	}
	
	#newfolder {
		margin:10px 10px;
	}
   
	.box h3 {
		margin-left:10px !important;
	}
   
	.fa-folder-open {
		margin-left: 5px;
	}
   
</style>
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Alert;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\boffins_vendor\components\controllers\Menu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Tmfoldersearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tycol | Folders';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('createFolder'); ?>

   
<?php $this->endBlock(); ?>

<?php $this->beginBlock('folderview'); ?>
Folder
<small>Select Folder<?//= $model->tycref; ?></small>

   
<?php $this->endBlock(); ?>

<?php $this->beginBlock('folderSidebar'); ?>
	<?= Menu::widget(); ?>

<?php $this->endBlock(); ?>

<section class="content">
    <? if (Yii::$app->session->getFlash('successfully_updated') !==NULL): ?>
     <?= Alert::widget([
   'options' => ['class' => 'alert-info'],
   'body' => Yii::$app->session->getFlash('successfully_updated'),
     ]);?>
    <? endif ?>
     <? $getsingletyc =  $folder->find()->one(); ?>
   <? $folderReff=''; if( $folder->find()->count() <= 0){$folderReff=0;}else{$folderReff=!empty($getsingletyc->id)?$getsingletyc->id:0; } ?>

    <div class="row">
        <div class="col-xs-7">
			<div class="box">
				<h3><?= Html::encode($this->title) ?><div class="floatright btn btn-danger newfolder" id="newfolder" data-values="<?= Url::to(['/folder/create']) ?>">Create New Folder</div></h3>
                <br>
				<div class="tm-folder-index">
					<div class="box-body">
					   
						<table id="example1" class="table table-bordered table-striped ">
							<thead>
								<tr>
									<th>Folder</th> <!-- WHY IS THIS NOT AN OUTPUT OF LABELS??? -->
									<th>Description</th>
									<th>Type</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<? if (empty($displayFolder)) { ?>
								<? } else { ?>
									<?php $sn=1; foreach($displayFolder as $k=>$v) { ?>
										<tr class="folderTable folderurltr <?if($sn === 1){echo 'activelist';} ?>" data-url="<?=Url::to(['folder/folderview','id' => $v['id']]) ?>" title="Click to view folder">
											<td class="folderurl"><?= $v['tyc_ref'];  ?></td>
											<td class="folderurl"><?= $v['description'];  ?></td>
											<td class="folderurl"><?= $v['type'];  ?></td>
											<td><?=Html::a(Html::tag( 'span', Html::tag('i', '', ['class' => 'fa fa-folder-open fa-fw','title' => 'Open folder']) ),['view','id' => $v['id']] );
											
												$deletUrl=Url::to(['folder/delete','id'=>$v['id']]);
												$updateUrl=Url::to(['folder/update','id'=> $v['id']]);
												?>
										   
												<?= Html::tag('i', '', ['class' => 'fa fa-pencil update', 'data-url'=>$updateUrl]); ?>
												<?= Html::tag('i', '', ['class' => 'fa fa-trash delete', 'data-url'=>$deletUrl]); ?>
											</td>
										</tr>
									<?php $sn++;}}?>
							</tbody>
							<tfoot>
								<tr>
									<th>Folder</th>
									<th>Description</th>
									<th>Type</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
            </div>
        </div>
		<div class="col-xs-5" id="folderView"></div>
    </div>
</section>



<? 
	Modal::begin([
		'header' =>'<h1 id="headers"></h1>',
		'id' => 'dashboard',
		'size' => 'modal-md',  
		]);
		echo "<div id='formcontent'> </div>";
		Modal::end();
?>

<?php 
	$urlView = Url::to(['folder/folderview','id'=>$folderReff]);
	
$js3 = <<<JS

$("#folderView").load('$urlView',{var2:1});

$(document).on('click','.folderurl',function(){
var parent = $(this).parent();

$('.folderurltr').removeClass('activelist');
parent.addClass('activelist');

$('#folderView').html('<img class="loadergif" src="images/loader.gif"  />');
var url = parent.data('url');
$("#folderView").load(url);
})

$(document).on('click','#createinvoice',function(){
var formUrl = $(this).data('formurl');
$('#invoiceviewcreate').modal('show').find('#invoicecreateform').load(formUrl);

})




JS;
 
$this->registerJs($js3);
?><style>
    .floatright{
        float: right;
    }
   /* .newfolder{
    margin: 10px 10px;
   }*/
   .box-body{
    font-family: candara !important;
   }
   #newfolder{
    margin:10px 10px;
   }
   .box h3{
     margin-left:10px !important;
   }
   .fa-folder-open{
    margin-left: 5px;
   }
   
</style>
