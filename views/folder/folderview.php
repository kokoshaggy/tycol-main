<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Invoice */
$this->title = $folder->tyc_ref;
$this->params['breadcrumbs'][] = ['label' => 'Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #folder{
        background: #fff;
        min-height: 600px;
    }
    .box{
        border: none;
        margin-top: 22px;
    }
</style>
<div class="folder">
    <div class="box" id="folder">
<div class="box-body">



   

    <?= DetailView::widget([
        'model' => $folder,
		
        'attributes' => [
            'tyc_ref',
            'type' ,
            'description',
            'notes',
            
        ],
    ]) ?>

</div>
</div>
</div>
