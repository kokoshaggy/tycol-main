
<style>
    .inner:hover {
      background: #555;
    }
    .inner{
      transition: all 0.5s ease-in-out;
      cursor: pointer;
    }
   
    .active{
    border-top-color:#DD4B39 !important;
  }

    .inner:hover .name:before{
      content: "View " !important;
    
      }
    .floatright{
        float: right;
    }
    .foldernote{
        line-height: 24px;
        text-underline-position: alphabetic;
        margin-top: 10px;
        
    }
  #headers{
    text-align: center;
    font-family: candara;
  }
  
  .dashboardicons{
        background:#ccc;
        color: #000;
    }
  .small-box-footer,.col-lg-3,.col-xs-6{
    cursor: pointer;
  }
  .small-box-footer{
    color: #D9534F !important;
    background: #AEAEAE !important;
    font-weight: bold !important;

  }
  .fa-arrow-circle-right{
    color: #fff !important;
  }
  .row1{
    padding:0px 20px;

  }
  .row{
    padding:0px 20px;

  }
  .fa-image{
    font-size:90px;
  }
  .icon{
    opacity: 0.1;
  }
  .white{
    background: #fff;
  }

	
	.newstape {

	  background-color: #fff;

	  color: #000;

	  height: 400px;

	  overflow: hidden;




	}

	 
	.newstape-content {

	  position: relative;

	  padding: 15px;

	}
  #name{
    font-weight: bold; 
  }
  .pro{
   
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis
  }

  #namee{
    font-weight: bold; 
  }
  .pros{
   
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis
  }


	.newstape-drag { cursor: ns-resize; }

	 

	.text-center { text-align: center; }

	 

	.text-right { text-align: right; }

	.text-justify { text-align: justify; }
  .box-black .with-border{
    
  }

</style>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use app\models\Corporation;
use yii\bootstrap\Alert;
use yii\bootstrap\Modal;
use app\boffins_vendor\components\controllers\Menu;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\TmFolder */


$this->params['breadcrumbs'][] = ['label' => 'Folders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if(($key = array_search('js-filepath/file.js', $this->js)) !== false) {
                    unset($this->js[$key]);
   }
?>
<?php $this->beginBlock('test'); ?>
Folder > Project
<small> > test</small>

   
<?php $this->endBlock(); ?>

<?php $this->beginBlock('folderview'); ?>
Folder
<small>Tycol Reference: <?= ' ' . $tyc_ref->tyc_ref;?></small>

   
<?php $this->endBlock(); ?>

<?php $this->beginBlock('folderSidebar'); ?>
  <?= Menu::widget(); ?>

<?php $this->endBlock(); ?>
<div class="folder-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--<p>
        <?= Html::a('Update', ['update', 'id' => $tyc_ref->tyc_ref], ['class' => 'btn btn-danger']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $tyc_ref->tyc_ref], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p> -->

   

</div>



<section class="content">
  
  
  
    <!-- flash messages start -->
    <? if (Yii::$app->session->getFlash('created_successfully') !==NULL): ?>
     <?= Alert::widget([
   'options' => ['class' => 'alert-info'],
   'body' => Yii::$app->session->getFlash('created_successfully'),
     ]);?>
    <? endif ?>
    
    <? if (Yii::$app->session->getFlash('created_folder') !==NULL): ?>
     <?= Alert::widget([
   'options' => ['class' => 'alert-info'],
   'body' => Yii::$app->session->getFlash('created_folder'),
     ]);?>
    <? endif ?>
    
    <? if (Yii::$app->session->getFlash('instruction') !==NULL): ?>
     <?= Alert::widget([
   'options' => ['class' => 'alert-info'],
   'body' => Yii::$app->session->getFlash('instruction'),
     ]);?>
    <? endif ?>
    <!-- flash messages end -->
  <div class="row">
        
        <!-- header for folder  -->
    
    <div class="col-md-6 col-sm-6 col-xs-6">
         
        </div>
          
          
          <div class="col-md-6 col-sm-6 col-xs-6" style="font-family: candara;">
          <div class="info-box" >

            <span class = "icon" style="color:#ccc"><i class="fa fa-image" aria-hidden="true" style="float: left;margin: 5px 10px 5px 5px;cursor: pointer" id="modelbutton"></i></span>

            <div class="info-box-content">
                <span class="info-box-text"><strong>TYC REF:</strong> <?= $tyc_ref->tyc_ref; ?></span>
              <span class=" info-box-text"><strong>Description:</strong><?= $tyc_ref->description; ?></span>

                <span class="info-box-text"><strong>Type:</strong> <?= $tyc_ref->type; ?></span>
        <? if(!empty($tyc_ref->notes)){ ?>

                <span style="" class="info-box-text"><strong>Note:</strong> <?= $tyc_ref->notes; ?></span>
                <? } ?>
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
   
        
        <div class="row"> 
        <div class="col-lg-2 col-md-6"  id="projectclick" data-values="<?= Url::to(['/project/projectlistview']) ?>" data-tyc_ref="<?= implode(',',$projectIdValues);?>">
          <!-- small box -->
          <div class="small-box dashboardicons" >
            <div class="inner" style = "text-align: center">
              <h4 class="name" style="font-weight: bold">Projects</h4>
              <h3><?= $totalProject; ?><sup style="font-size: 20px"></sup></h3>

              <p ><h1 style="text-align:center"></h1></p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-briefcase"></i>
            </div>
            <span  class="small-box-footer"> Create &nbsp; &nbsp;<i class="fa fa-arrow-circle-right"></i></span>
          </div>
  
        </div>
        <!-- ./col -->

		<div class="col-lg-2 col-md-6" id="rpoclick" data-values="<?= Url::to(['/receivedpurchaseorder/rpolistview']) ?>" data-tyc_ref="<?= serialize($rpoIdValues);?>">

          <!-- small box -->
          <div class="small-box dashboardicons">
            <div class="inner innerr" style="text-align: center">
              <h4 class="name pro" id = "name">Recieved Purchase Orders</h4>
              <h3><?= $totalRpo; ?></h3>

              <p ><h1 style="text-align:center"></h1></p>
            </div>
            <div class="icon">
              <i class=""></i>
            </div>
            <span class="small-box-footer">Create &nbsp; &nbsp;<i class="fa fa-arrow-circle-right"></i></span>
          </div>
        </div>


		<div class="col-lg-2 col-xs-6" id="invoiceclick" data-values="<?= Url::to(['/invoice/invoicelistview']) ?>" data-tyc_ref="<?= serialize($invoiceIdValues);?>">

          <!-- small box -->
          <div class="small-box dashboardicons">
            <div class="inner" style="text-align: center">
              <h4 class="name" style="font-weight: bold">Invoices</h4>
              <h3><?= $totalInvoice; ?></h3>

              <p ><h1 style="text-align:center"></h1></p>
            </div>
            <div class="icon">
              <i class="ion ion-folder"></i>
            </div>
            <span class="small-box-footer">Create  &nbsp; &nbsp;<i class="fa fa-arrow-circle-right"></i></span>
          </div>
        </div>

		 <!-- ./col -->
        <div class="col-lg-2 col-xs-6" id="orderclick" data-values="<?= Url::to(['/order/orderlistview']) ?>" data-tyc_ref="<?= serialize($orderIdValues);?>">

          <!-- small box -->
          <div class="small-box dashboardicons">
           
            <div class="inner" style="text-align: center">
               <h4 class="name" style="font-weight: bold">Orders</h4>
              <h3><?= $totalOrder; ?></h3>

              <p ><h1 style="text-align:center"></h1></p>
            </div>
            <div class="icon">
              <i class=""></i>
            </div>
            <span class="small-box-footer">Create &nbsp; &nbsp;<i class="fa fa-arrow-circle-right"></i></span>
          </div>
        </div>

         <div class="col-lg-2 col-xs-6" id="paymentclick" data-values="<?= Url::to(['/payment/paymentlistview']) ?>" data-tyc_ref="<?= serialize($paymentIdValues);?>" >


          <!-- small box -->
          <div class="small-box dashboardicons">
            <div class="inner" style="text-align: center">

     <h4 class="name" style="font-weight: bold">Payments</h4>
              <h3><?= $totalPayment; ?><sup style="font-size: 20px"></sup></h3>

              <p ><h1 style="text-align:center"></h1></p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-briefcase"></i>
            </div>



            <span class="small-box-footer">Create &nbsp; &nbsp;<i class="fa fa-arrow-circle-right"></i></span>



          </div>
        </div>
        <!-- ./col -->
      <div class="col-lg-2 col-xs-6" id="correspondenceclick" data-values="<?= Url::to(['/correspondence/correspondencelistview']) ?>" data-tyc_ref="<?= serialize($correspondenceIdValues);?>">
          <!-- small box -->
          <div class="small-box dashboardicons">
            <div class="inner innerrr" style="text-align: center">
              <h4 class="name pros" id = "namee">Correspondences</h4>
              <h3><?= $totalCorrespondence; ?></h3>

              <p ><h1 style="text-align:center"></h1></p>
            </div>
            <div class="icon">
              <i class=""></i>
            </div>
            <span class="small-box-footer">Create &nbsp; &nbsp;<i class="fa fa-arrow-circle-right"></i></span>
          </div>
        </div>
        <!-- ./col -->
      </div>
	<div class="row">
        <div class="col-lg-6 col-xs-6" >
			<div class="box box-black">
			 	<div class="box-header with-border" style="text-align: center;">              
					<h3 class="box-title" style="font-family: candara;font-weight: bold;">Task and Reminders2</h3>
					<div class="box-tools  pull-right">
						<button  class="btn btn-box-tool" data-widget="collapse" style="background: #fff !important;font-size: 16px" ><i class="fa fa-caret-down"></i></button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" style="background: #fff !important"><i class="fa fa-times"></i></button>
					</div>
				</div>   
			 	<div class="box-body">
					<div class="nav-tabs-custom" id = "nav1">
						<ul class="nav nav-tabs pull-right" id = "nav">
							<li class="active"><a href="#task" data-toggle="tab"  id = "demo">Task</a></li>
							<li><a href="#reminder" data-toggle="tab">Reminder 3</a></li>              
						</ul>
						<div class="tab-content" style="border:1px solid #ccc">
							<!-- /.tab-pane -->
							<div class="tab-pane active" id="task" style="border-right: none !important; border-left: none !important">
								
							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="reminder" style="border-right: none !important;border-left: none !important">
								
							</div>
						</div>
					<!-- /.tab-content -->
					</div>
                </div>
			</div>
        </div>
        <!-- ./col -->
        
        <div class="col-lg-6 col-xs-6" id="remarks" data-values="<?= Url::to(['/remark/view','id' => $id]) ?>">

          <!-- small box -->
          
        </div>
        <!-- ./col -->
       
        <!-- ./col -->
      </div>

  

  





<? Modal::begin([
      'header' =>'<h1 id="taskreminderheaders"></h1>',
      'id' => 'taskremindermodal',
      'size' => 'modal-md',  
    ]);
    
      ?>
    <div class="row" id='taskreminder'>
  
  <div class="col-lg-6 col-xs-6" id="taskclick" data-values="<?= Url::to(['/folder/foldertask','id' => $id]) ?>">
          <!-- small box -->
          <div class="small-box dashboardicons">
            <div class="inner" style="text-align: center">
              <h3>3</h3>

              <p ><h1 style="text-align:center"></h1></p>
            </div>
            <div class="icon">
              <i class="ion ion-folder"></i>
            </div>
            <span class="small-box-footer">Task 21 &nbsp; &nbsp;<i class="fa fa-arrow-circle-right"></i></span>
          </div>
        </div>

  <div class="col-lg-6 col-xs-6" id="reminderclick" data-values="<?= Url::to(['/reminder/folder-reminder','id' => $id]) ?>">
          <!-- small box -->
          <div class="small-box dashboardicons">
            <div class="inner" style="text-align: center">
              <h3>3</h3>

              <p ><h1 style="text-align:center"><?//= $total_products; ?></h1></p>
            </div>
            <div class="icon">
              <i class="ion ion-folder"></i>
            </div>
            <span class="small-box-footer">Reminder 22 &nbsp; &nbsp;<i class="fa fa-arrow-circle-right"></i></span>
          </div>
        </div>
</div>

<?
    Modal::end();
  ?>

<?
 Modal::begin([
            'header' => '<h4>Upload your folder image</h4>',
            'id'     => 'modall',
            'size'   => 'modal-md',
    ]);
    
   ?>
   <form method="POST" action="" enctype="multipart/form-data">
   
       <div style="min-height: 230px; width: 220px; border: 1px solid #ccc" class="picture">
            <?= Html::img('@web/images/default-property.jpg', ['alt' => 'logo', 'class' => 'folder-image' ]); ?>
            <input type="file" name="folderimage" id = "wizard-picture">
       </div>
    
       <br />
       <button class="btn btn-primary">Upload</button>
   </form>
  <?  
    Modal::end();
?>

<? Modal::begin([

			'header' =>'<h1 id="pay_and_sourceheaders"></h1>',
			'id' => 'pay_and_sourcemodal',
			'size' => 'modal-md',  
		]);
		
			?>
		<div class="row" id='taskreminder'>
	
	<div class="col-lg-6 col-xs-6" id="paymentclick" data-values="<?= Url::to(['/payment/paymentlistview']) ?>" data-tyc_ref="<?= serialize($paymentIdValues);?>">
          <!-- small box -->
          <div class="small-box dashboardicons">
            <div class="inner" style="text-align: center">
              <h3><?= $totalPayment; ?></h3>

              <p ><h1 style="text-align:center"></h1></p>
            </div>
            <div class="icon">
              <i class="ion ion-folder"></i>
            </div>
            <span class="small-box-footer">Payment  &nbsp; &nbsp;<i class="fa fa-arrow-circle-right"></i></span>
          </div>
        </div>

  <div class="col-lg-6 col-xs-6" id="paymentsourcerclick" data-values="<?= Url::to(['/paymentsourcer/index']) ?>">
          <!-- small box -->
          <div class="small-box dashboardicons">
            <div class="inner" style="text-align: center">
              <h3><?//= $total_products; ?></h3>

              <p ><h1 style="text-align:center"><?= $totalPaymentSource; ?></h1></p>
            </div>
            <div class="icon">
              <i class="ion ion-folder"></i>
            </div>
            <span class="small-box-footer">Payment source  &nbsp; &nbsp;<i class="fa fa-arrow-circle-right"></i></span>
          </div>
        </div>
</div>

<?
    Modal::end();
  ?>




<?
Modal::begin([
      'header' =>'<h1 id="headers2"></h1>',
      'id' => 'projectmodal2',
      'size' => 'modal-lg',  
    ]);
    echo "<div id='modalcontent2'> </div>";
    Modal::end();
  ?>






<? 
    Modal::begin([
      'header' =>'<h1 id="viewcreateheader"></h1>',
      'id' => 'viewcreatemodal',
      'size' => 'modal-md',  
    ]);
    echo "<div id='viewcreatecontent'> </div>";
?>


<?
	Modal::end();
?>


<? 
		Modal::begin([
			'header' =>'<h1 id="headers"></h1>',
			'id' => 'dashboard',
			'size' => 'modal-md',  
		]);
?>


<div id="formcontent"></div>
<?
	Modal::end();
?>

</section>

<?
$taskurl = Url::to(['folder/foldertask','id'=>$id]);
$reminderUrl = Url::to(['/reminder/folder-reminder', 'folderID' => $id]);
$remarksUrl =  Url::to(['/remark/view','id' => $id]);
$paymentJs = <<<JS
$(document).ready(function(){
 

$('.newstape').newstape({

	period: 30,

	offset: 1,

	mousewheel: true,

	mousewheelRate: 30,

	dragable: true,
	});


$(document).on('click','.paymenturl',function(){
    
	var parent = $(this).parent();
	$('.paymenturltr').removeClass('activelist');
	parent.addClass('activelist');
	var url = parent.data('url');

	$('#viewcreateheader').html('Payment');
	$('#viewcreatemodal').modal('show').find('#viewcreatecontent').html(' ');
	$('#viewcreatemodal').modal('show').find('#viewcreatecontent').load(url);

})



$(document).on('click','#createproject',function(){

var url = $(this).data('formurl');
  $('#viewcreatemodal').modal('show').find('#viewcreatecontent').html(' ');
  $('#viewcreatemodal').modal('show').find('#viewcreatecontent').load(url+'&id=$id&hiddenDropdown=$id');

})


$(document).on('click','#createinvoice',function(){

var url = $(this).data('formurl');
  $('#viewcreatemodal').modal('show').find('#viewcreatecontent').html(' ');
  $('#viewcreatemodal').modal('show').find('#viewcreatecontent').load(url+'&id=$id&hiddenDropdown=$id');

})


$(document).on('click','#createrpo',function(){

var url = $(this).data('formurl');
  $('#viewcreatemodal').modal('show').find('#viewcreatecontent').html(' ');
  $('#viewcreatemodal').modal('show').find('#viewcreatecontent').load(url+'&id=$id&hiddenDropdown=$id');

})


$(document).on('click','#createpayment',function(){

var url = $(this).data('formurl');
  $('#viewcreatemodal').modal('show').find('#viewcreatecontent').html(' ');
  $('#viewcreatemodal').modal('show').find('#viewcreatecontent').load(url+'&id=$id&hiddenDropdown=$id');

})


$(document).on('click','#createcorrespondence',function(){

var url = $(this).data('formurl');
  $('#viewcreatemodal').modal('show').find('#viewcreatecontent').html(' ');
  $('#viewcreatemodal').modal('show').find('#viewcreatecontent').load(url+'&id=$id&hiddenDropdown=$id');

})

$(document).on('click','#createorder',function(){

var url = $(this).data('formurl');
	$('#viewcreatemodal').modal('show').find('#viewcreatecontent').html(' ');
	$('#viewcreatemodal').modal('show').find('#viewcreatecontent').load(url+'&id=$id&hiddenDropdown=$id');

})



$(document).on('click','#projectnewremark',function(){
$(document).find('#projectviewremark').modal('show').find('#projectremark');

})


$(document).on('click','.orderurl',function(){
var parent = $(this).parent();

$('.orderurltr').removeClass('activelist');
parent.addClass('activelist');
var url = parent.data('url');

$('#viewcreateheader').html('Order');
	$('#viewcreatemodal').modal('show').find('#viewcreatecontent').html(' ');
	$('#viewcreatemodal').modal('show').find('#viewcreatecontent').load(url);

})


$(document).on('click','.projecturl',function(){
var parent = $(this).parent();

$('.projecturltr').removeClass('activelist');
parent.addClass('activelist');
var url = parent.data('url');

$('#viewcreateheader').html('Project');
	$('#viewcreatemodal').modal('show').find('#viewcreatecontent').html(' ');
	$('#viewcreatemodal').modal('show').find('#viewcreatecontent').load(url);

})



$(document).on('click','.rpourl',function(){
var parent = $(this).parent();

$('.rpourltr').removeClass('activelist');
parent.addClass('activelist');
var url = parent.data('url');

$('#viewcreateheader').html('RPO');
	$('#viewcreatemodal').modal('show').find('#viewcreatecontent').html(' ');
	$('#viewcreatemodal').modal('show').find('#viewcreatecontent').load(url);

})

$(document).on('click','.correspondenceurl',function(){
var parent = $(this).parent();

$('.correspondenceurltr').removeClass('activelist');
parent.addClass('activelist');
var url = parent.data('url');

$('#viewcreateheader').html('Correspondence');
	$('#viewcreatemodal').modal('show').find('#viewcreatecontent').html(' ');
	$('#viewcreatemodal').modal('show').find('#viewcreatecontent').load(url);

})

$(document).on('click','.invoiceurl',function(){
var parent = $(this).parent();

$('.invoiceurltr').removeClass('activelist');
parent.addClass('activelist');
var url = parent.data('url');

$('#viewcreateheader').html('Invoice');
	$('#viewcreatemodal').modal('show').find('#viewcreatecontent').html(' ');
	$('#viewcreatemodal').modal('show').find('#viewcreatecontent').load(url);

})

})


//$(document).find('#task').load('$taskurl');
//$(document).find('#reminder').load('$reminderUrl');
//$(document).find('#remarks').load('$remarksUrl');

$.get('$remarksUrl', function (response) {
  $(document).find('#remarks').html(response);
});
$.get('$reminderUrl', function (response) {
  $(document).find('#reminder').html(response);
});
$.get('$taskurl', function (response) {
  $(document).find('#task').html(response);
});


JS;

$this->registerJs($paymentJs);
?>
