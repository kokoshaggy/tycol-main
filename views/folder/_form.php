<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TmFolder */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="folder-form">

    
	<?php $form = ActiveForm::begin(['id' => 'folderform', 'enableClientValidation' => true, 'attributes' => $model->attributes(), 'enableAjaxValidation' => false,]); ?>


    <?= $form->field($model, 'tyc_ref')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList([ 'project' => 'Project', 'administrative' => 'Administrative', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notes')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-default' : 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$js = <<<JS

$('#folderform').on('beforeSubmit', function (e) {
	$('#buttonText').hide();
 	$('#loader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#loader').hide();
   if(result==1){
	   
	   $(document).find('#loader1').html(result).show();
	   
    
    }else{
    $(document).find('#loader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#loader').hide();
	$(document).find('#loader1').hide();
	$(document).find('#buttonText').show();
	}, 5000);
    return false;
    
    
    
});
JS;
 
$this->registerJs($js);
?>



