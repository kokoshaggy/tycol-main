<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
?>
<div class="folder-create">
    <section class="content">
        <? if(!empty($client) and !empty($supplier) ): ?>
   
        <h1><?= Html::encode($this->title) ?></h1>
   
    <?php $form = ActiveForm::begin([
   'enableClientValidation' => true, 'attributes' => $project->attributes(),'enableAjaxValidation' => false,
    
    'action' =>['createnewproject','id'=>$id]
    ]
    ); ?>

   

    <?=$form->field($project, 'client_id')->dropdownList(ArrayHelper::map($client, 'id', 'name'));?>
    <?=$form->field($project, 'supplier_id')->dropdownList(ArrayHelper::map($supplier, 'id', 'name'));?>

   

    <?= $form->field($project, 'client_reference')->textInput(['maxlength' => true]) ?>

    <?= $form->field($project, 'manufacturer_ref')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($project, 'project_status')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($project, 'tyc_ref')->hiddenInput(['value'=>$id])->label(false); ?>
    
    
    <div class="form-group">
        <?= Html::submitButton($project->isNewRecord ? 'Create' : 'Update', ['class' => $project->isNewRecord ? 'btn btn-success' : 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>



     <? endif?>

</div>
</section>
