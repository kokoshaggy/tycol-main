<?php
use app\models\Folder;


use app\controllers\ProjectController as project;
use yii\helpers\Html;
use app\models\Foldertask as userName;



/* @var $this yii\web\View */


//$this->title = 'My Yii Application';
?>


<style>
	#tab_1-3{
		border: none !important;
	}
	.tab-content{
		border: 1px solid #ccc !important;
	}
	
</style>




<!-- Main content -->
<section class="content">
      <!-- Small boxes (Stat box) -->
     
    <div class="row">
        
         <div class="col-lg-12 col-xs-12">
			 
			    <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
                
              <li class="active"><a href="#tab_1-3" data-toggle="tab">Create Tasks</a></li>
              <li ><a href="#tab_1-4" data-toggle="tab">Assigned Tasks</a></li>
              <li><a href="#tab_2-2" data-toggle="tab">Created Tasks</a></li>
              
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1-3">
				  <?= $this->render('/task/_form',[
				'folderRef' => $folderRef,
				'folderTask' => $folderTask,
				

			]); ?>
				</div>
              <div class="tab-pane" id="tab_1-4">
                
                  
                    <div class="folder-index">

                    	 <div class="box-group" id="accordion">
							 <?php $i=1; foreach($assignedTo as $key => $value){ ?>
                
							 
							<div class="panel box box-danger">
								  <div class="box-header">
									
									  <a data-toggle="collapse" data-parent="#accordion" href="#task<?= $i; ?>">
										  <span style="float:left;">
										<?= $value['title']; ?>  
											  (<?='Assinged by: '.userName::getUsername($value['owner'])->username;?>)
										  </span>
										  <span id="assigntotimer<?= $i?>" style="float:right;"></span>
									  </a>
									
								  </div>
								  <div id="task<?= $i; ?>" class="panel-collapse collapse">
									<div class="box-body">

										<?= $value['details']; ?>
										
										 

									</div>
								  </div>
							</div>
							 <? 
								$assignToDuedate = strtotime($value['due_date']);
								$assignTo = date("M d, Y g:i ", $assignToDuedate);
							?>
							 <script>  
								dueDateCountDown("assigntotimer"+<?=$i;?>,"<?= $assignTo; ?>");
							</script>
							 <? $i++;} ?>
							 
						</div>
               
    
                  <div class="box-body">
                    <!-- Content Here -->
                </div>
              </div>
           
              </div>
				 <div class="tab-pane" id="tab_2-2">
               
                  
                 
                    
                    <div class="folder-index">

                
    
               
    
                  
					  <div class="box-group" id="owner">
					   <?php $i=1; foreach($ownersTasks as $key => $value){ ?>
                
							 
							<div class="panel box box-danger">
								  <div class="box-header">
									  <a data-toggle="collapse" data-parent="#owner" href="#owner<?= $i; ?>">
											<span style="float:left;">
												<?= $value['title']; ?> 
										  	</span>

												  
												<span id="ownertimer<?= $i?>" style="float:right;" >
												
												</span>
												
												

											
										</a>
								  </div>
								  <div id="owner<?= $i; ?>" class="panel-collapse collapse">
									<div class="box-body">
										<?= $value['details']; ?>
										
									  </div>
								  </div>
							</div>
						  	<? 
								$time = strtotime($value['due_date']);
								$myFormatForView = date("M d, Y g:i ", $time);
							?>
						  	<script>  
								dueDateCountDown("ownertimer"+<?=$i;?>,"<?= $myFormatForView; ?>");
							</script>
							 <? $i++;} ?>
					 
                    
                </div>
              </div>
                
              </div>
              </div>
              </div>
              <!-- /.tab-pane -->
             
              
              
             
            </div>
            <!-- /.tab-content -->
          </div>
			 
        </div>

        </div>
      
</section>

<?
$folderTask = <<<JS

$(document).on('beforeSubmit','#task', function (e) {
	$('#taskbuttonText').hide();
 	$('#taskloader').show();
    var \$taskforms = $(this);
    $.post(\$taskforms.attr('action'),\$taskform.serialize())
    .always(function(result){
	
	$(document).find('#taskloader').hide();
   if(result=='sent'){
	   
	   $(document).find('#taskloader1').html(result).show();
	  // $(document).find('#flash').append(result).show();
	   
	 
	  // $(document).find('#personforms').trigger('reset');
	   //$("html, body").delay(200).animate({
      //  scrollTop: $('#flash').offset().top
		
    //}, 2000);
		
	   $('#dashboard').modal('toggle');
	 	$.pjax.reload({container:"#person_reload",async: false
}); 
	  
	   
    
    }else{
    $(document).find('#taskloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#taskloader').hide();
	$(document).find('#taskloader1').hide();
	$(document).find('#taskbuttonText').show();
	}, 5000);
    return false;
    
    
    
});
JS;

$this->registerJs($folderTask);
?>






