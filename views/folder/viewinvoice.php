<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Corporation;

/* @var $this yii\web\View */
/* @var $model app\models\TmFolder */


$this->params['breadcrumbs'][] = ['label' => 'Folders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('folderSidebar'); ?>

     <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-folder-open fa-fw','title' => 'Open folder']). Html::tag('span', 'Update Folder', ['class' => '','title' => 'Open folder']), ['update', 'id' => $tyc_ref->tyc_ref], [
        'class' => '', 
        ]) ?>
   </li>


    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-folder-open fa-fw','title' => 'Open folder']). Html::tag('span', 'Delete Folder', ['class' => '','title' => 'Open folder']), ['delete', 'id' => $tyc_ref->tyc_ref], ['class' => '', 'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
                  ],]) ?>
   </li>

    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-folder-open fa-fw','title' => 'Open folder']). Html::tag('span', 'Create New invoice', ['class' => '','title' => 'Open folder']), ['createinvoice','id' => $tyc_ref->tyc_ref], ['class' => '']) ?>
   </li>


    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-folder-open fa-fw','title' => 'Open folder']). Html::tag('span', 'View invoices', ['class' => '','title' => 'Open folder']), ['view','id' => $id], ['class' => '']) ?>
   </li>

    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-folder-open fa-fw','title' => 'Open folder']). Html::tag('span', 'Create New Invoice', ['class' => '','title' => 'Open folder']), ['create','id' => 100], ['class' => '']) ?>
   </li>


    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-folder-open fa-fw','title' => 'Open folder']). Html::tag('span', 'View Invoice', ['class' => '','title' => 'Open folder']), ['viewinvoice','id' => $tyc_ref->tyc_ref], ['class' => '']) ?>
   </li>

    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-folder-open fa-fw','title' => 'Open folder']). Html::tag('span', 'Create New Comment', ['class' => '','title' => 'Open folder']), ['create','id' => 100], ['class' => '']) ?>
   </li>


    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-folder-open fa-fw','title' => 'Open folder']). Html::tag('span', 'View Comments', ['class' => '','title' => 'Open folder']), ['create','id' => 100], ['class' => '']) ?>
   </li>

    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-folder-open fa-fw','title' => 'Open folder']). Html::tag('span', 'Create New Order', ['class' => '','title' => 'Open folder']), ['create','id' => 100], ['class' => '']) ?>
   </li>


    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-folder-open fa-fw','title' => 'Open folder']). Html::tag('span', 'View Orders', ['class' => '','title' => 'Open folder']), ['create','id' => 100], ['class' => '']) ?>
   </li>

<?php $this->endBlock(); ?>

<div class="folder-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--<p>
        <?= Html::a('Update', ['update', 'id' => $tyc_ref->tyc_ref], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $tyc_ref->tyc_ref], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p> -->

   

</div>



<section class="content">
      <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-google-plus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Likes</span>
              <span class="info-box-number">41,410</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
          
          
          <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-google-plus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Likes</span>
              <span class="info-box-number">41,410</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
          
          <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-google-plus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Likes</span>
              <span class="info-box-number">41,410</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-xs-12">
          <div class="box">
              <div class="folder-index">

                <h1><?= Html::encode($this->title) ?></h1>
                  
    
               
    
                  <div class="box-body">
                    <div class="" style="text-align:center;"><h1>Folder Invoices</h1></div>
                    <table id="example1" class="table table-bordered table-striped">
                    
                    
                    
                        
                    <thead>
                        <tr>
                          <th>invoice  No</th>
                          <th>Description</th>
                          <th>Amount</th>
                          <th>Currency</th>
                          <th>Date</th>
                          <th>Action</th>

                        </tr>
                    </thead>
                        <tbody>
                <?php foreach($invoices as $k=>$v){ ?>
                    <tr>
                      <td><?= $v['receivedpurchaseorder_id'];  ?></td>
                      <td><?= $v['description'];  ?></td>
                      <td><?= $v['amount'];  ?></td>
                      <td><?= $v['currency_id'];  ?></td>
                      <td><?= $v['creation_date'];  ?></td>
                      <td>
						  <?=Html::a(Html::tag('span',
								Html::tag('i', '', ['class' => 'fa fa-eye fa-fw','title' => 'Open folder'])
							 ),['view','id' => $v['tyc_ref']]);?><?=Html::a(Html::tag('span',
								Html::tag('i', '', ['class' => 'fa fa-pencil fa-fw','title' => 'Edit folder'])
							 ),['edit','id' => $v['tyc_ref']]);?> 
						</td>
                      
                    </tr>
                <?php }?>
                   
                  </tbody>
                        <tfoot>
                  <tr>
                  <th>invoice</th>
                  <th>Client</th>
                  <th>Supplier</th>
                  <th>Status</th>
                  <th>Action</th>
                  
                </tr>
                </tfoot>
                
                    </table>
                </div>
              </div>
            </div>
          </div>
    </div>
</section>
