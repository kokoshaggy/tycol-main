<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TmFolder */

$this->title = 'Create New Folder';
$this->params['breadcrumbs'][] = ['label' => 'Folders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="folder-create">
	<?php if(Yii::$app->controller->action->id == 'index' and Yii::$app->controller->id == 'site'){ ?>
    	<h1><?//= Html::encode($this->title) ?></h1>
    <?php } ?>

    <?= $this->renderAjax('_form', [
        'model' => $model,
    ]) ?>

</div>

