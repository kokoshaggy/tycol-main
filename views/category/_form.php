<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;



/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(['enableClientValidation' => false,'enableAjaxValidation' => false, 'id' => 'dashboardcategory']); ?>
    <div class="indexform">

    <?= $form->field($model, 'category_name')->textInput(['maxlength' => true]) ?>

	</div>

    <div class="form-group">
        <?= Html::submitButton('Create', ['class' => 'btn btn-success', 'id' => 'catbutton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$getCategory = Url::to(['/category/newcategory']);
$categoryform = <<<JS

$('#dashboardcategory').on('beforeSubmit', function (e) {
    var \$form = $(this);
	var data = \$form.serialize();
	data['final'] = true;
    $.post( \$form.attr('action'), data )
	
		.always(function(result) {
			if (result.success == true) {
				$.get("$getCategory", function(data){
        		$(document).find('.categoryRefesh select').prepend('<option selected value="'+data.categoryId+'" >'+data.catName+'</option>');
    			});
				$(document).find('#categoryModal').modal('hide');
				$("html, body").delay(200).animate({
					scrollTop: $('#flash').offset().top
				}, 1000);   
			} else {
				alert("Something went wrong");
			}
		})
		
		.fail(function() {
			console.log('Server Error');
		});
	
    return false; //Stopping the submit. 
});
JS;
 
$this->registerJs($categoryform);
?>
