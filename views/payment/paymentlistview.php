<?php
use yii\helpers\Url;
use app\boffins_vendor\components\controllers\ComponentListViewWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Payment */
$attributes = [
            'amount' => 'valueAndSymbol',
            'receiver' => 'receiverName',
            'payment_date',
              
        ];
$action = [
	'update'=> Url::to(['payment/update']),
	'delete'=> Url::to(['payment/delete']),
];
?>

<?= ComponentListViewWidget::widget([
									'model'=>$model,
									'content'=>$payment,
									'attributes'=>$attributes,
									'hoverEffect'=>$hoverEffect,
									'action'=>$action,
									
								]); ?>


