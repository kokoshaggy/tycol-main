<?php
use app\models\Folder;

use yii\widgets\Pjax;
use app\boffins_vendor\controllers\ProjectController as project;
use yii\helpers\Html;
use app\models\Foldertask as userName;



/* @var $this yii\web\View */


//$this->title = 'My Yii Application';
?>

<!-- Main content -->
<section class="content">
      <!-- Small boxes (Stat box) --> 
    <div class="row">
        <div class="col-md-5 ">
			<?= $this->render('_form', [
				'model' => $model,
				'folder' => $folder,
				'corporation' => $corporation,
				'paymentSource' => $paymentSource,
				'currencies' => $currencies,
				'language' => $language,
				]);
			?>
        </div>

         <div class="col-md-7 ">
			 <div class="box-group" id="accordion">
				 
				<?php $i=1; foreach($folderPayment as $key => $value){ ?>


					<div class="panel box box-danger">
						<div class="box-header">

							<a data-toggle="collapse" data-parent="#accordion" href="#task<?= $i; ?>">
								<span style="float:left;">
									Payment made to <?= $value['payment_source_id']; ?>  

								</span>
								<span id="payment<?= $i?>" style="float:right;"><?= $value['payment_date']; ?>  </span>
							</a>

						</div>
						<div id="task<?= $i; ?>" class="panel-collapse collapse">
							<div class="box-body">
								Folder: Tycref<?= $value['tyc_ref']; ?> </br> </hr>
								Source: <?= $value['payment_source_id']; ?> </br> </hr>
			 					Amount: Tycref<?= $value['currency_id'].$value['value']; ?> </br> </hr>
							
							</div>
						</div>
					</div>

				<? $i++;} ?>
							 
			</div>			 
        </div>
	</div>
      
</section>





