<?php
use app\boffins_vendor\components\controllers\ComponentViewWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Payment */
$viewAttributes = [
            'receiver',
            
			
            ['label'=>'Amount',
			 'value' => $model->owner->valueAndSymbol,
			],
            'payment_date',
            'last_updated',
        ];
?>
<?= ComponentViewWidget::widget([
									'model'=>$model,
									'subComponents'=>$subComponents,
									'viewAttributes'=>$viewAttributes,
								]); ?>
