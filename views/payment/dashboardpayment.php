<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\money\MaskMoney;
use yii\widgets\MaskedInput;
use app\models\FolderComponent;
/* @var $this yii\web\View */
/* @var $model app\models\Payment */
/* @var $form yii\widgets\ActiveForm */
$folderComponent = new FolderComponent();
?>

<style>
#paymentloader,#paymentloader1{
display: none;
}
.button-basic{
	background: #ccc;
}
	.field-component2{
		visibility: hidden;
		height: 0px !important;
	}
</style>

<div class="payment-form">

   
	<?php $form = ActiveForm::begin(['action' =>['payment/create-new'],'enableAjaxValidation' => false,'options' => ['id' => 'paymentdashboardform']]); ?>
	
	<? $componentName= ['folder' => 'Folder', 'project' => 'Project', 'invoice' => 'Invoice','order' => 'Order', 'rpo' => 'Received Purchase Order (L/C OR LPO)']; ?>
	
	<?= Html::label('Choose item Categories to link to', 'folder', ['class' => 'control-label folder']) ?>
	
    <?= $form->field($model, 'itemType')->dropdownList($componentName,[ 'prompt'=> 'Select...', 'class' => 'form-control', 'id' => 'componentname'] ) ?>
	
	<?= Html::label('Choose an Item', 'folder', ['class' => 'control-label folder']) ?>
	
	<?= $form->field($model, 'itemID')->widget(DepDrop::classname(), [
			'options'=>['id' => 'component'],
			'pluginOptions' => [
				'depends' => ['componentname'],
				'placeholder' => 'Select...',
				'url'=>Url::to(['/payment/list-items'])
			]
		]);
	?>
	
	<?/*=$form->field($folderComponent, 'tyc_ref')->widget(DepDrop::classname(),[
		
			'options'=>['id'=>'component2'],
			'pluginOptions'=>[
				'depends'=>['componentname','component'],
				'placeholder'=>'Select...',
				'url'=>Url::to(['/payment/prod'])
			]
		]);*/?>
	
	<?=$form->field($model, 'receiver_corporation_id')->dropdownList(ArrayHelper::map($corporation, 'id', 'short_name'));?>
	
	<?=$form->field($model, 'payment_source_id')->dropdownList(ArrayHelper::map($paymentSource, 'id', 'source_code'));?>
	
  
	
	<?= $form->field($model, 'value')->widget(MaskedInput::classname(), $valueSettings)  ?>
	

    <?= $form->field($model, 'currency_id')->dropDownList(ArrayHelper::map($currencies,'id', 'currencyString'), ['options' => ['class' => 'form_input'] ]) ?>

	
	<?= $form->field($model, 'payment_date')->widget(DatePicker::classname(), [
																	'options' => ['placeholder' => 'Select Payment Date ...','id' => 'payment_date'],
																	'pluginOptions' => [
																		'format' => 'dd/mm/yyyy',
																		'todayHighlight' => true
																	],
								
								]) 
		?>

    <div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? '<span id="paymentbuttonText">Create</span> <img id="paymentloader" src="images/45.gif" " /> <span id="paymentloader1"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic' : 'btn btn-basic','id'=>'paymentsubmit_id']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$jsDashboardPayment = <<<JS

$('#paymentdashboardform').on('beforeSubmit', function (e) {
	$('#paymentbuttonText').hide();
 	$('#paymentloader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#paymentloader').hide();
   if(result=='sent'){
	   
	   $(document).find('#paymentloader1').html(result).show();
	   
	   $(document).find('#flash').append(result).show();
	   
	 
	   $(document).find('#paymentdashboardform').trigger('reset');
	   $("html, body").delay(200).animate({
        scrollTop: $('#flash').offset().top
		
    }, 2000);
	  
    
    }else{
    $(document).find('#paymentloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#paymentloader').hide();
	$(document).find('#paymentloader1').hide();
	$(document).find('#paymentbuttonText').show();
	
	}, 5000);
    return false;
    
    
    
});
JS;
 
$this->registerJs($jsDashboardPayment);
?>
 