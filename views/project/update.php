<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tmproject */

$this->title = 'Update Project: ' . $model->pro_description;
$this->params['breadcrumbs'][] = ['label' => 'projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->project_id, 'url' => ['view', 'id' => $model->project_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="project-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        		'client' => $client,
                'supplier' => $supplier,
                'model' => $model,
                'folder' => $folder,
				'folderComponent' => $folderComponent
    ]) ?>

</div>

<?php 

	$projectUpdateJs = <<<JS

$(document).on('beforeSubmit','#projectforms', function (e) {
alert(1);
//return false;
	$('#projectbuttonText').hide();
 	$('#projectloader').show();
    var \$projectform = $(this);
    $.post(\$projectform.attr('action'),\$projectform.serialize())
    .always(function(result){
	
	$(document).find('#projectloader').hide();
   if(result=='sent'){
	   
	   $(document).find('#projectloader1').html(result).show();
	  // $(document).find('#flash').append(result).show();
	   
	 
	  // $(document).find('#personforms').trigger('reset');
	   //$("html, body").delay(200).animate({
      //  scrollTop: $('#flash').offset().top
		
    //}, 2000);
		
	   $('#dashboard').modal('toggle');
	 	$.pjax.reload({container:"#person_reload",async: false
}); 
	  
	   
    
    }else{
    $(document).find('#projectloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#projectloader').hide();
	$(document).find('#projectloader1').hide();
	$(document).find('#projectbuttonText').show();
	}, 5000);
    return false;
    
    
    
});
JS;

$this->registerJs($projectUpdateJs);
?>
