<style>

    .info-box span{
        
        font-size:14px !important;
        color: #333;
        margin-bottom:5px;
    }

    .form-group{
        margin: 0 0 10px 0 !important;
    }
    #project-desc{
    	float: left;
    	font-family: candara;
    	font-size: 14px;
    }
    .box{
    	border:0px;
	-webkit-box-shadow:none;
	box-shadow: none;
    }
    .tab-pane{
    	border:0px;
	-webkit-box-shadow:none;
	box-shadow: none;
    }
    .tab-content{
    	border:1px solid #ccc;
    	font-family: candara;
    }
    .nav-tabs{
    	font-family: candara;
    }
    .input-group{
    	font-family: candara;
    }
    .box-header h3{
    	font-family: candara;

    }
    .box-header{
    	text-align: center
    }
    
    .form_control{
    	width: 100% !important;
    }
    .input-group{
    	width: 100% !important;
    }
</style>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Corporation;
use  yii\bootstrap\Alert;
use app\models\Client;
use app\models\Supplier;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

/* @var $this yii\web\View */
/* @var $model app\models\TmFolder */


$this->params['breadcrumbs'][] = ['label' => 'Folders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginBlock('folderview'); ?>
Project > Project
<small> > Remarks</small>

   
<?php $this->endBlock(); ?>

<div class="folder-view">

    <h1><?= Html::encode($this->title); ?></h1>

    
   

</div>



<section class="content">
    
    
    
    
    
    <!-- flash messages start -->
    <? if (Yii::$app->session->getFlash('created_successfully') !==NULL): ?>
		 <?= Alert::widget([
	   'options' => ['class' => 'alert-info'],
	   'body' => Yii::$app->session->getFlash('created_successfully'),
		 ]);?>
    <? endif ?>
    
    <? if (Yii::$app->session->getFlash('created_folder') !==NULL): ?>
		 <?= Alert::widget([
	   'options' => ['class' => 'alert-info'],
	   'body' => Yii::$app->session->getFlash('created_folder'),
		 ]);?>
    <? endif ?>
    
    <? if (Yii::$app->session->getFlash('instruction') !==NULL): ?>
		 <?= Alert::widget([
	   'options' => ['class' => 'alert-info'],
	   'body' => Yii::$app->session->getFlash('instruction'),
		 ]);?>
    <? endif ?>
    <!-- flash messages end -->
	<div class="row">
          
          <div class="col-md-8 col-sm-7 col-xs-7" id = "project-desc">
          <div class="info-box">
            <span class="info-box-icon iconimage "><i class="" aria-hidden="true"  ></i><img src="./images/male.png"></span>
              <div class="info-box-content">
              <span class=" info-box-number" style="margin-bottom:10px;"><strong>TYC REF: </strong><?//= $folderdetails->tyc_ref;?> </span>
              
              <span class=" info-box-number" style="margin-bottom:10px;"><strong>Folder description:  </strong><?//= $folderdetails->description;?> </span>
                
            </div>
              
            <div class="info-box-content">
               <span class=" info-box-number" style=""><strong>Project description: </strong><?= $model->pro_description;?> </span>
              
              <span class=" info-box-number" style=""><strong>Client:  </strong><?= Client::get_client_name($model->client_id); ?> </span>
                <span class=" info-box-number" style=""><strong>Supplier: </strong><?= Supplier::findOne([$model->supplier_id])->name; ?> </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
          
       
    
    
	</div>
	
	<div class="row">     
		<div class="col-md-8">
			<div class="box box-black">
			 	<div class="box-header with-border" style="text-align: center;">              
					<h3 class="box-title" style="font-family: candara;font-weight: bold;">Task and Reminders</h3>
					<div class="box-tools  pull-right">
						<button  class="btn btn-box-tool" data-widget="collapse" style="background: #fff !important;font-size: 16px" ><i class="fa fa-caret-down"></i></button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" style="background: #fff !important"><i class="fa fa-times"></i></button>
					</div>
				</div>
        	<div class="nav-tabs-custom">
				<ul class="nav nav-tabs pull-right">
                
				  <li class="active"><a href="#tab_1-1" data-toggle="tab">Project Remarks</a></li>
				  <li><a href="#tab_2-2" data-toggle="tab">Invoice Remarks</a></li>
				  <li><a href="#tab_3-2" data-toggle="tab">Order Remarks</a></li>
				  <li><a href="#tab_4_4" data-toggle="tab">All Remarks</a></li>

				  <li class="pull-left header"><i class=""></i>Remarks </li>
            	</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab_1-1">
						<div class="box">
							<h1 style="text-align:center;">Project Remarks</h1>
							<div class="folder-index">
								<h1><?= Html::encode($this->title) ?></h1>
								<div class="box-body">
									<table id="project_table" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th></th>
											</tr>
										</thead>
										<tbody>

											<?php foreach($projectcomment as $k=>$v){ $sn=1;?>

												<tr>
										
													<td>
														<div class="box-footer box-comments">
															<div class="box-comment">
															
																<div class="comment-text">
																	<span class="username">
																	<i class="fa fa-comments"></i>
																	&nbsp;   <?= $this->context->convert_tycref_tag($v['text']); ?>
																	<span class="text-muted pull-right"><?= $v['remark_date'];  ?></span>
																	</span>

																</div>
															<!-- /.comment-text -->
															</div>
														</div>
													</td>
												</tr>
											<?php $sn++;}?>

										</tbody>
										<tfoot>
											<tr>
												<th></th>
											</tr>
										</tfoot>

									</table>
								</div>
							</div>
						</div>
				  	</div>
              
					<div class="tab-pane" id="tab_2-2">
						<div class="box">
							<h1 style="text-align:center;">Invoice Remarks</h1>
							<div class="folder-index">

								<h1><?= Html::encode($this->title) ?></h1>

								<div class="box-body">

									<table id="invoice_table" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th></th>


											</tr>
										</thead>
										<tbody>

										<?php foreach($invoicecomment as $k=>$v){ $sn=1;?>

											<tr>
											  <td>
												  <div class="box-footer box-comments">
													  <div class="box-comment">

														<div class="comment-text">
															<span class="username">
															<i class="fa fa-comments"></i>
															&nbsp;   <?= $this->context->convert_tycref_tag($v['text']); ?>
															<span class="text-muted pull-right"><?= $v['remark_date'];  ?></span>
															</span>    
														</div>

													  </div>

												  </div>

											  </td>
											</tr>
										<?php $sn++;}?>

										</tbody>
										<tfoot>
											<tr>
												<th></th>
											</tr>
										</tfoot>


									</table>

								</div>

							</div>
						</div>
					</div>
              
					<div class="tab-pane" id="tab_3-2">
						<div class="box">
							<h1 style="text-align:center;">Order Remarks</h1>
							<div class="folder-index">

							 	<h1><?= Html::encode($this->title) ?></h1>
								<div class="box-body">
									<table id="order_table" class="table table-bordered table-striped">
										<thead>
											<tr>
											  <th></th>
											</tr>
										</thead>
										<tbody>

											<?php foreach($ordercomment as $k=>$v){ $sn=1;?>

												<tr>
												  <td>
													  <div class="box-footer box-comments">
														  <div class="box-comment">

															<div class="comment-text">
																  <span class="username">
																	<i class="fa fa-comments"></i>
																	&nbsp;   <?= $this->context->convert_tycref_tag($v['text']); ?>
																	<span class="text-muted pull-right"><?= $v['remark_date'];  ?></span>
																  </span><!-- /.username -->

															</div>


														  </div>
													  </div>

												  </td>
												</tr>
											<?php $sn++;}?>

									    </tbody>
										<tfoot>
											<tr>
												<th></th>
											</tr>
										</tfoot>


								
									</table>

							  </div>
							
							</div>
					
						</div>
					</div>
                
					<div class="tab-pane" id="tab_4_4">
						<div class="box">
                    		<h1 style="text-align:center;">All Remarks</h1>
                    		<div class="folder-index">

                    			<h1><?= Html::encode($this->title) ?></h1>
       
                  				<div class="box-body">
                    
                    				<table id="all_table" class="table table-bordered table-striped">
										<thead>
											<tr>
											  <th></th>


											</tr>
										</thead>
                        				<tbody>
                            
											<?php foreach($allremark as $k=>$v){ $sn=1;?>

												<tr>

												  <td>
													  <div class="box-footer box-comments">
														  <div class="box-comment">
																<div class="comment-text">
																	  <span class="username">
																		<i class="fa fa-comments"></i>
																		&nbsp;   <?= $this->context->convert_tycref_tag($v['text']); ?>
																		<span class="text-muted pull-right"><?= $v['remark_date'];  ?></span>
																	  </span>
																</div>
															</div>
														</div>				   
												  </td>
												</tr>
											<?php $sn++;}?>
                   
                  					</tbody>
									<tfoot>
										<tr>
											<th></th>
										</tr>
									</tfoot>
                
                    
									</table>
                				</div>
              				</div>
            			</div>
              		</div>
            	</div>
            </div>
        </div>
		</div>
        
        <div class="col-md-4" >
			<div class="box box-danger">
				<div class="box-header">
				  	<h3 class="box-title">New Remark</h3>
				</div>
            	<div class="box-body">
             
					<div class="form-group" id = "form-group">
                
						<div class="input-group">
                  
							<?php $form = ActiveForm::begin([
								'id' => 'login-form',
								'options' => ['class' => 'form-horizontal'],
								'action' =>['createremark','id'=>$model->project_id,]
								]); 
							?>



							<?=   $form->field($remark, 'remark_type')->dropdownList([
										'project'=> 'Project', 
										'invoice'=> 'Invoice',
										'order'=>'Order'
									],
									['prompt'=>'Select remark type']);
							?>

							<?= $form->field($remark, 'text')->textarea(['id'=>'test','class'=>'form_control']); ?>
							<div class="form-group">
								<?= Html::submitButton($remark->isNewRecord ? 'Create' : 'Update', ['class' => $remark->isNewRecord ? 'btn btn-success' : 'btn btn-danger']) ?>
							</div>


							<?//= $form->field($remark, 'tyc_ref')->hiddenInput(['maxlength' => true, 'value'=>$model->tyc_ref])->label(false); ?>

							<?= $form->field($remark, 'project_id')->hiddenInput(['maxlength' => true,'value'=>$model->project_id])->label(false); ?>

							<? $now=date('Y-m-d h:i:s');?>
							<?= $form->field($remark, 'remark_date')->hiddenInput(['maxlength' => true,'value'=>$now])->label(false); ?>


    					<?php ActiveForm::end(); ?>
                
						</div>
                
					</div>
          
				</div>
			</div>
		</div>
	</div>            
</section>

