
<style>
    .floatright{
        float: right;
    }
    .foldernote{
        line-height: 24px;
        text-underline-position: alphabetic;
        margin-top: 10px;
        
    }
    .box-body h3{
      font-family: candara;
      font-weight: bold;
      float: left;
    }
    .box{
      border:none !important;
      -webkit-box-shadow:none !important;
      box-shadow: none !important;
    }
    .folder-index{
       border:none !important;
       -webkit-box-shadow:none !important;
       box-shadow: none !important;
    }
    .box-body{
       border:none !important;
       -webkit-box-shadow:none !important;
       box-shadow: none !important;
    }
    .floatright{
      margin-right: 10px;
    }
    #info-box{
        border:2px solid #ccc !important;
       -webkit-box-shadow:none !important;
       box-shadow: none !important;
       font-family: candara;
    }
    .headers{
      font-family: candara;
    }
</style>
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use app\models\Corporation;
use app\models\Supplier;
use  yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\models\TmFolder */


$this->params['breadcrumbs'][] = ['label' => 'Folders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('folderview'); ?>
Folder
<small>Select Project</small>

   
<?php $this->endBlock(); ?>

<?php $this->beginBlock('folderSidebar'); ?>

     


    

    

    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-eye fa-fw','title' => 'Open folder']). Html::tag('span', 'View Projects', ['class' => '','title' => 'Open folder']), ['view','id' => $id], ['class' => '']) ?>
   </li>

    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-plus-square','title' => 'Open folder']). Html::tag('span', 'Create New Invoice', ['class' => '','title' => 'Open folder']), ['create','id' => 100], ['class' => '']) ?>
   </li>


    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-eye fa-fw','title' => 'Open folder']). Html::tag('span', 'View Invoice', ['class' => '','title' => 'Open folder']), ['viewinvoice','id' => $tyc_ref->tyc_ref], ['class' => '']) ?>
   </li>

    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-plus-square fa-fw','title' => 'Open folder']). Html::tag('span', 'Create New Comment', ['class' => '','title' => 'Open folder']), ['create','id' => 100], ['class' => '']) ?>
   </li>


    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-eye fa-fw','title' => 'Open folder']). Html::tag('span', 'View Comments', ['class' => '','title' => 'Open folder']), ['create','id' => 100], ['class' => '']) ?>
   </li>

    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-plus-square fa-fw','title' => 'Open folder']). Html::tag('span', 'Create New Order', ['class' => '','title' => 'Open folder']), ['create','id' => 100], ['class' => '']) ?>
   </li>


    <li>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-eye fa-fw','title' => 'Open folder']). Html::tag('span', 'View Orders', ['class' => '','title' => 'Open folder']), ['create','id' => 100], ['class' => '']) ?>
   </li>

<?php $this->endBlock(); ?>

<div class="folder-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--<p>
        <?= Html::a('Update', ['update', 'id' => $tyc_ref->tyc_ref], ['class' => 'btn btn-danger']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $tyc_ref->tyc_ref], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p> -->

   

</div>



<section class="content">
    <!-- flash messages start -->
    <? if (Yii::$app->session->getFlash('created_successfully') !==NULL): ?>
     <?= Alert::widget([
   'options' => ['class' => 'alert-info'],
   'body' => Yii::$app->session->getFlash('created_successfully'),
     ]);?>
    <? endif ?>
    
    <? if (Yii::$app->session->getFlash('created_folder') !==NULL): ?>
     <?= Alert::widget([
   'options' => ['class' => 'alert-info'],
   'body' => Yii::$app->session->getFlash('created_folder'),
     ]);?>
    <? endif ?>
    
    <? if (Yii::$app->session->getFlash('instruction') !==NULL): ?>
     <?= Alert::widget([
   'options' => ['class' => 'alert-info'],
   'body' => Yii::$app->session->getFlash('instruction'),
     ]);?>
    <? endif ?>
    <!-- flash messages end -->
      <div class="row">
          <div class="col-md-5 col-sm-5 col-xs-5">
         
        </div>
          
          
          <div class="col-md-7 col-sm-7 col-xs-7">
          <div class="info-box" id = "info-box">
            <span class="info-box-icon iconimage "><i class="" aria-hidden="true"></i></span>

            <div class="info-box-content">
                <span class="info-box-text"><strong>TYC REF:</strong> <?= $tyc_ref->tyc_ref; ?></span>
              <span class=" info-box-text"><strong>Description:</strong><?= $tyc_ref->description; ?></span>
                <span class="info-box-text foldernote"><strong>Note:</strong> <?= $tyc_ref->notes; ?></span>
                
                
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
          
          
        <div class="col-xs-12">
          <div class="box">
              <div class="folder-index">

                <h1><?= Html::encode($this->title) ?> <div class="floatright"><div class="floatright btn btn-danger" id="createproject" data-formurl="<?= Url::to(['createproject']) ?>">Create new payment
						</div></div>
                  
                </h1>
                  
                <span style="" class="info-box-text"></span>
               
    
                  <div class="box-body">
                    
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                              <th>Project</th>
                              <th>Client</th>
                              <th>Supplier</th>
                              <th>Status</th>
                              <th>Action</th>
                  
                            </tr>
                        </thead>
                        <tbody>
							<? if(empty($projects)){ ?>
							<tr>
							<td colspan="5" style="text-align:center;">
								No Available Project
							</td>
								
							</tr>
							<? }else{?>
                            <?php foreach($projects as $k=>$v){ ?>
                                <tr>
                                  <td><?//= $v['tyc_ref'];  ?></td>
                                  <td><?= Corporation::getclientname($v['client_id']);  ?></td>
                                  <td>
									  <? $supplier = Supplier::findOne($v['supplier_id']); ?>
									  <?=$supplier->corporation->name; ?>
									  
									</td>
                                  <td><?= $v['project_status'];  ?></td>
                                  <td>
                                      <?= Html::a(Html::tag('span',
                                        Html::tag('i', '', ['class' => 'fa fa-folder-open fa-fw','title' => 'view project'])
                                     ),['project/view','id' => $v['project_id']]); ?>
															   
															   
                                                          <?=Html::a(Html::tag('span',Html::tag('i', '', ['class' => 'fa fa-pencil fa-fw','title' => 'Edit project'])
                                     ),['../project/view','id' => $v['project_id']]); ?>
                                    </td>

                                </tr>
                            <?php }}?>
                   
                  </tbody>
                        <tfoot>
                            <tr>
                      <th>Project</th>
                      <th>Client</th>
                      <th>Supplier</th>
                      <th>Status</th>
                      <th>Action</th>

                    </tr>
                </tfoot>
                
                    </table>
                </div>
              </div>
            </div>
          </div>
    </div>
</section>
