 <style>
 
    .floatright {
        float: right;
    }
	
    .foldernote {
        line-height: 24px;
        text-underline-position: alphabetic;
        margin-top: 10px;
    }
	
	#proloader {
		display:none;
	}
	
	#flash {
		display: none;
	}
	 
</style>
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use app\models\Corporation;
use yii\bootstrap\Alert;
use app\models\Supplier;
use app\boffins_vendor\components\controllers\Menu;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Folder */
$this->title = 'Tycol | Project';
$this->params['breadcrumbs'][] = ['label' => ' Folders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('folderview'); ?>
Project
<small>Select Project</small>
   
<?php $this->endBlock(); ?>

<?php $this->beginBlock('folderSidebar'); ?>
	<?= Menu::widget(); ?>

<?php $this->endBlock(); ?>

<div class="folder-view">

    

    <!--<p>
        <?= Html::a('Update', ['update', 'id' => 1], ['class' => 'btn btn-danger']) ?>
        <?= Html::a('Delete', ['delete', 'id' => 1], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p> -->

   

</div>



<section class="content">
    <!-- flash messages start -->
    <? if (Yii::$app->session->getFlash('created_successfully') !==NULL): ?>
		 <?= Alert::widget([
	   'options' => ['class' => 'alert-info'],
	   'body' => Yii::$app->session->getFlash('created_successfully'),
		 ]);?>
    <? endif ?>
    
    <? if (Yii::$app->session->getFlash('created_folder') !==NULL): ?>
		 <?= Alert::widget([
	   'options' => ['class' => 'alert-info'],
	   'body' => Yii::$app->session->getFlash('created_folder'),
		 ]);?>
    <? endif ?>
    
    <? if (Yii::$app->session->getFlash('instruction') !==NULL): ?>
		 <?= Alert::widget([
	   'options' => ['class' => 'alert-info'],
	   'body' => Yii::$app->session->getFlash('instruction'),
		 ]);?>
    <? endif ?>
    <!-- flash messages end -->
	
	<div class="row">
		<div class="col-lg-12">
			<?= Alert::widget([
				   'options' => ['class' => 'alert-info','id'=>'flash'],
				   'body' => Yii::$app->session->getFlash('created_successfully'),
					 ]);?>
		</div>
	</div>
      <div class="row">
          
          
          
          
          
		  <div class="col-xs-7 " >
			  <? $projectId=''; if($model->find()->count() <= 0){$projectId=1; echo 'no records';}else{$projectId=!empty($findOneProject->project_id)?$findOneProject->project_id:0; } ?>
			  
			  <div id="projectListView">
			  <img class="loadergif" src="images/loader.gif"  />
			  </div>
			
		  </div>
		  
		  <div class="col-xs-5 " style="padding:0px !important;margin-top: 20px !important">
			  
			  <div id="projectviewcontainer" class="fixede">
				  <div id="projectView" style='position: relative;'>
				  <img class="loadergif" src="images/loader.gif"  />
				  </div>
				  
			  </div>

			  
	</div>
		  </div>
    </div>
</section>

<? 
		Modal::begin([
			'header' =>'<h1 id="headers"></h1>',
			'id' => 'dashboard',
			'size' => 'modal-md',  
		]);
?>
<div id="formcontent"></div>
<?
	Modal::end();
?>


<? 
		Modal::begin([
			'header' =>'<h1 id="headers"></h1>',
			'id' => 'clientsupplier',
			'size' => 'modal-sm',  
		]);
?>
<div id="contentclientnsuppliers"></div>
<?
	Modal::end();
?>





<?php 
	$urlListView = Url::to(['project/projectlistview']);
	$urlView = Url::to(['project/projectview','id'=>$projectId]);
	
$js3 = <<<JS
$("#projectListView").load('$urlListView');
$("#projectView").load('$urlView',{var2:1});
$(document).on('click','.projecturl',function(){
var parent = $(this).parent();

$('.projecturltr').removeClass('activelist');
parent.addClass('activelist');

$('#projectviewcontainer').fadeOut('fast');
$('#projectView').html('<img class="loadergif" src="images/loader.gif" " />');
var url = parent.data('url');
$("#projectView").load(url,{var2:1},function(){

$('#projectviewcontainer').slideDown('fast');

});
})



$(document).on('click','#createproject',function(){
var formUrl = $(this).data('formurl');




$.ajax({
  url: formUrl,
  headers: {
     'Cache-Control': 'no-cache, no-store, must-revalidate', 
     'Pragma': 'no-cache', 
     'Expires': '0'
   },
  
  success: function(response){
  console.log(response);
    $('#projectviewcreate').modal('show').find('#projectcreateform').html(response);
  },
  
});
})

$(document).on('click','#projectnewremark',function(){
$('#projectviewremark').modal('show').find('#projectremark');

})



JS;
 
$this->registerJs($js3);
?>



																  
																  
																  
