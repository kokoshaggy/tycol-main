<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tmprojectsearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tyc_ref') ?>

    <?= $form->field($model, 'project_id') ?>

    <?= $form->field($model, 'client_id') ?>

    <?= $form->field($model, 'supplier_id') ?>

    <?= $form->field($model, 'client_reference') ?>

    <?php // echo $form->field($model, 'manufacturer_ref') ?>

    <?php // echo $form->field($model, 'project_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
