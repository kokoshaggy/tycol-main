<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\boffins_vendor\components\controllers\ComponentLinkWidget;
use app\models\FolderComponent;
use app\models\StatusType as status;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model app\models\Tmproject */
/* @var $form yii\widgets\ActiveForm */

$clientUrl = Url::to(['client/adhockclient']);
$supplierUrl = Url::to(['supplier/adhocsupplier']);
?>


<style>
    .form_form_input{
        resize:none;
        width:100%;
    }
	
	#projectloader,#iprojectloader1 {
		display: none;
	}
	
	.glyphicon.active{
		color: red !important;
	}
</style>
<section>
	
<div class="project-form">

    <?php $form = ActiveForm::begin(['enableClientValidation' => true,'id'=>'projectforms', 'attributes' => $model->attributes(),'enableAjaxValidation' => false,]); ?>
	
	<?= ComponentLinkWidget::widget(['model'=>$model,'form'=>$form]); ?>
    

	<?php Pjax::begin(['id' => 'clientRefresh','timeout' => 5000]); ?>
	<?= $form->field($model, 'client_id',['options'=>[
                'tag'=>'div',
                'class'=>'form-group clientRefesh'],
                'template'=>'{label} <i class="glyphicon glyphicon-plus active newClients" title="Add new Client" data-Url="'.$clientUrl.'"></i>{input}{error}{hint}'
                    ])->dropdownList(ArrayHelper::map($client, 'id', 'name'));?>
	<?php Pjax::end(); ?>
	
	<?= $form->field($model, 'supplier_id',['options'=>[
                'tag'=>'div',
                'class'=>'form-group supplierRefesh'],
                'template'=>'{label} <i class="glyphicon glyphicon-plus active newSuppliers"  title="Add new Supplier" data-Url="'.$supplierUrl.'" ></i>{input}{error}{hint}'
                    ])->dropdownList(ArrayHelper::map($supplier, 'id', 'name'));?>


    <?= $form->field($model, 'client_reference')->textInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'pro_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'manufacturer_ref')->textInput(['maxlength' => true]) ?>
    
	<?= $form->field($model, 'project_status')->dropDownList(ArrayHelper::map(status::find()->where(['status_group'=>'project'])->all(), 'id', 'status_title')) ?>
    
    
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<span id="projectbuttonText">Create</span> <img id="projectloader" src="images/45.gif" " /> <span id="projectloader1"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic' : 'btn btn-basic','id'=>'projectsubmit_id']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>



</section>






