<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tmproject */

$this->title = 'Create project';
$this->params['breadcrumbs'][] = ['label' => 'projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="project-create">
	
	<?php if(Yii::$app->controller->action->id == 'index' and Yii::$app->controller->id == 'site'){ ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php } ?>
    <?= $this->render('_form', [
                'client' => $client,
                'supplier' => $supplier,
                'model' => $model,
                'folder' => $folder,
				
            ]);?>

</div>

<?php 

	$projectCreateJs = <<<JS
alert(1);
$(document).on('beforeSubmit','#projectforms', function (e) {
	$('#projectbuttonText').hide();
 	$('#projectloader').show();
    var \$projectform = $(this);
    $.post(\$projectform.attr('action'),\$projectform.serialize())
    .always(function(result){
	
	$(document).find('#projectloader').hide();
   if(result=='sent'){
	   
	   $(document).find('#projectloader1').html(result).show();
	  // $(document).find('#flash').append(result).show();
	   
	 
	  // $(document).find('#personforms').trigger('reset');
	   //$("html, body").delay(200).animate({
      //  scrollTop: $('#flash').offset().top
		
    //}, 2000);
		
	   $('#dashboard').modal('toggle');
	 	$.pjax.reload({container:"#person_reload",async: false
}); 
	  
	   
    
    }else{
    $(document).find('#projectloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#projectloader').hide();
	$(document).find('#projectloader1').hide();
	$(document).find('#projectbuttonText').show();
	}, 5000);
    return false;
    
    
    
});
JS;

$this->registerJs($projectCreateJs);
?>

