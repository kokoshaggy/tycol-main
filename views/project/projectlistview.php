

<style>
    .floatright {
		float: right;
		margin:10px;
    }
	
    .foldernote {
        line-height: 24px;
        text-underline-position: alphabetic;
        margin-top: 10px;
    }
	.projecturl {
		cursor: pointer;
	}
   #box {
		border: none;
		margin-top: 0px;
    }
	
   .box-body h3 {
		font-family: calibri !important;
		margin-top: 0px !important;
		width:50% !important;
		text-align:left;
	}
   
</style>
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\bootstrap\Alert;
use yii\bootstrap\Modal;
use app\models\Corporation;
use app\models\Supplier;

/* @var $this yii\web\View */
/* @var $model app\models\Folder */
?>

<div class="folder-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <!--<p>
        <?= Html::a('Update', ['update', 'id' => 1], ['class' => 'btn btn-danger']) ?>
        <?= Html::a('Delete', ['delete', 'id' => 1], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p> -->
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="box"  style="border-radius:5px !important;border: none;">
			<div class="folder-index">
				<h1><?= Html::encode($this->title) ?> 
					<div class="floatright btn btn-danger" id="createproject" data-formurl="<?= Url::to(['create']) ?>">Create New Project
					</div>
				</h1>
	
				<div class="box-body" id = "project-title">
					<h3>Projects List</h3>
					<table id="projectlistview" class="table table-bordered table-striped">
						
						<thead>
							<tr>
								<th>SN</th>
								<th>Folder</th>
								<th>Description</th>
								<th>Client</th>
								<th>Supplier</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						
						<tbody>
							<?php $sn = 1; foreach ($projects as $k => $v) { ?>
							
							<tr class="projecturltr <?if($sn === 1){echo 'activelist';} ?>" data-url="<?=Url::to(['project/projectview','id' => $v['project_id']]) ?>" title="Click to view project">
							
								<td class="projecturl"><?= $sn; ?></td>
								<td class="projecturl"><?= !empty($v['tycref']) && is_array($v['tycref']) ? implode("</br>", $v['tycref']) : 'not Available';  ?></td>
								<td class="projecturl"><?= $v['pro_description'];  ?></td>
								<td class="projecturl"><?= Corporation::getclientname($v['client_id']);  ?></td>
								<td class="projecturl"><? $supplier = Supplier::findOne($v['supplier_id']); echo $supplier->corporation->name; ?></td>
								<td class="projecturl"><? $statusTitle = $status::findOne($v['project_status']); echo !empty($statusTitle->status_title) ? $statusTitle->status_title: 'No Status';  ?></td>
																
							    <td>
								  <?/*=Html::a(Html::tag('span',
									Html::tag('i', '', ['class' => 'fa fa-eye fa-fw','title' => 'view project'])
								 ),['project/view','id' => $v['project_id']]);//= Html::a('test', ['view','id' => $v['tyc_ref']],['div'] ) */?>
								  
									<? 
									  $deletUrl = Url::to(['project/delete','id'=>$v['project_id']]);
									  $updateUrl = Url::to(['project/update','id'=> $v['project_id']]);
											?>
											<p>
												<?= Html::tag('i', '', ['class' => 'fa fa-pencil update', 'data-url'=>$updateUrl]); ?>
												<?= Html::tag('i', '', ['class' => 'fa fa-trash delete', 'data-url'=>$deletUrl]); ?>
											</p>
								</td>
								

							</tr>
							
							<?php $sn++; }?>
			  
						</tbody>
						<tfoot>
							<tr>
								<th>SN</th>
								<th>Folder</th>
								<th>Description</th>
								<th>Client</th>
								<th>Supplier</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
                		</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<? 
		Modal::begin([
			'header' =>'<h1 id="headers"></h1>',
			'id' => 'projectviewcreate',
			'size' => 'modal-md',  
		]);
		echo "<div id='projectcreateform'> </div>";
		Modal::end();
	?>

<script>
$("#projectlistview").DataTable({
        "aaSorting": [],
		"responsive": "true",
		"pagingType": "simple",
    });
</script>

