<style>
	#projectnewremark {
		float: right;
		margin: 5px;
	}
	
	#remarks-div {
		min-height: 280px; 
		width: 100%; 
		background:#fff; 
		position: relative;
		overflow: hidden;
	}
	
    .info-box span {
        font-family: calibri;
        font-size:14px;
        color: #333;
        margin-bottom:5px;
    }
    
    .form-group {
        margin: 0 0 10px 0 !important;
    }
	
    #box {
    	min-height:108px;
    	border:0px;
	    -webkit-box-shadow:none;
		box-shadow: none;
    }
	
    # { /* ***************************************** WHAT IS THIS???? ********************************** */
    	min-height:108px;
    	border:0px;
	    -webkit-box-shadow:none;
		box-shadow: none;
    }
    
    #tab {
    	font-size: 12px;
    	width:24% !important;
    	font-weight: bold;
    }
	
    #ul {
    	width:100%;
    }
    #tab-content {
    	border:1px solid #ccc;
    	border-top:none !important;
    }
	
    #tab_1-1 {
    	border:none !important;
    }
	
    #tab_2-2 {
    	border:none !important;
    }
	
    #tab_3-2 {
    	border:none !important;
    }
    #tab_4_4 {
    	border:none !important;
    }
    .folder-index {
    	border:none !important;
    }
    . {
    	border:none!important;
    }
	
    .box h3 {
    	font-family: calibri;
    }
	
    #image {
    	width: 100%;
    	height:100%;
    	opacity:0.5;
    }
	
    .active {
		border-top-color: #DD4B39 !important;
	}
</style>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Conewremarkration;
use yii\bootstrap\Alert;
use app\models\Client;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
use yii\bootstrap\Modal;
use app\models\Supplier;
use yii\widgets\Pjax;
use app\boffins_vendor\components\controllers\DisplayLinkedComponents;
/* @var $this yii\web\View */
/* @var $model app\models\TmFolder */


$this->params['breadcrumbs'][] = ['label' => 'Folders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginBlock('folderview'); ?>
Project > Project
<small> > Remarks</small>

   
<?php $this->endBlock(); ?>

<div class="folder-view">

    <h1><?= Html::encode($this->title); ?></h1>

    
   

</div>



<section class="content">
    
    <? 
	if (empty($model)) {
		echo "You do not have any projects. ";
	}
	?>
    
    
    
    <!-- flash messages start         **** WHY ARE YOUR FLASH MESSAGES HARD CODED? WHAT IF ANOOTHER FLASH MESSAGE IS CREATED SOMEWHERE ELSE? -->
    <? if (Yii::$app->session->getFlash('created_successfully') !==NULL): ?>
		 <?= Alert::widget([
	   'options' => ['class' => 'alert-info'],
	   'body' => Yii::$app->session->getFlash('created_successfully'),
		 ]);?>
    <? endif ?>
    
    <? if (Yii::$app->session->getFlash('created_folder') !==NULL): ?>
		 <?= Alert::widget([
	   'options' => ['class' => 'alert-info'],
	   'body' => Yii::$app->session->getFlash('created_folder'),
		 ]);?>
    <? endif ?>
    
    <? if (Yii::$app->session->getFlash('instruction') !==NULL): ?>
		 <?= Alert::widget([
	   'options' => ['class' => 'alert-info'],
	   'body' => Yii::$app->session->getFlash('instruction'),
		 ]);?>
    <? endif ?>
    <!-- flash messages end -->
	<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon iconimage "><img src = "../web/images/male.png" aria-hidden="true" style="font-size:70px; opacity: 0.5 !important;margin: 2px 10px 2px 5px;" ></i></span>
				<div class="info-box-content">
					<span class=" info-box-number"><strong>Folder Number: </strong><?php echo !empty($model->tycref) && is_array($model->tycref) ? implode("</br>", $model->tycref) : 'not Available'?> </span>
					  
					<span class=" info-box-number"><strong>Folder description:  </strong><?= !empty($model->folderDescription->description)?$model->folderDescription->description:'Not Available';?> </span>
					<span class=" info-box-number" style=""><strong>Project description: </strong><?= $model->pro_description;?> </span>
					<span class=" info-box-number" style=""><strong>Client:  </strong><?= Client::get_client_name($model->client_id); ?> </span>
					<span class=" info-box-number" style=""><strong>Supplier: </strong><? $supplier = Supplier::findOne($model->supplier_id); echo $supplier->corporation->name; ?> </span>
				</div>
			</div>
        </div>
	</div>
	
	<div  id="remarks-div" >
		<div class="row">     
			<div class="col-md-12">
				<div class="box box-black collapsed-box">
					<div class="box-header with-border" style="text-align: center;">              
						<h3 class="box-title" style="font-family: candara;font-weight: bold;">Remarks</h3>
						<div class="box-tools  pull-right">
							<button  class="btn btn-box-tool" data-widget="collapse" style="background: #fff !important;font-size: 16px" ><i class="fa fa-caret-down"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" style="background: #fff !important"><i class="fa fa-times"></i></button>
						</div>
					</div> 
					<div class="nav-tabs-custom">
						<ul id = "ul" class="nav nav-tabs pull-right">
						
							<li id="tab" class="active"><a href="#tab_1-1" data-toggle="tab">Project</a></li>
							<li id="tab"><a href="#tab_2-2" data-toggle="tab">Invoice</a></li>
							<li id="tab"><a href="#tab_3-2" data-toggle="tab">Order</a></li>
							<li id="tab"><a href="#tab_4_4" data-toggle="tab">All Remarks</a></li>
						  
						</ul>
						<div class="tab-content" id = "tab-content">
							<div class="tab-pane active" id="tab_1-1" >
								<div class="box" id = "box" style="height:108 !important">
									<h3 style="text-align:center;">Project Remarks</h3>
									<div class="folder-index">
										<h1><?= Html::encode($this->title) ?></h1>
										<div class=" " >
											<table id="project_table" class="table table-bordered table-striped">
												<thead>
													<tr>
														
													</tr>
												</thead>
												<tbody>
													
													<? if (!empty($projectcomment)) { ?>
														<?php foreach ($projectcomment as $k=>$v) { $sn=1; ?>

														<tr>
															<td>
																<div class="  box-comments">
																	<div class="box-comment">
																	
																		<div class="comment-text">
																			<span class="username">
																			<i class="fa fa-comments"></i>
																			&nbsp;   <?= $this->context->convert_tycref_tag($v['text']); ?>
																				
																			<span class="text-muted pull-right"><?= $v['remark_date'];  ?></span>
																			</span>

																		</div>
																	<!-- /.comment-text -->
																	</div>
																</div>
															</td>
														</tr>
													<?php $sn++;}?>
													<? }?>

												</tbody>
												<tfoot>
													<tr>
														<th></th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
					  
							<div class="tab-pane" id="tab_2-2">
								<div class="box" id = "box">
									<h3 style="text-align:center;">Invoice Remarks</h3>
									<div class="folder-index">
										<h1><?= Html::encode($this->title) ?></h1>
										<div class=" ">

											<table id="invoice_table" class="table table-bordered table-striped">
												<thead>
													<tr>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<? if (!empty($invoicecomment)) { ?> 
														<?php foreach ($invoicecomment as $k => $v) { $sn=1; ?>

															<tr>
																<td>
																	<div class="  box-comments">
																		<div class="box-comment">
																			<div class="comment-text">
																				<span class="username">
																				<i class="fa fa-comments"></i>
																				&nbsp;   <?= $this->context->convert_tycref_tag($v['text']); ?>
																				<span class="text-muted pull-right"><?= $v['remark_date'];  ?></span>
																				</span>    
																			</div>
																		</div>
																	</div>
																</td>
															</tr>
														<?php $sn++; }?>
													<?php }?>
												</tbody>
												<tfoot>
													<tr>
														<th></th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
					  
							<div class="tab-pane" id="tab_3-2">
								<div class="box" id = "box">
									<h3 style="text-align:center;">Order Remarks</h3>
									<div class="folder-index">

										<h1><?= Html::encode($this->title) ?></h1>
										<div class="">
											<table id="order_table" class="table table-bordered table-striped">
												<thead>
													<tr>
													  <th></th>
													</tr>
												</thead>
												<tbody>
													<? if(!empty($ordercomment)){ ?>
														<?php foreach($ordercomment as $k=>$v){ $sn=1;?>
														<tr>
															<td>
																<div class="box-comments">
																	<div class="box-comment">
																		<div class="comment-text">
																			<span class="username">
																				<i class="fa fa-comments"></i>
																				&nbsp;   <?= $this->context->convert_tycref_tag($v['text']); ?>
																				<span class="text-muted pull-right"><?= $v['remark_date'];  ?></span>
																			</span><!-- /.username -->
																		</div>
																	</div>
																</div>
															</td>
														</tr>
														<?php $sn++;}?>
													<?php }?>
												</tbody>
												<tfoot>
													<tr>
														<th></th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
						
							<div class="tab-pane" id="tab_4_4">
								<div class="box" id = " " style="border:none !important;">
									<h3 style="text-align:center;">All Remarks</h3>
									<div class="folder-index">
										<h1><?= Html::encode($this->title) ?></h1>
										<div class=" " >
											<table id="all_table" class="table table-bordered table-striped">
												<thead>
													<tr>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<? if(!empty($ordercomment)){ ?>
														<?php foreach ($allremark as $k=>$v) { $sn=1; ?>
														<tr>
															<td>
																<div class="  box-comments">
																	<div class="box-comment">
																		<div class="comment-text">
																			<span class="username">
																				<i class="fa fa-comments"></i>
																				&nbsp;   <?= $this->context->convert_tycref_tag($v['text']); ?>
																				<span class="text-muted pull-right"><?= $v['remark_date'];  ?></span>
																			</span>
																		</div>
																	</div>
																</div>				   
															</td>
														</tr>
													<?php $sn++;}?>
													<?php }?>
						   
												</tbody>
												<tfoot>
													<tr>
														<th></th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="btn btn-danger" id="projectnewremark" >New Remark</div> 
	</div>
							

</section>

<? 
		Modal::begin([
			'header' =>'<h1 id="headers"></h1>',
			'id' => 'projectviewremark',
			'size' => 'modal-sm',  
		]);
?>
		<div id='projectremark'> 
			<div class="box box-danger">
				<div class="box-header">
				  	<h3 class="box-title">New Remark</h3>
				</div>
            	<div class=" ">
					<div class="form-group">
						<div class="input-group">
							<?php $form = ActiveForm::begin([
								'id' => 'newremarks',
								'options' => ['class' => 'form-horizontal'],
								'action' =>['createremark','id'=>$model->project_id,]
								]); 
							?>

							<?=   $form->field($remark, 'remark_type')->dropdownList([
										'project'=> 'Project', 
										'invoice'=> 'Invoice',
										'order'=>'Order'
									],
									['prompt'=>'Select remark type']);
							?>

							<?= $form->field($remark, 'text')->textarea(['rows' => '6']); ?>

							<?//= $form->field($remark, 'tyc_ref')->hiddenInput(['maxlength' => true, 'value'=>$model->tyc_ref])->label(false); ?>

							<?= $form->field($remark, 'project_id')->hiddenInput(['maxlength' => true,'value'=>$model->project_id])->label(false); ?>

							<? $now = date('Y-m-d h:i:s'); //there is a bug here soon to show itself! ?>
							<?= $form->field($remark, 'remark_date')->hiddenInput(['maxlength' => true,'value'=>$now])->label(false); ?>

							<div class="form-group">
								<?= Html::submitButton($remark->isNewRecord ? 'Create' : 'Update', ['class' => $remark->isNewRecord ? 'btn btn-success' : 'btn btn-danger']) ?>
							</div>

							<?php ActiveForm::end(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
<? Modal::end(); ?>


<?= DisplayLinkedComponents::widget(['subComponents' => $subComponents]); ?>
<?php /*** THE FOLLOWING JS SHOULD ALSO BE FORMATTED PROPERLY TO MAKE IT READABLE! ***/
$remarks= <<<JS

$('#newremarks').on('beforeSubmit', function (e) {
	$('#newremarkformbuttonText').hide();
 	$('#newremarkloader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#newremarkloader').hide();
   if(result.sent=='sent'){
	   
	   $(document).find('#newremarkloader1').html(result).show();
	    $(document).find('#flash').html(result.message).show();
	   
	 	$(document).find('.modal').modal('hide');
	   $(document).find('#newremarkform').trigger('reset');
	   $("html, body").delay(200).animate({
        scrollTop: $('#flash').offset().top
		
    }, 2000);
		
	   
	 	//$.pjax.reload({container:"#supplier_reload",async: false}); 
	  
    }else{
    $(document).find('#newremarkloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
		$(document).find('#newremarkloader').hide();
		$(document).find('#newremarkloader1').hide();
		$(document).find('#newremarkbuttonText').show();
		$(document).find('#flash').fadeOut();
	}, 5000);
    return false;
    
    
    
});

	
JS;
 
$this->registerJs($remarks);
?>






