<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\date\DatePicker;
use app\boffins_vendor\components\controllers\ComponentLinkWidget;


/* @var $this yii\web\View tenderjonal */
/* @var $model app\models\Receivedpurchaseorder */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
	.form_input{
		width: 100% !important;
	}
</style>

<div class="receivedpurchaseorder-form">

	
	
    <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'attributes' => $model->attributes(),'enableAjaxValidation' => false,'id' => 'rpoforms']); ?>
	
	<?= ComponentLinkWidget::widget(['model'=>$model,'form'=>$form]); ?>
	
	
    <?= $form->field($model, 'receivedpurchaseorder_reference')->textInput(['maxlength' => true, 'class' => 'form_input']) ?>

    <?= $form->field($model, 'receivedpurchaseorder_type')->dropDownList([ 'Letter of Credit' => 'Letter of Credit', 'Local Purchase Order' => 'Local Purchase Order', 'Internal Order' => 'Internal Order', ], [ 'prompt' => 'RPOTypesDropdownPrompt', 'options' => ['class' => 'form_input'] ]) ?>

    <?= $form->field($model, 'description')->textarea([ 'maxlength' => true, 'class' => 'form_input']) ?>

    <?= $form->field($model, 'client_id')->dropDownList($clients, [ 'prompt'=> $language['clientsDropdownPrompt'], 'options' => ['class' => 'form_input'] ]) ?>
	
	<?= $form->field($model, 'order_value')->widget(MaskedInput::classname(), $value_settings)  ?>

    <?= $form->field($model, 'order_currency')->dropDownList($currencies, [ 'prompt'=> $language['currenciesDropdownPrompt'], 'options' => ['class' => 'form_input'] ]) ?>

    <?= $form->field($model, 'supplier_id')->dropDownList($suppliers, [ 'prompt'=> $language['suppliersDropdownPrompt'], 'options' => ['class' => 'form_input'] ]) ?>

    <?= $form->field($model, 'issue_date')->widget(DatePicker::classname(), [
							'options' => ['placeholder' => 'Select issue date ...','id' => 'issue_date'],
							 'pluginOptions' => [
								 'format' => 'dd/mm/yyyy',
								 'todayHighlight' => true
									],
								
								])  ?>

    <?= $form->field($model, 'receivedpurchaseorder_duedate')->widget(DatePicker::classname(), [
							'options' => ['placeholder' => 'received purchase order duedate ...','id' => 'receivedpurchaseorder_duedate'],
							 'pluginOptions' => [
								 'format' => 'dd/mm/yyyy',
								 'todayHighlight' => true
									],
								
								]) ?>

    <?= $form->field($model, 'final_delivery_date')->widget(DatePicker::classname(), [
							'options' => ['placeholder' => 'received purchase order duedate ...','id' => 'final_delivery_date'],
							 'pluginOptions' => [
								 'format' => 'dd/mm/yyyy',
								 'todayHighlight' => true
									],
								
								]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<span id="rpobuttonText">Create</span> <img id="rpoloader" src="images/45.gif" " /> <span id="rpoloader1"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic' : 'btn btn-basic','id'=>'rposubmit_id']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$rpoform = <<<JS

$('#rpoforms').on('beforeSubmit', function (e) {
	$('#rpoformbuttonText').hide();
 	$('#rpoloader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#rpoloader').hide();
   if(result.sent=='sent'){
	   
	   $(document).find('#rpoloader1').html(result).show();
	    $(document).find('#flash').html(result.message).show();
	   
	 	$(document).find('.modal').modal('hide');
	   $(document).find('#rpoform').trigger('reset');
	   $("html, body").delay(200).animate({
        scrollTop: $('#flash').offset().top
		
    }, 2000);
		
	   
	 	$.pjax.reload({container:"#supplier_reload",async: false
}); 
	  
    }else{
    $(document).find('#rpoloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
		$(document).find('#rpoloader').hide();
		$(document).find('#rpoloader1').hide();
		$(document).find('#rpobuttonText').show();
	}, 5000);
    return false;
    
    
    
});
JS;
 
$this->registerJs($rpoform);
?>


