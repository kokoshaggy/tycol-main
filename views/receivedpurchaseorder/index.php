 <style>
    .floatright{
        float: right;
    }
    .foldernote{
        line-height: 24px;
        text-underline-position: alphabetic;
        margin-top: 10px;
        
    }
	 #rpoloader{
		 display:none;
	 }
	 
	 #flash{
		 display: none;
	 }
	 
</style>
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use app\models\Corporation;
use yii\bootstrap\Alert;
use app\models\Supplier;
use app\boffins_vendor\components\controllers\Menu;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Folder */
$this->title = 'Tycol | Received Purchase Order';
?>

<?php $this->beginBlock('folderview'); ?>
Received Purchase Order

   
<?php $this->endBlock(); ?>

<?php $this->beginBlock('folderSidebar'); ?>
	<?= Menu::widget(); ?>

<?php $this->endBlock(); ?>

<section class="content">
    
      <div class="row">
		   <div class="col-lg-12">
			  <?= Alert::widget([
				   'options' => ['class' => 'alert-info','id'=>'flash'],
				   'body' => Yii::$app->session->getFlash('created_successfully'),
					 ]);?>
		  </div>
		  <? $rpoId=''; if($model->find()->count() <= 0){$rpoId=0;}else{$rpoId=!empty($findOneRpo->receivedpurchaseorder_reference)?$findOneRpo->receivedpurchaseorder_reference:0; } ?>
		  <div class="col-xs-7" id="rpoListView">
			<img class="loadergif" src="images/loader.gif"  />
		  </div>
		  
		  <div class="col-xs-5" >
			  
			  <div id="rpoviewcontainer">
				  <div id="rpoView">
				  	<img class="loadergif" src="images/loader.gif"  />
				  </div>
			  </div>
		  </div>
    </div>
</section>

<? 
		Modal::begin([
			'header' =>'<h1 id="headers"></h1>',
			'id' => 'dashboard',
			'size' => 'modal-md',  
		]);
?>
<div id="formcontent"></div>
<?
	Modal::end();
?>



<?php 
	$urlListView = Url::to(['receivedpurchaseorder/rpolistview']);
	$urlView = Url::to(['receivedpurchaseorder/rpoview','id'=>$rpoId]);
	
$js3 = <<<JS


$("#rpoListView").load('$urlListView');
$("#rpoView").load('$urlView');

$(document).on('click','.rpourl',function(){
var parent = $(this).parent();
$('.rpourltr').removeClass('activelist');
parent.addClass('activelist');
$('#rpoView').html('<img class="loadergif" src="images/loader.gif"  />');
var url = parent.data('url');
$("#rpoView").load(url,[],function(){
$('#rpoloader').fadeOut('fast');
$('#rpoviewcontainer').slideDown('slow');

});
})
$(document).on('click','#createrpo',function(){
var formUrl = $(this).data('formurl');
$('#rpoviewcreate').modal('show').find('#rpocreateform').load(formUrl);

})





JS;
 
$this->registerJs($js3);
?>
