<!--
<div class="order-index">

    <h1><?//= Html::encode($this->title) ?></h1>

    <p>
        <?//= Html::a('Create Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?/*= GridView::widget([
        'dataProvider' => $dataProvider,
		
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'order_number',
            'tyc_ref',
            'component_id',
            'supplier_id',
            'supplier_ref',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);*/ ?>
</div>
-->


 <style>
    .floatright{
        float: right;
    }
    .foldernote{
        line-height: 24px;
        text-underline-position: alphabetic;
        margin-top: 10px;
        
    }
	 .projecturl{
		 cursor: pointer;
	 }
   .box{
    border: none;
   }
    #createrpo{
    margin: 10px 10px;
   }
	 
</style>
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\bootstrap\Alert;
use yii\bootstrap\Modal;
use app\models\Corporation;
use app\models\Supplier;

/* @var $this yii\web\View */
/* @var $model app\models\Folder */
?>

      <div class="row">

      		  <div class="col-xs-12">
      			<div class="box">
                    <div class="folder-index">
                                      	         <h1><?= Html::encode($this->title) ?> 
                                    						        <div class="floatright btn btn-danger" id="createrpo" data-formurl="<?= Url::to(['create']) ?>">Create New Received Purchase Order
                                    						        </div>
                      					                 </h1>
                      					
                                  <div class="box-body ">
                      						<h3>Received Purchase Order List</h3>
                                          	<table id="rpolistviewtable" class="table table-bordered table-striped ">
                                              	<thead>
                                                  <tr>
                                                    	<th>SN</th>
                                                    	<th>RPO Reference</th>
                                                    	<th>RPO Type</th>
                                                    	
                                                    	<th>Description</th>
                                                    	<th>Client</th>
                                                    	<th>Supplier</th>
                      								<th>Action</th>
                                                    
                                                  </tr>
                                              
                      							</thead>
                                              	<tbody>
                                                  <?php $sn=1; foreach($dataProvider as $k=>$v){ ?>
                                                      <tr class="rpourltr <?if($sn === 1 && $hoverEffect === 'true'){echo 'activelist';} ?>" data-url="<?=Url::to(['receivedpurchaseorder/rpoview','id' => $v['receivedpurchaseorder_reference']]) ?>" title="Click to view rpo">
                      									              <td class="rpourl"><?= $sn; ?></td>
                                                      <td class="rpourl"><?= $v['receivedpurchaseorder_reference'];  ?></td>
                                                      <td class="rpourl"><?= $v['receivedpurchaseorder_type'];  ?></td>
                      									              <td class="rpourl"><?= $v['description'];  ?></td>
                                                      <td class="rpourl"><?= Corporation::getclientname($v['client_id']);  ?></td>
                      									              <td class="rpourl"><?= Supplier::findOne([$v['supplier_id']])->nameString;  ?></td>
                                                      <td>
                                        									 <? 
                                          										  $deletUrl=Url::to(['receivedpurchaseorder/delete','id'=>$v['receivedpurchaseorder_reference']]);
                                          										  $updateUrl=Url::to(['receivedpurchaseorder/update','id'=> $v['receivedpurchaseorder_reference']]);
                                        										?>
                                        										<p>
                                        												<?= Html::tag('i', '', ['class' => 'fa fa-pencil update', 'data-url'=>$updateUrl]); ?>
                                        												<?= Html::tag('i', '', ['class' => 'fa fa-trash delete', 'data-url'=>$deletUrl]); ?>
                                        										</p>
                      								                </td>
                                                       
                                                      </tr>
                                                    <?php $sn++; }?>
                                         
                                        
                      							</tbody>
                                    <tfoot>
                      							      <tr>
                      									              <th>SN</th>
                                                    	<th>RPO Reference</th>
                                                    	<th>RPO Type</th>
                                                    	
                                                    	<th>Description</th>
                                                    	<th>Client</th>
                                                    	<th>Supplier</th>
                      								                <th>Action</th>
                      								    </tr>
                                    </tfoot>
                                      
                                          
                      						</table>
                      				  	</div>
          		
				</div>
			  
			</div>
		  </div>
    </div>
<? 
		Modal::begin([
			'header' =>'<h1 id="headers"></h1>',
			'id' => 'rpoviewcreate',
			'size' => 'modal-md',  
		]);
		echo "<div id='rpocreateform'> </div>";
		Modal::end();
	?>


<script>
$("#rpolistviewtable").DataTable({
        "aaSorting": [],
		"responsive": "true",
		"pagingType": "simple",
    });
</script>




