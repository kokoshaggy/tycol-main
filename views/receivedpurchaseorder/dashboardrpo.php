<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Receivedpurchaseorder */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
	.button-basic{
		background: #ccc;
	}
	#receivedpurchaseorderloader,#receivedpurchaseorderloader1{
display: none;
}

</style>

<div class="receivedpurchaseorder-form">

    <?php $form = ActiveForm::begin(['action'=>['/receivedpurchaseorder/create'],'enableClientValidation' => true, 'attributes' => $model->attributes(),'enableAjaxValidation' => false,'id' => 'receivedpurchaseorderdashboardform']); ?>
	
	<?= $form->field($model, 'foldersList')->dropdownList(ArrayHelper::map($folder,'tyc_ref', 'tyc_ref'), [ 'prompt'=> 'Select folder ', 'options' => ['class' => 'form_input'] ]) ?>

    <?= $form->field($model, 'receivedpurchaseorder_reference')->textInput(['maxlength' => true, 'class' => 'form_input']) ?>

    <?= $form->field($model, 'receivedpurchaseorder_type')->dropDownList([ 'Letter of Credit' => 'Letter of Credit', 'Local Purchase Order' => 'Local Purchase Order', 'Internal Order' => 'Internal Order', ], [ 'prompt' => 'RPOTypesDropdownPrompt', 'options' => ['class' => 'form_input'] ]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => '6', 'maxlength' => true]) ?>

    <?= $form->field($model, 'client_id')->dropDownList(ArrayHelper::map($client, 'id', 'NameString'), [ 'prompt'=> 'select client', 'options' => ['class' => 'form_input'] ]) ?>
	
	<?= $form->field($model, 'order_value')->widget(MaskedInput::classname(), $value_settings)  ?>

    <?= $form->field($model, 'order_currency')->dropDownList(ArrayHelper::map($currencies,'id', 'currencyString'), [ 'prompt'=> 'select currency', 'options' => ['class' => 'form_input'] ]) ?>

    <?= $form->field($model, 'supplier_id')->dropDownList(ArrayHelper::map($suppliers, 'id', 'nameString'), [ 'prompt'=>'Select supplier', 'options' => ['class' => 'form_input'] ]) ?>

    <?= $form->field($model, 'issue_date')->widget(DatePicker::classname(), [
							'options' => ['placeholder' => 'Select issue date ...','id' => 'issue_date'],
							 'pluginOptions' => [
								 'format' => 'dd/mm/yyyy',
								 'todayHighlight' => true
									],
								
								])  ?>

    <?= $form->field($model, 'receivedpurchaseorder_duedate')->widget(DatePicker::classname(), [
							'options' => ['placeholder' => 'Select due date ...','id' => 'receivedpurchaseorder_duedate'],
							 'pluginOptions' => [
								 'format' => 'dd/mm/yyyy',
								 'todayHighlight' => true
									],
								
								]) ?>

    <?= $form->field($model, 'final_delivery_date')->widget(DatePicker::classname(), [
							'options' => ['placeholder' => 'Select delivery date ...','id' => 'final_delivery_date'],
							 'pluginOptions' => [
								 'format' => 'dd/mm/yyyy',
								 'todayHighlight' => true
									],
								
								]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<span id="receivedpurchaseorderbuttonText">Create</span> <img id="receivedpurchaseorderloader" src="images/45.gif" " /> <span id="receivedpurchaseorderloader1"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic' : 'btn btn-basic','id'=>'receivedpurchaseordersubmit_id']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$jsDashboardRpo = <<<JS

$('#receivedpurchaseorderdashboardform').on('beforeSubmit', function (e) {
	$('#receivedpurchaseorderbuttonText').hide();
 	$('#receivedpurchaseorderloader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#receivedpurchaseorderloader').hide();
   if(result=='sent'){
	   
	   $(document).find('#receivedpurchaseorderloader1').html(result).show();
	   
	   $(document).find('#flash').append(result).show();
	   
	 
	   $(document).find('#receivedpurchaseorderdashboardform').trigger('reset');
	   $("html, body").delay(200).animate({
        scrollTop: $('#flash').offset().top
		
    }, 2000);
	  
    
    }else{
    $(document).find('#receivedpurchaseorderloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#receivedpurchaseorderloader').hide();
	$(document).find('#receivedpurchaseorderloader1').hide();
	$(document).find('#receivedpurchaseorderbuttonText').show();
	
	}, 5000);
    return false;
    
    
    
});
JS;
 
$this->registerJs($jsDashboardRpo);
?>

