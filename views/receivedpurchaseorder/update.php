<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Receivedpurchaseorder */

$this->title = 'Update Received Purchase Order: ' . $model->receivedpurchaseorder_reference;
$this->params['breadcrumbs'][] = ['label' => 'Receivedpurchaseorders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->receivedpurchaseorder_reference, 'url' => ['view', 'id' => $model->receivedpurchaseorder_reference]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="receivedpurchaseorder-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'allFolders' => $allFolders,
		'suppliers' => $suppliers,
		'clients' => $clients,
		'currencies' => $currencies,
		'value_settings' => $value_settings,
		'language' => $language,
		'folderComponent' => $folderComponent,
    ]);
	
	?>

</div>
