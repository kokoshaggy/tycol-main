<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\boffins_vendor\components\controllers\DisplayLinkedComponents;
/* @var $this yii\web\View */
/* @var $model app\models\Receivedpurchaseorder */

$this->title = $model->receivedpurchaseorder_type . ': ' . $model->receivedpurchaseorder_reference;
$this->params['breadcrumbs'][] = ['label' => 'Receivedpurchaseorders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
	
     #rpocontent{
        background: #fff;
        margin-top: 22px;
        min-height: 600px;
    }
</style>
<div class="receivedpurchaseorder-view">
	<div class="box" id="rpocontent">
<div class="box-body">
    <h3><?= Html::encode($this->title) ?></h3>

    <!--<p>
       
		<?= Html::a('Undo Delete', ['undodelete', 'id' => $model->receivedpurchaseorder_reference], ['class' => 'btn btn-danger']) ?>
    </p>-->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
			[
				'label' => 'Folders',
				'value' => $model->owner->foldersString,

			],
	
	
            'receivedpurchaseorder_reference',
            'receivedpurchaseorder_type',
            'description',
            'clientNameString',
            'orderValueString',
            'supplierNameString',
            'issue_date',
            'receivedpurchaseorder_duedate',
            'final_delivery_date',
            'last_updated',
        ],
    ]) ?>
	
	
<?= DisplayLinkedComponents::widget(['subComponents' => $subComponents]); ?>
	</div>
		</div>

</div>
