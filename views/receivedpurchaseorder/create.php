<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Receivedpurchaseorder */

$this->title = 'Create RPO (Received Purchase Order: L/C or LPO) ';
$this->params['breadcrumbs'][] = ['label' => 'Receivedpurchaseorders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


    <h1><?= Html::encode($this->title) ?></h1>
    <h1><?= Html::encode($id); ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'allFolders' => $allFolders,
		'suppliers' => $suppliers,
		'clients' => $clients,
		'currencies' => $currencies,
		'value_settings' => $value_settings,
		'language' => $language,
		'folderComponent' => $folderComponent,
    ]) ?>


