<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\boffins_vendor\components\controllers\DisplayLinkedComponents;

/* @var $this yii\web\View */
/* @var $model app\models\Invoice */
//$this->title = $model->order_number;
$this->params['breadcrumbs'][] = ['label' => 'Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #order {
        background: #fff;
        min-height: 600px;
        margin-top: 40px;
    }
    .box {
        border: none;
    }
    .box-body h3 {
    	font-family: candara;
    }
</style>
<div class="order">
    <div class="box" id="order">
		<div class="box-body">
			<h3><?php echo Yii::t('order', 'order_number') . $model->order_number;?></h3>
		    <?= DetailView::widget([
		        'model' => $model,
				
		        'attributes' => [
		            
		            [
					 'label' => 'Order Number',
		            'value' => $model->owner->order_number,
					],
					
					[
					 'label' => 'Folder',
		            'value' => $model->owner->folder_id,
					],
			
			
					[
					 'label' => 'Supplier',
		            'value' => $model->owner->supplier->nameString,
					],
			
					[
					 'label' => 'Supplier Refrence',
		            'value' => $model->owner->supplier_ref,
					],
			
					[
					 'label' => 'Amount',
		            'value' => $model->owner->currencyAndAmount,
					],
			
					[
					 'label' => 'Issue Date',
		            'value' => $model->owner->issue_date,
					],
			
					[
					 'label' => 'Supplier Completion Date',
		            'value' => $model->owner->supplier_completion_date,
					],
			
					[
					 'label' => 'Order Status',
		            'value' => $model->owner->statustype->status_title,
					],
		        ],
		    ]) ?>
			
<?= DisplayLinkedComponents::widget(['subComponents' => $subComponents]); ?>
		</div>
	</div>
</div>
