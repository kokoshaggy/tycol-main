<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Folder;
use app\models\Supplier;
use app\models\Currency;
use app\models\StatusType;
use app\boffins_vendor\components\controllers\ComponentLinkWidget;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */

$value_settings = [
			'name' => 'masked-input',
			'clientOptions' => [
				'alias' => 'decimal',
				'digits' => 2,
				'digitsOptional' => false,
				'radixPoint' => '.',
				'groupSeparator' => ',',
				'autoGroup' => true,
				'removeMaskOnSubmit' => true,
			],
		];
?>
<style>
    .form_input {
        resize:none;
        width:100%;
    }
	
	#orderloader {
		display: none;
	}
</style>

<div class="order-form">

    <?php $form = ActiveForm::begin(['enableClientValidation' => true,'id'=>'orderform', 'attributes' => $model->attributes(), 'enableAjaxValidation' => true, 'validationUrl' => ['order/ajax-validate-form', 'id' => $model->isNewRecord ? '' : $model->order_number] ]); ?>
	
	<?= ComponentLinkWidget::widget(['model'=>$model,'form'=>$form]); ?> 

    <?= $form->field($model, 'order_number')->textInput(['autofocus' => true]) ?>

	<?= $form->field($model, 'supplier_id')->dropdownList(ArrayHelper::map(Supplier::find()->all(),'id', 'name')) ?>

    <?= $form->field($model, 'supplier_ref')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_value')->widget(MaskedInput::classname(), $value_settings)  ?>

    <?= $form->field($model, 'order_currency')->dropdownList(ArrayHelper::map(Currency::find()->all(),'id', 'currencyString')) ?>

    <?= $form->field($model, 'issue_date')->widget(DatePicker::classname(), [
							'options' => ['placeholder' => 'Select issue date ...'],
							 'pluginOptions' => [
								 'format' => 'dd/mm/yyyy',
								 'todayHighlight' => true
									]
								]) 
		?>

	<?= $form->field($model, 'supplier_completion_date')->widget(DatePicker::classname(), [
							'options' => ['placeholder' => 'Select supplier completion date ...'],
							 'pluginOptions' => [
								 'format' => 'dd/mm/yyyy',
								 'todayHighlight' => true
									]
								]) 
		?>

    <?= $form->field($model, 'order_status')->dropdownList(ArrayHelper::map(StatusType::find()->where(['status_group'=>'order'])->all(),'id', 'status_title')) ?>

    <?= $form->field($model, 'order_file')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<span id="orderbuttonText">Create</span> <img id="orderloader" src="images/45.gif" /> ' : '<span id="orderbuttonText">Update<span> <img id="orderloader" src="images/45.gif"  /> ', ['class' => 'btn btn-basic', 'id' => $model->isNewRecord ? '' : 'ordersubmit_id' ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$orderform = <<<JS

$('#orderform').on('beforeSubmit', function (e) {
	$('#orderbuttonText').hide();
 	$('#orderloader').show();
    var \$form = $(this);
	var data = \$form.serialize();
	data['final'] = true;
    $.post( \$form.attr('action'), data )
	
		.always(function(result) {
			$(document).find('#orderloader').hide();
			if (result.success == true) {
				$(document).find('#orderloader1').html(result).show(); //#orderloader1??? what is this span for???
				$(document).find('#flash').html(result.message).show();
				$(document).find('.modal').modal('hide');
				$(document).find('#orderform').trigger('reset');
				$("html, body").delay(200).animate({
					scrollTop: $('#flash').offset().top
				}, 1000);   
				$.pjax.reload({container:"#supplier_reload", async: false}); 
			} else {
				$(document).find('#orderloader1').html(result).show();
			}
		})
		
		.fail(function() {
			console.log('Server Error');
		});
	
	setTimeout(function() { 
		$(document).find('#orderloader').hide();
		$(document).find('#orderloader1').hide();
		$(document).find('#orderbuttonText').show();
	}, 5000);
	
    return false; //Stopping the submit. 
});
JS;
 
$this->registerJs($orderform);
?>


