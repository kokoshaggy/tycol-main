<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = 'Create Order';
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-create">
	<?php if(Yii::$app->controller->action->id == 'index' and Yii::$app->controller->id == 'site'){ ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php } ?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
