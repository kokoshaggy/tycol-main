 <style>
    .floatright{
        float: right;
    }
    .foldernote{
        line-height: 24px;
        text-underline-position: alphabetic;
        margin-top: 10px;
        
    }
	 #orderloading{
		 display:none;
	 }
	 #flash{
		 display: none;
	 }
</style>
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use app\models\Corporation;
use yii\bootstrap\Alert;
use app\models\Supplier;
use app\boffins_vendor\components\controllers\Menu;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Folder */
$this->title = 'Tycol | Order';
$this->params['breadcrumbs'][] = ['label' => ' Folders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('folderview'); ?>
Order
<small>Select Order</small>

<?php $this->endBlock(); ?>

<?php $this->beginBlock('folderSidebar'); ?>
	<?= Menu::widget(); ?>

<?php $this->endBlock(); ?>

<section class="content">
    <!-- flash messages start -->
    <? if (Yii::$app->session->getFlash('created_successfully') !==NULL): ?>
		 <?= Alert::widget([
	   'options' => ['class' => 'alert-info'],
	   'body' => Yii::$app->session->getFlash('created_successfully'),
		 ]);?>
    <? endif ?>
    
    <? if (Yii::$app->session->getFlash('created_folder') !==NULL): ?>
		 <?= Alert::widget([
	   'options' => ['class' => 'alert-info'],
	   'body' => Yii::$app->session->getFlash('created_folder'),
		 ]);?>
    <? endif ?>
    
    <? if (Yii::$app->session->getFlash('instruction') !==NULL): ?>
		 <?= Alert::widget([
	   'options' => ['class' => 'alert-info'],
	   'body' => Yii::$app->session->getFlash('instruction'),
		 ]);?>
    <? endif ?>
    <!-- flash messages end -->
      <div class="row">
		  <div class="col-lg-12">
			  <?= Alert::widget([
				   'options' => ['class' => 'alert-info','id'=>'flash'],
				   'body' => Yii::$app->session->getFlash('created_successfully'),
					 ]);?>
		  </div>
		  
		  <? $orderId=''; if($model->find()->count() <= 0){$orderId=0;}else{$orderId=!empty($findOneOrder->order_number)?$findOneOrder->order_number:0; } ?>
		  <div class="col-xs-7" id="orderListView">
			<img class="loadergif" src="images/loader.gif"  />
		  </div>
		  
		   <div class="col-xs-5" >
			  
			  <div id="orderviewcontainer">
				  <div id="orderview">
				  <img class="loadergif" src="images/loader.gif"  />
				  </div>
			  </div>
		  </div>
    </div>
</section>
<? 
		Modal::begin([
			'header' =>'<h1 id="headers"></h1>',
			'id' => 'dashboard',
			'size' => 'modal-md',  
		]);
?>
<div id="formcontent"></div>
<?
	Modal::end();
?>



<?php 
	$urlListView = Url::to(['order/orderlistview']);
	$urlView = Url::to(['order/orderview','id'=>$orderId]);
	
$js3 = <<<JS


$("#orderListView").load('$urlListView');
$("#orderview").load('$urlView');

$(document).on('click','.orderurl',function(){
	var parent = $(this).parent();

	$('.orderurltr').removeClass('activelist');
	parent.addClass('activelist');


	$('#orderview').html('<img class="loadergif" src="images/loader.gif"  />');
	var url = parent.data('url');
	
	$("#orderview").load(url,[],function(){
		$('#orderloader').slideUp('slow');
		$('#orderviewcontainer').slideDown('fast');

	});
})

$(document).on('click','#createorder',function(){
	var formUrl = $(this).data('formurl');
	$('#orderviewcreate').modal('show').find('#ordercreateform').load(formUrl);

})





JS;
 
$this->registerJs($js3);
?>

