<?php
use yii\helpers\Url;
use app\boffins_vendor\components\controllers\ComponentListViewWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Payment */
$attributes = [
            'order_number',
            'supplier_ref',
			'Supplier Name'=>'suppliername',
            

            
        ];
$action = [
	'update'=> Url::to(['order/update']),
	'delete'=> Url::to(['order/delete']),
];
?>

<?= ComponentListViewWidget::widget([
									'model'=>$model,
									'content'=>$dataProvider,
									'attributes'=>$attributes,
									'hoverEffect'=>$hoverEffect,
									'action'=>$action,
									
								]); ?>





