<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Tmuser */
/* @var $form yii\widgets\ActiveForm */
?>
<?php Pjax::begin(['id' => 'clientformpjax']) ?>
<div class="corporation-form">
	<?  $formId = $hiddenValue; ?>

    <?php $form = ActiveForm::begin(['action' =>['corporation/create'],'enableAjaxValidation' => false,'id' => 'dashboardclient']); ?>

     <?//=$form->field($model, 'person_id')->dropdownList(ArrayHelper::map($person1, 'id',function($test){return $test['first_name'].' '.$test['surname']; }));?>

	<div  class="indexFormContainer">
		<section class="indexFormTitle"> Basic info </section>
    <?= $form->field($corporation, 'name')->textInput() ?>

    <?= $form->field($corporation, 'short_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($corporation, 'notes')->textInput(['maxlength' => true]) ?>
    </div>

 	<div class="indexFormContainer">
		<section class="indexFormTitle"> Contact details </section>

    <?= $form->field($telephoneModel, 'telephone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($emailModel, 'address')->textInput(['maxlength' => true]) ?>
    <?= $form->field($addressModel, 'address_line')->textInput(['maxlength' => true]) ?>

    <?= $form->field($addressModel, 'state')->textInput(['maxlength' => true]) ?>

    <?= $form->field($addressModel, 'country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($addressModel, 'code')->textInput(['maxlength' => true]) ?>
    
	</div>
    
	
	
	<div class="form-group">
		<p>
			<br>
			<?= Html::hiddenInput('corporationhidden[hidden]', $hiddenValue) ?>
        	<?= Html::submitButton($corporation->isNewRecord ? '<span id="copbuttonText">Create</span> <img id="coploader" src="images/45.gif" " /> <span id="coploader1"><span>' : 'Update', ['class' => $corporation->isNewRecord ? 'btn btn-danger' : 'btn btn-danger','id'=>'corporation_id']) ?>
		</p>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php Pjax::end() ?>

<?php 
$getClient = Url::to(['/client/newclient']);
$corporation = <<<JS

$('#dashboardclient').on('beforeSubmit', function (e) {
	$('#copbuttonText').hide();
 	$('#coploader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#coploader').hide();
   if(result=='sent'){
	   
	   
	   //$.pjax.reload({container:"#client_reload"}); 
	   
	   
	   
	   if($(document).find('#clientsupplier').length){
	  
	  	$.get(function(data){
        	$(document).find('.clientRefesh select').prepend('<option selected value="'+data.clientId+'" >'+data.corpName+'</option>');
    	});
	  
	  	
	  	$(document).find('#clientsupplier').modal('toggle');
		
	  
	  } else{
	  
	  	$(document).find('#coploader1').html(result).show();
	   
	   $(document).find('#flash').append(result).show();
	   
	 
	   $(document).find('#dashboardclient').trigger('reset');
	   $("html, body").delay(200).animate({
        scrollTop: $('#flash').offset().top
		
    }, 2000);
		
	   $('#dashboard').modal('toggle');
	  
	  }
	  
	$(document).find('#clientloader1').html('Sent').show();
    
    }else{
	//$.pjax.reload({container:"#client_reload"}); 
    $(document).find('#coploader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
		$(document).find('#coploader').hide();
		$(document).find('#coploader1').hide();
		$(document).find('#copbuttonText').show();
	}, 5000);
    return false;
    
    
    
});
JS;
 
$this->registerJs($corporation);
?>

