


<div id='clientcontent' class='hideall'>
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs pull-right">
			<li class="active"><a href="#useexisting" data-toggle="tab">Use Existing</a></li>
			<li><a href="#usenew" data-toggle="tab">Create new client</a></li>
		</ul>
		<div class="tab-content">
			<!-- /.tab-pane -->
			<div class="tab-pane active" id="useexisting">
				<div class="box">
					<h1 style="text-align:center;">Use Existing </h1>
					<div class="folder-index">
						<h1></h1>
						<div class="box-body">
							<?= $this->render('/site/client_form',
												  ['newClient' =>$newClient,
												   'clientModel'=>$clientModel,
												   ]) 
								?>
						</div>
					</div>
				</div>
			</div>
			<!-- /.tab-pane -->
			<div class="tab-pane" id="usenew">
				<div class="box">
					<h1 style="text-align:center;"></h1>
					<div class="folder-index">
						<h1>Create new client</h1>
						<div class="box-body">
							 <?=  $this->render('../client/dashboardcreateclient',
								  ['corporation' => $corporationModel,
								   //'action' => $action,
								   'personModel' => $personModel,
								   'emailModel' => $emailModel,
								   'telephoneModel' => $telephoneModel,
								   'addressModel' => $addressModel,
								   'hiddenValue' => 'client',
								   'actionid'=>'client',]) ;
							   ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.tab-content -->
	</div>

</div>

