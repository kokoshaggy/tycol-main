<?
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="folder-index">

										
										
										<div class="box-body">

											<table id="client_table" class="table table-bordered table-striped">
												
												<thead>
													<tr>
														<th>Name</th>
														<th>Short name</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
												<?php foreach($displayAllClient as $key => $client){ $corporation=$corporationModel->findOne($client['corporation_id']); ?>
												
													<tr>
													
														<td>
															<?=$corporation->name; ?>
														</td>
														<td>
															<?=$corporation->short_name; ?>
														</td>
														<td>
															<? $deletUrl=Url::to(['client/delete','id'=>$client->id]);
															$updateUrl=Url::to(['corporation/update','id'=> $corporation->id]);
															?>
															<p>
																<?= Html::tag('i', '', ['class' => 'fa fa-pencil update', 'data-url'=>$updateUrl]); ?>
																<?= Html::tag('i', '', ['class' => 'fa fa-trash delete', 'data-url'=>$deletUrl]); ?>
															</p>
														</td>
													</tr>
												
												<? } ?>
												</tbody>
												<tfoot>
													<tr>
														<th></th>
													</tr>
												</tfoot>

											</table>
										</div>
              
									</div>






	

<?
$strin = '<span id="newclient" class="btn btn-black"><span class = "main">Create</span><span class = "hide" style = "display:none">Create Clients</span></span>';
$secon =  "$('#client_table_length').html('".$strin."')";
$this->registerJs(
    "$('document').ready(function(){ 
	
	$('#client_table').DataTable({
        'aaSorting': [],
		'pagingType': 'simple',
		
    });
	
	".$secon."
	
	});"
	
	
);
?>



 








