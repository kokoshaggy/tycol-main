<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tmclient */

$this->title = 'Create client';
$this->params['breadcrumbs'][] = ['label' => 'clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
