<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Tmuser */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
	#suppliloader,#suppliloader1{
		display: none;
	}
	
</style>
<?php Pjax::begin(['id' => 'supplierformpjax']) ?>
<div class="corporation-form">
	<?  $formId = $hiddenValue; ?>
	<?php $form = ActiveForm::begin(['action' =>['corporation/create'],'enableClientValidation' => true, 'attributes' => $corporation1->attributes(),'enableAjaxValidation' => false,'id' => 'dashboardsupplier']); ?>
	

     <?//=$form->field($model, 'person_id')->dropdownList(ArrayHelper::map($person1, 'id',function($test){return $test['first_name'].' '.$test['surname']; }));?>

	<div  class="indexFormContainer">
		<section class="indexFormTitle"> Basic info </section>
    <?= $form->field($corporation1, 'name')->textInput() ?>

    <?= $form->field($corporation1, 'short_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($corporation1, 'notes')->textInput(['maxlength' => true]) ?>
    </div>

 	<div class="indexFormContainer">
		<section class="indexFormTitle"> Contact details </section>

    <?= $form->field($telephoneModel, 'telephone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($emailModel, 'address')->textInput(['maxlength' => true]) ?>
    <?= $form->field($addressModel, 'address_line')->textInput(['maxlength' => true]) ?>

    <?= $form->field($addressModel, 'state')->textInput(['maxlength' => true]) ?>

    <?= $form->field($addressModel, 'country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($addressModel, 'code')->textInput(['maxlength' => true]) ?>
    
	</div>
    
	
	
	<div class="form-group">
		<p>
			<br>
			<?= Html::hiddenInput('corporationhidden[hidden]', $hiddenValue) ?>
        	<?= Html::submitButton($corporation1->isNewRecord ? '<span id="supplibuttonText">Create</span> <img id="suppliloader" src="images/45.gif"  /> <span id="suppliloader1"><span>' : 'Update', ['class' => $corporation1->isNewRecord ? 'btn btn-danger' : 'btn btn-danger','id'=>'supsupplier_id']) ?>
		</p>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php Pjax::end() ?>

<?php 
$getSupplier = Url::to(['/supplier/newsupplier']);
$supplierDashboardFrom = <<<JS

$('#dashboardsupplier').on('beforeSubmit', function (e) {
	$('#supplibuttonText').hide();
 	$('#suppliloader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#suppliloader').hide();
   if(result=='sent'){
   
   
   
   
   if($(document).find('#clientsupplier').length){
	  
	  	$.get("$getSupplier", function(data){
        	$(document).find('.supplierRefesh select').prepend('<option selected value="'+data.clientId+'" >'+data.corpName+'</option>');
    	});
	  
	  	
	  	$(document).find('#clientsupplier').modal('toggle');
		
	  
	  } else{
	  
	  		   $(document).find('#suppliloader1').html(result).show();
	    $(document).find('#flash').append(result).show();
	   
	 
	   $(document).find('#dashboardsupplier').trigger('reset');
	   $("html, body").delay(200).animate({
        scrollTop: $('#flash').offset().top
		
    }, 2000);
		
	   $('#dashboard').modal('toggle');
	 	$.pjax.reload({container:"#supplier_reload",async: false
});
	  }
	   
	    
	  
    }else{
    $(document).find('#suppliloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
		$(document).find('#suppliloader').hide();
		$(document).find('#suppliloader1').hide();
		$(document).find('#supplibuttonText').show();
	}, 5000);
    return false;
    
    
    
});
JS;
 
$this->registerJs($supplierDashboardFrom);
?>


