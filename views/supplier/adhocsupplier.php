<div id='suppliercontent' class='hideall'>
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs pull-right">
			<li class="active"><a href="#useexistingsup" data-toggle="tab">Use Existing</a></li>
			<li><a href="#usenewsup" data-toggle="tab">Create new supplier</a></li>
		  
		  
		</ul>
		<div class="tab-content">
			<!-- /.tab-pane -->
			<div class="tab-pane active" id="useexistingsup">
				<div class="box">
					<h1 style="text-align:center;">Use Existing </h1>
					<div class="folder-index">
						<h1></h1>
						<div class="box-body">
						   <?= $this->render('/site/supplier_form',
												  ['newSupplier' =>$newSupplier,
												   'supplierModel'=>$supplierModel,
												   ]) 
								?>
						</div>
					</div>
				</div>
			</div>
			<!-- /.tab-pane -->
			<div class="tab-pane" id="usenewsup">
				<div class="box">
					<h1 style="text-align:center;"></h1>
					<div class="folder-index">
						<div class="box-body">
							<h1>Create new supplier</h1>
							<?= $this->render('../supplier/dashboardcreatesupplier',[
							  'corporation1' => $corporationModel,
							  //'action' => $action,
							  'personModel' => $personModel,
							  'emailModel' => $emailModel,
							  'telephoneModel' => $telephoneModel,
							  'addressModel' => $addressModel,
							  'hiddenValue' => 'supplier',
							  'actionid'=>'supplier',]) 
								?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.tab-content -->
	</div>
</div>
