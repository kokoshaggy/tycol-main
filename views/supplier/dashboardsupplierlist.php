<?
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="folder-index">

										
										
										<div class="box-body">

											<table id="supplier_table" class="table table-bordered table-striped">
												
												<thead>
													<tr>
														<th>Name</th>
														<th>Short Name</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
												<?php foreach($displayAllSupplier as $key => $supplier){ $corporation=$corporationModel->findOne($supplier['corporation_id']); ?>
												
													<tr>
													
													<td>
														<?=$corporation->name; ?>
									
													</td>
													<td>
														<?=$corporation->short_name; ?>
									
													</td>
													<td>
														<? $deletUrl=Url::to(['supplier/delete','id'=>$supplier->id]);
														$updateUrl=Url::to(['corporation/update','id'=> $corporation->id]);
														?>
														<p>
															<?= Html::tag('i', '', ['class' => 'fa fa-pencil update', 'data-url'=>$updateUrl]); ?>
															<?= Html::tag('i', '', ['class' => 'fa fa-trash delete', 'data-url'=>$deletUrl]); ?>
														</p>
									
													</td>
													</tr>
												
												<? } ?>
												</tbody>
												<tfoot>
													<tr>
														<th></th>
													</tr>
												</tfoot>

											</table>
										</div>
              
									</div>

<?
$string = '<span id="newsupplier" class="btn btn-black"><span class = "main">Create</span><span class = "hide" style = "display:none">Create suppliers</span></span>';
$javascriptSupplierButton =  "$('#supplier_table_length').html('".$string."')";
$this->registerJs(
    "$('document').ready(function(){ 
	
	$('#supplier_table').DataTable({
        'aaSorting': [],
		'pagingType': 'simple',
		
    });
	
	".$javascriptSupplierButton."
	
	});"
	
	
);
?>