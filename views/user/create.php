<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $userForm app\models\UserDb */

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_userform', [
        'userForm' => $userForm,
		'action' => $action,
    ]) ?>

</div>
