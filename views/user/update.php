<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $userForm app\models\UserDb */

$this->title = 'Update user: ' . $userForm->username;
$this->params['breadcrumbs'][] = ['label' => 'users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $userForm->username, 'url' => ['view', 'username' => $userForm->username]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_userform', [
        'userForm' => $userForm,
		'action' => $action,
    ]) ?>

</div>
