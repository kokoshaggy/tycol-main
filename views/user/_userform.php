<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use app\models\Role;



/* @var $this yii\web\View */
/* @var $model app\models\Tmuser */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
#userloader,#userloader1{
display: none;
}
</style>
<div class="user-form">


    <?php $form = ActiveForm::begin(['action' => $action,'enableClientValidation' => true, 'attributes' => $userForm->attributes(),'enableAjaxValidation' => false,'id' => 'userform']); ?>

     <?//=$form->field($model, 'person_id')->dropdownList(ArrayHelper::map($person1, 'id',function($test){return $test['first_name'].' '.$test['surname']; }));?>
	<div  class="indexFormContainer">
		<section class="indexFormTitle"> Basic info </section>
		<?= $form->field($userForm, 'first_name')->textInput() ?>

		<?= $form->field($userForm, 'surname')->textInput(['maxlength' => true]) ?>

		<?= $form->field($userForm, 'dob')->widget(DatePicker::classname(), [
							'options' => ['placeholder' => 'Select Date Of Birth ...','id' => 'datepicker'],
							 'pluginOptions' => [
								 'format' => 'dd/mm/yyyy',
								 'todayHighlight' => true
									],
								
								]) 
		?>
    </div>
	<div class="indexFormContainer">
		<section class="indexFormTitle" > Login details </section>
		<?= $form->field($userForm, 'username')->textInput(['maxlength' => true, 'minlenght'=>8]) ?>
		<?= $form->field($userForm, 'password')->passwordInput() ?>
		<?= $form->field($userForm, 'basic_role')->dropDownList(ArrayHelper::map(Role::find()->all(),'id', 'name'), ['prompt'=> Yii::t('user', 'Choose Role'), 'options' => ['class' => 'form_input'] ]) ?>
	</div>
	<div class="indexFormContainer">
		<section class="indexFormTitle"> Contact details </section>

		<?= $form->field($userForm, 'telephone_number')->textInput(['maxlength' => true]) ?>

		<?= $form->field($userForm, 'address')->textInput(['maxlength' => true]) ?>
		<?= $form->field($userForm, 'address_line')->textInput(['maxlength' => true]) ?>

		<?= $form->field($userForm, 'state_id')->textInput(['maxlength' => true]) ?>

		<?= $form->field($userForm, 'country_id')->textInput(['maxlength' => true]) ?>

		<?= $form->field($userForm, 'code')->textInput(['maxlength' => true]) ?>
	</div>

    <div class="form-group">
		<p>

			<br>
        	<?= Html::submitButton($userForm->isNewRecord ? '<span id="userbuttonText">Create</span> <img id="userloader" src="images/45.gif" " /> <span id="userloader1"><span>' : 'Update', ['class' => $userForm->isNewRecord ? 'btn btn-danger' : 'btn btn-danger','id'=>'usersubmit_id']) ?>

		</p>
    </div>

    <?php ActiveForm::end(); ?>
   

</div>
<?php 
$js = <<<JS

$('#userform').on('beforeSubmit', function (e) {
	$('#userbuttonText').hide();
 	$('#userloader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#userloader').hide();
   if(result == 'sent'){
	   
	   $(document).find('#userloader1').html(result).show();
	   
    
    }else{
    $(document).find('#userloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#userloader').hide();
	$(document).find('#userloader1').hide();
	$(document).find('#userbuttonText').show();
	}, 5000);
    return false;
    
    
    
});
JS;
 
$this->registerJs($js);
?>


