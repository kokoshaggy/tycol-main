<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;



/* @var $this yii\web\View */
/* @var $model app\models\Tmuser */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
#userloader,#userloader1{
display: none;
}
</style>
<div class="user-form">

<?php Pjax::begin(['id' => 'userformpjax']) ?>
    <?php $form = ActiveForm::begin(['action' =>[$action],'enableClientValidation' => true, 'attributes' => $userModel->attributes(),'enableAjaxValidation' => false,'id' => 'userform','options' => ['data-pjax' => true ]]); ?>

     <?//=$form->field($model, 'person_id')->dropdownList(ArrayHelper::map($person1, 'id',function($test){return $test['first_name'].' '.$test['surname']; }));?>
	<div  class="indexFormContainer">
		<section class="indexFormTitle"> Basic info </section>
		<?= $form->field($personModel, 'first_name')->textInput() ?>

		<?= $form->field($personModel, 'surname')->textInput(['maxlength' => true]) ?>

		<?= $form->field($personModel, 'dob')->widget(DatePicker::classname(), [
							'options' => ['placeholder' => 'Select Date Of Birth ...','id' => 'datepicker'],
							 'pluginOptions' => [
								 'format' => 'dd/mm/yyyy',
								 'todayHighlight' => true
									],
								
								]) 
		?>
    </div>
	<div class="indexFormContainer">
		<section class="indexFormTitle" > Login details </section>
		<?= $form->field($userModel, 'username')->textInput(['maxlength' => true, 'minlenght'=>8]) ?>
		<?= $form->field($userModel, 'password')->passwordInput() ?>
	</div>
	<div class="indexFormContainer">
		<section class="indexFormTitle"> Contact details </section>

		<?= $form->field($telephoneModel, 'telephone_number')->textInput(['maxlength' => true]) ?>

		<?= $form->field($emailModel, 'address')->textInput(['maxlength' => true]) ?>
		<?= $form->field($addressModel, 'address_line')->textInput(['maxlength' => true]) ?>

		<?= $form->field($addressModel, 'state')->textInput(['maxlength' => true]) ?>

		<?= $form->field($addressModel, 'country')->textInput(['maxlength' => true]) ?>

		<?= $form->field($addressModel, 'code')->textInput(['maxlength' => true]) ?>
	</div>

    <div class="form-group">
		<p>

			<br>
        	<?= Html::submitButton($userModel->isNewRecord ? '<span id="userbuttonText">Create</span> <img id="userloader" src="images/45.gif" " /> <span id="userloader1"><span>' : 'Update', ['class' => $userModel->isNewRecord ? 'btn btn-danger' : 'btn btn-danger','id'=>'usersubmit_id']) ?>

		</p>
    </div>

    <?php ActiveForm::end(); ?>
	<?php Pjax::end() ?>
   

</div>
<?php 
$js = <<<JS

$('#userform').on('beforeSubmit', function (e) {
	$('#userbuttonText').hide();
 	$('#userloader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#userloader').hide();
   if(result == 'sent'){
	   
	   $(document).find('#userloader1').html(result).show();
	   $(document).find('#test2').append(result).show();
	   
	   $.pjax.reload({container:"#user_reload"}); 
	   
	  $(document).find("#user_table").DataTable().destroy();
	    $(document).find("#user_table").empty();

	$(document).find("#user_table").DataTable({
        "aaSorting": [],
		responsive: true
    });
	   
    
    }else{
    $(document).find('#userloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#userloader').hide();
	$(document).find('#userloader1').hide();
	$(document).find('#userbuttonText').show();
	}, 5000);
    return false;
    
    
    
});
JS;
 
$this->registerJs($js);
?>


