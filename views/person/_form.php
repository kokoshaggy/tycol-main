<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Tmuser */
/* @var $personsform yii\widgets\ActiveForm */
?>
<?php Pjax::begin(['id' => 'personformpjax']) ?>
<div class="user-form">
	<?php $personsform = ActiveForm::begin(['action' =>[$action],'enableClientValidation' => true, 'attributes' => $personModel->attributes(),'enableAjaxValidation' => false,'id' => 'personforms']); ?>

    
     <?//=$personsform->field($model, 'person_id')->dropdownList(ArrayHelper::map($person1, 'id',function($test){return $test['first_name'].' '.$test['surname']; }));?>

	<div  class="indexFormContainer">
		<section class="indexFormTitle"> Basic info </section>
		<?= $personsform->field($personModel, 'first_name')->textInput() ?>

		<?= $personsform->field($personModel, 'surname')->textInput(['maxlength' => true]) ?>

		
		<?= $personsform->field($personModel, 'dob')->widget(DatePicker::classname(), [
							'options' => ['placeholder' => 'Select issue date ...'],
							 'pluginOptions' => [
								 'format' => 'yyyy-mm-dd',
								 'todayHighlight' => true
									]
								]) 
		?>
	</div>

	<div class="indexFormContainer">
		<section class="indexFormTitle"> Contact details </section>
		<?= $personsform->field($telephoneModel, 'telephone_number')->textInput(['maxlength' => true]) ?>

		<?= $personsform->field($emailModel, 'address')->textInput(['maxlength' => true]) ?>
		<?= $personsform->field($addressModel, 'address_line')->textInput(['maxlength' => true]) ?>

		<?= $personsform->field($addressModel, 'state')->textInput(['maxlength' => true]) ?>

		<?= $personsform->field($addressModel, 'country')->textInput(['maxlength' => true]) ?>

		<?= $personsform->field($addressModel, 'code')->textInput(['maxlength' => true]) ?>
	</div>

    <div class="form-group">
		<p>
			<br>
        	<?= Html::submitButton($personModel->isNewRecord ? '<span id="perbuttonText">Create</span> <img id="perloader" src="images/45.gif" " /> <span id="perloader1"><span>' : 'Update', ['class' => $personModel->isNewRecord ? 'btn btn-danger' : 'btn btn-danger','id'=>'persubmit_id']) ?>
		</p>

    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php Pjax::end(); ?>

<?php 
if($personModel->isNewRecord){
	$personJs = <<<JS

$('#personforms').on('beforeSubmit', function (e) {
	$('#perbuttonText').hide();
 	$('#perloader').show();
    var \$personsform = $(this);
    $.post(\$personsform.attr('action'),\$personsform.serialize())
    .always(function(result){
	
	$(document).find('#perloader').hide();
   if(result=='sent'){
	   
	   $(document).find('#perloader1').html(result).show();
	   $(document).find('#flash').append(result).show();
	   
	 
	   $(document).find('#personforms').trigger('reset');
	   $("html, body").delay(200).animate({
        scrollTop: $('#flash').offset().top
		
    }, 2000);
		
	   $('#dashboard').modal('toggle');
	 	$.pjax.reload({container:"#person_reload",async: false
}); 
	  
	   
    
    }else{
    $(document).find('#perloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#perloader').hide();
	$(document).find('#perloader1').hide();
	$(document).find('#perbuttonText').show();
	}, 5000);
    return false;
    
    
    
});
JS;

} else {
	
	$personJs = <<<JS

$('#personforms').on('beforeSubmit', function (e) {
	$('#perbuttonText').hide();
 	$('#perloader').show();
    var \$personsform = $(this);
    $.post(\$personsform.attr('action')+'&id='+$id,\$personsform.serialize())
    .always(function(result){
	
	$(document).find('#perloader').hide();
   if(result=='sent'){
	   
	   $(document).find('#perloader1').html(result).show();
	   $(document).find('#flash').append(result).show();
	   
	 
	   $(document).find('#personforms').trigger('reset');
	   $("html, body").delay(200).animate({
        scrollTop: $('#flash').offset().top
		
    }, 2000);
		
	   $('#dashboard').modal('toggle');
	 	$.pjax.reload({container:"#person_reload",async: false
}); 
	  
	   
    
    }else{
    $(document).find('#perloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#perloader').hide();
	$(document).find('#perloader1').hide();
	$(document).find('#perbuttonText').show();
	}, 5000);
    return false;
    
    
    
});
JS;

}
 
$this->registerJs($personJs);
?>





