<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tmperson */


?>
<div class="person-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
								'action' => 'person/update',
								'id' => $id,
							   'personModel' => $personModel,
							   'emailModel' => $emailModel,
							   'telephoneModel' => $telephoneModel,
							   'addressModel' => $addressModel,]) ?>

</div>