<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="folder-index">

										
										
										<div class="box-body">

											<table id="person_table" class="table table-bordered table-striped">
												
												<thead>
													<tr>
														<th>Surname</th>
														<th>First Name</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													
												<?php foreach($displayAllPerson as $key => $persons){  ?>
												
													<tr>
													
													<td>
														<?=$persons->surname; ?>
													</td>
														<td>
															<?=$persons->first_name; ?>
															
														</td>
														<td>
															<? $deletUrl=Url::to(['person/delete','id'=>$persons->id]);
															$updateUrl=Url::to(['person/update','id'=>$persons->id]);
															?>
															<p>
        <?= Html::tag('i', '', ['class' => 'fa fa-pencil update', 'data-url'=>$updateUrl]); ?>
        <?= Html::tag('i', '', ['class' => 'fa fa-trash delete', 'data-url'=>$deletUrl]); ?>
    </p>
														</td>
													</tr>
												
												<? } ?>
												</tbody>
												<tfoot>
													<tr>
														<th></th>
													</tr>
												</tfoot>

											</table>
										</div>
              
									</div>

<?
$string = '<span id="newperson" class="btn btn-black"><span class = "main">Create</span><span class = "hide" style = "display:none">Create Contacts</span></span>';
$javascriptPersonButton =  "$('#person_table_length').html('".$string."')";
$loaderImage ='<img class="loadergif" src="images/loader.gif"  />';
$this->registerJs(
    "$('document').ready(function(){ 
	
	$('#person_table').DataTable({
        'aaSorting': [],
		'pagingType': 'simple',
		
    });
	
	
	
	
	
	
	".$javascriptPersonButton."
	
	});"
	
	
);
?>

