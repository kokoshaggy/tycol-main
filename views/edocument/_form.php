<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use app\boffins_vendor\components\controllers\ComponentLinkWidget;

/* @var $this yii\web\View */
/* @var $model app\models\EDocument */
/* @var $form yii\widgets\ActiveForm */
?>
 <style>
    .floatright{
        float: right;
    }
    .foldernote{
        line-height: 24px;
        text-underline-position: alphabetic;
        margin-top: 10px;
        
    }
	 #edocumentloader{
		 display:none;
	 }
	 
	 #flash{
		 display: none;
	 }
	 
</style>
<div class="edocument-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id'=>'edocumentform']); ?>

    <?= $form->field($model, 'reference')->textInput(['maxlength' => true]) ?>

	
	<?= $form->field($model, 'upload_file[]')->widget(FileInput::classname(), [
    'options' => ['accept' => 'image/*',
				 'multiple' => true,
				 'uploadUrl' => Url::to(['/edocument/create']),
				 'downloadUrl' => Url::to(['/edocument/create']),
				  'id' => 'edoc'.$id,
				 ],
	'pluginOptions' => [
                'maxFileSize'=>2800,
				'previewFileType' => 'any',
				'showUpload' => false,
				'initialPreview'=>$image,
				'overwriteInitial'=>true,
        		'initialPreviewAsData'=>true,
    ],
]);
?>
	<?= ComponentLinkWidget::widget(['model'=>$model,'form'=>$form]); ?> 
	<div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<span id="edocumentbuttonText">Create</span> <img id="edocumentloader" src="images/45.gif" " /> <span id="edocumentloader1"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic' : 'btn btn-basic','id'=>'edocumentsubmit_id']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$url = Url::to(['/edocument/create']);
$edocumentform = <<<JS

$('#edocumentform').on('beforeSubmit', function (e) {
$('#edocumentbuttonText').hide();
 	$('#edocumentloader').show();

var \$form = $(this);

	formData = new FormData(this);
	
 $.ajax( {
      url: '$url',
      type: 'POST',
      data: formData,
      processData: false,
      contentType: false,
	  success: function(data){
	  $('#edocumentbuttonText').hide();
 		$('#edocumentloader').hide();
	  if(data == 1){
	  	$('#edocumentloader1').show().html('Created Successfully');
	  }else{
	  	$('#edocumentloader1').show().html('Opps An Error Ocored !');
	  }
	  
	  setTimeout(function(){
	  
		$(document).find('#edocumentloader').hide();
		$(document).find('#edocumentloader1').hide();
		$(document).find('#edocumentbuttonText').show();
		$(document).find('#flash').html('test').show();
		$(document).find('#edocumentform').trigger('reset');
		
		$(document).find('#edocumentviewcreate').modal('toggle');
		
		 $("html, body").delay(200).animate({
        scrollTop: $('#flash').offset().top
		
    }, 2000);
		
	   
	 	$.pjax.reload({container:"#listviewtablereload",async: false
		}); 

	}, 5000);  
	
	setTimeout(function(){
		$(document).find('#flash').hide();
	}, 10000);
               
           }
    } );
	
	
	
	
    return false;
    
    
    
});
JS;
 
$this->registerJs($edocumentform);
?>





 
	   
	 	
	   
	  
