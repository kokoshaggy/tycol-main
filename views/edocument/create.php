<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EDocument */

$this->title = 'Create Edocument';
$this->params['breadcrumbs'][] = ['label' => 'Edocuments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edocument-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'image' => $data,
		'id' => 1
    ]) ?>

</div>
