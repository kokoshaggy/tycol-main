<?php
use yii\helpers\Url;
use app\boffins_vendor\components\controllers\ComponentListViewWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Payment */
$attributes = [
            'reference',
			'number of files' => 'numberOfFiles'
			
            

            
        ];
$action = [
	'update'=> Url::to(['edocument/update']),
	'delete'=> Url::to(['edocument/delete']),
];
?>

<?= ComponentListViewWidget::widget([
									'model'=>$model,
									'content'=>$edocument,
									'attributes'=>$attributes,
									'hoverEffect'=>$hoverEffect,
									'action'=>$action,
									
								]); ?>
