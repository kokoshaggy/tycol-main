<?
use himiklab\thumbnail\EasyThumbnailImage;
use kartik\file\FileInput;
?>
<style>
	.btn.btn-primary.btn-block.btn-file,.file-preview .close {
		display: none;
	}
	.flipback{
		width: 40px;
		background: red;
		position: absolute;
		top: 10px;
		right: 20px;
		z-index: 100000;
	}
</style>

<script>
		$(document).find('.flipback').bind('click',function(){
		
		var clikedObject = $(this);
		$('.sponsorFlip').revertFlip();
			
			// Unsetting the flag:
			$('.sponsorFlip').data('flipped',false)
	})
</script>
<div class='flipback'>Close</div>
<?
$datas = implode(',',$data);
echo FileInput::widget([
    'name' => 'attachment_50',
    'pluginOptions' => [
                        'initialPreview'=>$data,
        'initialPreviewAsData'=>true,
        
        'overwriteInitial'=>false,
  
        
        'showCaption' => false,
        'showRemove' => false,
        'showUpload' => false,
        'browseClass' => 'btn btn-primary btn-block',
        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
        'browseLabel' =>  'Select Photo'

        
    ]
]);
?>

 

