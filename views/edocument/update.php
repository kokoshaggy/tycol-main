<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EDocument */

$this->title = 'Update Edocument: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Edocuments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="edocument-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'image' => $data,
		'id' => $model->id,
    ]) ?>

</div>
