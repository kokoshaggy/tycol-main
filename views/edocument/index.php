<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use app\models\Corporation;
use yii\bootstrap\Alert;
use app\models\Supplier;
use app\boffins_vendor\components\controllers\DisplayComponentViewLayout;
use yii\bootstrap\Modal;
use yii\jui\Draggable;

/* @var $this yii\web\View */
/* @var $model app\models\Folder */
$this->title = 'Tycol | Edocument';
$this->params['breadcrumbs'][] = ['label' => ' Folders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<? $eDocumentId=''; if($model->find()->count() <= 0){$eDocumentId=0;}else{$eDocumentId=!empty($findOneEDocumentId->id)?$findOneEDocumentId->id:0; } ?>

 <?= DisplayComponentViewLayout::widget(['model'=>$model,'id'=>$eDocumentId,]); ?>

