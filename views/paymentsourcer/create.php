<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Paymentsource */

$this->title = 'Create Paymentsource';
$this->params['breadcrumbs'][] = ['label' => 'Paymentsources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paymentsource-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
