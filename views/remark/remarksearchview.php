<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\controllers\ProjectController;
use yii\helpers\Url;
?>

	

        	<div class="nav-tabs-custom">
				<ul class="nav nav-tabs pull-right">
				  <li class="active"><a href="#tab_1-1" data-toggle="tab">Project Remarks</a></li>
            	</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab_1-1">
						<div class="box">
							<h1 style="text-align:center;">Project Remarks</h1>
							<div class="folder-index">
								<h1><?= Html::encode($this->title) ?></h1>
								<div class="box-body">
									<table id="project_table" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th></th>
											</tr>
										</thead>
										<tbody>

											<?php foreach($remarks as $k=>$v){ $sn=1;?>

												<tr>
										
													<td>
														<div class="box-footer box-comments">
															<div class="box-comment">
															
																<div class="comment-text">
																	<span class="username">
																	<i class="fa fa-comments"></i>
																	&nbsp;   <?= ProjectController::convert_tycref_tag($v['text']); ?>
																	<span class="text-muted pull-right"><?= $v['remark_date'];  ?></span>
																	</span>

																</div>
															<!-- /.comment-text -->
															</div>
														</div>
													</td>
												</tr>
											<?php $sn++;}?>

										</tbody>
										<tfoot>
											<tr>
												<th></th>
											</tr>
										</tfoot>

									</table>
								</div>
							</div>
						</div>
				  	</div>
            	</div>
            </div>
		
	
