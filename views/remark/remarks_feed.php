<section id="remarks-section" >
	<!--<div class="box box-solid">
		<div class="box box-black collapsed-box">
			<div class="box-header with-border" id = "box-header">
				<div  style="border: 1px solid #fff;width:30%;float: left">

					<button type="button" class="btn btn-box-tool btn-black hidde his" data-classs="newremark" id="newremark" style="display: none">View Remarks</button>

				</div>
				<div style="border: 1px solid #fff;width:40%;float: left;text-align: center">
					<h3 class="box-title">Recent Remarks</h3>
				</div>
				<div class="box-tools  pull-right">
					<button  class="btn btn-box-tool" data-widget="collapse" style="background: #fff !important;font-size: 16px" ><i class="fa fa-caret-down"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove" style="background: #fff !important"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<!-- /.box-header -->
			<!--<div class="box-body">
				<?php //Pjax::begin(['id' => 'remark_reload']) ?>
					<ul class="products-list product-list-in-box">
						<? //foreach($recent_remarks as $k=>$v){?> 
						<li class="item" >
							<div class="product-info">
								<a href="javascript:void(0)" class="product-title"><strong><?php //Folder::findOne(['id' => $v['folder_id']])->description; ?></strong> 
								<span style="float:right;" class="label remark"><span style="color:#555; font-family: candara; font-size: 16px"> <?php //$v['remark_date'];?></span></span></a>
								<span class="product-description" style="font-family: candara"><?php //Project::convert_tycref_tag($v['text']); ?></span>
							</div>
						</li>
						<? //}?>
					</ul>
				<?php //Pjax::end() ?>
			</div>			  
		</div>
	</div>-->
	
	<header id="remarks-header">
        <h3><?php //echo Yii::t('common', 'remarks_header_title');?></h3>
    </header>
	<div id="remarks-content">
		<?php echo $this->render('view', ['remarks' => $remarks]);?>
	</div>
	<footer>
	</footer>
</section>
