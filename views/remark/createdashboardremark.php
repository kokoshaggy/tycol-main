<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DepDrop;
use app\models\Folder;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Remark */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
#remarkloader,#remarkloader1{
display: none;
}
</style>
<div class="user-form">
			<?php $form = ActiveForm::begin([
							'options' => ['class' => 'form-horizontal'],
							'action' =>['remark/createremark'],
							'id' => 'remarkdashboardform',
							'options' => ['data-pjax' => true ]

							]); 
								?>



		<?=$form->field($remarkModel, 'folder_id')->dropDownList(ArrayHelper::map(Folder::find()->all(), 'id', 'tyc_ref'), ['id'=>'folderid']); ?>
	

		<?= $form->field($remarkModel, 'project_id')->widget(DepDrop::classname(), [
			'options'=>['id'=>'projectid'],
			'pluginOptions'=>[
				'depends'=>['folderid'],
				'placeholder'=>'Select Project... ',
				'url'=>Url::to(['/site/dependentdropdown'])
			]
		]); ?>



	<?=   $form->field($remarkModel, 'remark_type')->dropdownList([
				'project'=> 'Project', 
				'invoice'=> 'Invoice',
				'order'=>'Order'
			],
			['prompt'=>'Select remark type']);
	?>

	<?= $form->field($remarkModel, 'text')->textarea(['rows' => '6']); ?>

	<? $now=date('Y-m-d h:i:s');?>
	
	<?= $form->field($remarkModel, 'remark_date')->hiddenInput(['maxlength' => true,'value'=>$now])->label(false); ?>

	<div class="form-group">
		<?= Html::submitButton($remarkModel->isNewRecord ? '<span id="remarkbuttonText">Create</span> <img id="remarkloader" src="images/45.gif" " /> <span id="remarkloader1"><span>' : 'Update', ['class' => $remarkModel->isNewRecord ? 'btn btn-danger' : 'btn btn-danger','id'=>'remarksubmit_id']) ?>
	</div>
		<?php ActiveForm::end(); ?>

	</div>
	
	
<?php 
$jsDashboardremark = <<<JS

$('#remarkdashboardform').on('beforeSubmit', function (e) {
	$('#remarkbuttonText').hide();
 	$('#remarkloader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#remarkloader').hide();
   if(result=='sent'){
	   
	   $(document).find('#remarkloader1').html(result).show();
	   $(document).find('#flash').append(result).show();
	   
	 
	   
	   $("html, body").delay(200).animate({
        scrollTop: $('#flash').offset().top
		
    }, 2000);
		$(document).find('#remarkdashboardform').trigger('reset');
	   $('#dashboard').modal('toggle');
	   $.pjax.reload({container:"#remark_reload"});
	  
    
    }else{
    $(document).find('#remarkloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#remarkloader').hide();
	$(document).find('#remarkloader1').hide();
	$(document).find('#remarkbuttonText').show();
	
	}, 5000);
    return false;
    
  
});
JS;
 
$this->registerJs($jsDashboardremark);
?>
