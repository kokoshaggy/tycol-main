<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\controllers\ProjectController;
use yii\helpers\Url;

?>
<div id="remark_view">
	<?php foreach($remarks as $k => $v) { $sn = 1;?>
		<div class="remark-item">
			<header class="remark-title">
				<span class="remark-folder-title" >
					<?php 
						$title = $v['folder']['description'] . ' (' . $v['folder']['tyc_ref'] . ')';
						echo Html::a($title, ['folder/view', 'id' => $v['folder']['id']] );
					?>
				</span>
				<small class="remark-date"><span class="remark-date-title" ><?php echo Yii::t('remark', 'remark_date') . ': ' ?></span><span class="remark-date-value"><?php echo $v['remark_date'];  ?></span></small>
			</header>
			<p class="remark-text"><?= ProjectController::convert_tycref_tag($v['text']); ?></p>
		</div>
	<?php $sn++; } ?>			
</div>





 


