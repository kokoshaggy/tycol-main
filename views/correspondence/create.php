<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Correspondence */

$this->title = 'Create Correspondence';
$this->params['breadcrumbs'][] = ['label' => 'Correspondences', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


