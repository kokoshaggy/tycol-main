<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use app\models\Userdb as user;
use app\models\Corporation as corporation;
use app\models\Person as person;
use yii\helpers\Url;
use app\boffins_vendor\components\controllers\ComponentLinkWidget;
use kartik\datetime\DateTimePicker;
use kartik\date\DatePicker;
//use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Correspondence */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
	.correspondence-form{
		position: relative;
		z-index: 100;
	}
	
	#correspondenceloader,#correspondenceloader1 {
		display: none;
	}
</style>
<div class="correspondence-form">

    <?php $form = ActiveForm::begin(['action'=>['create'],'id'=>'correspondenceform']); ?>
	
	<?= ComponentLinkWidget::widget(['model'=>$model,'form'=>$form]); ?> 
	
    <?= $form->field($model, 'title_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'particular_reference')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_id')->dropdownList(ArrayHelper::map($model->find()->asArray()->all(), 'id', 'title_description'))?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'corresponding_entity')->dropdownList(ArrayHelper::map(corporation::find()->asArray()->all(),'entity_id', 'name')) ?>


	<?= $form->field($model, 'specific_corresponding_entity')->dropdownList(ArrayHelper::map(person::find()->asArray()->all(),'entity_id','first_name') ) ?>

	<?= $form->field($model, 'authorising_entity')->dropdownList(ArrayHelper::map(user::find()->asArray()->all(),'id', 'username')) ?>

    <?= $form->field($model, 'create_date')->widget(DatePicker::classname(), [
							'options' => ['placeholder' => 'Select Payment Date ...','id' => 'payment_date'],
							 'pluginOptions' => [
								 
								 'todayHighlight' => true
									],
								
								]) 
		?>

    

    <?= $form->field($model, 'notes')->textInput(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<span id="correspondencebuttonText">Create</span> <img id="correspondenceloader" src="images/45.gif" " /> <span id="correspondenceloader1"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic' : 'btn btn-basic','id'=>'correspondencesubmit_id']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$correspondenceform = <<<JS

$('#correspondenceform').on('beforeSubmit', function (e) {
	$('#correspondenceformbuttonText').hide();
 	$('#correspondenceformloader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#correspondenceloader').hide();
   if(result.sent=='sent'){
	   
	   $(document).find('#correspondenceloader1').html(result).show();
	    $(document).find('#flash').html(result.message).show();
	   
	 	$(document).find('.modal').modal('hide');
	   $(document).find('#correspondenceform').trigger('reset');
	   $("html, body").delay(200).animate({
        scrollTop: $('#flash').offset().top
		
    }, 2000);
		
	   
	 	$.pjax.reload({container:"#supplier_reload",async: false
}); 
	  
    }else{
    $(document).find('#correspondenceloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
		$(document).find('#correspondenceloader').hide();
		$(document).find('#correspondenceloader1').hide();
		$(document).find('#correspondencebuttonText').show();
	}, 5000);
    return false;
    
    
    
});
JS;
 
$this->registerJs($correspondenceform);
?>


