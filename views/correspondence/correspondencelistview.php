 <style>
    .floatright{
        float: right;
    }
    .foldernote{
        line-height: 24px;
        text-underline-position: alphabetic;
        margin-top: 10px;
        
    }
	 .projecturl{
		 cursor: pointer;
	 }
	 .box {
        border:none;
        -webkit-box-shadow:none;
        box-shadow: none;
        padding-right: 10px;

      }
    .box-body{
      font-family: candara;
    }
    #createcorrespondence{
    	margin-top: 10px;
    	
    }
    
</style>
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\bootstrap\Alert;
use yii\bootstrap\Modal;
use app\models\Corporation;
use app\models\Supplier;
use app\models\Payment;

/* @var $this yii\web\View */
/* @var $model app\models\Folder */
?>

      <div class="row">

		  <div class="col-xs-12">
			<div class="box">
                <div class="folder-index">
                	<h1><?= Html::encode($this->title) ?> 
						<div class="floatright btn btn-danger" id="createcorrespondence" data-formurl="<?= Url::to(['create']) ?>">Create New Correspondence
						</div>
					</h1>
					
                  	<div class="box-body">
						<h3>Correspondence List</h3>
                    	<table id="correspondencelistview" class="table table-bordered table-striped">
                        	<thead>
                            <tr>
                                <th>SN</th>
								
								<th>Title</th>
								<th>Reference</th>
								<th>Contact Person</th>
								<th>Drafted By</th>
								<th>Date</th>
								<th>Action</th>
                  
                            </tr>
                        
							</thead>
                        	<tbody>
                            <?php $sn=1; foreach($correspondence as $k=>$v){ ?>
                                <tr class="correspondenceurltr <?if($sn === 1 && $hoverEffect === 'true'){echo 'activelist';} ?>" data-url="<?=Url::to(['correspondence/correspondenceview','id' => $v['id']]) ?>" title="Click to view correspondence">
                                  <td class="correspondenceurl"><?= $sn; ?></td>
                                  <td class="correspondenceurl"><?= $v['title_description'];  ?></td>
                                  <td class="correspondenceurl"><?= $v['particular_reference'];  ?></td>
                                  <td class="correspondenceurl"><?= $v['specificCorrespondingEntityName'];  ?></td>
                                  <td class="correspondenceurl"><?= $v['draftingEntityPersonName'];  ?></td>
									<td class="correspondenceurl"><?= $v['create_date'];  ?></td>
                                  <td>
									<? 
										  $deletUrl=Url::to(['correspondence/delete','id'=>$v['id']]);
										  $updateUrl=Url::to(['correspondence/update','id'=> $v['id']]);
												?>
												<p>
													<?= Html::tag('i', '', ['class' => 'fa fa-pencil update', 'data-url'=>$updateUrl]); ?>
													<?= Html::tag('i', '', ['class' => 'fa fa-trash delete', 'data-url'=>$deletUrl]); ?>
												</p>
									</td>
                                 
                                </tr>
                            <?php $sn++; }?>
                   
                  
							</tbody>
                        	<tfoot>
								<tr>
									<th>SN</th>
								
								<th>Title</th>
								<th>Reference</th>
								<th>Contact Person</th>
								<th>Drafted By</th>
								<th>Date</th>
								<th>Action</th>
                  
									

								</tr>
                			</tfoot>
                
                    
						</table>
				  	</div>
          		
				</div>
			  
			</div>
		  </div>
    </div>
<? 
		Modal::begin([
			'header' =>'<h1 id="headers"></h1>',
			'id' => 'correspondenceviewcreate',
			'size' => 'modal-md',  
		]);
		echo "<div id='correspondencecreateform' class = 'test'> </div>";
		Modal::end();
	?>
<script>
$("#correspondencelistview").DataTable({
        "aaSorting": [],
		"responsive": "true",
		"pagingType": "simple",
    });
</script>

