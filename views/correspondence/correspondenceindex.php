<?php
use app\models\Folder;

use yii\widgets\Pjax;
use app\controllers\ProjectController as project;
use yii\helpers\Html;
use app\models\Foldertask as userName;



/* @var $this yii\web\View */


//$this->title = 'My Yii Application';
?>

<!-- Main content -->
<section class="content">
      <!-- Small boxes (Stat box) --> 
    <div class="row">
        <div class="col-md-5 ">
			<?= $this->render('_form', [
				'model' => $model,
				
				]);
			?>
        </div>

         <div class="col-md-7 ">
			 <div class="box-group" id="accordion">
				 
				<?php $i=1;foreach($correspondence as $key => $value){ ?>


					<div class="panel box box-danger">
						<div class="box-header">

							<a data-toggle="collapse" data-parent="#accordion" href="#task<?= $i; ?>">
								<span style="float:left;">
									corresponding_entity
									<?=$corporation->findOne(['entity_id' => $value['corresponding_entity'] ])->NameString; ?>

								</span>
								<span id="assigntotimer<?= $i?>" style="float:right;">No: <?= $value['create_date']; ?>  </span>
							</a>

						</div>
						<div id="task<?= $i; ?>" class="panel-collapse collapse">
							<div class="box-body">
								drafted by : <?=!empty($users->findOne(['id'=>$value['drafting_entity']])->Username)?$users->findOne(['id'=>$value['drafting_entity']])->Username:'empty';?> </br> </hr>
						
								Authorised by :<? $test = $users->findById($value['authorising_entity']); echo $test['username'] ?>  </br> </hr>
			 
			 					Tycref:<?= $value['tyc_ref']; ?> </br> </hr>
	
								company:<?= $corporation->findOne(['entity_id' => $value['corresponding_entity'] ])->NameString; ?> </br> </hr>

								person:<?=!empty($person->findOne(['entity_id' => $value['specific_corresponding_entity'] ])->PersonString)?$person->findOne(['entity_id' => $value['specific_corresponding_entity'] ])->PersonString:'empty'; ?> </br> </hr>
								content:<?= $value['content']; ?> </br> </hr>
								note:<?= $value['notes']; ?> </br> </hr>
							
							</div>
						</div>
					</div>

				<? $i++;}?>
							 
			</div>			 
        </div>
	</div>
      
</section>





