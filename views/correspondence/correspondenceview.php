<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Correspondence */
 
$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Correspondences', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #correspondence{
        background: #fff;
        margin-top: 22px;
        min-height: 600px;
    }
</style>
<div class="Correspondence">
    <div class="box" id="correspondence">
<div class="box-body">


   

    
    <h3><?=$model->title_description ."- ".$model->particular_reference;?></h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'particular_reference',
            'parent_id',
            'title_description',
            'content',
            'correspondingEntityNameString',
            'specificCorrespondingEntityName',
            'draftingEntityPersonName',
            'authorisingEntityPersonName',
            'create_date',
            'notes:ntext',
        ],
    ]) ?>


</div>
</div>
</div>
