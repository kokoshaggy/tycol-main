 <style>
    .floatright{
        float: right;
    }
    .foldernote{
        line-height: 24px;
        text-underline-position: alphabetic;
        margin-top: 10px;
        
    }
	 #correspondenceloading{
		 display:none;
	 }
	 
	 #flash{
		display: none;
	}
	 
</style>
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use app\models\Corporation;
use yii\bootstrap\Alert;
use app\models\Supplier;
use app\boffins_vendor\components\controllers\Menu;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $model app\models\Folder */

$this->params['breadcrumbs'][] = ['label' => ' Folders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('folderview'); ?>
Correspondence
<small>Select Correspondence</small>
   
<?php $this->endBlock(); ?>

<?php $this->beginBlock('folderSidebar'); ?>
	<?= Menu::widget(); ?>

<?php $this->endBlock(); ?>





<section class="content">
   <div class="row">
		<div class="col-lg-12">
			<?= Alert::widget([
				   'options' => ['class' => 'alert-info','id'=>'flash'],
				   'body' => Yii::$app->session->getFlash('created_successfully'),
					 ]);?>
		</div>
	</div>
	
      <div class="row">
          
          
          
          
          <? $correspondenceId=''; if($model->find()->count() <= 0){$correspondenceId=0;}else{$correspondenceId=!empty($findOneCorrespondence->id)?$findOneCorrespondence->id:0; } ?>
		  <div class="col-xs-7" id="correspondenceListView">
			<img class="loadergif" src="images/loader.gif"  />
		  </div>
		  
		  <div class="col-xs-5" >
			  
			  <div id="correspondenceviewcontainer">
				  <div id="correspondenceView">
				  <img class="loadergif" src="images/loader.gif"  />
				  </div>
			  </div>
		  </div>
    </div>
</section>
<? 
		Modal::begin([
			'header' =>'<h1 id="headers"></h1>',
			'id' => 'dashboard',
			'size' => 'modal-md',  
		]);
?>
<div id="formcontent"></div>
<?
	Modal::end();
?>



<?php 
	$urlListView = Url::to(['correspondence/correspondencelistview']);
	$urlView = Url::to(['correspondence/correspondenceview','id'=>$correspondenceId]);
	
$correspondenceJs = <<<JS


$("#correspondenceListView").load('$urlListView');
$("#correspondenceView").load('$urlView',{var2:1});

$(document).on('click','.correspondenceurl',function(){
var parent = $(this).parent();
$('.correspondenceurltr').removeClass('activelist');
parent.addClass('activelist');
$('#correspondenceView').html('<img class="loadergif" src="images/loader.gif"  />');
var url = parent.data('url');
$("#correspondenceView").load(url,{var2:1},function(){
$('#correspondenceloader').slideUp('slow');
$('#correspondenceviewcontainer').slideDown('fast');

});
})
$(document).on('click','#createcorrespondence',function(){
var formUrl = $(this).data('formurl');
$('#correspondenceviewcreate').modal('show').find('#correspondencecreateform').load(formUrl);

})





JS;
 
$this->registerJs($correspondenceJs);
?>
