<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use app\models\Userdb as user;
use app\models\Corporation as corporation;
use app\models\Person as person;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Correspondence */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
	.button-basic{
		background: #ccc;
	}
	#correspondenceloader,#correspondenceloader1 {
		display: none;
	}

</style>
<div class="correspondence-form">
	
    <?php $form = ActiveForm::begin(['action'=>['/correspondence/create'],'enableClientValidation' => true, 'attributes' => $model->attributes(),'enableAjaxValidation' => false,'id' => 'correspondencedashboardform']); ?>
	
	<? $componentName= ['folder' => 'Folder', 'project' => 'Project', 'invoice' => 'Invoice','order' => 'Order', 'rpo' => 'Received Purchase Order (L/C OR LPO)']; ?>
	
    <?= $form->field($model, 'itemType')->dropdownList($componentName,[ 'prompt'=> 'Select...', 'class' => 'form-control', 'id' => 'cor_componentname'] ) ?>

	<?= Html::label('Choose an Item', 'folder', ['class' => 'control-label folder']) ?>
	
	<?= $form->field($model, 'itemID')->widget(DepDrop::classname(), [
			'options'=>['id' => 'cor_component'],
			'pluginOptions' => [
				'depends' => ['cor_componentname'],
				'placeholder' => 'Select...',
				'url'=>Url::to(['/correspondence/list-items'])
			]
		]);
	?>

    <?= $form->field($model, 'title_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'particular_reference')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_id')->dropdownList(ArrayHelper::map($model->find()->where(['tyc_ref' => '1349'])->asArray()->all(), 'id', 'title_description'))?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'corresponding_entity')->dropdownList(ArrayHelper::map(corporation::find()->asArray()->all(),'entity_id', 'name')) ?>


	<?= $form->field($model, 'specific_corresponding_entity')->dropdownList(ArrayHelper::map(person::find()->asArray()->all(),'entity_id','first_name') ) ?>

	<?= $form->field($model, 'authorising_entity')->dropdownList(ArrayHelper::map(user::find()->asArray()->all(),'id', 'username')) ?>

    <?= $form->field($model, 'create_date')->textInput() ?>

    <?= $form->field($model, 'notes')->textInput(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<span id="correspondencebuttonText">Create</span> <img id="correspondenceloader" src="images/45.gif" " /> <span id="correspondenceloader1"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic' : 'btn btn-basic','id'=>'correspondencesubmit_id']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
$jsDashboardCorrespondence = <<<JS

$('#correspondencedashboardform').on('beforeSubmit', function (e) {
	$('#correspondencebuttonText').hide();
 	$('#correspondenceloader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#correspondenceloader').hide();
   if(result=='sent'){
	   
	   $(document).find('#correspondenceloader1').html(result).show();
	   
	   $(document).find('#flash').append(result).show();
	   
	 
	   $(document).find('#correspondencedashboardform').trigger('reset');
	   $("html, body").delay(200).animate({
        scrollTop: $('#flash').offset().top
		
    }, 2000);
	  
    
    }else{
    $(document).find('#correspondenceloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#correspondenceloader').hide();
	$(document).find('#correspondenceloader1').hide();
	$(document).find('#correspondencebuttonText').show();
	
	}, 5000);
    return false;
    
    
    
});
JS;
 
$this->registerJs($jsDashboardCorrespondence);
?>


