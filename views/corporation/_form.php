<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Tmuser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="corporation-form">
	<?  $formId = $hiddenValue; ?>

    <?php $form = ActiveForm::begin(['action' =>['corporation/update','id'=>$id],'enableAjaxValidation' => false,'id' => 'corporation']); ?>

     <?//=$form->field($model, 'person_id')->dropdownList(ArrayHelper::map($person1, 'id',function($test){return $test['first_name'].' '.$test['surname']; }));?>

	<div  class="indexFormContainer">
		<section class="indexFormTitle"> Basic info </section>
    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'short_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notes')->textInput(['maxlength' => true]) ?>
    </div>

 	<div class="indexFormContainer">
		<section class="indexFormTitle"> Contact details </section>

    <?= $form->field($telephoneModel, 'telephone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($emailModel, 'address')->textInput(['maxlength' => true]) ?>
    <?= $form->field($addressModel, 'address_line')->textInput(['maxlength' => true]) ?>

    <?= $form->field($addressModel, 'state')->textInput(['maxlength' => true]) ?>

    <?= $form->field($addressModel, 'country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($addressModel, 'code')->textInput(['maxlength' => true]) ?>
    
	</div>
    
	
	
	<div class="form-group">
		<p>
			<br>
			<?= Html::hiddenInput('corporationhidden[hidden]', $hiddenValue) ?>
        	<?= Html::submitButton($model->isNewRecord ? '<span id="copbuttonText">Create</span> <img id="coploader" src="images/45.gif" " /> <span id="coploader1"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-danger' : 'btn btn-danger','id'=>'corporation_id']) ?>
		</p>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$corporation = <<<JS

$('#corporation').on('beforeSubmit', function (e) {
	$('#copbuttonText').hide();
 	$('#coploader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#coploader').hide();
   if(result==1){
	   
	   $(document).find('#coploader1').html(result).show();
	   
    
    }else{
    $(document).find('#coploader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
		$(document).find('#coploader').hide();
		$(document).find('#coploader1').hide();
		$(document).find('#copbuttonText').show();
	}, 5000);
    return false;
    
    
    
});
JS;
 
$this->registerJs($corporation);
?>


