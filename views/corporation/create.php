<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tmcorporation */

$this->title = 'Create Corporation';
$this->params['breadcrumbs'][] = ['label' => 'corporations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="corporation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
