 <style>
    .floatright{
        float: right;
    }
    .foldernote{
        line-height: 24px;
        text-underline-position: alphabetic;
        margin-top: 10px;
        
    }
	 .producturl{
		 cursor: pointer;
	 }
   .box {
        border:1px solid #fff;
        -webkit-box-shadow:none;
        box-shadow: none;
        padding-right: 10px;

      }
    .box-body{
      font-family: candara;
    }
    #createproduct{
      margin-top: 10px;
    }
	 
</style>
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\bootstrap\Alert;
use yii\bootstrap\Modal;
use app\models\Corporation;
use app\models\Supplier;
use app\models\Category;


/* @var $this yii\web\View */
/* @var $model app\models\Folder */
?>

<div class="folder-view">

    <h1><?= Html::encode($this->title) ?></h1>

</div>




      <div class="row">

		  <div class="col-xs-12">
			<div class="box">
                <div class="folder-index">
                	<h1><?= Html::encode($this->title) ?> 
						<div class="floatright btn btn-danger" id="createproduct" data-formurl="<?= Url::to(['create']) ?>">Create Product
						</div>
					</h1>
					
                  	<div class="box-body ">
						<h3>Product List</h3>
                    	<table id="productlistviewtable" class="table table-bordered table-striped ">
                        	<thead>
                            <tr>
                              	<th>SN</th>
                                <th>Image</th>
                              	<th>Product Name</th>
                                <th>Category</th>
                              	<th>Main Attribute</th>
                              	<th>Action</th>                              
                            </tr>
                        
							             </thead>
                        	 
                           <tbody>
                            <?php 
                              $serialNum = 1; 
                              foreach($product as $key => $value){ ?>
                                <tr class="producturltr <? if($serialNum === 1 && $hoverEffect === 'true'){ echo 'activelist';} ?>" data-url="<?=Url::to(['product/productview','id' => $value['id']]) ?>" title="Click to view product">
                                  <td class="producturl"><?= $serialNum; ?></td>
                                  <td class="producturl"><img src="<?= $value['image'];?>" width="50" height="50"></td>
                                  <td class="producturl"><?= $value['product_name'];  ?></td>
                                  <td class="producturl"><?= $value['categoryName']; ?></td>
                                  <td class="producturl"><?= $value['main_attribute'];  ?></td>
                                  <td>
									                   <? 
                										  $deleteUrl=Url::to(['product/delete','id'=>$value['id']]);
                										  $updateUrl=Url::to(['product/update','id'=> $value['id']]);
                										 ?>
              												<p>
              													<?= Html::tag('i', '', ['class' => 'fa fa-pencil update', 'data-url'=>$updateUrl]); ?>
              													<?= Html::tag('i', '', ['class' => 'fa fa-trash delete', 'data-url'=>$deleteUrl]); ?>
              												</p>
								                  </td>
                                 
                                </tr>
                            <?php $serialNum++; }?>
                   
                  
							       </tbody>
						</table>
				  	</div>
          		
				</div>
			  
			</div>
		  </div>
    </div>
<? 
		Modal::begin([
			'header' =>'<h1 id="headers"></h1>',
			'id' => 'productviewcreate',
			'size' => 'modal-md',  
		]);
		echo "<div id='productcreateform'> </div>";
		Modal::end();
?>

<script>
$("#productlistviewtable").DataTable({
        "aaSorting": [],
		"responsive": "true",
		"pagingType": "simple",
    });
</script>






