<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = 'Create Product';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">
	<?php if(Yii::$app->controller->action->id == 'index' and Yii::$app->controller->id == 'site'){ ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php } ?>
    <?= $this->render('_form', [
        'model' => $model,
        'category' => $category,
        'attribute' => $attribute,
        'productCorporation' => $productCorporation,
        'supplier' => $supplier,
    ]) ?>

</div>
