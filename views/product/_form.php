<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\boffins_vendor\components\controllers\ComponentLinkWidget;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use wbraganca\dynamicform\DynamicFormWidget;


/* @var $this yii\web\View */
/* @var $model app\models\Tmproduct */
/* @var $form yii\widgets\ActiveForm */
$categoryUrl = Url::to(['category/create']);
$supplierUrl = Url::to(['supplier/adhocsupplier']);
?>


<style>
	#productloader,#iproductloader1 {
		display: none;
	}
</style>

<section>
	
<div class="product-form">

    <?php $form = ActiveForm::begin(['enableClientValidation' => true,'id'=>'productforms','enableAjaxValidation' => false, 'options' => ['enctype' => 'multipart/form-data']]); ?>
    
        <?= ComponentLinkWidget::widget(['model'=>$model,'form'=>$form]); ?>
        
        <?php Pjax::begin(['id' => 'categoryRefresh','timeout' => 5000]); ?>
            <?= $form->field($model, 'product_category',['options'=>[
                'tag'=>'div',
                'class'=>'form-group categoryRefesh'],
                'template'=>'{label} <button type="button" class="btn btn-primary btn-sm" id="modalButton" title="Add new category" data-Url="'.$categoryUrl.'">Create category</button>{input}{error}{hint}'
                    ])->dropdownList(ArrayHelper::map($category, 'id', 'category_name'), ['prompt'=>'Select...']);?>
        <?php Pjax::end(); ?>
		<?= $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'main_attribute')->textInput(['maxlength' => true]) ?>


    <?php DynamicFormWidget::begin([

        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
        'widgetBody' => '.container-items', // required: css class selector
        'widgetItem' => '.item', // required: css class
        'limit' => 5, // the maximum times, an element can be cloned (default 999)
        'min' => 0, // 0 or 1 (default 1)
        'insertButton' => '.add-item', // css class
        'deleteButton' => '.remove-item', // css class
        'model' => $attribute[0],
        'formId' => 'productforms',
        'formFields' => [
            'attribute_name',
            'value',
        ],
    ]); ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <button type="button" class="pull-left add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Add attribute</button>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body container-items">
            <?php foreach ($attribute as $index => $attribute): ?>
                <div class="item panel panel-default">
                    <div class="panel-heading">
                        <button type="button" class="pull-left remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (!$attribute->isNewRecord) {
                                echo Html::activeHiddenInput($attribute, "[{$index}]id");
                            }
                        ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $form->field($attribute, "[{$index}]attribute_name")->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($attribute, "[{$index}]value")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php DynamicFormWidget::end(); ?>
    <a href="#demo" class="btn btn-info" data-toggle="collapse">Add Supplier</a>
        <div id="demo" class="collapse">
        <?= $form->field($productCorporation, 'supplier_id',['options'=>[
                'tag'=>'div',
                'class'=>'form-group supplierRefesh'],
                'template'=>'{label}  <button type="button" class="btn btn-primary btn-sm" id="newSupplier" title="Add new supplier" data-Url="'.$supplierUrl.'">Create Supplier</button>{input}{error}{hint}'
                    ])->dropdownList(ArrayHelper::map($supplier, 'id', 'name'), ['prompt'=>'Select...']);?>
        </div>
	<?= $form->field($model, 'image')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<span id="productbuttonText">Create</span> <img id="productloader" src="images/45.gif" " /> <span id="productloader1"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic' : 'btn btn-basic','id'=>'productsubmit_id']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</section>


<?php 
$productform = <<<JS

$('#productforms').on('beforeSubmit', function (e) {
	
	$('#productformbuttonText').hide();
 	$('#productloader').show();
    var \$form = $(this);
    var formData = new FormData($('form')[0]);
    $.ajax({
        url: "create.php",
        type: 'POST',
        data: formData,
        

        success: function(result) {
        	if(result){
				$(document).find('#flash').html(result.message).show();
				$(document).find('.modal').modal('hide');
			   	$(document).find('#productform').trigger('reset');
			   	$("html, body").delay(200).animate({
	        	scrollTop: $('#flash').offset().top
    			}, 2000);
    	 	}else {
				$(document).find('#productloader1').html(result).show();
			}	  
            
        },
        cache: false,
        contentType: false,
        processData: false,
    });
 });


$(".dynamicform_wrapper").on("afterInsert", function(e, item) {

    $(".dynamicform_wrapper .panel-title-attribute").each(function(index) {

        $(this).html("attribute: " + (index + 1))

    });

});


$(".dynamicform_wrapper").on("afterDelete", function(e) {

    $(".dynamicform_wrapper .panel-title-attribute").each(function(index) {

        $(this).html("attribute: " + (index + 1))

    });

});


$(function(){
    $('#modalButton').click(function(){
        $('#categoryModal').modal('show')
        .find('#createcategory')
        .load($(this).attr('data-Url'));
        });

    $('#newSupplier').click(function(){
        $('#newSupplierModal').modal('show')
        .find('#createsupplier')
        .load($(this).attr('data-Url'));
        });
    });
JS;
 
$this->registerJs($productform);
?>
