<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use app\models\Supplier;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->product_name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

  
    <div class="prouct">
        <div class="box" id="product">
            <div class="box-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                                'label' => 'Product Name',
                                'value' => $model->product_name,
                                ],
                                [
                                 'label' => 'Image',
                                 'value' => $model->image,
                                 'format' => ['image',['width' => '150', 'height' => '150']],
                                ],
                                
                                [
                                 'label' => 'Main Attribute',
                                'value' => $model->main_attribute,
                                ],
                                
                        'last_updated',
                    ],
                ]) ?>
                <table id="view" class="table table-striped table-bordered detail-view2">
                    <tbody>
                        <tr>
                            <th>Attributes</th>
                            <td><?= $model->attributeName; ?></td>
                        </tr>
                            <?php if(!empty($model->corporationName)): ?>
                                <tr>
                                <th>Supplier</th>
                                <td><?= $model->corporationName; ?></td>
                                </tr>
                            <?php endif ?>
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
    
</div>
