 <style>
 
    .floatright {
        float: right;
    }
    
    #proloader {
        display:none;
    }
    
    #flash {
        display: none;
    }
     
</style>
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Category;
use yii\widgets\DetailView;
use yii\bootstrap\Alert;
use app\boffins_vendor\components\controllers\Menu;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Folder */
$this->title = 'Tycol | Product';
$this->params['breadcrumbs'][] = ['label' => 'Product', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('folderview'); ?>
Product
<small>Select Product</small>
   
<?php $this->endBlock(); ?>

<?php $this->beginBlock('folderSidebar'); ?>
    <?= Menu::widget(); ?>

<?php $this->endBlock(); ?>

<div class="folder-view">
   

</div>

<section class="content">
    <!-- flash messages start -->
    <? if (Yii::$app->session->getFlash('created_successfully') !==NULL): ?>
         <?= Alert::widget([
       'options' => ['class' => 'alert-info'],
       'body' => Yii::$app->session->getFlash('created_successfully'),
         ]);?>
    <? endif ?>
    
    <? if (Yii::$app->session->getFlash('created_folder') !==NULL): ?>
         <?= Alert::widget([
       'options' => ['class' => 'alert-info'],
       'body' => Yii::$app->session->getFlash('created_folder'),
         ]);?>
    <? endif ?>
    
    <? if (Yii::$app->session->getFlash('instruction') !==NULL): ?>
         <?= Alert::widget([
       'options' => ['class' => 'alert-info'],
       'body' => Yii::$app->session->getFlash('instruction'),
         ]);?>
    <? endif ?>
    <!-- flash messages end -->
    
    <div class="row">
      <div class="col-lg-12">
        <?= Alert::widget([
           'options' => ['class' => 'alert-info','id'=>'flash'],
           'body' => Yii::$app->session->getFlash('created_successfully'),
           ]);?>
      </div>
      
      <? $productId=''; 
        if($model->find()->count() <= 0)
          {
            $productId=0;
          }else{
            $productId = !empty($findOneProduct->id) ? $findOneProduct->id: 0; 
          } ?>
      <div class="col-xs-7" id="productListView">
      <img class="loadergif" src="images/loader.gif"  />
      </div>
      
       <div class="col-xs-5" >
        
        <div id="productviewcontainer">
          <div id="productView">
          <img class="loadergif" src="images/loader.gif"  />
          </div>
        </div>
      </div>
    </div>
</section>

<? 
    Modal::begin([
      'header' =>'<h1 id="headers"></h1>',
      'id' => 'dashboard',
      'size' => 'modal-md',  
    ]);
?>
<div id="formcontent"></div>
<?
  Modal::end();
?>

<? 
        Modal::begin([
            'header' =>'<h1 id="headers"></h1>',
            'id' => 'categoryModal',
            'size' => 'modal-md',  
        ]);
?>
<div id="createcategory"></div>
<?
    Modal::end();
?>

<? 
        Modal::begin([
            'header' =>'<h1 id="headers"></h1>',
            'id' => 'newSupplierModal',
            'size' => 'modal-sm',  
        ]);
?>
<div id="createsupplier"></div>
<?
    Modal::end();
?>

<?php 
  $urlListView = Url::to(['product/productlistview']);
  $urlView = Url::to(['product/productview','id'=>$productId]);
  
$js3 = <<<JS

options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": true,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": true,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

toastr.error("Noooo oo oo ooooo!!!", "Title", options);
$("#productListView").load('$urlListView');
$("#productView").load('$urlView', {var2:1});

$(document).on('click','.producturl',function(){
  var parent = $(this).parent();

  $('.producturltr').removeClass('activelist');
  parent.addClass('activelist');


  $('#productView').html('<img class="loadergif" src="images/loader.gif"  />');
  var url = parent.data('url');
  
  $("#productView").load(url,{var2:1},function(){
    $('#productloader').slideUp('slow');
    $('#productviewcontainer').slideDown('fast');

  });
})

$(document).on('click','#createproduct',function(){
  var formUrl = $(this).data('formurl');
  $('#productviewcreate').modal('show').find('#productcreateform').load(formUrl);

})


JS;
 
$this->registerJs($js3);
?>