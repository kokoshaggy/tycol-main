<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Reminder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reminder-form">

  	<?php $form = ActiveForm::begin(['action' =>['create'],'enableAjaxValidation' => false,'options' => ['id' => 'reminder']]); ?>
	<?= Html::dropDownList('reminder[task]',null, ArrayHelper::map($task, 'id', 'title'),['class'=>'form-control']) ?>

    <?= $form->field($model, 'reminder_time')->widget(DateTimePicker::classname(), [
							'options' => ['placeholder' => 'Select time','id' => 'timepicker'],
							 'pluginOptions' => [
								 
									],
								
								]) 
		?>

    <?= $form->field($model, 'notes')->textarea(['maxlength' => true]) ?>
	
	
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
