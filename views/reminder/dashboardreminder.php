<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datetime\DateTimePicker;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Reminder */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
#reminderloader,#reminderloader1{
display: none;
}
</style>

<?php Pjax::begin(['id' => 'reminderformpjax']) ?>
<div class="reminder-form">

  	<?php $form = ActiveForm::begin(['action' =>['reminder/create'],'enableAjaxValidation' => false,'options' => ['id' => 'reminderdashboardform']]); ?>
  	<?= Html::label('Select Task', 'reminder[task]', ['class' => 'control-label']) ?>
  	
	<?= Html::dropDownList('reminder[task]',null, ArrayHelper::map($task, 'id', 'title'),['class'=>'form-control','label'=>'test']) ?>
	
	<?= $form->field($model, 'reminder_time')->widget(DateTimePicker::classname(), [
							'options' => ['placeholder' => 'Select time','id' => 'timepicker'],
							 'pluginOptions' => [
								 
									],
								
								]) 
		?>

    <?= $form->field($model, 'notes')->textInput(['maxlength' => true]) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<span id="reminderbuttonText">Create</span> <img id="reminderloader" src="images/45.gif" " /> <span id="reminderloader1"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-danger' : 'btn btn-danger','id'=>'remindersubmit_id']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php Pjax::end(); ?>

<?php 
$jsDashboardReminder = <<<JS

$('#reminderdashboardform').on('beforeSubmit', function (e) {
	$('#reminderbuttonText').hide();
 	$('#reminderloader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#reminderloader').hide();
   if(result=='sent'){
	   
	   $(document).find('#reminderloader1').html(result).show();
	   
	   $(document).find('#flash').append(result).show();
	   
	 
	   $(document).find('#reminderdashboardform').trigger('reset');
	   $("html, body").delay(200).animate({
        scrollTop: $('#flash').offset().top
		
    }, 2000);
	 	$.pjax.reload({container:"#reminder_reload",async: false}); 
	  
    
    }else{
    $(document).find('#reminderloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#reminderloader').hide();
	$(document).find('#reminderloader1').hide();
	$(document).find('#reminderbuttonText').show();
	
	}, 5000);
    return false;
    
    
    
});
JS;
 
$this->registerJs($jsDashboardReminder);
?>
