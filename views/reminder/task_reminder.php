<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DateTimePicker;
use app\models\Reminder;
use app\models\TaskReminder;

/* @var $this yii\web\View */
/* @var $model app\models\Reminder */
/* @var $form yii\widgets\ActiveForm */
$taskModel = new TaskReminder();
$model = new Reminder();
?>

<div class="reminder-form">

  	<?php 
	
	$form = ActiveForm::begin(['action' =>['reminder/create-from-task'], 'enableAjaxValidation' => false, 'options' => ['id' => 'reminder'.$forTask]]); ?>
	
    <?= $form->field($model, "reminder_time")->widget(DateTimePicker::classname(), [
																					//'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
																					'size' => 'sm',
																					'pickerButton' => ['icon' => 'time'],
																					'removeButton' => false,
																					'options' => [
																								'placeholder' => 'Select time',
																								'id' => 'timepicker'
																					],
																					'pluginOptions' => [
																								'autoclose' => true,
																										'todayHighlight' => true,
		'todayBtn' => true,
																								
																					],
																					'options' => [
																								'id' => 'reminder'.$forTask,
]
																					]); 
	?>

    <?= $form->field($model, 'notes')->textarea(['maxlength' => true]); ?>
    <?= $form->field($taskModel, 'task_id')->hiddenInput(['maxlength' => true,'value' => $forTask]); ?>
    
    <div class="form-group">
        <?= Html::submitButton(Yii::t('reminder', 'create'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$jsDashboardReminder = <<<JS

$('form[id^="reminder"]').on('beforeSubmit', function (e) {


	$('#reminderbuttonText').hide();
 	$('#reminderloader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#reminderloader').hide();
   if(result=='sent'){
	   alert(1);
	   $(document).find('#reminderloader1').html(result).show();
	   
	   $(document).find('#flash').append(result).show();
	   
	 
	   $(document).find('#reminderdashboardform').trigger('reset');
	   $("html, body").delay(200).animate({
        scrollTop: $('#flash').offset().top
		
    }, 2000);
	 	$.pjax.reload({container:"#reminder_reload",async: false}); 
	  
    
    }else{
	alert(0);
    $(document).find('#reminderloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	$(document).find('#reminderloader').hide();
	$(document).find('#reminderloader1').hide();
	$(document).find('#reminderbuttonText').show();
	
	}, 5000);
    return false;
    
    
    
});
JS;
 
$this->registerJs($jsDashboardReminder);
?>

