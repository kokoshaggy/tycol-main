<?php
use app\models\Folder;

use yii\widgets\Pjax;
use app\controllers\ProjectController as project;
use yii\helpers\Html;
use app\models\FolderTask as userName;



/* @var $this yii\web\View */


//$this->title = 'My Yii Application';
?>
<?php $i=1; foreach($viewTask as $key => $value){ ?>
<? if($value['owner'] == Yii::$app->user->identity->id){ ?>
	<div class="panel box box-danger" style="border:none !important">
		<div class="box-header" style="background:#fff; font-size: 14px; font-family: candara; font-weight:bold;color:#555">
			<button  class="fa fa-caret-down test1" data-number="<?=$i;?>" style="float: right; color:#001a23 !important;font-size: 14px !important"></button>
				<span style="float:left; color: #001a23">
					<?= $value['title']; ?>  	
				</span><br />
				<span style="color: #ccc; font-size: 16px; font-family: gabriola">Seen 09:08AM|Today</span>
				<span id="assigntotimer2<?= $i?>" style="float:right;"></span>
		

		</div>
		<div id="task2<?= $i; ?>" style = "display: none; font-family: candara">
			<div class="box-body">
				<?= $value['details']; ?>
			</div>
		</div>
	</div>
<? 
$assignToDuedate = strtotime($value['due_date']);
$assignTo = date("M d, Y g:i ", $assignToDuedate);
?>


<? $i++;}} ?>

<?php 
$indexJs = <<<JS
$('.test1').each(function(){
	$(this).click(function(){
		$('#task2'+$(this).data('number')).slideToggle();

		if($(this).hasClass('fa-caret-down')){
				$(this).removeClass('fa-caret-down').addClass('fa-caret-up');
			} else {
				$(this).removeClass('fa-caret-up').addClass('fa-caret-down');
			}
		})
	})
JS;
?>
			


 
