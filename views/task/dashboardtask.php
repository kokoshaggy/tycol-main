<style>

/* IT IS ESSENTIAL ALL THIS INLINE CSS ARE MOVED TO DEDICATED CSS FOR THEMES */
.panel-group {
	border-bottom: 1px solid #ccc !important;
	border-radius:10px !important;
}

.box-header {
	background:#fff;
	font-size: 14px;
	font-family: candara;
	font-weight:bold;
	color:#555;
}

.fa.test {
	float: right !important;
	color:#001a23 !important;
	font-size: 14px !important;
}

#task-title {
	float: left; 
	color: #001a23;
	margin-right: 5px;
}

#task-assigned-by {
	font-size: 12px;
	color: #847777;
}

.task-assigned-to-timer {
	float: right;
}

.task-detals {
	display: none;
	font-family: candara;
}

</style>

<?php
use app\models\Folder;

use yii\widgets\Pjax;
use app\controllers\ProjectController as project;
use yii\helpers\Html;
use app\models\FolderTask as userName;



/* @var $this yii\web\View */


//$this->title = 'My Yii Application';
?>

<?php $i=1; 
	foreach($viewTask as $key => $currentTask) { 
		if($currentTask['assigned_to'] == Yii::$app->user->identity->id) { ?>
			<div class="panel-group" >
				<div class="box-header">
					<div >
						<button  class="fa fa-caret-down test" data-number="<?= $i; ?>"></button>
						<span id="task-title" >
							<?= $currentTask['title']; ?>  
						</span>
						<span id="task-assigned-by" >
							(<?='Assinged by: ' . userName::getUsername($currentTask['owner'])->fullName;?>)
						</span>
						<span id="assigntotimer<?= $i?>" class="task-assigned-to-timer" ></span>
					</div>

				</div>
				<div id="task<?= $i; ?>" class="task-detals" >
					<div class="panel-body" >
						<?= $currentTask['details']; ?>
					</div>
				</div>
			</div>
			<? 
			$assignToDuedate = strtotime($currentTask['due_date']);
			$assignTo = date("M d, Y g:i ", $assignToDuedate);
			$i++;
		} 
	} ?>

<?php
$indexJs = <<<JS
$('.test').each(function(){
	$(this).click(function(){
		$('#task'+$(this).data('number')).slideToggle();

		if($(this).hasClass('fa-caret-down')){
				$(this).removeClass('fa-caret-down').addClass('fa-caret-up');
			} else {
				$(this).removeClass('fa-caret-up').addClass('fa-caret-down');
			}
		})
	})
JS;
		
?>

 
