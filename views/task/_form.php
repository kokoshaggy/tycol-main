<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\UserDb as users;
use app\models\Task;
use app\models\StatusType as status;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
$model = new Task;
?>

<div class="task-form">

   <?php $form = ActiveForm::begin(['action' =>['task/createfoldertask'],'enableAjaxValidation' => false,'options' => ['id' => 'task']]); ?>
   
	<?= $form->field($folderTask, 'folder_id')->hiddenInput(['maxlength' => true,'value'=>$folderRef]) ?>
	
	<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'details')->textarea(['maxlength' => true]) ?>
	
	
	<?= $form->field($model, 'assigned_to')->dropDownList(ArrayHelper::map(users::find()->all(), 'id', 'username')) ?>
	
    <?= $form->field($model, 'due_date')->widget(DateTimePicker::classname(), [
							'options' => ['placeholder' => 'Select time','id' => 'timepicker'],
							 'pluginOptions' => [
								 
									],
								
								]) 
		?>

	
	
	<?= $form->field($model, 'status_id')->dropDownList(ArrayHelper::map(status::fetchAllUsers('task'), 'id', 'status_title')) ?>

    <?= Html::hiddenInput('folderref[folderref]', $folderRef); ?>
	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
