
<?php
use yii\widgets\Pjax;
use yii\helpers\Html;
use app\models\FolderTask;

?>

<?php $i = 1; $self_assigned = false;
	foreach ( $viewTask as $key => $currentTask ) { ?>
		<div class="panel-group" >
			<div class="box-header">
				<div>
					<button  class="fa fa-caret-down test" data-number="<?= $i; ?>"></button>
					<span id="task-title">
						<?= $currentTask['title']; ?>  
					</span>
					<span id="task-assigned-by">
						<?php 
							if ( $currentTask['owner'] == $currentTask['assigned_to'] && $currentTask['assigned_to'] == Yii::$app->user->identity->id  ) {
								echo 'SELF ASSIGNED';
								$self_assigned = true;
							} else {
								echo '(Assinged by: ' . FolderTask::getUsername($currentTask['owner'])->fullName . ')';
							}
						?>
					</span>
					<span id="assigntotimer<?= $i?>" class="task-assigned-to-timer" ></span>
				</div>
			</div>
			<div id="task<?= $i; ?>" class="task-detals" >
				<div class="panel-body" >
					<?= $currentTask['details']; ?>
				</div>
			</div>
			<div>
				<span id="assigned-to" > 
					<?php
						if ( !$self_assigned ) {
							if ( $currentTask['assigned_to'] == Yii::$app->user->identity->id ) {
								echo 'Assigned to me';
							} else {
								echo 'Assigned to ' . FolderTask::getUsername($currentTask['assigned_to'])->fullName;
							}
						}
					?>
				</span>
				<span id="due-date" >
					<?php 
						$assignToDuedate = strtotime($currentTask['due_date']);
						$assignTo = date("M d, Y g:i ", $assignToDuedate);
						echo "Due Date {$assignTo}";
					?>
				</span>
				<span id="task-reminder">
					<?php echo $this->render('/reminder/task_reminder', ['forTask' => $currentTask['id'] ]); ?>
				</span>
			</div>
		</div>
		<? 
			$i++;
	} 	?>

<?php
$indexJs = <<<JS
$('.test').each(function() {
	$(this).click(function() {
		$('#task'+$(this).data('number')).slideToggle();

		if($(this).hasClass('fa-caret-down')) {
			$(this).removeClass('fa-caret-down').addClass('fa-caret-up');
		} else {
			$(this).removeClass('fa-caret-up').addClass('fa-caret-down');
		}
	})
})
JS;
		
?>

 
