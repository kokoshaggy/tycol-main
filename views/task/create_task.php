<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\UserDb as Users;
use app\models\Task;
use app\models\Folder;
use app\models\StatusType as status;
use kartik\datetime\DateTimePicker;
use yii\widgets\Pjax;
use kartik\widgets\Select2

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
//$model = new Task;
?>
<style>
	option:nth-child(1){ 
		color: red;
	}
	
	.btn-basic{
		background:#ccc;
	}
</style>
<div class="task-form">

   <?php $form = ActiveForm::begin(['action' =>['task/dashboard-new-create-task'], 'enableAjaxValidation' => false, 'options' => ['id' => 'taskdashboardform']]); ?>

	<?= $form->field($folderTask, 'folder_id')->widget(Select2::classname(), [
																			'data' => ArrayHelper::map(Folder::find()->all(), 'id', 'tycDescription'),
																			'size' => Select2::MEDIUM,
																			'options' => [
																				'placeholder' => 'Select a Folder ...',
																				'multiple' => true, 
																				'class' => 'form-control',
																				'id' => 'folder'
																			],
																			'pluginOptions' => [
																				'allowClear' => true
																			],
																		]);
	?>
	
	<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'details')->textarea(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'assigned_to')->widget(Select2::classname(), [
																		'data' => ArrayHelper::map(Users::find()->all(), 'id', 'fullName'),
																		'size' => Select2::MEDIUM,
																		'options' => [
																			'placeholder' => 'Select a user ...',
																			'class' => 'form-control',
																			'id' => 'assigned_to'
																		],
																		'pluginOptions' => [
																			'allowClear' => true
																		],
																	]);
	?>
	
	<?= $form->field($model, 'due_date')->widget(DateTimePicker::classname(), [
																			'options' => [
																				'placeholder' => 'Select time',
																				'id' => 'Tasktimepicker'
																			],
																			'pluginOptions' => [
																				 
																			],
																]); 
	?>
	
	<?= $form->field($model, 'status_id')->dropDownList(ArrayHelper::map(status::fetchAllUsers('task'), 'id', 'status_title')) ?>

    
    <div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? '<span id="buttonText">Submit</span> <img id="loader" src="images/45.gif" " /> <span id="loader1"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic' : 'btn btn-basic','id'=>'submit_id']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
$jsDashboardTask = <<<JS

$('#taskdashboardform').on('beforeSubmit', function (e) {
	$('#buttonText').hide();
 	$('#loader').show();
    var \$form = $(this);
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result) {
	
		$(document).find('#loader').hide();
		if(result=='sent'){
		   
			$(document).find('#loader1').html(result).show();
			$(document).find('#flash').html(result).show();
		   
		 
			$(document).find('#taskdashboardform').trigger('reset');
			$("html, body").delay(200).animate( {
				scrollTop: $('#flash').offset().top
			}, 2000);
			$.pjax.reload({container:"#task_reload",async: false}); 
			$.pjax.reload({container:"#reminder_reload",async: false}); 
		} else {
			$(document).find('#loader1').html(result).show();
		}
	
    }).fail(function(){
		console.log('Server Error');
    });
	
	setTimeout(function() {
		$(document).find('#loader').hide();
		$(document).find('#loader1').hide();
		$(document).find('#buttonText').show();
	}, 5000);
    return false;
});
JS;
 
$this->registerJs($jsDashboardTask);
?>

