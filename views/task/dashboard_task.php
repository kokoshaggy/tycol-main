<?php
use yii\widgets\Pjax;
use yii\helpers\Html;
use app\models\FolderTask;
use app\models\Task;
use kartik\tabs\TabsX;

?>

<?php
	$assignedByMeTasks = [];
	$assignedToMeTasks = [];
	foreach ( $viewTask as $key => $userTask ) {
		if ( $userTask['assigned_to'] == Yii::$app->user->identity->id  ) {
			$assignedToMeTasks[] = $userTask;
		} elseif ( $userTask['owner'] == Yii::$app->user->identity->id  ) {
			$assignedByMeTasks[] = $userTask;
		}
	} 
	$tabs = [
				[
				'label' => 'Tasks',
				'items' => [
					
						[
							'label' => 'My tasks',
							'active' => true,
							'content' => $this->render('list_task', ['viewTask' => $assignedToMeTasks] ),
						],
						[
							'label' => 'Others tasks',
							'content' => $this->render('list_task', ['viewTask' => $assignedByMeTasks] ),
						],
						[
							'label' => 'New Task',
							'content' => $this->render('create_task', ['model' => new Task, 'folderTask' => new folderTask ] ),
						]
					]
				]
			];
		
		echo TabsX::widget([
			'items' => $tabs,
			'position' => TabsX::POS_ABOVE,
			'bordered' => true,
			'encodeLabels' => false
		]);


$indexJs = <<<JS
$('.test').each(function() {
	$(this).click(function() {
		$('#task'+$(this).data('number')).slideToggle();
		if($(this).hasClass('fa-caret-down')) {
			$(this).removeClass('fa-caret-down').addClass('fa-caret-up');
		} else {
			$(this).removeClass('fa-caret-up').addClass('fa-caret-down');
		}
	})
})
JS;
		
?>

 
