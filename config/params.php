<?php

return [
    'adminEmail' => 'admin@example.com',
	'language' => 'en-GB',
	'siteName' => 'Premux',
	'sourceLanguage' => 'en-GB',
	'fileUploadUrl' => '../web/uploads/',
	'maskMoneyOptions' => [
		//'prefix' => 'US$ ',
		'suffix' => '',
		'affixesStay' => true,
		'thousands' => ',',
		'decimal' => '.',
		'precision' => 2, 
		'allowZero' => false,
		//'allowNegative' => true,
    ]
];
