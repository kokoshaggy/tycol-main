<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
$db_backup = require(__DIR__ . '/db_backup.php');
$transport = require(__DIR__ . '/transport.php');

$config = [ 
	'name' => 'TycolMain',
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
	'defaultRoute' => 'site/login',
	'language' => 'en-GB',
	'sourceLanguage' => 'en-GB',
	'timeZone' => 'Africa/Lagos',
	'modules' => [
        'api' => [
            'class' => 'app\modules\api\Api',
        ],
		'settings' => [
			'class' => 'app\settings\Module',
		],
		'ti_xml_excel_parser' => [
			'class' => 'app\modules\ti_xml_excel_parser\Module',
		],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'N#onesense...1',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
			'class' => 'app\boffins_vendor\classes\UserComponent',
            'identityClass' => 'app\models\UserDb',
            'enableAutoLogin' => true,		//needs to be true to enable cookie based identity storage
			'enableSession' => true,
			'authTimeout' => 21600, //6 hours 
			'absoluteAuthTimeout' => 86400, //24 hours 
			'on '.\yii\web\User::EVENT_BEFORE_LOGIN => ['app\models\UserDb', 'handleBeforeLogin'],
			'on '.\yii\web\User::EVENT_AFTER_LOGIN => ['app\models\UserDb', 'handleAfterLogin'],
			'on '.\yii\web\User::EVENT_BEFORE_LOGOUT => ['app\models\UserDb', 'handleBeforeLogout'],
			'on '.\yii\web\User::EVENT_AFTER_LOGOUT => ['app\models\UserDb', 'handleAfterLogout'],
        ],
		'authManager' => [
			'class' => 'yii\rbac\PhpManager',
        ],        
		'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            //'useFileTransport' => false,
			'transport' => $transport,
        ],
		'formatter' => [
			'class' => 'yii\i18n\Formatter',
			'thousandSeparator' => ',',
			'decimalSeparator' => '.',
			'dateFormat' => 'php: dd/MM/yyyy',
			'datetimeFormat' => 'php: dd/MM/yyyy H:i:s',
			'defaultTimeZone' => 'Africa/Lagos',
		],
		'settingscomponent' => [
        	'class' => 'app\settings\components\SettingsComponent',
    	],
		'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages', // if advanced application, set @frontend/messages
                    'sourceLanguage' => 'en-GB',
					'forceTranslation'=> true,
                    'fileMap' => [
                    //'main' => 'main.php',
                    ],
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'db_backup' => $db_backup,
        /*'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
				'class' => 'yii\rest\UrlRule',
                'controller' => 'api',
            ],
        ],*/
        
    ],
	'as beforeRequest' => [
		'class' => 'yii\filters\AccessControl',
		'rules' => [
			[
				'allow' => true,
				'controllers' =>['site'],
				'actions' => ['login'],
			],
			[
				'allow' => true,
				'controllers' =>['api/project', 'api/folder', 'ti_xml_excel_parser/xml-excel'],
			],
			[
				'allow' => true,
				'roles' => ['@'],
			],
			/*[
				'allow' => true,
				'roles' => ['?'],
			],*/
		],
		'denyCallback' => function () {
			$comingFrom = ['project/view'];
			$comingFrom = $comingFrom[0] == 'site/login' || empty($comingFrom[0]) ? [Yii::$app->defaultRoute] : $comingFrom;
			Yii::$app->request->isPjax || Yii::$app->request->isAjax ? Yii::$app->session->set( 'comingFrom', [Yii::$app->defaultRoute] ) : Yii::$app->session->set( 'comingFrom', $comingFrom );
			return Yii::$app->response->redirect(['site/login']);
		},
	],

    'params' => $params,
	'aliases' => [
		'@topideaExcelParser' => 'app\modules\ti_xml_excel_parser',
		'@topideaExcelParserModels' => 'app/modules/ti_xml_excel_parser/models',
	],

];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '169.159.115.6', '169.159.89.61', '154.118.77.215', '169.159.87.104', '169.159.98.62', '160.152.38.70'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '154.118.96.97', '169.159.89.61', '169.159.87.104', '169.159.98.62'],
    ];
}

return $config;
