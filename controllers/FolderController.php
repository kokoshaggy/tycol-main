<?php

namespace app\controllers;

use Yii;
use app\models\Folder;
use app\models\FolderTask;
use app\models\FolderSearch;
use app\models\Invoice;
use app\models\Receivedpurchaseorder;
use app\models\Correspondence;
use app\models\Order;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Project;
use app\models\Payment;
use app\models\PaymentSource;
use app\models\Corporation;
use app\models\Component as component;
use app\models\FolderComponent as folderComponent;
use app\models\Task as task;
use app\models\Remark as Remarks;
use app\boffins_vendor\classes\BoffinsBaseController;

/**
 * TmfolderController implements the CRUD actions for TmFolder model.
 */
class FolderController extends BoffinsBaseController
{
    public static $idss;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TmFolder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FolderSearch();
        $folder = new Folder();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $displayFolder =Folder::find()->asArray()->all();
		
        return $this->render('index', [
            //'searchModel' => $searchModel,
            //'dataProvider' => $dataProvider,
            'displayFolder' => $displayFolder,
			'folder' => $folder,
        ]);
    }

    /**
     * Displays a single TmFolder model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
		$folderModel = Folder::findOne(['id' => $id]);
		$folderComponent = folderComponent::countTotal($id);
		$remarks = new Remarks();
		$componetidarray = [];
		$remarksCont = $remarks->find()->where(['folder_id'=>$id])->count();
		
		foreach($folderComponent as $key => $value ){
			array_push($componetidarray, $value['component_id']);
			}
		
		$fetchComponents = component::findAll($componetidarray);
		$componetidArrayProjectSeperation = [];
		$componetidArrayProjectSeperation2 = $folderModel->getSubComponents('project') ;
		$componetidArrayInvoiceSeperation = [];
		$componetidArrayOrderSeperation = [];
		$componetidArrayRpoSeperation = [];
		$componetidArrayCorrespondenceSeperation = [];
		$componetidArrayPaymentSeperation =[];
		
		
		foreach($fetchComponents as $key => $value ){
			if($value['component_type'] == 'project'){
				array_push($componetidArrayProjectSeperation,$value['id']);
			} elseif($value['component_type'] == 'invoice'){
				array_push($componetidArrayInvoiceSeperation,$value['id']);
			} elseif($value['component_type'] == 'order'){
				array_push($componetidArrayOrderSeperation,$value['id']);
			} elseif($value['component_type'] == 'receivedpurchaseorder'){
				array_push($componetidArrayRpoSeperation,$value['id']);
			} elseif($value['component_type'] == 'correspondence'){
				array_push($componetidArrayCorrespondenceSeperation,$value['id']);
			} elseif($value['component_type'] == 'payment'){
				array_push($componetidArrayPaymentSeperation,$value['id']);
			}
		}
		// final output of all folder total components
		$totalProject = count(Folder::findOne(['id' => $id])->getSubComponents('project'));
		
		$totalInvoice = count(Folder::findOne(['id' => $id])->getSubComponents('invoice'));
		
		$totalOrder = count(Folder::findOne(['id' => $id])->getSubComponents('order')) ;
		
		$totalRpo = count(Folder::findOne(['id' => $id])->getSubComponents('receivedpurchaseorder')) ;
		
		$totalCorrespondence = count(Folder::findOne(['id' => $id])->getSubComponents('correspondence'));
		
        //$thfolder = Folder::get_folder_project($id);
		$totalPayment = count(Folder::findOne(['id' => $id])->getSubComponents('payment'));
		
		$totalPaymentSource = count(PaymentSource::find()->all());
		
        return $this->render('view', [
            //'projects' => $thfolder,
            'tyc_ref' => $folderModel,
            'id' =>$id,
			'totalProject' => $totalProject,
			'totalInvoice' => $totalInvoice ,
			'totalRpo' => $totalRpo,
			'totalOrder' => $totalOrder,
			'remarks' => $remarksCont,
			'totalCorrespondence' => $totalCorrespondence,
			'totalPayment' => $totalPayment,
			'totalPaymentSource' => $totalPaymentSource,
			'invoiceIdValues' => array_values($componetidArrayInvoiceSeperation),
			'orderIdValues' => array_values($componetidArrayOrderSeperation),
			'projectIdValues' => array_values($componetidArrayProjectSeperation),
			'rpoIdValues' => array_values($componetidArrayRpoSeperation),
			'correspondenceIdValues' => array_values($componetidArrayCorrespondenceSeperation),
			'paymentIdValues' => array_values($componetidArrayPaymentSeperation),
			'fetchRemarks' => $remarks->findAll(['folder_id'=>$id]),
            
        ]);
    }
	
	public function actionFolderprojectview($id)
    {  
        $thfolder = Folder::get_folder_project($id);
        return $this->renderAjax('/project/folderprojectview', [
            'projects' => $thfolder,
            'tyc_ref' =>$this->findModel($id),
            'id' =>$id,
        ]);
    }
	
	public function actionFolderview($id)
    {  
        $folder = $this->findModel($id);
        return $this->renderAjax('/folder/folderview', [
            'folder' => $folder,
        ]);
    }

    /**
     * Creates a new TmFolder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
	 
     */
	
    public function actionCreate()
    {
        $model = new Folder();

        if ($model->load(Yii::$app->request->post()) && $model->save() ) {
            $creat_folder = Yii::$app->session;
            $new_folder_instruction = Yii::$app->session;
            $creat_folder->setFlash('created_folder', 'You have successfully created a new folder with Tycol Base Reference  '.$model->tyc_ref);
            $new_folder_instruction->setFlash('instruction', 'Folder is empty,<br/> Start populatings the folder by cliking on any of the icons on the left panel.');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
			return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }
    
    
    public function actionDashboardcreatefolder()
    {
        $model = new Folder();
			return $this->renderAjax('create', [
                'model' => $model,
            ]);
        
    }
    

    /**
     * Updates an existing TmFolder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModelcolumn('id', $id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $updateflash = Yii::$app->session;
            $updateflash->setFlash('successfully_updated', 'You have successfully updated folder with Tycol Reference '. $model->tyc_ref . '.');
            return $this->redirect(['index', 'id' => $model->tyc_ref]);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TmFolder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the TmFolder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TmFolder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
    	if (($model = Folder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    
    protected function findExternalModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
     public function actionCreateproject($id)
	 {
        $project = new Project();
        $client_model = Corporation::get_corp_name();
        $supplier_model = Corporation::get_supp_name();
        return $this->renderAjax('createnewproject', [
                'client' => $client_model,
                'supplier' => $supplier_model,
                'id' => $id,
                'project' => $project,
            ]);
        
    }
    
    
     public function actionCreatenewproject()
	 {	//instantiating   all required model
        $model = new Project();
		$folderComponent = new folderComponent();
		$component = new component();
        //calling of required model methods
        $model1=Corporation::get_corp_name();
        $model2=Corporation::get_supp_name();
		//assinging static values to componet
		$component->component_type = 'project';
		 
		if($component->save(false)){
			$componentId = $component->id;
			$model->component_id = $componentId ;
			
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				$folderComponent->component_id = $componentId ;
				$post =Yii::$app->request->post('Project');
				$tycRef = $post['tyc_ref'];
				$folderComponent->tyc_ref = $tycRef;
				$folderComponent->save(false) ;
            	$creat_folder_project = Yii::$app->session;
            	$creat_folder_project->setFlash('created_successfully', 'You have successfully created a new project inside the folder with TYC_REF '.$tycRef.'.');
            	return $this->redirect(['project/view', 'id' =>$model->project_id]);
        	}
			} else {
            
            return $this->render('project/create', [
                'client' => $model1,
                'supplier' => $model2,
                'id'=>$id,
                'project' => $model,
                'folder' => new Folder,
            ]);
        }
    }
    
    
    public function actionViewinvoice($id)
    {        
		$invoice = Folder::get_folder_invoice($id);
        return $this->render('viewinvoice', [
            'invoices' => $invoice,
            'tyc_ref' =>$this->findModel($id),
            'id' =>$id,
        ]);
    }
	
	// used to display all task inside of a folder 
	public function actionFoldertask($id)
	{
		$task = new task;
		$folderTask = new Foldertask();
		$ownersTasks = $task->fetchAllTask('owner',$id, Yii::$app->user->identity->id) ;
		$assignedTo = $task->fetchAllTask('assigned_to',$id, Yii::$app->user->identity->id) ;
		 return $this->renderAjax('foldertask', [
			 'ownersTasks' => $ownersTasks,
			 'assignedTo' => $assignedTo,
			 'folderRef' => $id, 
			 'folderTask' => $folderTask,
            ]);
	}
	
	/***
	 *
	 */
	public function actionDisplayLatest()
	{
		$items = Folder::getDashboardItems(10);
        return $this->render('latest', [
            'folders' => $items,
			]);
	}
	
    
    protected function findModelcolumn($column,$id)
	{
		 if (($model = Folder::findOne([$column => $id,])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
