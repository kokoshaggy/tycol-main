<?php

namespace app\controllers;

use Yii;
use app\models\Corporation;
use app\models\CorporationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Email;
use app\models\Address;
use app\models\Telephone;
use app\models\Entity;
use  app\models\EmailEntity ;
use app\models\AddressEntity ;
use app\models\TelephoneEntity ;
use app\models\Client ;
use app\models\Supplier ;
use app\models\Person ;

/**
 * TmcorporationController implements the CRUD actions for Tmcorporation model.
 */
class CorporationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tmcorporation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CorporationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tmcorporation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
		]);
    }

    /**
     * Creates a new Tmcorporation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$corporation= new Corporation();
        $email = new Email();
        $address = new Address();
        $telephone = new Telephone();
        $entity = new Entity();
        //calling models for entities
        $email_entity = new EmailEntity ;
        $address_entity = new AddressEntity ;
        $telephone_entity = new TelephoneEntity ;

        if ($corporation->load(Yii::$app->request->post()) & $address->load(Yii::$app->request->post())  & 		$email->load(Yii::$app->request->post()) & $telephone->load(Yii::$app->request->post())) {
           
            
            if( $telephone->validate() & $address->validate()  & $email->validate() & $corporation->validate() ) {
				
				$this->createnewcorporation($entity,$corporation,
											$email,$address,$telephone,
											$email_entity,$address_entity,
											$telephone_entity);
				 $data = 'sent';
				//Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				echo $data;
            } else {
				$data = 'An error occured ';
				Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return $data;
            }
            
        }else{
			echo 'not available';
        }
    }
    
    public static function createnewcorporation($entity,$corporation,$email,$address,$telephone,$email_entity,$address_entity,$telephone_entity)
	{
		$entity->entity_type='corporation';
        if($entity->save(false)){
			$entity_id = $entity->id;
            $corporation->entity_id = $entity_id;
			$telephone->save(false);
			$address->save(false) ;
			$email->save(false) ;
			$corporation->save(false);
			$telephone_id = $telephone->id;
			$email_id = $email->id;
			$address_id = $address->id;
		   //give values to entities
			$email_entity->email_id = $email_id;
			$email_entity->entity_id = $entity_id;
			$address_entity->address_id = $address_id;
			$address_entity->entity_id = $entity_id;
			$telephone_entity->telephone_id = $telephone_id;
			$telephone_entity->entity_id = $entity_id;
			$email_entity->save(false) ;
			$address_entity->save(false) ;
			$telephone_entity->save(false) ;
			$post = Yii::$app->request->post('corporationhidden');
			$hidden = $post['hidden'];
			$modelSupplier = new Supplier;
			$modelClient = new Client;
			if($hidden == 'client'){
				$modelClient->corporation_id = $corporation->id; 
				$modelClient->save(false);
			} else {
				$modelSupplier->corporation_id = $corporation->id; 
				$modelSupplier->save(false);
			}
		} 
    }
        
   

    /**
     * Updates an existing Tmcorporation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$model = $this->findModel($id);
		$emailEntityId = EmailEntity::find()->where(['entity_id'=>$model->entity_id])->one();
		$personModel = Person::find()->where(['entity_id'=>$model->entity_id])->one();
		$telephoneEntityId = TelephoneEntity::find()->where(['entity_id'=>$model->entity_id])->one();
		$addressEntityId = AddressEntity::find()->where(['entity_id'=>$model->entity_id])->one();
        $emailModel = Email::findOne($emailEntityId->email_id);
        $addressModel = Address::findOne($addressEntityId->address_id);
        $telephoneModel =  Telephone::findOne($telephoneEntityId->telephone_id);
		
        if ($model->load(Yii::$app->request->post()) && $emailModel->load(Yii::$app->request->post()) && $telephoneModel->load(Yii::$app->request->post()) && $addressModel->load(Yii::$app->request->post()) && $model->validate() && $emailModel->validate() && $telephoneModel->validate() && $addressModel->validate() ) {
			$model->save(false);
			//$personModel->save(false);
			$emailModel->save(false);
			$telephoneModel->save(false);
			$addressModel->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
				'personModel' => $model,
				'emailModel' => $emailModel,
				'addressModel' => $addressModel,
				'telephoneModel' => $telephoneModel,
				'id' => $id,
            ]);
        }
    }

    /**
     * Deletes an existing Tmcorporation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Tmcorporation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tmcorporation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Corporation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
