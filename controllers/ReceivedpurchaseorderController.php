<?php

namespace app\controllers;

use Yii;
use app\models\Receivedpurchaseorder;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Client;
use app\models\Supplier;
use app\models\Currency;
use app\models\Folder;
use app\models\FolderComponent;
use yii\helpers\ArrayHelper;
use app\boffins_vendor\classes\BoffinsBaseController;

/**
 * ReceivedpurchaseorderController implements the CRUD actions for Receivedpurchaseorder model.
 */
class ReceivedpurchaseorderController extends BoffinsBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Receivedpurchaseorder models.
     * @return mixed
     */
    public function actionIndex()
    {	$model = new Receivedpurchaseorder();
		$findOneRpo = $model->find()->one();
        $dataProvider = new ActiveDataProvider([
            'query' => Receivedpurchaseorder::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
			'model' => $model,
			'findOneRpo' => $findOneRpo,
        ]);
    }
	
	
	public function actionRpolistview()
    {
        
        $model = new Receivedpurchaseorder;
		if(isset($_POST['option'])){
			$option = $_POST['option'];
			$hoverEffect = 'false';
			$dataProvider = $model->find()->where(['in','component_id',array_values(unserialize($option))])->all();
		} else {
			$hoverEffect = 'true';
			$dataProvider = $model->find()->all();
		}
       	
        return $this->renderAjax('rpolistview', [
            
            'dataProvider' => $dataProvider,
			'hoverEffect' => $hoverEffect,
			
        ]);
    }


    /**
     * Displays a single Receivedpurchaseorder model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
	
	public function actionRpoview($id)
    {
		if($id === 0){
			return $this->renderAjax('/nodata/nodataavailable',['message'=>'No RPO available']);
		} elseif($id == 0 and !is_string($id)){
			return $this->renderAjax('/nodata/nodataavailable',['message'=>'No RPO available']);
		}
		$model = $this->findModel($id);
        return $this->renderAjax('view', [
            'model' => $model,
			'subComponents' => $model->subComponents
        ]);
    }

    /**
     * Creates a new Receivedpurchaseorder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Receivedpurchaseorder();
		$folderComponent = new FolderComponent;
		$allFolders = ArrayHelper::map(Folder::find()->all(), 'tyc_ref', 'NameString');
		$suppliers = ArrayHelper::map(Supplier::find()->all(), 'id', 'NameString');
		$clients = ArrayHelper::map(Client::find()->all(), 'id', 'NameString');
		$currencies = ArrayHelper::map(Currency::find()->all(), 'id', 'currencyString');
		$value_settings = [
			'name' => 'masked-input',
			'clientOptions' => [
				'alias' => 'decimal',
				'digits' => 2,
				'digitsOptional' => false,
				'radixPoint' => '.',
				'groupSeparator' => ',',
				'autoGroup' => true,
				'removeMaskOnSubmit' => true,
			],
		];
		
		if(isset($_GET['id'])){
			$getId = $_GET['id'];
			if(!empty($model->itemType) && !empty($model->itemID)){
				$model->itemType = $model->itemType.',folder';
				$this->owner->itemID = $model->itemID.','.$getId;
			} else{
				$model->itemType = 'folder';
				$model->itemID = $getId;
			}
		}
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
			//$session = Yii::$app->session;
			//$session->setFlash('created_successfully', 'You have successfully created a new Received Purchase Order linked to the folder ' . $model->folders[0]->tyc_ref . '.');
            $data=[];
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$data['sent'] = 'sent';
			$data['message'] = 'New received purchase order was created with  received purchaseorder reference '.$model-> receivedpurchaseorder_reference ;
			return $data;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
				'allFolders' => $allFolders,
				'suppliers' => $suppliers,
				'clients' => $clients,
				'currencies' => $currencies,
				'value_settings' => $value_settings,
				'language' => $this->language,
				'folderComponent' => $folderComponent,
				'id' => $getId,
            ]);
        }
    }

    /**
     * Updates an existing Receivedpurchaseorder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$FolderComponent = new FolderComponent;//::find()->where(['component_id'=>$model->component_id])->one();
		$allFolders = ArrayHelper::map(Folder::find()->all(), 'folder_id', 'NameString');
		$suppliers = ArrayHelper::map(Supplier::find()->all(), 'id', 'NameString');
		$clients = ArrayHelper::map(Client::find()->all(), 'id', 'NameString');
		$currencies = ArrayHelper::map(Currency::find()->all(), 'id', 'currencyString');
		$value_settings = [
			'name' => 'masked-input',
			'clientOptions' => [
				'alias' => 'decimal',
				'digits' => 2,
				'digitsOptional' => false,
				'radixPoint' => '.',
				'groupSeparator' => ',',
				'autoGroup' => true,
				'removeMaskOnSubmit' => true,
			],
		];
		
		

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
            $data=[];
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$data['sent'] = 'sent';
			$data['message'] = 'Updated received purchase order  with  received purchaseorder reference '.$model-> receivedpurchaseorder_reference ;
			return $data;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
				'allFolders' => $allFolders,
				'suppliers' => $suppliers,
				'clients' => $clients,
				'currencies' => $currencies,
				'value_settings' => $value_settings,
				'language' => $this->language,
				'FolderComponent' => $FolderComponent,
            ]);
        }
    }

    /**
     * Deletes an existing Receivedpurchaseorder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return 'Deleted';
    }
	
	public function actionUndodelete($id)
    {
        $this->findModel($id)->undoDelete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Receivedpurchaseorder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Receivedpurchaseorder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Receivedpurchaseorder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
	 * CHANGES BY BOFFINS TEAM 
	 */
	
	public $language;		//added by Anthony
	
	//functions by Anthony
	public function init() 
	{
		Yii::$app->formatter->locale = 'en-GB';
		$this->language = array();
		$this->language['currenciesDropdownPrompt'] = "Please choose a currency";
		$this->language['clientsDropdownPrompt'] = "Select Client Name";
		$this->language['RPOTypesDropdownPrompt'] = "What kind of RPO is this?";
		$this->language['suppliersDropdownPrompt'] = "Select Supplier Name";
		$this->language['foldersDropdownPrompt'] = "Select a Folder";
		
	}
	
}
