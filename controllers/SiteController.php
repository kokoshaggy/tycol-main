<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\db\Expression;
use yii\helpers\Json;

//models
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Folder;
use app\models\Project;
use app\models\Invoice;
use app\models\Remark;
use app\models\Order;
use app\models\UserDb;
use app\models\Person;
use app\models\Corporation;
use app\models\Email;
use app\models\Address;
use app\models\Telephone;
use app\models\Entity;
use app\models\EmailEntity ;
use app\models\AddressEntity ;
use app\models\TelephoneEntity ;
use app\models\Client;
use app\models\Supplier;
use app\models\Reminder;
use app\models\Task;
use app\models\TaskReminder;
use app\models\Payment;
use app\models\PaymentSource;
use app\models\Currency;
use app\models\Receivedpurchaseorder;
use app\models\Correspondence;
use app\models\FolderTask;
use app\models\UserForm;
use app\boffins_vendor\classes\BoffinsBaseController;


class SiteController extends BoffinsBaseController {
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() 
	{
		$this->layout = 'indexdashboard';
		
		//Instantiating required models
        $folderModel = new Folder();
        $projectModel = new Project();
        $invoiceModel = new Invoice();
		$orderModel = new Order();
        $remarkModel = new Remark();
        $userModel = new UserDb();
        $corporationModel = new Corporation();
        $personModel = new Person();
        $email = new Email();
        $address = new Address();
        $telephone = new Telephone();
        $entity = new Entity();
		$client = new Client;
		$supplier = new Supplier;
        $emailEntity = new EmailEntity ;
        $addressEntity = new AddressEntity ;
        $telephoneEntity = new TelephoneEntity ;
		$invoiceModel = new Invoice; 
		$orderModel = new Order;
		$presentTime = new Expression('NOW()');
		$reminder = new Reminder();
		$taskReminder = new TaskReminder();
		$paymentModel = new Payment();
		$paymentSourceModel = new PaymentSource();
		$currenciesModel = new Currency();
		$rpoModel = new Receivedpurchaseorder();
		$correspondenceModel = new Correspondence();
		$folderTask = new FolderTask();
		$userForm = new UserForm;
		
        //Calling requied model methods 
		$getNewClien = $corporationModel->fetchNewClient();
		$getNewSupplier = $corporationModel->fetchNewSupplier();
        $fetchTotalFolders = Folder::find()->count();
        $fetchTotalProjects = $projectModel->allrecords();
        $fetchTotalInvoices = $invoiceModel->allrecords();
        $fetchTotalOrders = $orderModel->allrecords();
        $remaks = $remarkModel->remarksdash();
        $goto_folder_page = Url::to(['/folder/index']);
        $goto_project_page = Url::to(['/project/index']);
        $goto_invoice_page = Url::to(['/invoice/index']);
        $goto_order_page = Url::to(['/order/index']);
		$displayAllUsers = $userModel->find()->asArray()->all();
		$userId = Yii::$app->user->identity->id;
		$activeTask = Reminder::getActiveTask($presentTime,$userId,$userId);
		$task = Task::find()->select('id')->where(["assigned_to" =>$userId])
				->Where(['<','create_date',$presentTime])->asArray()->all();
		$taskArray = [];
		foreach ($task as $k =>$v) {
			array_push($taskArray,$v['id']);
		}
		$taskReminder = $taskReminder->find()->select('reminder_id')->where(['in','task_id',array_values($taskArray)])->asArray()->all();
		$reminderId = [];
		$dashboardFolders = Folder::getDashboardItems(5);
		
		foreach ($taskReminder as $k =>$v) {
			array_push($reminderId,$v['reminder_id']);
		}
		$userReminders = $reminder->findAll(array_values($reminderId));
		$viewTask = Task::selectTaskByStatus($userId, Task::TASK_COMPLETED, false);
		$value_settings = [
			'name' => 'masked-input',
			'clientOptions' => [
				'alias' => 'decimal',
				'digits' => 2,
				'digitsOptional' => false,
				'radixPoint' => '.',
				'groupSeparator' => ',',
				'autoGroup' => true,
				'removeMaskOnSubmit' => true,
			],
		];

        return $this->render('index',[
			'folders' => $dashboardFolders,
            'total_products' => $fetchTotalFolders,
            'total_projects' => $fetchTotalProjects,
            'total_invoices' => $fetchTotalInvoices,
			'total_order' => $fetchTotalOrders,
            'goto_folder_page' => $goto_folder_page,
            'goto_project_page' => $goto_project_page,
            'goto_invoice_page' => $goto_invoice_page,
            'goto_order_page' => $goto_order_page,
            'recent_remarks' => $remaks,
            'action' => 'user/create', //'dashboard' => '1'],
            'userModel' => $userModel,
            'personModel' => $personModel,
            'emailModel' => $email,
            'addressModel' => $address,
            'telephoneModel' => $telephone,
            'corporationModel' => $corporationModel,
			'newClient' => $getNewClien,
			'clientModel'=>$client,
			'newSupplier' => $getNewSupplier,
			'supplierModel'=>$supplier,
			'supplierHidden'=>'supplier',
			'clientHidden'=>'client',
			'displayAllUsers' => $displayAllUsers,
			'activeTask' =>  $activeTask,
			'reminder' => $reminder,
			'userReminders' => $userReminders,
			'task'=> $task,
			'taskReminder' =>$taskReminder,
			'viewTask' => $viewTask,
			'remarkModel' => $remarkModel,
			'paymentModel' => $paymentModel,
			'folderModel' => $folderModel ,
			'paymentSourceModel' => $paymentSourceModel ,
			'currenciesModel' => $currenciesModel,
			'rpoModel' => $rpoModel,
			'value_settings' => $value_settings,
			'correspondenceModel' => $correspondenceModel,
			'folderTask' => $folderTask,
			'userForm' => $userForm,
        ]);
       
    }

    
	/*public function actionIndex() 
	{
		if ( Yii::$app->request->get('newIndex') ) {
			$this->runactionNewIndex();
		} else {
			$this->runOLDactionIndex();
		}
	}*/
	
	public function actionNewIndex() {
		$this->layout = 'new_index_dashboard_layout';
		$dashboardFolders = Folder::getDashboardItems(5);
		$viewTask = Task::selectTaskByStatus(Yii::$app->user->identity->id, Task::TASK_COMPLETED, false);
        $remarks = Remark::latestRemarks();
		$value_settings = [ //move this to the view
			'name' => 'masked-input',
			'clientOptions' => [
				'alias' => 'decimal',
				'digits' => 2,
				'digitsOptional' => false,
				'radixPoint' => '.',
				'groupSeparator' => ',',
				'autoGroup' => true,
				'removeMaskOnSubmit' => true,
			],
		];
		return $this->render('new_index', [
			'folders' => $dashboardFolders,
			'viewTask' => $viewTask,
            'recent_remarks' => $remarks,
			'value_settings' => $value_settings,
        ]);
	}
	
	public function actionLogin() 
	{	
	
		$model = new LoginForm();
		$this->layout = 'loginlayout';
		$authenticated = false;

		if ( Yii::$app->request->get('testingEmergency') && YII_ENV_DEV ) {
			//incompleted backend hack. 
			$model->scenario = $model::SCENARIO_LOGIN;
			if ( $model->load(Yii::$app->request->post() ) ) {
				if ( $model->emergencyLogin() ) {
					$landingPage = ['site/index']; //isset(Yii::$app->session['comingFrom']) ? Yii::$app->session['comingFrom'] : Url::to(['/site/index']);
					return $this->redirect($landingPage);			
				} 
			} else {
				return $this->render('login', [
					'model' => $model,
				]);
			}
			//should be deleted before production 
		}
		
		if ( Yii::$app->request->get('testing') ) {
			//should be removed before production. 
			Yii::$app->session->destroy();
			Yii::$app->session->close();
			Yii::$app->session->open();
			Yii::$app->user->clearDeviceSessionAndCookieData( ['authenticateNewDevice'], ['deviceString', 'ds'] );
		}
		
		
		if ( isset(Yii::$app->session['authenticateNewDevice']) 
			&& Yii::$app->session['authenticateNewDevice'] === true ) {
			$model->scenario = $model::SCENARIO_LOGIN_NEW_DEVICE;
			if ( $model->load(Yii::$app->request->post()) ) { 
				if ( $model->login() ) {
					$authenticated = true;
				} else {
					Yii::$app->session->setFlash('error', 'Invalid login details.');
					return $this->render('authenticate_new_device', [
						'model' => $model,
					]);
				}
			} else {
				return $this->render('authenticate_new_device', [
					'model' => $model,
				]);			
			}
		} elseif ( isset(Yii::$app->session['newDevice']) 
					&& Yii::$app->session['newDevice'] === true ) {
			return $this->render('new_device', [
				'model' => $model,
			]);
		} else {
			$model->scenario = $model::SCENARIO_LOGIN;
			if ( $model->load(Yii::$app->request->post()) ) {
				if ( $model->login() ) {
					$authenticated = true;
				} elseif ( isset(Yii::$app->session['authenticateNewDevice']) 
							&& Yii::$app->session['authenticateNewDevice'] === true ) {
					return $this->render('new_device', [
						'model' => $model,
					]);
				} else {
					Yii::$app->session->setFlash('error', 'Invalid login details.');
					return $this->render('login', [
						'model' => $model,
					]);
				}		
			} else {
				return $this->render('login', [
					'model' => $model,
				]);			
			}
		}
		
		if ($authenticated) {
			$landingPage = ['site/index']; //isset(Yii::$app->session['comingFrom']) ? Yii::$app->session['comingFrom'] : Url::to(['/site/index']);
			return $this->redirect($landingPage);
		}
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
		
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        }
		
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
	
	
	public function actionDependentdropdown() 
	{
		$project = new Project;
    	$out = [];
        $parents = Yii::$app->request->post('depdrop_all_params');
		
			$out = $project->find()->select(['project_id As id','pro_description  As name'])->where(['tyc_ref'=>$parents])->asArray()->all(); 
			echo Json::encode(['output'=>$out, 'selected'=>'1']);    
        	return;
	}
}
