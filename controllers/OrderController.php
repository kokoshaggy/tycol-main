<?php

namespace app\controllers;

use Yii;
use app\models\Order;
use app\models\Project;
use app\models\FolderComponent;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use app\boffins_vendor\classes\BoffinsBaseController;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends BoffinsBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
		$model = new Order();
		$findOneOrder = $model->find()->orderBy(['order_number'=>SORT_ASC])->one();
        $dataProvider = Order::find()->all();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
			'model' => $model,
			'findOneOrder' => $findOneOrder,
        ]);
    }
	
	public function actionOrderlistview()
    {
		$model = new Order();
		if(isset($_POST['option'])){
			$option = $_POST['option'];
			$hoverEffect = 'false';
			$dataProvider = Order::find()->where(['component_id'=>array_values(unserialize($option))])->all();
		} else {
			$hoverEffect = 'true';
			$dataProvider = Order::find()->all();
		}
        return $this->renderAjax('orderlistview', [
            'dataProvider' => $dataProvider,
			'hoverEffect' => $hoverEffect,
			'model' => $model,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		if($id==0){
			return $this->renderAjax('/nodata/nodataavailable');
		}
		
        return $this->render('view', [
            'model' => Order::find()->select('order_number','folder_id','supplier_id','supplier_ref','order_value','issue_date','supplier_completion_date','order_status','order_file')->where(['order_number'=>$id])->asArray()->one(),
        ]);
		
		
    }
	
	
	public function actionOrderview($id)
    {
		if($id == 0 or $id === 0){
			return $this->renderAjax('/nodata/nodataavailable',['message'=>'No Order available']);
		}
		$getSpecificInvoice = $this->findModel($id);
		
        return $this->renderAjax('view', [
            'model' => $getSpecificInvoice,
            'subComponents' => $getSpecificInvoice->subComponents,
			]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();
		if(isset($_GET['id'])){
			$getId = $_GET['id'];
			if(!empty($model->itemType) && !empty($model->itemID)){
				$model->itemType = $model->itemType.',folder';
				$this->owner->itemID = $model->itemID.','.$getId;
			} else{
				$model->itemType = 'folder';
				$model->itemID = $getId;
			}
		}
		if( $model->load(Yii::$app->request->post()) ) {
			if ( $model->save() ) {
				if ( Yii::$app->request->isAjax ) {
					Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					$data = [];
					$data['success'] = true;
					$data['message'] = 'New order was created with order number  '.$model->order_number  ;
					return $data;
				} else {
					return $this->redirect(['view', 'id' => $model->order_number]);
				}			
			} else {
				if ( Yii::$app->request->isAjax ) {
					$data['success'] = false;
					$data['message'] = 'Unable to create order - order number:  '.$model->order_number  ;
					return $data;
				} else {
					return $this->render('create', [
						'model' => $model,
					]);
				}
			}
		} else {
			if ( Yii::$app->request->isAjax ) {
				return $this->renderAjax('create', [
					'model' => $model,
				]);
			} else {
				
			}
		}
    }

	
	public function actionAjaxValidateForm()
	{
        $model = empty( Yii::$app->request->get('id') ) ? new Order(): Order::findOne(['order_number' =>  Yii::$app->request->get('id')]) ;
		$model->setComponentID = false; //this is essential to ensure that the component behavior does not create a new component (until before insert is created)
		$formErrors = false;
		Yii::trace('Begin Ajax Validation (Orders)');
		if ( Yii::$app->request->isAjax ) {
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		}
		
		if( $model->load(Yii::$app->request->post()) ) { 
			$formErrors = ActiveForm::validate($model);
			if ( empty($formErrors) && $formErrors !== false  ) {
				Yii::trace('Ajax validation passed (Orders)');
				return true;
			}
		}
		Yii::trace( 'Ajax Validation failed' );
		return $formErrors;
	}
    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$FolderComponent = FolderComponent::findOne(['component_id'=>$model->component_id]);
		/*if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}*/
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $data=[];
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$data['success'] = true;
			$data['message'] = 'Updated order with order number  '.$model->order_number  ;
			return $data;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
				'FolderComponent' => $FolderComponent
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
