<?php

namespace app\controllers;

use Yii;
use app\models\Payment;
use app\models\PaymentSource;
use app\models\Corporation;
use app\models\PaymentComponent;
use app\models\Folder;
use app\models\FolderComponent;
use app\models\Invoice;
use app\models\Order;
use app\models\Component;
use app\models\Receivedpurchaseorder;
use app\models\Project;
use app\models\Currency;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use app\boffins_vendor\classes\BoffinsBaseController;


/**
 * PaymentController implements the CRUD actions for Payment model.
 */
class PaymentController extends BoffinsBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Payment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Payment;
		$paymentSource = PaymentSource::displayPaymentSource();
		$folder = new Folder;
		$corporation = Corporation::displayAllCorporation();
		$folderArray = $folder->find()->all();
		$currencies = ArrayHelper::map(Currency::find()->all(), 'id', 'currencyString');
		$findOnePayment = $model->find()->orderBy(['id'=>SORT_ASC])->one();

        return $this->render('index', [
           'model' =>  $model,
			'paymentSource' => $paymentSource,
			'folder' => $folderArray,
			'corporation' => $corporation,
			'currencies' => $currencies,
			'language' => $this->language,
			//'folderPayment' => $model->fetchFolderPayment('1349'),
			'findOnePayment' => $findOnePayment,
        ]);
    }
	
	public function actionPaymentlistview()
    {
        $model = new Payment;
		if(isset($_POST['option'])){
			$option = $_POST['option'];
			$hoverEffect = 'false';
			$payment = $model->find()->where(['in','component_id',array_values(unserialize($option))])->all();
		} else {
			$hoverEffect = 'true';
			$payment = $model->find()->all();
		}
		
		$paymentSource = PaymentSource::displayPaymentSource();
		$folder = new Folder;
		$corporation = Corporation::displayAllCorporation();
		$folderArray = $folder->find()->all();
		$currencies = ArrayHelper::map(Currency::find()->all(), 'id', 'currencyString');
		

        return $this->renderAjax('paymentlistview', [
           'payment' =>  $payment,
			'model' => $model,
			'paymentSource' => $paymentSource,
			'folder' => $folderArray,
			'corporation' => $corporation,
			'currencies' => $currencies,
			'language' => $this->language,
			'hoverEffect' => $hoverEffect,
			//'folderPayment' => $model->fetchFolderPayment('1349'),
			//'test' => $payment->componentTyc,
        ]);
    }
	
	public function actionFolderindex()
    {
        $model = new Payment;
		$paymentSource = PaymentSource::displayPaymentSource();
		$folder = new Folder;
		$corporation = Corporation::displayAllCorporation();
		$folderArray = $folder->find()->all();
		$currencies = ArrayHelper::map(Currency::find()->all(), 'id', 'currencyString');
		

        return $this->renderAjax('folderindex', [
           'model' =>  $model,
			'paymentSource' => $paymentSource,
			'folder' => $folderArray,
			'corporation' => $corporation,
			'currencies' => $currencies,
			'language' => $this->language,
			'folderPayment' => $model->fetchFolderPayment('1349'),
        ]);
    }

    /**
     * Displays a single Payment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
	public function actionPaymentview($id)
    {
		if($id == 0 or $id === 0){
			return $this->renderAjax('/nodata/nodataavailable',['message'=>'No Payment available']);
		}
		$model = $this->findModel($id);
        return $this->renderAjax('paymentview', [
            'model' => $model,
			'subComponents' => $model->subComponents
        ]);
    }
	
	public function actionCreate()
    {
        $model = new Payment;
		$paymentSource = PaymentSource::displayPaymentSource();
		$folder = new Folder;
		$corporation = Corporation::displayAllCorporation();
		$folderArray = $folder->find()->all();
		$currencies = Currency::find()->all();
		$valueSettings = [
			'name' => 'masked-input',
			'clientOptions' => [
				'alias' => 'decimal',
				'digits' => 2,
				'digitsOptional' => false,
				'radixPoint' => '.',
				'groupSeparator' => ',',
				'autoGroup' => true,
				'removeMaskOnSubmit' => true,
			],
		];
		

        return $this->renderAjax('create', [
			'model' =>  $model,
			'paymentSource' => $paymentSource,
			'folder' => $folderArray,
			'corporation' => $corporation,
			'currencies' => $currencies,
			'language' => $this->language,
			//'folderPayment' => $model->fetchFolderPayment('1349'),
			'valueSettings' => $valueSettings,
        ]);
    }

    /**
     * Creates a new Payment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate1()
    {
        $model = new Payment();
		if(isset($_GET['id'])){
			$getId = $_GET['id'];
			if(!empty($model->itemType) && !empty($model->itemID)){
				$model->itemType = $model->itemType.',folder';
				$this->owner->itemID = $model->itemID.','.$getId;
			} else{
				$model->itemType = 'folder';
				$model->itemID = $getId;
			}
		}
        if ($model->load(Yii::$app->request->post()) && $model->save() ) {
			$data=[];
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$data['sent'] = 'sent';
			$data['message'] = 'New payment was created with payment id  '.$model->id  ;
			return $data;

        } else {
            echo "something went wrong ";
        }
    }
	
	public function actionCreateNew()
	{
		$paymentModel = new Payment;
		if ($paymentModel->load(Yii::$app->request->post()) && $paymentModel->save() ) {
			$session = Yii::$app->session;
			//$session->setFlash('created_successfully', 'You have successfully created a new User' . $paymentModel->first_name . ' ' . $user->surname);
			//return $this->redirect(['view', 'id' => $user->id]);
		} else {
			die('fails');
		}

		
	}

    /**
     * Updates an existing Payment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$paymentSource = PaymentSource::displayPaymentSource();
		$folder = new Folder;
		$corporation = Corporation::displayAllCorporation();
		$folderArray = $folder->find()->all();
		$currencies = Currency::find()->all();
		$valueSettings = [
			'name' => 'masked-input',
			'clientOptions' => [
				'alias' => 'decimal',
				'digits' => 2,
				'digitsOptional' => false,
				'radixPoint' => '.',
				'groupSeparator' => ',',
				'autoGroup' => true,
				'removeMaskOnSubmit' => true,
			],
		];

        if ($model->load(Yii::$app->request->post())) {
			$model->save();
			$data=[];
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$data['sent'] = 'sent';
			$data['message'] = 'Updated payment  with payment id  '.$model->id  ;
			return $data;

        } else {
            return $this->renderAjax('update', [
                'model' =>  $model,
				'paymentSource' => $paymentSource,
				'folder' => $folderArray,
				'corporation' => $corporation,
				'currencies' => $currencies,
				'language' => $this->language,
				'valueSettings' => $valueSettings,
            ]);
        }
    }

    /**
     * Deletes an existing Payment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        //return $this->redirect(['index']);
    }

    /**
     * Finds the Payment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public $language;		//added by Anthony
	
	//functions by Anthony
	public function init() {
		$this->language = [];
		$this->language['currencyDropDownPrompt'] = "Choose a Currency";
	}
	
	public function actionSubcat1() //what exactly does this 'Subcat1???' action mean or do???
	{
		$folder = new Folder;
		$project = new Project;
		$invoice = new Invoice;
		$order = new Order;
		$rpo = new Receivedpurchaseorder;
    	$out = [];
		$post = Yii::$app->request->post('depdrop_all_params');
		$mainpost = $post['componentname'];
        $parents = $mainpost;
		if($parents == 1){
			$out = $folder->find()->select(['tyc_ref As id','tyc_ref  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$out, 'selected'=>'1']);    
        	return;
		}
		
		if($parents == 2){
			$out = $project->find()->select(['component_id As id','pro_description  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$out, 'selected'=>'1']);    
        	return;
		}
		
		if($parents == 3){
			$out = $invoice->find()->select(['component_id As id','description  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$out, 'selected'=>'1']);    
        	return;
		}
		
		if($parents == 4){
			$out = $order->find()->select(['component_id As id','order_number  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$out, 'selected'=>'1']);    
        	return;
		}
		
		if($parents == 5){
			$out = $rpo->find()->select(['component_id As id','description  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$out, 'selected'=>'1']);    
        	return;
		}
    	//echo Json::encode(['output'=>'', 'selected'=>'']);
	}
	
	/***
	 * Action to retrieve a list of folders or components, based on the item sent via post to the action/function 
	 */ 
	public function actionListItems() 
	{
    	$result = [];
		$post = Yii::$app->request->post('depdrop_all_params');
		$parents = $post['componentname'];
		if ($parents == 'folder') {
			$folder = new Folder;
			$result = $folder->find()->select(['tyc_ref As id','tyc_ref  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$result, 'selected'=>'1']);    
        	return;
		}
		
		if ($parents == 'project') {
			$project = new Project;
			$result = $project->find()->select(['component_id As id','pro_description  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$result, 'selected'=>'1']);    
        	return;
		}
		
		if ($parents == 'invoice') {
			$invoice = new Invoice;
			$result = $invoice->find()->select(['component_id As id', 'description  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$result, 'selected'=>'1']);    
        	return;
		}
		
		if ($parents == 'order') {
			$order = new Order;
			$result = $order->find()->select(['component_id As id','order_number  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$result, 'selected'=>'1']);    
        	return;
		}
		
		if ($parents == 'rpo'){
			$rpo = new Receivedpurchaseorder;
			$result = $rpo->find()->select(['component_id As id','description  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$result, 'selected'=>'1']);    
        	return;
		}
    	//echo Json::encode(['output'=>'', 'selected'=>'']);
	}
	
	public function actionProd() {
    $out = [];
    if (isset($_POST['depdrop_parents'])) {
        $ids = $_POST['depdrop_parents'];
        $cat_id = empty($ids[0]) ? null : $ids[0];
        $subcat_id = empty($ids[1]) ? null : $ids[1];
        if ($cat_id != null) {
           
			$folder = new Folder;
			$project = new Project;
			$invoice = new Invoice;
			$order = new Order;
			$rpo = new Receivedpurchaseorder;
			$out = [];
			$post = Yii::$app->request->post('depdrop_all_params');
			$mainpost = $post['componentname'];
			$parents = $mainpost;
			if($cat_id == 1){
				$out = $folder->find()->select(['tyc_ref As id','tyc_ref  As name'])->where(['tyc_ref'=> $subcat_id])->asArray()->all(); 
				echo Json::encode(['output'=>$out, 'selected'=>isset($out[0]['id'])?$out[0]['id']:1]); 
			   	return;
			}

			if($cat_id == 2){
				$out = $project->find()->select(['tyc_ref As id','pro_description  As name'])->where(['component_id'=> $subcat_id])->asArray()->all(); 
				echo Json::encode(['output'=>$out, 'selected'=>isset($out[0]['id'])?$out[0]['id']:1]); 
			   	return;
			}

			if($cat_id == 3){
				$out = $invoice->find()->select(['component_id As id','description  As name'])->asArray()->all(); 
				echo Json::encode(['output'=>$out, 'selected'=>isset($out[0]['id'])?$out[0]['id']:1]); 
			   	return;
			}

			if($cat_id == 4){
				$out = $order->find()->select(['tyc_ref As id','order_number  As name'])->where(['component_id'=> $subcat_id])->asArray()->all(); 
				echo Json::encode(['output'=>$out, 'selected'=>isset($out[0]['id'])?$out[0]['id']:1]); 
			   	return;
			}

			if($cat_id == 5){
				$out = $rpo->find()->select(['component_id As id','description  As name'])->asArray()->all(); 
				echo Json::encode(['output'=>$out, 'selected'=>isset($out[0]['id'])?$out[0]['id']:1]); 
			   	return;
			}
			
        }
    }
    echo Json::encode(['output'=>$data['out'], 'selected'=>$data['selected']]);
}
}
