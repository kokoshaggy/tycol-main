<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TmAddressController implements the CRUD actions for TmAddress model.
 */
class LinkedapiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	
	/**
     * This method is respossible for fetching all folders and all components in a unique manner, 
	 * and fetched with ajax, 
     * there are different model atributes which conflict each other, 
	 * as such conditional statements are used to properly assinge fetched values to the return array 
     */
	public function actionIndex()
    {
        $components = ['Folder','Invoice','Project','Receivedpurchaseorder'];
		$data = []; // holds individual data fetched from the database
		$apiData = []; // hold all component id and descriptions 
		foreach($components as $value){
			// loop through components 
			$modelPath = "app\\models\\" . ucfirst($value); // create model path /  namespace dynamically based on the look value . 
			$model = new $modelPath; // instanciate model
			if($value == 'Project'){
				/* id and description are of a different column name as such to 
				 * maintain consistency of fetched values 
				*/
				$makeSelection = $model->find()->select(['component_id as c', 'pro_description  as n'])->asArray()->all();
			} elseif($value == 'Receivedpurchaseorder'){
				$makeSelection = $model->find()->select(['component_id as c', 'description as n'])->asArray()->all();
			} elseif($value == 'Folder'){
				$makeSelection = $model->find()->select(['id as c', 'description as n'])->asArray()->all();
			} else{
				$makeSelection = $model->find()->select(['component_id as c', 'description as n'])->asArray()->all();
			}
			
			$data['c'] = $value;// key of initial cascade value
			$data['n'] = $value; // value of initial cascade dropdown
			$data['d'] = $makeSelection; // nexted key and value
			
			array_push($apiData ,$data); 
			
			
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return  $apiData ;
    }
	
}
