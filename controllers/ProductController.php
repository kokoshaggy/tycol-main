<?php

namespace app\controllers;

use Yii;
use app\models\AttrModel;
use app\models\Product;
use app\models\ProductSearch;
use app\models\ProductAttribute;
use app\models\Category;
use app\models\Corporation;
use app\models\Attribute;
use app\models\ProductCorporation;
use app\models\Supplier;
use app\boffins_vendor\classes\BoffinsBaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;



/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends BoffinsBaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        //$searchModel = new ProductSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new Product;
        $display = $model->displayProducts;
        $findOneProduct = $model->find()->orderBy(['id'=>SORT_ASC])->one();

        return $this->render('index', [
            //'searchModel' => $searchModel,
            //'dataProvider' => $dataProvider,
            'products' => $display,
            'findOneProduct' => $findOneProduct,
            'model' => $model,
         ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if($id==0){
            return $this->renderAjax('/nodata/nodataavailable');
        }
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        $attributeModel = [new Attribute()];
        $categoryModel = Category::find()->all();
        $productCorporationModel = new ProductCorporation();
        $supplierModel=Supplier::find()->all();
		if(isset($_GET['id'])){
			$getId = $_GET['id'];
			if(!empty($model->itemType) && !empty($model->itemID)){
				$model->itemType = $model->itemType.',folder';
				$this->owner->itemID = $model->itemID.','.$getId;
			} else{
				$model->itemType = 'folder';
				$model->itemID = $getId;
			}
		}
        if( $model->load(Yii::$app->request->post()) && $productCorporationModel->load(Yii::$app->request->post())) {

            $attributeModel = AttrModel::createMultiple(Attribute::classname());
            AttrModel::loadMultiple($attributeModel, Yii::$app->request->post());

            $model->image = UploadedFile::getInstance($model, 'image');

            if(!empty($model->image)){
                        $fileName = $model->product_name.rand(1, 4000) . '.' . $model->image->extension;
                        $filePath = 'uploads/products/'.$fileName;
                        $model->image->saveAs($filePath);
                        $model->image = $filePath;
                    } else {
                        $model->image = '';
                    }
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                       ActiveForm::validateMultiple($attributeModel),
                       ActiveForm::validate($model)
                );
            }

            $valid = $model->validate();
            $valid = AttrModel::validateMultiple($attributeModel) && $valid;   

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($attributeModel as $attributeModel) {
                            if (! ($flag = $attributeModel->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                            $model->link('pAttributes', $attributeModel);
                        }
                    }
                    if ($flag) {
                        $productCorporationModel->product_id = $model->id;
                        $productCorporationModel->save(false);
                        $transaction->commit();
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

                return $this->renderAjax('create', [
                'model' => $model,
                'category' => $categoryModel,
                'attribute' => (empty($attributeModel)) ? [new Attribute] : $attributeModel,
                'supplier' => $supplierModel,
                'productCorporation' => $productCorporationModel,                
            ]);
    }


    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $categoryModel = Category::find()->all();
        $supplierModel=Supplier::find()->all();
        $productCorporationModel = $model->tmProductCorporations;
        if(empty($productCorporationModel)){
            $productCorporationModel = new ProductCorporation();
        } else {
			$productCorporationModelInit = new ProductCorporation();
			$productCorporationModel = $productCorporationModelInit->find()->where(['product_id' => $id])->one();
		}
        $attributeModel = $model->pAttributes;

        $currentImage = $model->image;

        if ($model->load(Yii::$app->request->post()) && $productCorporationModel->load(Yii::$app->request->post())) {  
            $oldIDs = ArrayHelper::map($attributeModel, 'id', 'id');
            $attributeModel = AttrModel::createMultiple(Attribute::classname(), $attributeModel);
            AttrModel::loadMultiple($attributeModel, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($attributeModel, 'id', 'id')));       
            $model->image = UploadedFile::getInstance($model, 'image');
            if(!empty($model->image)) {
                $filePath = 'uploads/products/' . $model->product_name.rand(1, 4000) . '.' .$model->image->extension;
                $model->image->saveAs($filePath);
                $model->image = $filePath; 
             }
            else{
                $model->image = $currentImage;
            }

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($attributeModel),
                    ActiveForm::validate($model)
                );
            }

            $valid = $model->validate();
            $valid = AttrModel::validateMultiple($attributeModel) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (!empty($deletedIDs)) {
                            Attribute::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($attributeModel as $attributeModel) {
                            if (! ($flag = $attributeModel->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        //$supplierModel->save(false);
						$productCorporationModel->save();
                        $transaction->commit();
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }     
        }
        return $this->renderAjax('update', [
                'model' => $model,
                'category' => $categoryModel,
                'attribute' => $attributeModel,
                'supplier' => $supplierModel,
                'productCorporation' => $productCorporationModel,
            ]);  
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionProductlistview()
    {
        if(isset($_POST['option'])){
            $option = $_POST['option'];
            $hoverEffect = 'false';
        } else {
            $hoverEffect = 'true';
            $dataProvider = product::find()->all();
        } 

        return $this->renderAjax('productlistview', [
            'product' => $dataProvider,
            'hoverEffect' => $hoverEffect,
        ]);
    }

    public function actionProductview($id)
    {
        if($id == 0){
            return $this->renderAjax('/nodata/nodataavailable',['message'=>'No Product available']);
        }
        
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),]);
    }


 

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
