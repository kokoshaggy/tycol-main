<?php

namespace app\controllers;

use Yii;
use app\models\Correspondence;
use app\models\CorrespondenceComponent;
use app\models\Component;
use app\models\Folder;
use app\models\Project;
use app\models\Invoice;
use app\models\Order;
use app\models\Receivedpurchaseorder;
use app\models\Person;
use app\models\Corporation;
use app\models\UserDb as user;
use app\models\FolderComponent;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\boffins_vendor\classes\BoffinsBaseController;

/**
 * CorrespondenceController implements the CRUD actions for Correspondence model.
 */
class CorrespondenceController extends BoffinsBaseController 
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Correspondence models.
     * @return mixed
     */
    public function actionIndex()
    {
		$model = new Correspondence;
		$findOneCorrespondence = $model->find()->orderBy(['id'=>SORT_ASC])->one();
		$correspondence =Correspondence::fetchAllcorrespondence();
		$corporation = new Corporation;
		$person = new Person;
		$user = new user;
		
        return $this->render('index', [
			'model' => $model,
			'correspondence' => $correspondence,
			'users' => $user,
			'corporation' => $corporation,
			'person' => $person,
			'findOneCorrespondence' => $findOneCorrespondence,
        ]);
    }
	
	public function actionCorrespondencelistview()
    {
		$model = new Correspondence();
		if(isset($_POST['option'])){
			$option = $_POST['option'];
			$hoverEffect = 'false';
			$dataProvider = $model->find()->where(['in','component_id',array_values(unserialize($option))])->all();
		} else {
			$hoverEffect = 'true';
			$dataProvider = $model->find()->all();
		}

        return $this->renderAjax('correspondencelistview', [
            'correspondence' => $dataProvider,
			'hoverEffect' => $hoverEffect,
        ]);
    }
		
		
    public function actionCorrespondenceindex($id)
    {
        
		$correspondence =Correspondence::fetchAllcorrespondence();
		$corporation = new Corporation;
		$person = new Person;
		$user = new user;
		
        return $this->renderAjax('index', [
			'model' => new Correspondence,
			'correspondence' => $correspondence,
			'users' => $user,
			'corporation' => $corporation,
			'person' => $person,
        ]);
    }

    /**
     * Displays a single Correspondence model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
	public function actionCorrespondenceview($id)
    {
		if($id === 0 or $id == 0){
			return $this->renderAjax('/nodata/nodataavailable',['message'=>'No Correspondence available']);
		}
		$model = $this->findModel($id);
        return $this->renderAjax('correspondenceview', [
            'model' => $model,
			'id' => $id,
			'subComponents' => $model->subComponents
        ]);
    }

    /**
     * Creates a new Correspondence model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
		 
	public function actionCreatecorrespondence()
    {
      
        return $this->renderAjax('create', [
			'model' => new Correspondence,
			
        ]);
    }
	
    public function actionCreate()
    {
        $model = new Correspondence();
		$model->drafting_entity = Yii::$app->user->identity->id ;
		if(isset($_GET['id'])){
			$getId = $_GET['id'];
			if(!empty($model->itemType) && !empty($model->itemID)){
				$model->itemType = $model->itemType.',folder';
				$this->owner->itemID = $model->itemID.','.$getId;
			} else{
				$model->itemType = 'folder';
				$model->itemID = $getId;
			}
		}
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           	$data=[];
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$data['sent'] = 'sent';
			$data['message'] = 'new Correspondence was created with Correspondence id '.$model->particular_reference ;
			return $data;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Correspondence model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $data=[];
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$data['sent'] = 'sent';
			$data['message'] = 'Updated Correspondence  with Correspondence id '.$model->particular_reference ;
			return $data;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Correspondence model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Correspondence model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Correspondence the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Correspondence::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
			
        }
    }
	
	public function actionListItems() 
	{
    	$result = [];
		$post = Yii::$app->request->post('depdrop_all_params');
		$parents = $post['cor_componentname'];
		if ($parents == 'folder') {
			$folder = new Folder;
			$result = $folder->find()->select(['tyc_ref As id','tyc_ref  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$result, 'selected'=>'1']);    
        	return;
		}
		
		if ($parents == 'project') {
			$project = new Project;
			$result = $project->find()->select(['component_id As id','pro_description  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$result, 'selected'=>'1']);    
        	return;
		}
		
		if ($parents == 'invoice') {
			$invoice = new Invoice;
			$result = $invoice->find()->select(['component_id As id', 'description  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$result, 'selected'=>'1']);    
        	return;
		}
		
		if ($parents == 'order') {
			$order = new Order;
			$result = $order->find()->select(['component_id As id','order_number  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$result, 'selected'=>'1']);    
        	return;
		}
		
		if ($parents == 'rpo'){
			$rpo = new Receivedpurchaseorder;
			$result = $rpo->find()->select(['component_id As id','description  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$result, 'selected'=>'1']);    
        	return;
		}
    	//echo Json::encode(['output'=>'', 'selected'=>'']);
	}
	
	public function actionDependentdropdown() 
	{
		$folder = new Folder;
		$project = new Project;
		$invoice = new Invoice;
		$order = new Order;
		$rpo = new Receivedpurchaseorder;
    	$out = [];
		$post = Yii::$app->request->post('depdrop_all_params');
		$mainpost = $post['components'];
        $parents = $mainpost;
		if($parents == 1){
			$out = $folder->find()->select(['tyc_ref As id','tyc_ref  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$out, 'selected'=>'1']);    
        	return;
		}
		
		if($parents == 2){
			$out = $project->find()->select(['component_id As id','pro_description  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$out, 'selected'=>'1']);    
        	return;
		}
		
		if($parents == 3){
			$out = $invoice->find()->select(['component_id As id','description  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$out, 'selected'=>'1']);    
        	return;
		}
		
		if($parents == 4){
			$out = $order->find()->select(['component_id As id','order_number  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$out, 'selected'=>'1']);    
        	return;
		}
		
		if($parents == 5){
			$out = $rpo->find()->select(['component_id As id','description  As name'])->asArray()->all(); 
			echo Json::encode(['output'=>$out, 'selected'=>'1']);    
        	return;
		}
    	//echo Json::encode(['output'=>'', 'selected'=>'']);
	}
	
	
}
