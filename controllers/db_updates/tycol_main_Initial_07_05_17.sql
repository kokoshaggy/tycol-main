-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 07, 2017 at 02:22 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tycol_main`
--
CREATE DATABASE IF NOT EXISTS `tycol_main` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `tycol_main`;

-- --------------------------------------------------------

--
-- Table structure for table `tm_address`
--

CREATE TABLE IF NOT EXISTS `tm_address` (
  `id` int(11) NOT NULL,
  `address_line` varchar(255) NOT NULL,
  `State` varchar(255) NOT NULL,
  `Country` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tm_address`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_address_corporation`
--

CREATE TABLE IF NOT EXISTS `tm_address_corporation` (
  `address_id` int(11) NOT NULL,
  `corporation_id` int(11) NOT NULL,
  KEY `address_id` (`address_id`),
  KEY `corporation_id` (`corporation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tm_address_corporation`:
--   `address_id`
--       `tm_address` -> `id`
--   `corporation_id`
--       `tm_corporation` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_categoroy_corporation`
--

CREATE TABLE IF NOT EXISTS `tm_categoroy_corporation` (
  `productcategory_id` int(11) NOT NULL,
  `corporation_id` int(11) NOT NULL,
  KEY `productcategory_id` (`productcategory_id`),
  KEY `corporation_id` (`corporation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tm_categoroy_corporation`:
--   `productcategory_id`
--       `tm_category` -> `id`
--   `corporation_id`
--       `tm_corporation` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_category`
--

CREATE TABLE IF NOT EXISTS `tm_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tm_category`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_client`
--

CREATE TABLE IF NOT EXISTS `tm_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporation_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `corporation_id_2` (`corporation_id`),
  KEY `corporation_id` (`corporation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tm_client`:
--   `corporation_id`
--       `tm_corporation` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_corporation`
--

CREATE TABLE IF NOT EXISTS `tm_corporation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `short_name` varchar(5) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tm_corporation`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_currency`
--

CREATE TABLE IF NOT EXISTS `tm_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(4) NOT NULL,
  `symbol` varchar(1) DEFAULT NULL,
  `unit_text` varchar(32) NOT NULL COMMENT 'dollar, pound etc',
  `subunit_text` varchar(32) NOT NULL COMMENT 'cents,pennies etc',
  PRIMARY KEY (`id`),
  UNIQUE KEY `currency` (`currency_code`),
  KEY `currency_2` (`currency_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tm_currency`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_email`
--

CREATE TABLE IF NOT EXISTS `tm_email` (
  `id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tm_email`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_email_entity`
--

CREATE TABLE IF NOT EXISTS `tm_email_entity` (
  `email_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  KEY `Entity Email` (`email_id`,`entity_id`) USING BTREE,
  KEY `tm_email_entity_ibfk_2` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- RELATIONS FOR TABLE `tm_email_entity`:
--   `entity_id`
--       `tm_entity` -> `id`
--   `email_id`
--       `tm_email` -> `id`
--   `entity_id`
--       `tm_entity` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_entity`
--

CREATE TABLE IF NOT EXISTS `tm_entity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type` enum('person','corporation','','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Connects to Persons and Corporate to allow payments from/to either';

--
-- RELATIONS FOR TABLE `tm_entity`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_file`
--

CREATE TABLE IF NOT EXISTS `tm_file` (
  `tyc_ref` varchar(16) CHARACTER SET utf8 NOT NULL,
  `type` enum('project','administrative') CHARACTER SET utf8 NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `notes` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`tyc_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tm_file`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_invoice`
--

CREATE TABLE IF NOT EXISTS `tm_invoice` (
  `id` int(11) NOT NULL,
  `receivedpurchaseorder_id` varchar(16) CHARACTER SET utf8 NOT NULL,
  `description` varchar(255) NOT NULL,
  `amount` decimal(19,4) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `creation_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `currency_id` (`currency_id`),
  KEY `receivedpurchaseorder_id` (`receivedpurchaseorder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tm_invoice`:
--   `currency_id`
--       `tm_currency` -> `id`
--   `receivedpurchaseorder_id`
--       `tm_receivedpurchaseorder` -> `receivedpurchaseorder_reference`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_order`
--

CREATE TABLE IF NOT EXISTS `tm_order` (
  `order_number` int(4) NOT NULL,
  `tyc_ref` varchar(16) DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier_ref` varchar(16) DEFAULT NULL,
  `order_value` decimal(19,4) NOT NULL,
  `order_currency` int(11) NOT NULL,
  `issue_date` datetime NOT NULL,
  `supplier_completion_date` datetime DEFAULT NULL,
  `order_status` varchar(255) DEFAULT NULL,
  `order_file` longblob,
  PRIMARY KEY (`order_number`),
  KEY `Manufacturer_ID` (`supplier_id`),
  KEY `tyc_ref` (`tyc_ref`,`supplier_id`,`supplier_ref`),
  KEY `manufacturer_ref` (`supplier_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tm_order`:
--   `tyc_ref`
--       `tm_project` -> `tyc_ref`
--   `supplier_id`
--       `tm_corporation` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_payment`
--

CREATE TABLE IF NOT EXISTS `tm_payment` (
  `id` int(11) NOT NULL,
  `tyc_ref` varchar(16) NOT NULL,
  `receiver_corporation_id` int(11) NOT NULL COMMENT 'connect to corporation or supplier',
  `payment_source_id` int(11) NOT NULL COMMENT 'connect to payment source',
  `value` decimal(19,4) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `payment_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tyc_ref` (`tyc_ref`),
  KEY `receiver_id` (`receiver_corporation_id`),
  KEY `source_id` (`payment_source_id`),
  KEY `currency_id` (`currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tm_payment`:
--   `receiver_corporation_id`
--       `tm_corporation` -> `id`
--   `payment_source_id`
--       `tm_corporation` -> `id`
--   `currency_id`
--       `tm_currency` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_payment_source`
--

CREATE TABLE IF NOT EXISTS `tm_payment_source` (
  `id` int(11) NOT NULL,
  `source_code` varchar(4) NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `source_code` (`source_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tm_payment_source`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_person`
--

CREATE TABLE IF NOT EXISTS `tm_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `dob` datetime NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tm_person`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_person_corporation`
--

CREATE TABLE IF NOT EXISTS `tm_person_corporation` (
  `person_id` int(11) NOT NULL,
  `corporation_id` int(11) NOT NULL,
  KEY `person_id` (`person_id`),
  KEY `corporation_id` (`corporation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tm_person_corporation`:
--   `person_id`
--       `tm_person` -> `id`
--   `corporation_id`
--       `tm_corporation` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_product`
--

CREATE TABLE IF NOT EXISTS `tm_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category` int(11) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_attribute` varchar(255) DEFAULT NULL,
  `refinery_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ProductID` (`product_category`),
  KEY `ProductListID` (`product_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tm_product`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_product_attribute`
--

CREATE TABLE IF NOT EXISTS `tm_product_attribute` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tm_product_attribute`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_product_corporation`
--

CREATE TABLE IF NOT EXISTS `tm_product_corporation` (
  `product_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  KEY `productdescription_id` (`product_id`),
  KEY `corporation_id` (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tm_product_corporation`:
--   `product_id`
--       `tm_product` -> `id`
--   `supplier_id`
--       `tm_corporation` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_project`
--

CREATE TABLE IF NOT EXISTS `tm_project` (
  `tyc_ref` varchar(16) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `client_reference` varchar(16) DEFAULT NULL,
  `manufacturer_ref` varchar(16) DEFAULT NULL,
  `project_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`tyc_ref`),
  KEY `Client` (`client_id`),
  KEY `Supplier` (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tm_project`:
--   `client_id`
--       `tm_client` -> `id`
--   `supplier_id`
--       `tm_supplier` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_project_order`
--

CREATE TABLE IF NOT EXISTS `tm_project_order` (
  `tyc_ref` varchar(16) NOT NULL,
  `order_number` int(5) NOT NULL,
  PRIMARY KEY (`tyc_ref`,`order_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tm_project_order`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_receivedpurchaseorder`
--

CREATE TABLE IF NOT EXISTS `tm_receivedpurchaseorder` (
  `receivedpurchaseorder_reference` varchar(16) NOT NULL,
  `receivedpurchaseorder_type` varchar(16) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `order_value` decimal(19,4) DEFAULT NULL,
  `order_currency` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `issue_date` datetime DEFAULT NULL,
  `receivedpurchaseorder_duedate` datetime DEFAULT NULL,
  `final_delivery_date` datetime DEFAULT NULL,
  PRIMARY KEY (`receivedpurchaseorder_reference`),
  KEY `client_id` (`client_id`,`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tm_receivedpurchaseorder`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_receivedpurchaseorder_order`
--

CREATE TABLE IF NOT EXISTS `tm_receivedpurchaseorder_order` (
  `receivedpurchaseorder_reference` varchar(255) NOT NULL,
  `order_number` int(32) NOT NULL,
  PRIMARY KEY (`receivedpurchaseorder_reference`(50),`order_number`),
  KEY `receivedpurchaseorder_Reference` (`receivedpurchaseorder_reference`),
  KEY `order_number` (`order_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tm_receivedpurchaseorder_order`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_receivedpurchaseorder_type`
--

CREATE TABLE IF NOT EXISTS `tm_receivedpurchaseorder_type` (
  `receivedpurchaseorder_type_code` int(11) NOT NULL AUTO_INCREMENT,
  `receivedpurchaseorder_type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`receivedpurchaseorder_type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tm_receivedpurchaseorder_type`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_remark`
--

CREATE TABLE IF NOT EXISTS `tm_remark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tyc_ref` varchar(16) DEFAULT NULL,
  `remark_type` varchar(255) DEFAULT NULL,
  `remark_date` datetime NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tm_remark`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_status_type`
--

CREATE TABLE IF NOT EXISTS `tm_status_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `phase` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tm_status_type`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_supplier`
--

CREATE TABLE IF NOT EXISTS `tm_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporation_id` int(11) NOT NULL,
  `supplier_type` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tm_supplier`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_telephone`
--

CREATE TABLE IF NOT EXISTS `tm_telephone` (
  `id` int(11) NOT NULL,
  `telephone_number` varchar(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tm_telephone`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_telephone_corporation`
--

CREATE TABLE IF NOT EXISTS `tm_telephone_corporation` (
  `telephone_id` int(11) NOT NULL,
  `corporation_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tm_telephone_corporation`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_user`
--

CREATE TABLE IF NOT EXISTS `tm_user` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `access_level` int(9) NOT NULL,
  `un` varchar(255) NOT NULL,
  `pw` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tm_user`:
--

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tm_address_corporation`
--
ALTER TABLE `tm_address_corporation`
  ADD CONSTRAINT `tm_address_corporation_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `tm_address` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tm_address_corporation_ibfk_2` FOREIGN KEY (`corporation_id`) REFERENCES `tm_corporation` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_categoroy_corporation`
--
ALTER TABLE `tm_categoroy_corporation`
  ADD CONSTRAINT `tm_categoroy_corporation_ibfk_1` FOREIGN KEY (`productcategory_id`) REFERENCES `tm_category` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tm_categoroy_corporation_ibfk_2` FOREIGN KEY (`corporation_id`) REFERENCES `tm_corporation` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_client`
--
ALTER TABLE `tm_client`
  ADD CONSTRAINT `tm_client_ibfk_1` FOREIGN KEY (`corporation_id`) REFERENCES `tm_corporation` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_email_entity`
--
ALTER TABLE `tm_email_entity`
  ADD CONSTRAINT `tm_email_entity_ibfk_1` FOREIGN KEY (`email_id`) REFERENCES `tm_email` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tm_email_entity_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `tm_entity` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_invoice`
--
ALTER TABLE `tm_invoice`
  ADD CONSTRAINT `tm_invoice_ibfk_1` FOREIGN KEY (`currency_id`) REFERENCES `tm_currency` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tm_invoice_ibfk_2` FOREIGN KEY (`receivedpurchaseorder_id`) REFERENCES `tm_receivedpurchaseorder` (`receivedpurchaseorder_reference`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_order`
--
ALTER TABLE `tm_order`
  ADD CONSTRAINT `tm_order_ibfk_1` FOREIGN KEY (`tyc_ref`) REFERENCES `tm_project` (`tyc_ref`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tm_order_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `tm_corporation` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_payment`
--
ALTER TABLE `tm_payment`
  ADD CONSTRAINT `tm_payment_ibfk_4` FOREIGN KEY (`receiver_corporation_id`) REFERENCES `tm_corporation` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tm_payment_ibfk_5` FOREIGN KEY (`payment_source_id`) REFERENCES `tm_corporation` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tm_payment_ibfk_6` FOREIGN KEY (`currency_id`) REFERENCES `tm_currency` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_person_corporation`
--
ALTER TABLE `tm_person_corporation`
  ADD CONSTRAINT `tm_person_corporation_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `tm_person` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tm_person_corporation_ibfk_2` FOREIGN KEY (`corporation_id`) REFERENCES `tm_corporation` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_product_corporation`
--
ALTER TABLE `tm_product_corporation`
  ADD CONSTRAINT `tm_product_corporation_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `tm_product` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tm_product_corporation_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `tm_corporation` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_project`
--
ALTER TABLE `tm_project`
  ADD CONSTRAINT `Client` FOREIGN KEY (`client_id`) REFERENCES `tm_client` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `Supplier` FOREIGN KEY (`supplier_id`) REFERENCES `tm_supplier` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
