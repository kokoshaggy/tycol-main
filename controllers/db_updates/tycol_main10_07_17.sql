-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2017 at 09:16 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tycol_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_address`
--

DROP TABLE IF EXISTS `tm_address`;
CREATE TABLE IF NOT EXISTS `tm_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_line` varchar(255) NOT NULL,
  `State` varchar(255) NOT NULL,
  `Country` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_address`
--

TRUNCATE TABLE `tm_address`;
--
-- Dumping data for table `tm_address`
--

INSERT INTO `tm_address` (`id`, `address_line`, `State`, `Country`, `code`) VALUES
(1, 'anything', 'anything', 'anything', '00900'),
(2, 'anything', 'anything', 'anything', '00900'),
(3, 'anything', 'anything', 'anything', '00900'),
(4, 'anything', 'anything', 'ukhrkj', '00900'),
(5, 'anything', 'anything', 'tttttt', '00900'),
(6, 'tgtgrtg', 'grtgtg', 'rtggtg', 'rtgrg'),
(7, 'tgtgrtg', 'grtgtg', 'rtggtg', 'rtgrg'),
(8, 'test', 'test', 'test', '3445'),
(9, 'test', 'test', 'test', '3445'),
(10, 'kboy', 'kboy', 'kboy3435', '3535'),
(11, 'kboy', 'kboy', 'kboy', '2333'),
(12, 'please work', 'please work', 'please work', '99999'),
(13, 'please work', 'please work', 'please work', '99999'),
(14, 'please work', 'please work', 'please work', '99999'),
(15, 'please work', 'please work', 'please work', '99999'),
(16, 'please work', 'please work', 'please work', '99999'),
(17, 'please work', 'please work', 'please work', '99999'),
(18, 'please work', 'please work', 'please work', '99999'),
(19, 'please work', 'please work', 'please work', '99999'),
(20, 'kingsley', 'kingsley', 'kingsley', '999909'),
(21, '87987khkjhj', 'hjhkjhkj', 'ech', '0000'),
(22, 'kingsley achumie', 'kingsley achumie', 'kingsley achumie', '38383838'),
(23, 'kingsley achumie', 'kingsley achumie', 'kingsley achumie', '38383838'),
(24, 'kingsley achumie', 'kingsley achumie', 'kingsley achumie', '38383838'),
(25, 'kingsley achumie', 'kingsley achumie', 'kingsley achumie', '38383838'),
(26, 'kingsley achumie', 'kingsley achumie', 'kingsley achumie', '38383838'),
(27, 'yftftgf', 'yftftgf', 'yftftgf', 'yftftgf'),
(28, 'sdfghjkl', 'sdfghj', 'dghj', '34567'),
(29, 'wouldwork', 'wouldwork', 'wouldwork', '123456789'),
(30, '10 danube close maitama abuja', 'FCT Abuja', 'Nigeria', '900001'),
(31, '435werdsfdfs', 'fdfdfdfsdf', 'fdfdfdffd', '12234'),
(32, 'rhrhtrhreh', 'erthrh', 'erhrht', '222222'),
(33, '34t3t34t', '34t3t3t', '34t3t3t4', '3t3t3t'),
(34, '34t3t34t', '34t3t3t', '34t3t3t4', '3t3t3t'),
(35, '', '', '', ''),
(36, '', '', '', ''),
(37, '', '', '', ''),
(38, '', '', '', ''),
(39, '', '', '', ''),
(40, '', '', '', ''),
(41, '', '', '', ''),
(42, '', '', '', ''),
(43, '', '', '', ''),
(44, '', '', '', ''),
(45, '', '', '', ''),
(46, '', '', '', ''),
(47, 'asd', 'asd', 'asd', '322323'),
(48, 'anything', 'anything', 'anything', '5555555'),
(49, '', '', '', ''),
(50, '', '', '', ''),
(51, '', '', '', ''),
(52, '', '', '', ''),
(53, '', '', '', ''),
(54, '', '', '', ''),
(55, '', '', '', ''),
(56, '', '', '', ''),
(57, '', '', '', ''),
(58, '', '', '', ''),
(59, '', '', '', ''),
(60, '', '', '', ''),
(61, '', '', '', ''),
(62, '', '', '', ''),
(63, '', '', '', ''),
(64, '', '', '', ''),
(65, '', '', '', ''),
(66, '', '', '', ''),
(67, '', '', '', ''),
(68, '', '', '', ''),
(69, '', '', '', ''),
(70, '', '', '', ''),
(71, '', '', '', ''),
(72, '', '', '', ''),
(73, '', '', '', ''),
(74, '', '', '', ''),
(75, '', '', '', ''),
(76, '', '', '', ''),
(77, '', '', '', ''),
(78, '', '', '', ''),
(79, '', '', '', ''),
(80, '', '', '', ''),
(81, '', '', '', ''),
(82, '', '', '', ''),
(83, '', '', '', ''),
(84, '', '', '', ''),
(85, '', '', '', ''),
(86, '', '', '', ''),
(87, '', '', '', ''),
(88, '', '', '', ''),
(89, '', '', '', ''),
(90, '', '', '', ''),
(91, '', '', '', ''),
(92, '', '', '', ''),
(93, '', '', '', ''),
(94, '', '', '', ''),
(95, '', '', '', ''),
(96, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tm_address_entity`
--

DROP TABLE IF EXISTS `tm_address_entity`;
CREATE TABLE IF NOT EXISTS `tm_address_entity` (
  `address_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  KEY `address_id` (`address_id`),
  KEY `corporation_id` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Truncate table before insert `tm_address_entity`
--

TRUNCATE TABLE `tm_address_entity`;
--
-- Dumping data for table `tm_address_entity`
--

INSERT INTO `tm_address_entity` (`address_id`, `entity_id`) VALUES
(15, 18),
(16, 19),
(17, 20),
(18, 21),
(20, 23),
(21, 24),
(22, 25),
(23, 26),
(24, 27),
(25, 28),
(26, 29),
(27, 30),
(28, 36),
(29, 37),
(30, 38),
(31, 39),
(32, 40),
(33, 41),
(34, 42),
(39, 43),
(40, 44),
(41, 45),
(42, 46),
(43, 47),
(44, 48),
(45, 49),
(46, 50),
(47, 51),
(48, 52),
(49, 53),
(50, 54),
(51, 55),
(52, 56),
(53, 57),
(54, 58),
(55, 59),
(56, 60),
(57, 61),
(58, 62),
(59, 63),
(60, 64),
(61, 65),
(62, 66),
(63, 67),
(64, 68),
(65, 69),
(66, 70),
(67, 71),
(68, 72),
(69, 73),
(70, 74),
(71, 75),
(72, 76),
(73, 77),
(74, 78),
(75, 79),
(76, 80),
(77, 81),
(78, 82),
(79, 83),
(80, 84),
(81, 85),
(82, 86),
(83, 87),
(84, 88),
(85, 89),
(86, 90),
(87, 91),
(88, 92),
(89, 93),
(90, 94),
(91, 95),
(92, 96),
(93, 97),
(94, 98),
(95, 99),
(96, 100);

-- --------------------------------------------------------

--
-- Table structure for table `tm_categoroy_corporation`
--

DROP TABLE IF EXISTS `tm_categoroy_corporation`;
CREATE TABLE IF NOT EXISTS `tm_categoroy_corporation` (
  `productcategory_id` int(11) NOT NULL,
  `corporation_id` int(11) NOT NULL,
  KEY `productcategory_id` (`productcategory_id`),
  KEY `corporation_id` (`corporation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tm_categoroy_corporation`
--

TRUNCATE TABLE `tm_categoroy_corporation`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_category`
--

DROP TABLE IF EXISTS `tm_category`;
CREATE TABLE IF NOT EXISTS `tm_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_category`
--

TRUNCATE TABLE `tm_category`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_client`
--

DROP TABLE IF EXISTS `tm_client`;
CREATE TABLE IF NOT EXISTS `tm_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporation_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `corporation_id_2` (`corporation_id`),
  KEY `corporation_id` (`corporation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tm_client`
--

TRUNCATE TABLE `tm_client`;
--
-- Dumping data for table `tm_client`
--

INSERT INTO `tm_client` (`id`, `corporation_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tm_component`
--

DROP TABLE IF EXISTS `tm_component`;
CREATE TABLE IF NOT EXISTS `tm_component` (
  `id` int(11) NOT NULL,
  `component_type` enum('project','order','invoice','received_purchase_order') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_component`
--

TRUNCATE TABLE `tm_component`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_corporation`
--

DROP TABLE IF EXISTS `tm_corporation`;
CREATE TABLE IF NOT EXISTS `tm_corporation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `short_name` varchar(5) DEFAULT NULL,
  `entity_id` int(11) NOT NULL,
  `notes` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_corporation`
--

TRUNCATE TABLE `tm_corporation`;
--
-- Dumping data for table `tm_corporation`
--

INSERT INTO `tm_corporation` (`id`, `name`, `short_name`, `entity_id`, `notes`) VALUES
(1, 'achumie kingsley', 'bla', 1, 'bla'),
(2, 'john', 'bla', 2, 'bla'),
(3, '', '', 0, ''),
(4, '', '', 0, ''),
(5, '', '', 0, ''),
(6, '', '', 0, ''),
(7, 'ffff', 'fffff', 0, 'fffff'),
(8, 'wdsd', 'sddsd', 0, 'sdsdds'),
(9, 'kings', 'male', 0, 'female'),
(10, 'badboy', 'next', 0, 'run'),
(11, 'finaltest', 'final', 0, 'ok oh '),
(12, 'rrr', 'rrrrr', 0, 'rrrr'),
(13, '', '', 0, ''),
(14, '', '', 0, ''),
(15, '', '', 0, ''),
(16, 'ljnjmn', 'ihukj', 24, 'iuhiuhj'),
(17, 'anything', 'anyth', 52, 'anything');

-- --------------------------------------------------------

--
-- Table structure for table `tm_currency`
--

DROP TABLE IF EXISTS `tm_currency`;
CREATE TABLE IF NOT EXISTS `tm_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(4) NOT NULL,
  `symbol` varchar(1) DEFAULT NULL,
  `unit_text` varchar(32) NOT NULL COMMENT 'dollar, pound etc',
  `subunit_text` varchar(32) NOT NULL COMMENT 'cents,pennies etc',
  PRIMARY KEY (`id`),
  UNIQUE KEY `currency` (`currency_code`),
  KEY `currency_2` (`currency_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_currency`
--

TRUNCATE TABLE `tm_currency`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_email`
--

DROP TABLE IF EXISTS `tm_email`;
CREATE TABLE IF NOT EXISTS `tm_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_email`
--

TRUNCATE TABLE `tm_email`;
--
-- Dumping data for table `tm_email`
--

INSERT INTO `tm_email` (`id`, `address`) VALUES
(1, ''),
(2, ''),
(3, ''),
(4, ''),
(5, ''),
(6, ''),
(7, ''),
(8, ''),
(9, ''),
(10, 'kingsonly13c@gmail.com'),
(11, 'kingsonly13c@gmail.com'),
(12, 'efghjk@gmail.com '),
(13, 'efghjk@gmail.com '),
(14, 'efghjk@gmail.com '),
(15, 'efghjk@gmail.com '),
(16, 'efghjk@gmail.com '),
(17, 'yftftgf'),
(18, 'wsedrfghjlkl'),
(19, 'wouldwork@gmail.com'),
(20, 'kingsonly13c@gmail.com'),
(21, 'kingsonly13c@gmail.com'),
(22, 'rthrthrh'),
(23, '3t3t34t'),
(24, '3t3t34t'),
(25, ''),
(26, ''),
(27, ''),
(28, ''),
(29, ''),
(30, ''),
(31, ''),
(32, ''),
(33, 'asd'),
(34, 'anything'),
(35, ''),
(36, ''),
(37, ''),
(38, ''),
(39, ''),
(40, ''),
(41, ''),
(42, ''),
(43, ''),
(44, ''),
(45, ''),
(46, ''),
(47, ''),
(48, ''),
(49, ''),
(50, ''),
(51, ''),
(52, ''),
(53, ''),
(54, ''),
(55, ''),
(56, ''),
(57, ''),
(58, ''),
(59, ''),
(60, ''),
(61, ''),
(62, ''),
(63, ''),
(64, ''),
(65, ''),
(66, ''),
(67, ''),
(68, ''),
(69, ''),
(70, ''),
(71, ''),
(72, ''),
(73, ''),
(74, ''),
(75, ''),
(76, ''),
(77, ''),
(78, ''),
(79, ''),
(80, ''),
(81, ''),
(82, '');

-- --------------------------------------------------------

--
-- Table structure for table `tm_email_entity`
--

DROP TABLE IF EXISTS `tm_email_entity`;
CREATE TABLE IF NOT EXISTS `tm_email_entity` (
  `email_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  KEY `Entity Email` (`email_id`,`entity_id`) USING BTREE,
  KEY `tm_email_entity_ibfk_2` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Truncate table before insert `tm_email_entity`
--

TRUNCATE TABLE `tm_email_entity`;
--
-- Dumping data for table `tm_email_entity`
--

INSERT INTO `tm_email_entity` (`email_id`, `entity_id`) VALUES
(5, 18),
(6, 19),
(7, 20),
(8, 21),
(10, 23),
(11, 24),
(12, 25),
(13, 26),
(14, 27),
(15, 28),
(16, 29),
(17, 30),
(18, 36),
(19, 37),
(20, 38),
(21, 39),
(22, 40),
(23, 41),
(24, 42),
(25, 43),
(26, 44),
(27, 45),
(28, 46),
(29, 47),
(30, 48),
(31, 49),
(32, 50),
(33, 51),
(34, 52),
(35, 53),
(36, 54),
(37, 55),
(38, 56),
(39, 57),
(40, 58),
(41, 59),
(42, 60),
(43, 61),
(44, 62),
(45, 63),
(46, 64),
(47, 65),
(48, 66),
(49, 67),
(50, 68),
(51, 69),
(52, 70),
(53, 71),
(54, 72),
(55, 73),
(56, 74),
(57, 75),
(58, 76),
(59, 77),
(60, 78),
(61, 79),
(62, 80),
(63, 81),
(64, 82),
(65, 83),
(66, 84),
(67, 85),
(68, 86),
(69, 87),
(70, 88),
(71, 89),
(72, 90),
(73, 91),
(74, 92),
(75, 93),
(76, 94),
(77, 95),
(78, 96),
(79, 97),
(80, 98),
(81, 99),
(82, 100);

-- --------------------------------------------------------

--
-- Table structure for table `tm_entity`
--

DROP TABLE IF EXISTS `tm_entity`;
CREATE TABLE IF NOT EXISTS `tm_entity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type` enum('person','corporation','','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1 COMMENT='Connects to Persons and Corporate to allow payments from/to either';

--
-- Truncate table before insert `tm_entity`
--

TRUNCATE TABLE `tm_entity`;
--
-- Dumping data for table `tm_entity`
--

INSERT INTO `tm_entity` (`id`, `entity_type`) VALUES
(1, 'person'),
(2, 'person'),
(3, 'person'),
(4, 'person'),
(5, 'person'),
(6, 'person'),
(7, 'person'),
(8, 'person'),
(9, 'person'),
(10, 'person'),
(11, 'person'),
(12, 'person'),
(13, 'person'),
(14, 'person'),
(15, 'person'),
(16, 'person'),
(17, 'person'),
(18, 'person'),
(19, 'person'),
(20, 'person'),
(21, 'person'),
(22, 'person'),
(23, 'person'),
(24, 'corporation'),
(25, 'person'),
(26, 'person'),
(27, 'person'),
(28, 'person'),
(29, 'person'),
(30, 'person'),
(31, 'person'),
(32, 'person'),
(33, 'person'),
(34, 'person'),
(35, 'person'),
(36, 'person'),
(37, 'person'),
(38, 'person'),
(39, 'person'),
(40, 'person'),
(41, 'person'),
(42, 'person'),
(43, 'person'),
(44, 'person'),
(45, 'person'),
(46, 'person'),
(47, 'person'),
(48, 'person'),
(49, 'person'),
(50, 'person'),
(51, 'person'),
(52, 'corporation'),
(53, 'person'),
(54, 'person'),
(55, 'person'),
(56, 'person'),
(57, 'person'),
(58, 'person'),
(59, 'person'),
(60, 'person'),
(61, 'person'),
(62, 'person'),
(63, 'person'),
(64, 'person'),
(65, 'person'),
(66, 'person'),
(67, 'person'),
(68, 'person'),
(69, 'person'),
(70, 'person'),
(71, 'person'),
(72, 'person'),
(73, 'person'),
(74, 'person'),
(75, 'person'),
(76, 'person'),
(77, 'person'),
(78, 'person'),
(79, 'person'),
(80, 'person'),
(81, 'person'),
(82, 'person'),
(83, 'person'),
(84, 'person'),
(85, 'person'),
(86, 'person'),
(87, 'person'),
(88, 'person'),
(89, 'person'),
(90, 'person'),
(91, 'person'),
(92, 'person'),
(93, 'person'),
(94, 'person'),
(95, 'person'),
(96, 'person'),
(97, 'person'),
(98, 'person'),
(99, 'person'),
(100, 'person');

-- --------------------------------------------------------

--
-- Table structure for table `tm_folder`
--

DROP TABLE IF EXISTS `tm_folder`;
CREATE TABLE IF NOT EXISTS `tm_folder` (
  `tyc_ref` varchar(16) CHARACTER SET utf8 NOT NULL,
  `type` enum('project','administrative') CHARACTER SET utf8 NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `notes` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`tyc_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Truncate table before insert `tm_folder`
--

TRUNCATE TABLE `tm_folder`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_folder_component`
--

DROP TABLE IF EXISTS `tm_folder_component`;
CREATE TABLE IF NOT EXISTS `tm_folder_component` (
  `tyc_ref` varchar(16) NOT NULL,
  `componenet_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_folder_component`
--

TRUNCATE TABLE `tm_folder_component`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_invoice`
--

DROP TABLE IF EXISTS `tm_invoice`;
CREATE TABLE IF NOT EXISTS `tm_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_id` int(11) NOT NULL,
  `receivedpurchaseorder_id` varchar(16) CHARACTER SET utf8 NOT NULL,
  `description` varchar(255) NOT NULL,
  `amount` decimal(19,4) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `creation_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `currency_id` (`currency_id`),
  KEY `receivedpurchaseorder_id` (`receivedpurchaseorder_id`),
  KEY `Invoice Component` (`component_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tm_invoice`
--

TRUNCATE TABLE `tm_invoice`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_invoice_component`
--

DROP TABLE IF EXISTS `tm_invoice_component`;
CREATE TABLE IF NOT EXISTS `tm_invoice_component` (
  `invoice_id` int(11) NOT NULL,
  `componenet_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_invoice_component`
--

TRUNCATE TABLE `tm_invoice_component`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_order`
--

DROP TABLE IF EXISTS `tm_order`;
CREATE TABLE IF NOT EXISTS `tm_order` (
  `order_number` int(4) NOT NULL,
  `tyc_ref` varchar(16) DEFAULT NULL,
  `component_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier_ref` varchar(16) DEFAULT NULL,
  `order_value` decimal(19,4) NOT NULL,
  `order_currency` int(11) NOT NULL,
  `issue_date` datetime NOT NULL,
  `supplier_completion_date` datetime DEFAULT NULL,
  `order_status` varchar(255) DEFAULT NULL,
  `order_file` longblob,
  PRIMARY KEY (`order_number`),
  KEY `Manufacturer_ID` (`supplier_id`),
  KEY `tyc_ref` (`tyc_ref`,`supplier_id`,`supplier_ref`),
  KEY `manufacturer_ref` (`supplier_ref`),
  KEY `Order Components` (`component_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_order`
--

TRUNCATE TABLE `tm_order`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_order_component`
--

DROP TABLE IF EXISTS `tm_order_component`;
CREATE TABLE IF NOT EXISTS `tm_order_component` (
  `order_number` int(11) NOT NULL,
  `componenet_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_order_component`
--

TRUNCATE TABLE `tm_order_component`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_payment`
--

DROP TABLE IF EXISTS `tm_payment`;
CREATE TABLE IF NOT EXISTS `tm_payment` (
  `id` int(11) NOT NULL,
  `tyc_ref` varchar(16) CHARACTER SET utf8 NOT NULL,
  `receiver_corporation_id` int(11) NOT NULL COMMENT 'connect to corporation or supplier',
  `payment_source_id` int(11) NOT NULL COMMENT 'connect to payment source',
  `value` decimal(19,4) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `payment_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tyc_ref` (`tyc_ref`),
  KEY `receiver_id` (`receiver_corporation_id`),
  KEY `source_id` (`payment_source_id`),
  KEY `currency_id` (`currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tm_payment`
--

TRUNCATE TABLE `tm_payment`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_payment_source`
--

DROP TABLE IF EXISTS `tm_payment_source`;
CREATE TABLE IF NOT EXISTS `tm_payment_source` (
  `id` int(11) NOT NULL,
  `source_code` varchar(4) NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `source_code` (`source_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tm_payment_source`
--

TRUNCATE TABLE `tm_payment_source`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_person`
--

DROP TABLE IF EXISTS `tm_person`;
CREATE TABLE IF NOT EXISTS `tm_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `dob` datetime NOT NULL,
  `entity_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tm_person`
--

TRUNCATE TABLE `tm_person`;
--
-- Dumping data for table `tm_person`
--

INSERT INTO `tm_person` (`id`, `first_name`, `surname`, `dob`, `entity_id`, `create_date`) VALUES
(1, 'test', 'person', '2017-06-07 00:00:00', 1, '2017-06-21 00:00:00'),
(2, 'test', 'person', '2017-06-07 00:00:00', 1, '2017-06-21 00:00:00'),
(3, 'grfrtr', 'rtgrtg', '0000-00-00 00:00:00', 123, '0000-00-00 00:00:00'),
(4, 'achumie', 'kboy', '0000-00-00 00:00:00', 13, '0000-00-00 00:00:00'),
(5, 'firs', 'kboy', '0000-00-00 00:00:00', 14, '0000-00-00 00:00:00'),
(6, 'please work', 'please work', '0000-00-00 00:00:00', 15, '0000-00-00 00:00:00'),
(7, 'please work', 'please work', '0000-00-00 00:00:00', 16, '0000-00-00 00:00:00'),
(8, 'please work', 'please work', '0000-00-00 00:00:00', 17, '0000-00-00 00:00:00'),
(9, 'please work', 'please work', '0000-00-00 00:00:00', 18, '0000-00-00 00:00:00'),
(10, 'please work', 'please work', '0000-00-00 00:00:00', 19, '0000-00-00 00:00:00'),
(11, 'please work', 'please work', '0000-00-00 00:00:00', 20, '0000-00-00 00:00:00'),
(12, 'please work', 'please work', '0000-00-00 00:00:00', 21, '0000-00-00 00:00:00'),
(13, 'please work', 'please work', '0000-00-00 00:00:00', 22, '0000-00-00 00:00:00'),
(14, 'kingsley', 'kingsley', '0000-00-00 00:00:00', 23, '0000-00-00 00:00:00'),
(15, 'kingsley achumie', 'kingsley achumie', '0000-00-00 00:00:00', 25, '0000-00-00 00:00:00'),
(16, 'test', 'kingsley achumie', '0000-00-00 00:00:00', 26, '0000-00-00 00:00:00'),
(17, 'test', 'kingsley achumie', '0000-00-00 00:00:00', 27, '0000-00-00 00:00:00'),
(18, 'test', 'kingsley achumie', '2005-05-06 00:00:00', 28, '0000-00-00 00:00:00'),
(19, 'test', 'kingsley achumie', '2005-05-06 00:00:00', 29, '0000-00-00 00:00:00'),
(20, 'yftftgf', 'yftftgf', '2005-05-06 00:00:00', 30, '0000-00-00 00:00:00'),
(21, 'ftgiulhjk', 'uygiuijik', '0000-00-00 00:00:00', 36, '0000-00-00 00:00:00'),
(22, 'wouldwork', 'wouldwork', '0000-00-00 00:00:00', 37, '0000-00-00 00:00:00'),
(23, 'kingsley', ' achumie', '0000-00-00 00:00:00', 38, '0000-00-00 00:00:00'),
(24, 'asdsad', 'sadfsd', '0000-00-00 00:00:00', 39, '0000-00-00 00:00:00'),
(25, '56y5yw6', 'tt5yr', '0000-00-00 00:00:00', 40, '0000-00-00 00:00:00'),
(26, '34r3r', '3t3t3', '0000-00-00 00:00:00', 41, '0000-00-00 00:00:00'),
(27, '34r3r', '3t3t3', '0000-00-00 00:00:00', 42, '0000-00-00 00:00:00'),
(28, '', '', '0000-00-00 00:00:00', 43, '0000-00-00 00:00:00'),
(29, '', '', '0000-00-00 00:00:00', 44, '0000-00-00 00:00:00'),
(30, '', '', '0000-00-00 00:00:00', 45, '0000-00-00 00:00:00'),
(31, '', '', '0000-00-00 00:00:00', 46, '0000-00-00 00:00:00'),
(32, '', '', '0000-00-00 00:00:00', 47, '0000-00-00 00:00:00'),
(33, '', '', '0000-00-00 00:00:00', 48, '0000-00-00 00:00:00'),
(34, '', '', '0000-00-00 00:00:00', 49, '0000-00-00 00:00:00'),
(35, '', '', '0000-00-00 00:00:00', 50, '0000-00-00 00:00:00'),
(36, 'asd', 'asd', '0000-00-00 00:00:00', 51, '0000-00-00 00:00:00'),
(37, '', '', '0000-00-00 00:00:00', 53, '0000-00-00 00:00:00'),
(38, '', '', '0000-00-00 00:00:00', 54, '0000-00-00 00:00:00'),
(39, '', '', '0000-00-00 00:00:00', 55, '0000-00-00 00:00:00'),
(40, '', '', '0000-00-00 00:00:00', 56, '0000-00-00 00:00:00'),
(41, '', '', '0000-00-00 00:00:00', 57, '0000-00-00 00:00:00'),
(42, '', '', '0000-00-00 00:00:00', 58, '0000-00-00 00:00:00'),
(43, '', '', '0000-00-00 00:00:00', 59, '0000-00-00 00:00:00'),
(44, '', '', '0000-00-00 00:00:00', 60, '0000-00-00 00:00:00'),
(45, '', '', '0000-00-00 00:00:00', 61, '0000-00-00 00:00:00'),
(46, '', '', '0000-00-00 00:00:00', 62, '0000-00-00 00:00:00'),
(47, '', '', '0000-00-00 00:00:00', 63, '0000-00-00 00:00:00'),
(48, '', '', '0000-00-00 00:00:00', 64, '0000-00-00 00:00:00'),
(49, '', '', '0000-00-00 00:00:00', 65, '0000-00-00 00:00:00'),
(50, '', '', '0000-00-00 00:00:00', 66, '0000-00-00 00:00:00'),
(51, '', '', '0000-00-00 00:00:00', 67, '0000-00-00 00:00:00'),
(52, '', '', '0000-00-00 00:00:00', 68, '0000-00-00 00:00:00'),
(53, '', '', '0000-00-00 00:00:00', 69, '0000-00-00 00:00:00'),
(54, '', '', '0000-00-00 00:00:00', 70, '0000-00-00 00:00:00'),
(55, '', '', '0000-00-00 00:00:00', 71, '0000-00-00 00:00:00'),
(56, '', '', '0000-00-00 00:00:00', 72, '0000-00-00 00:00:00'),
(57, '', '', '0000-00-00 00:00:00', 73, '0000-00-00 00:00:00'),
(58, '', '', '0000-00-00 00:00:00', 74, '0000-00-00 00:00:00'),
(59, '', '', '0000-00-00 00:00:00', 75, '0000-00-00 00:00:00'),
(60, '', '', '0000-00-00 00:00:00', 76, '0000-00-00 00:00:00'),
(61, '', '', '0000-00-00 00:00:00', 77, '0000-00-00 00:00:00'),
(62, '', '', '0000-00-00 00:00:00', 78, '0000-00-00 00:00:00'),
(63, '', '', '0000-00-00 00:00:00', 79, '0000-00-00 00:00:00'),
(64, '', '', '0000-00-00 00:00:00', 80, '0000-00-00 00:00:00'),
(65, '', '', '0000-00-00 00:00:00', 81, '0000-00-00 00:00:00'),
(66, '', '', '0000-00-00 00:00:00', 82, '0000-00-00 00:00:00'),
(67, '', '', '0000-00-00 00:00:00', 83, '0000-00-00 00:00:00'),
(68, '', '', '0000-00-00 00:00:00', 84, '0000-00-00 00:00:00'),
(69, '', '', '0000-00-00 00:00:00', 85, '0000-00-00 00:00:00'),
(70, '', '', '0000-00-00 00:00:00', 86, '0000-00-00 00:00:00'),
(71, '', '', '0000-00-00 00:00:00', 87, '0000-00-00 00:00:00'),
(72, '', '', '0000-00-00 00:00:00', 88, '0000-00-00 00:00:00'),
(73, '', '', '0000-00-00 00:00:00', 89, '0000-00-00 00:00:00'),
(74, '', '', '0000-00-00 00:00:00', 90, '0000-00-00 00:00:00'),
(75, '', '', '0000-00-00 00:00:00', 91, '0000-00-00 00:00:00'),
(76, '', '', '0000-00-00 00:00:00', 92, '0000-00-00 00:00:00'),
(77, '', '', '0000-00-00 00:00:00', 93, '0000-00-00 00:00:00'),
(78, '', '', '0000-00-00 00:00:00', 94, '0000-00-00 00:00:00'),
(79, '', '', '0000-00-00 00:00:00', 95, '0000-00-00 00:00:00'),
(80, '', '', '0000-00-00 00:00:00', 96, '0000-00-00 00:00:00'),
(81, '', '', '0000-00-00 00:00:00', 97, '0000-00-00 00:00:00'),
(82, '', '', '0000-00-00 00:00:00', 98, '0000-00-00 00:00:00'),
(83, '', '', '0000-00-00 00:00:00', 99, '0000-00-00 00:00:00'),
(84, '', '', '0000-00-00 00:00:00', 100, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tm_person_corporation`
--

DROP TABLE IF EXISTS `tm_person_corporation`;
CREATE TABLE IF NOT EXISTS `tm_person_corporation` (
  `person_id` int(11) NOT NULL,
  `corporation_id` int(11) NOT NULL,
  KEY `person_id` (`person_id`),
  KEY `corporation_id` (`corporation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tm_person_corporation`
--

TRUNCATE TABLE `tm_person_corporation`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_product`
--

DROP TABLE IF EXISTS `tm_product`;
CREATE TABLE IF NOT EXISTS `tm_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category` int(11) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_attribute` varchar(255) DEFAULT NULL,
  `refinery_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ProductID` (`product_category`),
  KEY `ProductListID` (`product_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_product`
--

TRUNCATE TABLE `tm_product`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_product_attribute`
--

DROP TABLE IF EXISTS `tm_product_attribute`;
CREATE TABLE IF NOT EXISTS `tm_product_attribute` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tm_product_attribute`
--

TRUNCATE TABLE `tm_product_attribute`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_product_corporation`
--

DROP TABLE IF EXISTS `tm_product_corporation`;
CREATE TABLE IF NOT EXISTS `tm_product_corporation` (
  `product_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  KEY `productdescription_id` (`product_id`),
  KEY `corporation_id` (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tm_product_corporation`
--

TRUNCATE TABLE `tm_product_corporation`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_project`
--

DROP TABLE IF EXISTS `tm_project`;
CREATE TABLE IF NOT EXISTS `tm_project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `tyc_ref` varchar(16) DEFAULT NULL,
  `component_id` int(11) NOT NULL,
  `pro_description` varchar(255) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `client_reference` varchar(16) DEFAULT NULL,
  `manufacturer_ref` varchar(16) DEFAULT NULL,
  `project_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`project_id`) USING BTREE,
  KEY `Client` (`client_id`),
  KEY `Supplier` (`supplier_id`),
  KEY `Folder` (`tyc_ref`),
  KEY `component_id` (`component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_project`
--

TRUNCATE TABLE `tm_project`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_project_component`
--

DROP TABLE IF EXISTS `tm_project_component`;
CREATE TABLE IF NOT EXISTS `tm_project_component` (
  `project_id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  PRIMARY KEY (`project_id`,`component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Truncate table before insert `tm_project_component`
--

TRUNCATE TABLE `tm_project_component`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_receivedpurchaseorder`
--

DROP TABLE IF EXISTS `tm_receivedpurchaseorder`;
CREATE TABLE IF NOT EXISTS `tm_receivedpurchaseorder` (
  `receivedpurchaseorder_reference` varchar(16) NOT NULL,
  `component_id` int(11) NOT NULL,
  `receivedpurchaseorder_type` varchar(16) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `order_value` decimal(19,4) DEFAULT NULL,
  `order_currency` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `issue_date` datetime DEFAULT NULL,
  `receivedpurchaseorder_duedate` datetime DEFAULT NULL,
  `final_delivery_date` datetime DEFAULT NULL,
  PRIMARY KEY (`receivedpurchaseorder_reference`),
  KEY `client_id` (`client_id`,`supplier_id`),
  KEY `RPO Component` (`component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_receivedpurchaseorder`
--

TRUNCATE TABLE `tm_receivedpurchaseorder`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_receivedpurchaseorder_component`
--

DROP TABLE IF EXISTS `tm_receivedpurchaseorder_component`;
CREATE TABLE IF NOT EXISTS `tm_receivedpurchaseorder_component` (
  `receivedpurchaseorder_reference` varchar(255) NOT NULL,
  `component_id` int(11) NOT NULL,
  PRIMARY KEY (`receivedpurchaseorder_reference`(50),`component_id`),
  KEY `receivedpurchaseorder_Reference` (`receivedpurchaseorder_reference`),
  KEY `order_number` (`component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Truncate table before insert `tm_receivedpurchaseorder_component`
--

TRUNCATE TABLE `tm_receivedpurchaseorder_component`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_receivedpurchaseorder_type`
--

DROP TABLE IF EXISTS `tm_receivedpurchaseorder_type`;
CREATE TABLE IF NOT EXISTS `tm_receivedpurchaseorder_type` (
  `receivedpurchaseorder_type_code` int(11) NOT NULL AUTO_INCREMENT,
  `receivedpurchaseorder_type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`receivedpurchaseorder_type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_receivedpurchaseorder_type`
--

TRUNCATE TABLE `tm_receivedpurchaseorder_type`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_remark`
--

DROP TABLE IF EXISTS `tm_remark`;
CREATE TABLE IF NOT EXISTS `tm_remark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tyc_ref` varchar(16) DEFAULT NULL,
  `project_id` varchar(255) NOT NULL,
  `remark_type` varchar(255) DEFAULT NULL,
  `remark_date` datetime NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tyc_ref` (`tyc_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_remark`
--

TRUNCATE TABLE `tm_remark`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_status_type`
--

DROP TABLE IF EXISTS `tm_status_type`;
CREATE TABLE IF NOT EXISTS `tm_status_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `phase` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_status_type`
--

TRUNCATE TABLE `tm_status_type`;
-- --------------------------------------------------------

--
-- Table structure for table `tm_supplier`
--

DROP TABLE IF EXISTS `tm_supplier`;
CREATE TABLE IF NOT EXISTS `tm_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporation_id` int(11) NOT NULL,
  `supplier_type` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `tm_supplier`
--

TRUNCATE TABLE `tm_supplier`;
--
-- Dumping data for table `tm_supplier`
--

INSERT INTO `tm_supplier` (`id`, `corporation_id`, `supplier_type`, `notes`) VALUES
(1, 1, 'hggchg', 'hgjhghb'),
(2, 2, 'kjbjkb', 'kjnjkn');

-- --------------------------------------------------------

--
-- Table structure for table `tm_telephone`
--

DROP TABLE IF EXISTS `tm_telephone`;
CREATE TABLE IF NOT EXISTS `tm_telephone` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `telephone_number` varchar(18) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tm_telephone`
--

TRUNCATE TABLE `tm_telephone`;
--
-- Dumping data for table `tm_telephone`
--

INSERT INTO `tm_telephone` (`id`, `telephone_number`) VALUES
(1, 'rtgrgrtg'),
(2, '08153259099'),
(3, '08153259099'),
(4, '0989900909'),
(5, '0989900909'),
(6, '0989900909'),
(7, '0989900909'),
(8, '0989900909'),
(9, '0989900909'),
(10, '0989900909'),
(11, '0989900909'),
(12, '0989900909'),
(13, '0989900909'),
(14, '939393939'),
(15, '567890'),
(16, '4567890'),
(17, '4567890'),
(18, '4567890'),
(19, '4567890'),
(20, '4567890'),
(21, 'yftftgf'),
(22, '1234567'),
(23, '07030355943'),
(24, '09033449900'),
(25, '432535'),
(26, 'rhtrth'),
(27, '34t34t'),
(28, '34t34t'),
(29, ''),
(30, ''),
(31, ''),
(32, ''),
(33, ''),
(34, ''),
(35, ''),
(36, ''),
(37, ''),
(38, ''),
(39, ''),
(40, ''),
(41, '424224'),
(42, '444444'),
(43, ''),
(44, ''),
(45, ''),
(46, ''),
(47, ''),
(48, ''),
(49, ''),
(50, ''),
(51, ''),
(52, ''),
(53, ''),
(54, ''),
(55, ''),
(56, ''),
(57, ''),
(58, ''),
(59, ''),
(60, ''),
(61, ''),
(62, ''),
(63, ''),
(64, ''),
(65, ''),
(66, ''),
(67, ''),
(68, ''),
(69, ''),
(70, ''),
(71, ''),
(72, ''),
(73, ''),
(74, ''),
(75, ''),
(76, ''),
(77, ''),
(78, ''),
(79, ''),
(80, ''),
(81, ''),
(82, ''),
(83, ''),
(84, ''),
(85, ''),
(86, ''),
(87, ''),
(88, ''),
(89, ''),
(90, '');

-- --------------------------------------------------------

--
-- Table structure for table `tm_telephone_entity`
--

DROP TABLE IF EXISTS `tm_telephone_entity`;
CREATE TABLE IF NOT EXISTS `tm_telephone_entity` (
  `telephone_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Truncate table before insert `tm_telephone_entity`
--

TRUNCATE TABLE `tm_telephone_entity`;
--
-- Dumping data for table `tm_telephone_entity`
--

INSERT INTO `tm_telephone_entity` (`telephone_id`, `entity_id`) VALUES
(10, 21),
(10, 23),
(10, 24),
(10, 25),
(10, 26),
(10, 27),
(10, 28),
(10, 29),
(21, 30),
(22, 36),
(23, 37),
(24, 38),
(25, 39),
(26, 40),
(27, 41),
(28, 42),
(33, 43),
(34, 44),
(35, 45),
(36, 46),
(37, 47),
(38, 48),
(39, 49),
(40, 50),
(41, 51),
(10, 52),
(43, 53),
(44, 54),
(45, 55),
(46, 56),
(47, 57),
(48, 58),
(49, 59),
(50, 60),
(51, 61),
(52, 62),
(53, 63),
(54, 64),
(55, 65),
(56, 66),
(57, 67),
(58, 68),
(59, 69),
(60, 70),
(61, 71),
(62, 72),
(63, 73),
(64, 74),
(65, 75),
(66, 76),
(67, 77),
(68, 78),
(69, 79),
(70, 80),
(71, 81),
(72, 82),
(73, 83),
(74, 84),
(75, 85),
(76, 86),
(77, 87),
(78, 88),
(79, 89),
(80, 90),
(81, 91),
(82, 92),
(83, 93),
(84, 94),
(85, 95),
(86, 96),
(87, 97),
(88, 98),
(89, 99),
(90, 100);

-- --------------------------------------------------------

--
-- Table structure for table `tm_user`
--

DROP TABLE IF EXISTS `tm_user`;
CREATE TABLE IF NOT EXISTS `tm_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `access_level` int(9) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `authKey` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tm_user`
--

TRUNCATE TABLE `tm_user`;
--
-- Dumping data for table `tm_user`
--

INSERT INTO `tm_user` (`id`, `person_id`, `access_level`, `username`, `password`, `salt`, `authKey`) VALUES
(1, 1, 2, 'demo1', 'demo1', 'derfgrfg', ''),
(2, 1, 1, '1', '1', '1', ''),
(3, 3, 3, '3', '3', '3', ''),
(4, 6, 0, '', '', '', ''),
(5, 7, 0, '', '', '', ''),
(6, 8, 0, '', '', '', ''),
(7, 9, 0, '', '', '', ''),
(8, 10, 0, '', '', '', ''),
(9, 11, 0, '', '', '', ''),
(10, 12, 0, '', '', '', ''),
(11, 14, 393939, 'kingsley', 'kingsley', '', ''),
(12, 20, 23456, 'yftftgf', 'yftftgf', '', ''),
(13, 35, 12, 'abc', 'abc', '', '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tm_client`
--
ALTER TABLE `tm_client`
  ADD CONSTRAINT `tm_client_ibfk_1` FOREIGN KEY (`corporation_id`) REFERENCES `tm_corporation` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_invoice`
--
ALTER TABLE `tm_invoice`
  ADD CONSTRAINT `Invoice Component` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`);

--
-- Constraints for table `tm_order`
--
ALTER TABLE `tm_order`
  ADD CONSTRAINT `Component` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`);

--
-- Constraints for table `tm_person_corporation`
--
ALTER TABLE `tm_person_corporation`
  ADD CONSTRAINT `tm_person_corporation_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `tm_person` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tm_person_corporation_ibfk_2` FOREIGN KEY (`corporation_id`) REFERENCES `tm_corporation` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_product_corporation`
--
ALTER TABLE `tm_product_corporation`
  ADD CONSTRAINT `tm_product_corporation_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `tm_product` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tm_product_corporation_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `tm_corporation` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_project`
--
ALTER TABLE `tm_project`
  ADD CONSTRAINT `Client` FOREIGN KEY (`client_id`) REFERENCES `tm_client` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `Folder` FOREIGN KEY (`tyc_ref`) REFERENCES `tm_folder` (`tyc_ref`) ON UPDATE CASCADE,
  ADD CONSTRAINT `Project Component` FOREIGN KEY (`component_id`) REFERENCES `tm_corporation` (`id`),
  ADD CONSTRAINT `Supplier` FOREIGN KEY (`supplier_id`) REFERENCES `tm_supplier` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_receivedpurchaseorder`
--
ALTER TABLE `tm_receivedpurchaseorder`
  ADD CONSTRAINT `RPO Component` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`);

--
-- Constraints for table `tm_remark`
--
ALTER TABLE `tm_remark`
  ADD CONSTRAINT `tm_remark_ibfk_1` FOREIGN KEY (`tyc_ref`) REFERENCES `tm_folder` (`tyc_ref`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
