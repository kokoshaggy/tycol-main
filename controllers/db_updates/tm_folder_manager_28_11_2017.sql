-- phpMyAdmin SQL Dump
-- version 4.0.10.15
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 28, 2017 at 07:57 AM
-- Server version: 5.1.68-community-log
-- PHP Version: 5.3.29

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `BBBm84a_tycol_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_folder_manager`
--

CREATE TABLE IF NOT EXISTS `tm_folder_manager` (
  `tyc_ref` varchar(16) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role` enum('author','user','','') NOT NULL,
  PRIMARY KEY (`tyc_ref`,`user_id`),
  KEY `FolderManager` (`user_id`),
  KEY `FolderItem` (`tyc_ref`),
  KEY `ReverseFM` (`user_id`,`tyc_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tm_folder_manager`
--
ALTER TABLE `tm_folder_manager`
  ADD CONSTRAINT `FolderItem` FOREIGN KEY (`tyc_ref`) REFERENCES `tm_folder` (`tyc_ref`),
  ADD CONSTRAINT `FolderManager` FOREIGN KEY (`user_id`) REFERENCES `tm_user` (`id`);
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
