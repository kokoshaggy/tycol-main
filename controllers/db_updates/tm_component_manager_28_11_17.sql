-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 28, 2017 at 10:08 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tycol_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_component_manager`
--

CREATE TABLE IF NOT EXISTS `tm_component_manager` (
  `component_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  KEY `ComponentItem` (`component_id`),
  KEY `Manager` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tm_component_manager`
--
ALTER TABLE `tm_component_manager`
  ADD CONSTRAINT `ComponentItem` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`),
  ADD CONSTRAINT `Manager` FOREIGN KEY (`user_id`) REFERENCES `tm_user` (`id`);
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
