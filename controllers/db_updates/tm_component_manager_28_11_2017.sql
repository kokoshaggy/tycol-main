-- phpMyAdmin SQL Dump
-- version 4.0.10.15
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 28, 2017 at 07:58 AM
-- Server version: 5.1.68-community-log
-- PHP Version: 5.3.29

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `BBBm84a_tycol_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_component_manager`
--

CREATE TABLE IF NOT EXISTS `tm_component_manager` (
  `component_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role` enum('author','user','','') NOT NULL,
  PRIMARY KEY (`component_id`,`user_id`),
  KEY `ComponentItem` (`component_id`),
  KEY `Manager` (`user_id`),
  KEY `ReverseUC` (`user_id`,`component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tm_component_manager`
--
ALTER TABLE `tm_component_manager`
  ADD CONSTRAINT `ComponentItem` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`),
  ADD CONSTRAINT `Manager` FOREIGN KEY (`user_id`) REFERENCES `tm_user` (`id`);
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
