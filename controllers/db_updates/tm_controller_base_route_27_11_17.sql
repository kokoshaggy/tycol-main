-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2017 at 03:01 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tycol_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_controller_base_route`
--

DROP TABLE IF EXISTS `tm_controller_base_route`;
CREATE TABLE IF NOT EXISTS `tm_controller_base_route` (
  `name` varchar(255) NOT NULL,
  `base_route` varchar(255) NOT NULL,
  `min_permission` int(2) DEFAULT NULL,
  `roles` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tm_controller_base_route`
--

INSERT INTO `tm_controller_base_route` (`name`, `base_route`, `min_permission`, `roles`) VALUES
('Correspondence', 'correspondence', NULL, NULL),
('EDocument', 'e-document', NULL, NULL),
('Folder', 'folder', NULL, NULL),
('Invoice', 'invoice', NULL, NULL),
('Order', 'order', NULL, NULL),
('Payment', 'payment', NULL, NULL),
('Project', 'project', NULL, NULL),
('Receivedpurchaseorder', 'receivedpurchaseorder', NULL, NULL),
('Site', 'site', NULL, NULL),
('User', 'user', NULL, NULL);
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
