-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2018 at 11:07 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tycol_main`
--
CREATE DATABASE IF NOT EXISTS `tycol_main` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `tycol_main`;

-- --------------------------------------------------------

--
-- Table structure for table `tm_access_permission`
--

CREATE TABLE IF NOT EXISTS `tm_access_permission` (
  `action` varchar(255) NOT NULL,
  `access_value` int(2) NOT NULL,
  `type` enum('action','other') NOT NULL,
  PRIMARY KEY (`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_address`
--

CREATE TABLE IF NOT EXISTS `tm_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_line` varchar(255) NOT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_address_entity`
--

CREATE TABLE IF NOT EXISTS `tm_address_entity` (
  `address_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  UNIQUE KEY `address_id_2` (`address_id`,`entity_id`),
  KEY `address_id` (`address_id`),
  KEY `corporation_id` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tm_asset`
--

CREATE TABLE IF NOT EXISTS `tm_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(255) NOT NULL,
  `unique_identifier` varchar(255) NOT NULL,
  `asset_condition` enum('New','Almost New','Old','Old - Working','Old - Not Working','Old - Damaged','Unusable') NOT NULL,
  `notes` varchar(255) NOT NULL,
  `last_updated` date NOT NULL,
  `deleted` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_identifier` (`unique_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_asset_attribute`
--

CREATE TABLE IF NOT EXISTS `tm_asset_attribute` (
  `asset_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  KEY `AssetAttribute` (`asset_id`),
  KEY `AttributeAsset` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_attribute`
--

CREATE TABLE IF NOT EXISTS `tm_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tm_categoroy_corporation`
--

CREATE TABLE IF NOT EXISTS `tm_categoroy_corporation` (
  `category_id` int(11) NOT NULL,
  `corporation_id` int(11) NOT NULL,
  KEY `productcategory_id` (`category_id`),
  KEY `corporation_id` (`corporation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_category`
--

CREATE TABLE IF NOT EXISTS `tm_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_client`
--

CREATE TABLE IF NOT EXISTS `tm_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporation_id` int(11) NOT NULL,
  `last_updated` date NOT NULL,
  `deleted` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `corporation_id_2` (`corporation_id`),
  KEY `corporation_id` (`corporation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_component`
--

CREATE TABLE IF NOT EXISTS `tm_component` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_type` enum('project','order','invoice','received_purchase_order','payment','correspondence','edocument') NOT NULL,
  `component_classname` varchar(255) NOT NULL COMMENT 'namespaced full classname',
  `component_junction` varchar(255) NOT NULL COMMENT 'component to component table for this component',
  `junction_foreign_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_component_manager`
--

CREATE TABLE IF NOT EXISTS `tm_component_manager` (
  `component_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role` enum('author','user','','') NOT NULL,
  PRIMARY KEY (`component_id`,`user_id`),
  KEY `ComponentItem` (`component_id`),
  KEY `Manager` (`user_id`),
  KEY `ReverseUC` (`user_id`,`component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_component_task`
--

CREATE TABLE IF NOT EXISTS `tm_component_task` (
  `component_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  KEY `TaskComponent` (`component_id`),
  KEY `ComponentTask` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_component_types`
--

CREATE TABLE IF NOT EXISTS `tm_component_types` (
  `component_name` varchar(255) NOT NULL,
  `component_simplename` varchar(255) NOT NULL,
  `component_classname` varchar(255) NOT NULL,
  `component_junction_model` varchar(255) NOT NULL,
  `junction_foreign_key` varchar(255) NOT NULL,
  PRIMARY KEY (`component_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tm_controller_base_route`
--

CREATE TABLE IF NOT EXISTS `tm_controller_base_route` (
  `name` varchar(255) NOT NULL,
  `base_route` varchar(255) NOT NULL,
  `min_permission` int(2) DEFAULT NULL,
  `roles` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tm_corporation`
--

CREATE TABLE IF NOT EXISTS `tm_corporation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `short_name` varchar(5) DEFAULT NULL,
  `entity_id` int(11) NOT NULL,
  `notes` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_correspondence`
--

CREATE TABLE IF NOT EXISTS `tm_correspondence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tyc_ref` varchar(16) NOT NULL,
  `component_id` int(11) NOT NULL,
  `particular_reference` varchar(16) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title_description` varchar(255) NOT NULL,
  `content` longblob NOT NULL,
  `corresponding_entity` int(11) NOT NULL,
  `specific_corresponding_entity` int(11) DEFAULT NULL,
  `drafting_entity` int(11) NOT NULL,
  `authorising_entity` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `notes` longtext,
  `last_updated` datetime NOT NULL,
  `deleted` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_correspondence_component`
--

CREATE TABLE IF NOT EXISTS `tm_correspondence_component` (
  `correspondence_id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  UNIQUE KEY `corrrespondence_id` (`correspondence_id`,`component_id`),
  KEY `CorrespondenceItem` (`component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_country`
--

CREATE TABLE IF NOT EXISTS `tm_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_currency`
--

CREATE TABLE IF NOT EXISTS `tm_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) NOT NULL,
  `currency_code` varchar(5) NOT NULL,
  `currency_title` varchar(255) NOT NULL,
  `symbol` varchar(1) DEFAULT NULL,
  `unit_text` varchar(32) DEFAULT NULL COMMENT 'dollar, pound etc',
  `subunit_text` varchar(32) DEFAULT NULL COMMENT 'cents,pennies etc',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_device`
--

CREATE TABLE IF NOT EXISTS `tm_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_serial` varchar(500) DEFAULT NULL,
  `authKey` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `last_used` datetime NOT NULL,
  `valid_to` datetime DEFAULT NULL COMMENT 'Determines how long this device may be used',
  PRIMARY KEY (`id`),
  UNIQUE KEY `authKey` (`authKey`),
  KEY `Status` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_device_access_token`
--

CREATE TABLE IF NOT EXISTS `tm_device_access_token` (
  `token` varchar(10) NOT NULL,
  `valid_to` datetime NOT NULL,
  `device_string` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`token`),
  KEY `User` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_email`
--

CREATE TABLE IF NOT EXISTS `tm_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_email_entity`
--

CREATE TABLE IF NOT EXISTS `tm_email_entity` (
  `email_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  PRIMARY KEY (`email_id`,`entity_id`),
  KEY `Entity Email` (`email_id`,`entity_id`) USING BTREE,
  KEY `tm_email_entity_ibfk_2` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tm_entity`
--

CREATE TABLE IF NOT EXISTS `tm_entity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type` enum('person','corporation','','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8 COMMENT='Connects to Persons and Corporate to allow payments from/to ';

-- --------------------------------------------------------

--
-- Table structure for table `tm_e_document`
--

CREATE TABLE IF NOT EXISTS `tm_e_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference` varchar(25) DEFAULT NULL,
  `component_id` int(11) NOT NULL,
  `file_location` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_e_document_component`
--

CREATE TABLE IF NOT EXISTS `tm_e_document_component` (
  `edocument_id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  UNIQUE KEY `edocument_id` (`edocument_id`,`component_id`),
  UNIQUE KEY `edocument_id_2` (`edocument_id`,`component_id`),
  KEY `EDocumentComponents` (`component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_folder`
--

CREATE TABLE IF NOT EXISTS `tm_folder` (
  `tyc_ref` varchar(16) NOT NULL,
  `type` enum('project','administrative') NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `last_updated` date NOT NULL,
  `deleted` int(1) NOT NULL,
  PRIMARY KEY (`tyc_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tm_folder_component`
--

CREATE TABLE IF NOT EXISTS `tm_folder_component` (
  `tyc_ref` varchar(16) NOT NULL,
  `component_id` int(11) NOT NULL,
  UNIQUE KEY `tyc_ref` (`tyc_ref`,`component_id`),
  KEY `FolderComponent` (`component_id`),
  KEY `ComponentFolder` (`tyc_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_folder_manager`
--

CREATE TABLE IF NOT EXISTS `tm_folder_manager` (
  `tyc_ref` varchar(16) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role` enum('author','user','','') NOT NULL,
  PRIMARY KEY (`tyc_ref`,`user_id`),
  KEY `FolderManager` (`user_id`),
  KEY `FolderItem` (`tyc_ref`),
  KEY `ReverseFM` (`user_id`,`tyc_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_folder_task`
--

CREATE TABLE IF NOT EXISTS `tm_folder_task` (
  `folder_ref` varchar(16) NOT NULL,
  `task_id` int(11) NOT NULL,
  UNIQUE KEY `folder_ref` (`folder_ref`,`task_id`),
  KEY `TaskFolder` (`folder_ref`),
  KEY `FolderTask` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_invoice`
--

CREATE TABLE IF NOT EXISTS `tm_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_id` int(11) NOT NULL,
  `invoice_reference` varchar(30) DEFAULT NULL,
  `receivedpurchaseorder_id` varchar(30) NOT NULL,
  `description` varchar(255) NOT NULL,
  `amount` decimal(19,4) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `creation_date` date DEFAULT NULL,
  `last_updated` datetime NOT NULL,
  `deleted` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `currency_id` (`currency_id`),
  KEY `receivedpurchaseorder_id` (`receivedpurchaseorder_id`),
  KEY `Invoice Component` (`component_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_invoice_component`
--

CREATE TABLE IF NOT EXISTS `tm_invoice_component` (
  `invoice_id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  UNIQUE KEY `invoice_id` (`invoice_id`,`component_id`),
  KEY `InvoiceComponent` (`component_id`),
  KEY `ComponentInvoice` (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_order`
--

CREATE TABLE IF NOT EXISTS `tm_order` (
  `order_number` int(4) NOT NULL,
  `tyc_ref` varchar(16) DEFAULT NULL,
  `component_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier_ref` varchar(30) DEFAULT NULL,
  `order_value` decimal(19,4) NOT NULL,
  `order_currency` int(11) NOT NULL,
  `issue_date` date NOT NULL,
  `supplier_completion_date` date DEFAULT NULL,
  `order_status` varchar(255) DEFAULT NULL,
  `order_file` longblob,
  `last_updated` datetime NOT NULL,
  `deleted` int(1) NOT NULL,
  PRIMARY KEY (`order_number`),
  KEY `Manufacturer_ID` (`supplier_id`),
  KEY `tyc_ref` (`tyc_ref`,`supplier_id`,`supplier_ref`),
  KEY `manufacturer_ref` (`supplier_ref`),
  KEY `Order Components` (`component_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_order_component`
--

CREATE TABLE IF NOT EXISTS `tm_order_component` (
  `order_number` int(11) NOT NULL,
  `componenet_id` int(11) NOT NULL,
  KEY `OrderComponent` (`componenet_id`),
  KEY `ComponentOrder` (`order_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_payment`
--

CREATE TABLE IF NOT EXISTS `tm_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_id` int(11) NOT NULL,
  `receiver_corporation_id` int(11) NOT NULL COMMENT 'connect to corporation or supplier',
  `payment_source_id` int(11) NOT NULL COMMENT 'connect to payment source',
  `value` decimal(19,4) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `payment_date` datetime NOT NULL,
  `last_updated` date NOT NULL,
  `deleted` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `receiver_id` (`receiver_corporation_id`),
  KEY `source_id` (`payment_source_id`),
  KEY `currency_id` (`currency_id`),
  KEY `PaymentComponent` (`component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_payment_component`
--

CREATE TABLE IF NOT EXISTS `tm_payment_component` (
  `payment_id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  UNIQUE KEY `payment_id` (`payment_id`,`component_id`),
  KEY `PaymentComponents` (`component_id`),
  KEY `ComponentPayments` (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_payment_source`
--

CREATE TABLE IF NOT EXISTS `tm_payment_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_code` varchar(7) NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `source_code` (`source_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_person`
--

CREATE TABLE IF NOT EXISTS `tm_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `dob` datetime NOT NULL,
  `entity_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_person_corporation`
--

CREATE TABLE IF NOT EXISTS `tm_person_corporation` (
  `person_id` int(11) NOT NULL,
  `corporation_id` int(11) NOT NULL,
  KEY `person_id` (`person_id`),
  KEY `corporation_id` (`corporation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_product`
--

CREATE TABLE IF NOT EXISTS `tm_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category` int(11) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `main_attribute` varchar(255) DEFAULT NULL,
  `refinery_location` varchar(255) DEFAULT NULL,
  `last_updated` date NOT NULL,
  `deleted` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ProductID` (`product_category`),
  KEY `ProductListID` (`product_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_product_attribute`
--

CREATE TABLE IF NOT EXISTS `tm_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  KEY `ProductAttribute` (`attribute_id`),
  KEY `AttributeProduct` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_product_corporation`
--

CREATE TABLE IF NOT EXISTS `tm_product_corporation` (
  `product_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  KEY `productdescription_id` (`product_id`),
  KEY `corporation_id` (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_project`
--

CREATE TABLE IF NOT EXISTS `tm_project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `component_id` int(11) NOT NULL,
  `pro_description` varchar(255) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `client_reference` varchar(30) DEFAULT NULL,
  `manufacturer_ref` varchar(30) DEFAULT NULL,
  `project_status` varchar(255) DEFAULT NULL,
  `last_updated` date NOT NULL,
  `deleted` int(1) NOT NULL,
  PRIMARY KEY (`project_id`) USING BTREE,
  KEY `Client` (`client_id`),
  KEY `Supplier` (`supplier_id`),
  KEY `component_id` (`component_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_project_component`
--

CREATE TABLE IF NOT EXISTS `tm_project_component` (
  `project_id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  PRIMARY KEY (`project_id`,`component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tm_receivedpurchaseorder`
--

CREATE TABLE IF NOT EXISTS `tm_receivedpurchaseorder` (
  `receivedpurchaseorder_reference` varchar(30) NOT NULL,
  `component_id` int(11) NOT NULL,
  `receivedpurchaseorder_type` enum('Letter of Credit','Local Purchase Order','Internal Order') DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `order_value` decimal(19,4) DEFAULT NULL,
  `order_currency` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `receivedpurchaseorder_duedate` date DEFAULT NULL,
  `final_delivery_date` date DEFAULT NULL,
  `last_updated` datetime NOT NULL,
  `deleted` int(1) NOT NULL,
  PRIMARY KEY (`receivedpurchaseorder_reference`),
  KEY `client_id` (`client_id`,`supplier_id`),
  KEY `RPO Component` (`component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_receivedpurchaseorder_component`
--

CREATE TABLE IF NOT EXISTS `tm_receivedpurchaseorder_component` (
  `receivedpurchaseorder_reference` varchar(255) NOT NULL,
  `component_id` int(11) NOT NULL,
  PRIMARY KEY (`receivedpurchaseorder_reference`(50),`component_id`),
  KEY `receivedpurchaseorder_Reference` (`receivedpurchaseorder_reference`),
  KEY `order_number` (`component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tm_receivedpurchaseorder_type`
--

CREATE TABLE IF NOT EXISTS `tm_receivedpurchaseorder_type` (
  `receivedpurchaseorder_type_code` int(11) NOT NULL AUTO_INCREMENT,
  `receivedpurchaseorder_type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`receivedpurchaseorder_type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_remark`
--

CREATE TABLE IF NOT EXISTS `tm_remark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tyc_ref` varchar(16) DEFAULT NULL,
  `project_id` varchar(255) NOT NULL,
  `remark_type` varchar(255) DEFAULT NULL,
  `remark_date` datetime NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tyc_ref` (`tyc_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_reminder`
--

CREATE TABLE IF NOT EXISTS `tm_reminder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reminder_time` datetime NOT NULL,
  `notes` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL,
  `deleted` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_role`
--

CREATE TABLE IF NOT EXISTS `tm_role` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `access_level` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `access_level` (`access_level`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tm_state`
--

CREATE TABLE IF NOT EXISTS `tm_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `Country` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4121 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tm_status_type`
--

CREATE TABLE IF NOT EXISTS `tm_status_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_group` varchar(255) DEFAULT NULL,
  `status_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_supplier`
--

CREATE TABLE IF NOT EXISTS `tm_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporation_id` int(11) NOT NULL,
  `supplier_type` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `last_updated` date NOT NULL,
  `deleted` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_task`
--

CREATE TABLE IF NOT EXISTS `tm_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `details` varchar(255) NOT NULL,
  `owner` int(11) NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `deleted` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `OwnerUser` (`owner`),
  KEY `AssignedToUser` (`assigned_to`),
  KEY `TaskStatus` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_task_reminder`
--

CREATE TABLE IF NOT EXISTS `tm_task_reminder` (
  `reminder_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  KEY `TaskReminder` (`reminder_id`),
  KEY `ReminderTask` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tm_telephone`
--

CREATE TABLE IF NOT EXISTS `tm_telephone` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `telephone_number` varchar(18) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_telephone_entity`
--

CREATE TABLE IF NOT EXISTS `tm_telephone_entity` (
  `telephone_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  PRIMARY KEY (`telephone_id`,`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tm_user`
--

CREATE TABLE IF NOT EXISTS `tm_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `basic_role` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `authKey` varchar(255) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `deleted` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Role` (`basic_role`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_user_device`
--

CREATE TABLE IF NOT EXISTS `tm_user_device` (
  `user_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  UNIQUE KEY `user_id` (`user_id`,`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tm_user_route_access`
--

CREATE TABLE IF NOT EXISTS `tm_user_route_access` (
  `user_id` int(11) NOT NULL,
  `route_name` varchar(255) NOT NULL,
  `access_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tm_asset_attribute`
--
ALTER TABLE `tm_asset_attribute`
  ADD CONSTRAINT `AssetAttribute` FOREIGN KEY (`attribute_id`) REFERENCES `tm_attribute` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `AttributeAsset` FOREIGN KEY (`asset_id`) REFERENCES `tm_asset` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_categoroy_corporation`
--
ALTER TABLE `tm_categoroy_corporation`
  ADD CONSTRAINT `CategoryCorporation` FOREIGN KEY (`corporation_id`) REFERENCES `tm_corporation` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `CorporationCategory` FOREIGN KEY (`category_id`) REFERENCES `tm_category` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_client`
--
ALTER TABLE `tm_client`
  ADD CONSTRAINT `tm_client_ibfk_1` FOREIGN KEY (`corporation_id`) REFERENCES `tm_corporation` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_component_manager`
--
ALTER TABLE `tm_component_manager`
  ADD CONSTRAINT `ComponentItem` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`),
  ADD CONSTRAINT `Manager` FOREIGN KEY (`user_id`) REFERENCES `tm_user` (`id`);

--
-- Constraints for table `tm_component_task`
--
ALTER TABLE `tm_component_task`
  ADD CONSTRAINT `ComponentTasks` FOREIGN KEY (`task_id`) REFERENCES `tm_task` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `TaskComponents` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_correspondence_component`
--
ALTER TABLE `tm_correspondence_component`
  ADD CONSTRAINT `ComponentCorrespondences` FOREIGN KEY (`correspondence_id`) REFERENCES `tm_correspondence` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `CorrespondenceComponents` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_device`
--
ALTER TABLE `tm_device`
  ADD CONSTRAINT `Status` FOREIGN KEY (`status_id`) REFERENCES `tm_status_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tm_device_access_token`
--
ALTER TABLE `tm_device_access_token`
  ADD CONSTRAINT `User` FOREIGN KEY (`user_id`) REFERENCES `tm_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tm_e_document_component`
--
ALTER TABLE `tm_e_document_component`
  ADD CONSTRAINT `ComponentEDocuments` FOREIGN KEY (`edocument_id`) REFERENCES `tm_e_document` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `EDocumentComponents` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_folder_component`
--
ALTER TABLE `tm_folder_component`
  ADD CONSTRAINT `ComponentFolders` FOREIGN KEY (`tyc_ref`) REFERENCES `tm_folder` (`tyc_ref`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FolderComponents` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_folder_manager`
--
ALTER TABLE `tm_folder_manager`
  ADD CONSTRAINT `FolderItem` FOREIGN KEY (`tyc_ref`) REFERENCES `tm_folder` (`tyc_ref`),
  ADD CONSTRAINT `FolderManager` FOREIGN KEY (`user_id`) REFERENCES `tm_user` (`id`);

--
-- Constraints for table `tm_folder_task`
--
ALTER TABLE `tm_folder_task`
  ADD CONSTRAINT `FolderTask` FOREIGN KEY (`task_id`) REFERENCES `tm_task` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `TaskFolder` FOREIGN KEY (`folder_ref`) REFERENCES `tm_folder` (`tyc_ref`);

--
-- Constraints for table `tm_invoice`
--
ALTER TABLE `tm_invoice`
  ADD CONSTRAINT `InvoiceComponent` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_invoice_component`
--
ALTER TABLE `tm_invoice_component`
  ADD CONSTRAINT `ComponentInvoices` FOREIGN KEY (`invoice_id`) REFERENCES `tm_invoice` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `InvoiceComponents` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_order`
--
ALTER TABLE `tm_order`
  ADD CONSTRAINT `OrderComponent` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_order_component`
--
ALTER TABLE `tm_order_component`
  ADD CONSTRAINT `ComponentOrders` FOREIGN KEY (`order_number`) REFERENCES `tm_order` (`order_number`) ON UPDATE CASCADE,
  ADD CONSTRAINT `OrderComponents` FOREIGN KEY (`componenet_id`) REFERENCES `tm_component` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_payment`
--
ALTER TABLE `tm_payment`
  ADD CONSTRAINT `PaymentComponent` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_payment_component`
--
ALTER TABLE `tm_payment_component`
  ADD CONSTRAINT `ComponentPayments` FOREIGN KEY (`payment_id`) REFERENCES `tm_payment` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `PaymentComponents` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_person_corporation`
--
ALTER TABLE `tm_person_corporation`
  ADD CONSTRAINT `tm_person_corporation_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `tm_person` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tm_person_corporation_ibfk_2` FOREIGN KEY (`corporation_id`) REFERENCES `tm_corporation` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_product_attribute`
--
ALTER TABLE `tm_product_attribute`
  ADD CONSTRAINT `AttributeProduct` FOREIGN KEY (`product_id`) REFERENCES `tm_product` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ProductAttribute` FOREIGN KEY (`attribute_id`) REFERENCES `tm_attribute` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_product_corporation`
--
ALTER TABLE `tm_product_corporation`
  ADD CONSTRAINT `tm_product_corporation_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `tm_product` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tm_product_corporation_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `tm_corporation` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_project`
--
ALTER TABLE `tm_project`
  ADD CONSTRAINT `Client` FOREIGN KEY (`client_id`) REFERENCES `tm_client` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ProjectComponent` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `Supplier` FOREIGN KEY (`supplier_id`) REFERENCES `tm_supplier` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_receivedpurchaseorder`
--
ALTER TABLE `tm_receivedpurchaseorder`
  ADD CONSTRAINT `RPOComponent` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_receivedpurchaseorder_component`
--
ALTER TABLE `tm_receivedpurchaseorder_component`
  ADD CONSTRAINT `ComponentRPOs` FOREIGN KEY (`receivedpurchaseorder_reference`) REFERENCES `tm_receivedpurchaseorder` (`receivedpurchaseorder_reference`) ON UPDATE CASCADE,
  ADD CONSTRAINT `RPOComponents` FOREIGN KEY (`component_id`) REFERENCES `tm_component` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_remark`
--
ALTER TABLE `tm_remark`
  ADD CONSTRAINT `tm_remark_ibfk_1` FOREIGN KEY (`tyc_ref`) REFERENCES `tm_folder` (`tyc_ref`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_state`
--
ALTER TABLE `tm_state`
  ADD CONSTRAINT `Country` FOREIGN KEY (`country_id`) REFERENCES `tm_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tm_task`
--
ALTER TABLE `tm_task`
  ADD CONSTRAINT `AssignedToUser` FOREIGN KEY (`assigned_to`) REFERENCES `tm_user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `OwnerUser` FOREIGN KEY (`owner`) REFERENCES `tm_user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `TaskStatus` FOREIGN KEY (`status_id`) REFERENCES `tm_status_type` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_task_reminder`
--
ALTER TABLE `tm_task_reminder`
  ADD CONSTRAINT `ReminderTask` FOREIGN KEY (`task_id`) REFERENCES `tm_task` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `TaskReminder` FOREIGN KEY (`reminder_id`) REFERENCES `tm_reminder` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tm_user`
--
ALTER TABLE `tm_user`
  ADD CONSTRAINT `Role` FOREIGN KEY (`basic_role`) REFERENCES `tm_role` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
