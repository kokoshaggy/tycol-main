<?php

namespace app\controllers;

use Yii;
use app\models\Reminder;
use app\models\Task;
use app\models\Telephone;
use app\models\TaskReminder;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;

/**
 * ReminderController implements the CRUD actions for Reminder model.
 */
class ReminderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Reminder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Reminder::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Reminder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Reminder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Reminder();
		$userId = Yii::$app->user->identity->id;
		$presentTime = new Expression('NOW()');
		$model->last_updated = $presentTime ;
		$taskInput = Yii::$app->request->post('reminder');
		$task = $taskInput['task'];
		$taskReminder = new TaskReminder ;
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$taskReminder->reminder_id = $model->id ;
			$taskReminder->task_id = $task;
			$taskReminder->save(false);
			echo 'sent';
        } else {
            echo 'An error occured';
        }
    }
	
	public function actionCreateFromTask()
    {
        $model = new Reminder();
		$taskReminder = new TaskReminder ;
		$userId = Yii::$app->user->identity->id;
		$presentTime = new Expression('NOW()');
		$model->last_updated = $presentTime ;
		
		
        if ($model->load(Yii::$app->request->post()) && $taskReminder->load(Yii::$app->request->post()) && $model->save()) {
			$taskReminder->reminder_id = $model->id ;
			$taskReminder->save(false);
			echo 'sent';
        } else {
            echo 'An error occured';
        }
    }

    /**
     * Updates an existing Reminder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Reminder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Reminder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Reminder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reminder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	public function actionFolderReminder($folderID)
    {
        $model = new Reminder();
		$taskOwner = Yii::$app->user->identity->id;
		$presentTime = new Expression('NOW()');
		$activeTask = Reminder::getActiveTask($presentTime, $taskOwner, $taskOwner);
		$userReminders = Reminder::getReminder($taskOwner, $folderID);
		
		return $this->renderAjax('folder-reminder', [
				'model' => $model,
				'activeTask' => $activeTask,
				'userReminders' => $userReminders,
            ]);
	}
	
	
	
}
