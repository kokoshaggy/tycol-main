<?php

namespace app\controllers;

use Yii;
use app\models\Remark;
use app\models\Project;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RemarkController implements the CRUD actions for Remark model.
 */
class RemarkController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Remark models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Remark::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Remark model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		$fetchAll = Remark::findAll(['folder_id' => $id]);
        return $this->renderAjax('view', [
			'remarks' => $fetchAll,
        ]);
    }
	
	public function actionViewbyproject($id)
    {
		$remark = new Remark();
		$project = new Project();
		$fetchAll = $remark->findAll(['project_id'=>$id]);
		$fetchProjectId = $remark->findAll(['project_id'=>$id]);
        return $this->renderAjax('remarksearchview', [
			'remarks' => $fetchAll,
			'project' => $fetchProjectId,
        ]);
    }

    /**
     * Creates a new Remark model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Remark();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Remark model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Remark model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Remark model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Remark the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Remark::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionCreateremark()
    {
        $remark = new Remark();
   
        if ($remark->load(Yii::$app->request->post()) && $remark->save()) {
            $creat_folder_project = Yii::$app->session;
            //$creat_folder_project->setFlash('created_successfully', 'You have successfully created a new project inside the folder with TYC_REF '.$id.'.');
            //return $this->redirect(['view', 'id' =>$id]);
			echo 'sent';
        } else { 
			echo 'An error has occured "please retry"' ;
        /*return $this->render('view', [
            'model' => $model ,
            'folderdetails'=>$folder,
            'projectcomment'=>$projectcomment,
            'invoicecomment'=>$invoicecomment,
            'ordercomment'=>$ordercomment,
            'remark'=>$remark,
            'allremark'=>$allcomment,
			]);*/
        }
    }
}
