<?php

namespace app\controllers;

use Yii;
use app\models\EDocument;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


/**
 * EdocumentController implements the CRUD actions for EDocument model.
 */
class EdocumentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EDocument models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new EDocument();
		$findOneEDocumentId = $model->find()->orderBy(['id'=>SORT_ASC])->one();
		$subComponents = $model->subComponents; 
        return $this->render('index', [
            'model' => $model,
            'findOneEDocumentId' => $findOneEDocumentId,
			'subComponents' => $subComponents,
        ]);
    }

    /**
     * Displays a single EDocument model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
	
	
	public function actionEdocumentlistview()
    {
		$model = new EDocument();
		if(isset($_POST['option'])){
			$option = $_POST['option'];
			$hoverEffect = 'false';
			$dataProvider = $model->find()->where(['component_id'=>array_values(unserialize($option))])->all();
		} else {
			$hoverEffect = 'true';
			$dataProvider = $model->find()->all();
		}
        return $this->renderAjax('edocumentlistview', [
            'edocument' => $dataProvider,
			'hoverEffect' => $hoverEffect,
			'model' => $model,
        ]);
    }
	
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
	public function actionThumbnail($type,$content)
    {
		$convertContentToArray = explode(',',$content);
        return $this->renderAjax('thumbnail', [
            'type' => $type,
            'data' => $convertContentToArray,
        ]);
    }

    /**
     * Creates a new EDocument model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       /* if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } */
		
		 $model = new EDocument();
		 $emptyData = [];
		if(isset($_GET['id'])){
			$getId = $_GET['id'];
			if(!empty($model->itemType) && !empty($model->itemID)){
				$model->itemType = $model->itemType.',folder';
				$this->owner->itemID = $model->itemID.','.$getId;
			} else{
				$model->itemType = 'folder';
				$model->itemID = $getId;
			}
		}
		if ($model->load(Yii::$app->request->post())) {
			$model->upload_file = UploadedFile::getInstances($model, 'upload_file');
			
		if ($model->upload()) {
			return 1;
			
		}
 		}
        return $this->renderAjax('create', [
            'model' => $model,
            'data' => $emptyData,
        ]);
    	
    }
	
	public function actionEdocumentview($id)
    {
		$model = new EDocument();
		$getSpecificEdocument = $model ->findOne($id);
		
		if($id == 0 or $id === 0){
			return $this->renderAjax('/nodata/nodataavailable',['message'=>'No Invoice available']);
		}
		
        return $this->renderAjax('view', [
            'model' => $getSpecificEdocument,
			'subComponents' => $getSpecificEdocument->subComponents
        ]);
    }

    /**
     * Updates an existing EDocument model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$model->scenario = EDocument::EDocumentUpdate;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
		$convertFileLocationToArray = explode(',',$model->fileLocationString);

        return $this->renderAjax('update', [
            'model' => $model,
			'data' => $convertFileLocationToArray,
        ]);
    }

    /**
     * Deletes an existing EDocument model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EDocument model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EDocument the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EDocument::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
