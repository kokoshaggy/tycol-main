<?php

namespace app\controllers;

use Yii;
use app\models\Client;
use app\models\Corporation;
use app\models\Supplier;
use app\models\Person;
use app\models\Email;
use app\models\Address;
use app\models\Telephone;
use app\models\ClientSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TmclientController implements the CRUD actions for Tmclient model.
 */
class ClientController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tmclient models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tmclient model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
	
	public function actionAdhockclient()
    {
        
		
		$corporationModel = new Corporation();
        
		$client = new Client;
		$supplier = new Supplier;
		$personModel = new Person();
		$email = new Email();
		$address = new Address();
		$telephone = new Telephone();
        
		
        //Calling requied model methods 
		$getNewClien = $corporationModel->fetchNewClient();
		$getNewSupplier = $corporationModel->fetchNewSupplier();
		

        return $this->renderAjax('adhockclient',[
			'newClient' => $getNewClien,
			'clientModel'=>$client,
			'newSupplier' => $getNewSupplier,
			'supplierModel'=>$supplier,
			'corporationModel' => $corporationModel,
			'personModel' => $personModel,
            'emailModel' => $email,
            'addressModel' => $address,
            'telephoneModel' => $telephone,
		
			

        ]);
    }

    /**
     * Creates a new Tmclient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Client();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $data = 'sent';
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return $data;
			//echo 'sent';
        } else {
           
			echo 'An error occured';
        }
    }

    /**
     * Updates an existing Tmclient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Tmclient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
	
	// use to get a new client which has just being added in the database
	
	public function actionNewclient()
    {
       	$model = new Client();
		$data = [];
		$findLastAdedClient = $model->find()->orderBy(['id' => SORT_DESC])->one();
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$data['clientId'] = $findLastAdedClient->id;
		$data['corpName'] = $findLastAdedClient->name ;
        return $data;
    }

    /**
     * Finds the Tmclient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tmclient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Client::findOne($id)) !== null) {
			return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}



    

     