<?php

namespace app\controllers;

use Yii;
use app\models\Project;
use app\models\Folder;
use app\models\ProjectSearch;
use app\models\Corporation;
use app\models\Remark;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
use app\models\Component as component;
use app\models\FolderComponent as folderComponent;
use app\models\StatusType ;
use app\models\Client ;
use app\models\Supplier ;
use app\boffins_vendor\classes\BoffinsBaseController;

/**
 * TmprojectController implements the CRUD actions for Tmproject model.
 */
class ProjectController extends BoffinsBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::classname(),
                'only' => ['create','update'],
                'rules' => [
                    [
            'allow'=>true,
                 //   'rules'=>[@],
                        ],
                    
            
                ]
            ],
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tmproject models.
     * @return mixed
     */
    public function actionIndex()
    {
        //$searchModel = new ProjectSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new Project;
        $root_method = $model->display_projects();
		$findOneProject = $model->find()->orderBy(['project_id'=>SORT_ASC])->one();

        return $this->render('index', [
            //'searchModel' => $searchModel,
            //'dataProvider' => $dataProvider,
            'projects' => $root_method,
			'findOneProject' => $findOneProject,
			'model' => $model,
        ]);
    }
	
	public function actionProjectlistview()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new Project();
		$allProjects = Project::findAll([]);
        
		$status = new StatusType();

		if(isset($_POST['option'])){
			$option = $_POST['option'];
			$hoverEffect = 'false';
			$newOption = explode(',',$option);
			$root_method =$model->find()->where(['component_id'=>array_values($newOption)])->all();
		} else {
			$hoverEffect = 'true';
			$root_method = $model->find()->all();
		}
        return $this->renderAjax('projectlistview', [
            'searchModel' => $allProjects,
            'dataProvider' => $dataProvider,
            'projects' => $root_method,
			'status' => $status,
        ]);
    }

    /**
     * Displays a single Tmproject model.
     * @param integer $id
     * @return mixed
     */
    
    public function actionView($id)
    {   
		$model = $this->findModel($id);
	 	$component = new Project;
		$testcomponent = Project::findOne($id);
		$testcomponent2 = $testcomponent->subComponents;
       // $folder = Folder::selectone($model->tyc_ref);
        $projectcomment = Project::get_project_comments($id,$model->project_id,'project');
        $invoicecomment = Project::get_project_comments($id,$model->project_id,'invoice');
        $ordercomment = Project::get_project_comments($id,$model->project_id,'order');
        $allcomment = Project::get_allproject_comments($id,$model->project_id,'order');
        $remark = new Remark();
		
        return $this->render('view', [
            'model' => $model ,
            'folderdetails' => 'test',//$folder,
            'projectcomment' => $projectcomment,
            'invoicecomment' => $invoicecomment,
            'ordercomment' => $ordercomment,
            'remark' => $remark,
            'allremark' => $allcomment,
			'testcomponent2' => $testcomponent2,
			
            
        ]);
    }
	
	
	
	public function actionProjectview($id)
    {   if($id == 0 or $id === 0 ){
		return $this->renderAjax('/nodata/nodataavailable',['message'=>'No Project available']);
	}
		$model = $this->findModel($id);
	 	$component = new Project;
		$testcomponent = Project::findOne($id);
		$testcomponent2 = $testcomponent->subComponents;
        //$folder = Folder::selectone($model->tyc_ref);
        $projectcomment = Project::get_project_comments($id,'project');
        $invoicecomment = Project::get_project_comments($id,'invoice');
        $ordercomment = Project::get_project_comments($id,'order');
        $allcomment = Project::get_allproject_comments($id,'order');
        $remark = new Remark();
	 
		if(isset($_POST['var2']) and $_POST['var2'] == 1){
			return $this->renderAjax('projectview', [
				'model' => $model ,
				'folderdetails' => 'test',//$folder,
				'projectcomment' => $projectcomment,
				'invoicecomment' => $invoicecomment,
				'ordercomment' => $ordercomment,
				'remark' => $remark,
				'allremark' => $allcomment,
				'testcomponent2' => $testcomponent2,
				'subComponents' => $model->subComponents


			]);
		}else {
			return $this->renderAjax('projectview', [
            'model' => $model ,
            'folderdetails' => 'test',//$folder,
            'projectcomment' => $projectcomment,
            'invoicecomment' => $invoicecomment,
            'ordercomment' => $ordercomment,
            'remark' => $remark,
            'allremark' => $allcomment,
			'testcomponent2' => $testcomponent2,
			'subComponents' => $model->subComponents
			
            
        ]);
		}
    }


    /**
     * Creates a new Tmproject model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {	//instantiating  of all required model
		$model = new Project();
		
		$folderModel = Folder::find()->all();
        $clientModel=Client::find()->all();
        $supplierModel=Supplier::find()->all(); 
		
		if(isset($_GET['id'])){
			$getId = $_GET['id'];
			if(!empty($model->itemType) && !empty($model->itemID)){
				$model->itemType = $model->itemType.',folder';
				$this->owner->itemID = $model->itemID.','.$getId;
			} else{
				$model->itemType = 'folder';
				$model->itemID = $getId;
			}
		}
		
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				
				$data=[];
				Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				$data['sent'] = 'sent';
				$data['message'] = 'New project was created with project id '.$model->project_id;
				
				return $data;
			}
		
                
            return $this->renderAjax('create', [
                'client' => $clientModel,
                'supplier' => $supplierModel,
                'model' => $model,
                'folder' => $folderModel,
				
				
            ]);
        
    }

    /**
     * Updates an existing Tmproject model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		$folderComponent = new folderComponent();
		$component = new component();
		//calling of required  model methods
		$folderModel = Folder::find()->all();
        $clientModel=Client::find()->all();
        $supplierModel=Supplier::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$data=[];
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$data['sent'] = 'sent';
			$data['message'] = 'Updated project  with project id '.$model->project_id;
			return $data;
        } else {
            return $this->renderAjax('update', [
               'client' => $clientModel,
                'supplier' => $supplierModel,
                'model' => $model,
                'folder' => $folderModel,
				'folderComponent' => $folderComponent
            ]);
        }
    }

    /**
     * Deletes an existing Tmproject model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Tmproject model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tmproject the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    static function convert_tycref_tag($str)
	{
        $regex = "/tyc_ref+([a-zA-Z0-9_]+)/";
        $str = preg_replace_callback($regex, 'self::convertTycrefLink', $str);
        return($str);
    }
    
    public static function convertTycrefLink($value1)
	{
        $values = Html::a($value1[0], ['folder/view','id' => $value1[1]]);
        return $values;
    }
    
    
    public function actionCreateremark($id)
    {
        $remark = new Remark();
        $model = $this->findModel($id);
        $folder = folderComponent::find()->where(['component_id'=>$model->component_id])->one()->tyc_ref;
        $remark->tyc_ref = $folder;
   
        if ($remark->load(Yii::$app->request->post()) && $remark->save()) {
            $creat_folder_project = Yii::$app->session;
            
            //return $this->redirect(['view', 'id' =>$id]);
			$data=[];
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$data['sent'] = 'sent';
			$data['message'] = 'Created a new Remark';
			return $data;
        } else { 
        echo 0;
        }
    }
}
