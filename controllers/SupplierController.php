<?php

namespace app\controllers;

use Yii;
use app\models\Supplier;
use app\models\Corporation;
use app\models\Person;
use app\models\Email;
use app\models\Address;
use app\models\Telephone;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SupplierController implements the CRUD actions for Supplier model.
 */
class SupplierController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Supplier models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Supplier::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Supplier model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Supplier model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Supplier();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			echo 'sent';
            
        } else {
			echo 0;
            
        }
    }

    /**
     * Updates an existing Supplier model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Supplier model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Supplier model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Supplier the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Supplier::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionAdhocsupplier()
    {
        
		
		$corporationModel = new Corporation();
        
		//$client = new Client;
		$supplier = new Supplier;
		$personModel = new Person();
		$email = new Email();
		$address = new Address();
		$telephone = new Telephone();
        
		
        //Calling requied model methods 
		//$getNewClien = $corporationModel->fetchNewClient();
		$getNewSupplier = $corporationModel->fetchNewSupplier();
		

        return $this->renderAjax('adhocsupplier',[
			//'newClient' => $getNewClien,
			//'clientModel'=>$client,
			'newSupplier' => $getNewSupplier,
			'supplierModel'=>$supplier,
			'corporationModel' => $corporationModel,
			'personModel' => $personModel,
            'emailModel' => $email,
            'addressModel' => $address,
            'telephoneModel' => $telephone,
		
			

        ]);
    }
	
	public function actionNewsupplier()
    {
       	$model = new Supplier();
		$data = [];
		$findLastAdedClient = $model->find()->orderBy(['id' => SORT_DESC])->one();
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$data['clientId'] = $findLastAdedClient->id;
		$data['corpName'] = $findLastAdedClient->name ;
        return $data;
    }

}
