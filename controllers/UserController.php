<?php

namespace app\controllers;

use Yii;
use app\models\UserDb;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\controllers\PersonController;
use app\models\Email;
use app\models\Address;
use app\models\Telephone;
use app\models\Entity;
use  app\models\EmailEntity ;
use app\models\AddressEntity ;
use app\models\TelephoneEntity ;
use app\models\Person ;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\UserForm;
use yii\helpers\Url;
use app\boffins_vendor\classes\BoffinsBaseController;




/**
 * userController implements the CRUD actions for user model.
 */
class UserController extends BoffinsBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all user models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => UserDb::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single user model.
     * @param string $username
     * @return mixed
     */
    public function actionView($un)
    {
        return $this->render('view', [
            'model' => $this->findModel($un),
        ]);
    }

    /**
     * Creates a new user model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $user = new UserForm;
		
		if (!Yii::$app->user->can('view:User')) {
			throw new ForbiddenHttpException(Yii::t('yii', 'This page does not exist or you do not have access'));
		}
        //yii\helpers\VarDumper::dump(Yii::$app->request->post());
        if ($user->load(Yii::$app->request->post()) && $user->save() ) {
			$session = Yii::$app->session;
			$session->setFlash('created_successfully', 'You have successfully created a new User' . $user->first_name . ' ' . $user->surname);
			return $this->redirect(['view', 'un' => $user->username]);
		} else {
            return $this->render('../user/create', [
				'userForm' => $user,
				'action' => ['user/create'],
			]);
		}

    }

    /**
     * Updates an existing user model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $un
     * @return mixed
     */
    public function actionUpdate($un)
    {
        $user = $this->findModel($un);

        if ($user->load(Yii::$app->request->post()) && $user->save()) {
            return $this->redirect(['view', 'un' => $user->username]);
        } else {
            return $this->render('update', [
                'userForm' => $user,
				'action' => [ 'user/update', "un" => $un ]
            ]);
        }
    }

    /**
     * Deletes an existing user model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $un
     * @return mixed
     */
    public function actionDelete($un)
    {
        $this->findModel($un)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the user model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $un - username
     * @return user the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($un)
    {
		$model = new UserForm($un);
		
		if ( empty($model) || $model->isNewRecord) {
            throw new NotFoundHttpException('The requested user does not exist.');
        }
		return $model;
    }
}
