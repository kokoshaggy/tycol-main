<?php

namespace app\controllers;

use Yii;
use app\models\Task;
use app\models\Userdb;
use app\models\Person;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;
use app\models\FolderTask;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Task::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Task();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
	
	
	public function actionCreatefoldertask()
    {
        $model = new Task();
		$model->owner=Yii::$app->user->identity->id;
		$model->create_date=new Expression('NOW()');
		$model->last_updated=new Expression('NOW()');
		$sendEmail = new ContactForm();
		$sendEmail->creatorName = Yii::$app->user->identity->username;
		$sendEmail->creatorEmail = Yii::$app->user->identity->email;
		
		
        if ($model->load(Yii::$app->request->post()) && $model->save())
		{
			$folderTask = new FolderTask();
			if ( $folderTask->load(Yii::$app->request->post()) && !empty($folderTask->folder_id) ) {
				$taskId = $model->id;
				$folderTask->task_id = $taskId ;
				$folderTask->save(false);
			}
		
    		$assingned = Userdb::findOne($model->assigned_to)->username;
			$userDb = new Userdb();
			$userDb2 = $userDb->find()->where(['id' => $model->assigned_to])->one();
    		$assignedToEmail =  $userDb2->email;
			$body = ' A new task has just been created by ';
			$body .= Yii::$app->user->identity->username.' and has been assigned to you <br>';
			$body .= '<li>Task tittle :</li>'.$model->title;
			$body .= '<li>Date created :</li>'.$model->create_date;
			$body .= '<li>Due Date :</li>'.$model->create_date.'<br>';
			$body .= '<article class="testings">';
			$body .= $model->details;
			$body .= '</article>';
			
			$bodyAdmin = ' A new task has just been created by you ';
			$bodyAdmin .= ' and has been assigned to '.$assingned.' <br>';
			$bodyAdmin .= '<li>Task tittle :</li>'.$model->title;
			$bodyAdmin .= '<li>Date created :</li>'.$model->create_date;
			$bodyAdmin .= '<li>Due Date :</li>'.$model->due_date.'<br>';
			$bodyAdmin .= '<article class="testings">';
			$bodyAdmin .= $model->details;
			$bodyAdmin .= '</article>';
			
			$sendEmail->body = $body;
			$sendEmail->bodyAdmin = $bodyAdmin;
			$sendEmail->assingedToEmail = $assignedToEmail;
			$sendEmail->assingedToname = $assingned;

			$sendEmail->sendFeedbackEmail();
			echo 'sent';
        } else {
			echo 2;
        }
    }
	
	/*** 
	 * Create a task from the dashboard 
	 * Typically this would be an ajax call 
	 * @return mixed (should be a message indicating failure or success.)
	 */
	public function actionDashboardCreateTask()
    {
        $model = new Task();
		$model->owner = Yii::$app->user->identity->id;
		$model->create_date = new Expression('NOW()');
		
        if ($model->load(Yii::$app->request->post()) && $model->save())
		{
			$folderTask = new FolderTask();
			if ( $folderTask->load(Yii::$app->request->post()) && !empty($folderTask->folder_id) ) {
				$taskId = $model->id;
				$folderTask->task_id = $taskId ;
				$folderTask->save(false);
			}
			echo Yii::t('dashboard', 'You have a created a task: {task}', ['task' => $model->title] );
        } else {
			echo Yii::t('dashboard', 'Sorry, this task was not created, please try again');
        }
    }
	
	/*** 
	 * Create a task using the create task view 
	 * This view can be pulled from any location - most frequently the dashboard 
	 * Typically this would be an ajax call 
	 * @return mixed (should be a message indicating failure or success.)
	 */
	public function actionDashboardNewCreateTask()
    {
        $model = new Task();
		$model->owner = Yii::$app->user->identity->id;
		$model->create_date = new Expression('NOW()');
		
		
        if ($model->load(Yii::$app->request->post()) && $model->save())
		{
			$folders = Yii::$app->request->post('FolderTask');
			$folders = isset($folders['folder_id']) ? $folders['folder_id'] : false;
			
			if ( !empty($folders) ) {
				foreach ($folders as $folder_id) {
					$folderTask = new FolderTask();
					$folderTask->folder_id = $folder_id;
					$folderTask->task_id = $model->id;
					$folderTask->save(false);
				}
			}
			
			echo Yii::t('dashboard', 'You have a created a task: {task}', ['task' => $model->title] );
        } else {
			echo Yii::t('dashboard', 'Sorry, this task was not created, please try again');
        }
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
