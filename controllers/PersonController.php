<?php

namespace app\controllers;

use Yii;
use yii\base\Model;
use app\models\Person;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
// test to see if it would work 
use yii\widgets\ActiveForm;

use app\models\Corporation;
use app\models\Email;
use app\models\EmailEntity;
use app\models\Address;
use app\models\Telephone;
use app\models\Entity;
use app\models\Addressentity ;
use app\models\Telephoneentity ;

/**
 * TmpersonController implements the CRUD actions for Tmperson model.
 */
class PersonController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tmperson models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Person::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tmperson model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tmperson model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $person = new Person();
        $email = new Email();
        $address = new Address();
        $telephone = new Telephone();
        $entity = new Entity();
        //calling models for entities
        $email_entity = new EmailEntity ;
        $address_entity = new AddressEntity ;
        $telephone_entity = new TelephoneEntity ;      
       
        if ($telephone->load(Yii::$app->request->post()) & $address->load(Yii::$app->request->post()) &  $person->load(Yii::$app->request->post()) & $email->load(Yii::$app->request->post()) ) {
            
            if( $telephone->validate() & $address->validate() & $person->validate() & $email->validate() ) {
                   $this->createnewperson($entity,$person,$email,$address,$telephone,$email_entity,$address_entity,$telephone_entity);
                   echo 'sent';
            } else {
                echo 'An error occured';
            }    
        }else{
            echo 'not available';
        }
        
    }
    
    public static function createnewperson($entity,$person,$email,$address,$telephone,$email_entity,$address_entity,$telephone_entity)
	{
		$entity->entity_type='person';
        if($entity->save(false)){
			$entity_id = $entity->id;
			$person->entity_id = $entity_id;
			$telephone->save(false);
			$address->save(false) ;
			$person->save(false) ;
			//person id
			$personid = $person->id;
			$email->save(false) ;
		   //fetch all ids
			$telephone_id = $telephone->id;
			$email_id = $email->id;
			$address_id = $address->id;
			//give values to entities
			$email_entity->email_id = $email_id;
			$email_entity->entity_id = $entity_id;
			$address_entity->address_id = $address_id;
			$address_entity->entity_id = $entity_id;
			$telephone_entity->telephone_id = $telephone_id;
			$telephone_entity->entity_id = $entity_id;
			$email_entity->save(false) ;
			$address_entity->save(false) ;
			$telephone_entity->save(false) ;
		}   
    }

    /**
     * Updates an existing Tmperson model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$model = $this->findModel($id);
		$emailEntityId = EmailEntity::find()->where(['entity_id'=>$model->entity_id])->one();
		$telephoneEntityId = Telephoneentity::find()->where(['entity_id'=>$model->entity_id])->one();
		$addressEntityId = Addressentity::find()->where(['entity_id'=>$model->entity_id])->one();
        $emailModel = Email::findOne($emailEntityId->email_id);
        $addressModel = Address::findOne($addressEntityId->address_id);
        $telephoneModel =  Telephone::findOne($telephoneEntityId->telephone_id);
		//$model->scenario = 'update';
		//$emailModel->scenario = 'update';
		//$telephoneModel->scenario = 'update';
		//$addressModel->scenario = 'update';
        if ($model->load(Yii::$app->request->post()) && $emailModel->load(Yii::$app->request->post()) && $telephoneModel->load(Yii::$app->request->post()) && $addressModel->load(Yii::$app->request->post()) && $model->validate() && $emailModel->validate() && $telephoneModel->validate() && $addressModel->validate()) {
			$model->save(false);
			$emailModel->save(false);
			$telephoneModel->save(false);
			$addressModel->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->renderAjax('update', [
                'personModel' => $model,
				'emailModel' => $emailModel,
				'addressModel' => $addressModel,
				'telephoneModel' => $telephoneModel,
				'id' => $id,
            ]);
        }
    }

    /**
     * Deletes an existing Tmperson model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        //return $this->redirect(['index']);
    }

    /**
     * Finds the Tmperson model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tmperson the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Person::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
