<?php

namespace app\models;

use Yii;
use app\boffins_vendor\classes\BoffinsArRootModel;


/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property string $details
 * @property integer $owner
 * @property integer $assigned_to
 * @property integer $status_id
 * @property string $create_date
 * @property string $due_date
 * @property string $last_updated
 * @property integer $deleted
 *
 * @property ComponentTask[] $ComponentTasks
 * @property FolderTask[] $FolderTasks
 * @property User $assignedTo
 * @property User $owner0
 * @property StatusType $status
 * @property TaskReminder[] $TaskReminders
 */
class Task extends BoffinsArRootModel
{
	/***
	 * accessible value linked to the database id of "completed" in status_type under task group.. 
	 * needs to be refactored. If the DB id changes, what happens??? What about other phases dynamically set?
	 */
	const TASK_COMPLETED = 24;
	/***
	 * accessible value linked to the database id of "cancelled" in status_type under task group.. 
	 * needs to be refactored. If the DB id changes, what happens??? What about other phases dynamically set?
	 */
	const TASK_CANCELLED = 23;
	/***
	 * accessible value linked to the database id of "completed" in status_type under task group.. 
	 * needs to be refactored. If the DB id changes, what happens??? What about other phases dynamically set?
	 */
	const TASK_IN_PROGRESS = 22;
	/***
	 * accessible value linked to the database id of "completed" in status_type under task group.. 
	 * needs to be refactored. If the DB id changes, what happens??? What about other phases dynamically set?
	 */
	const TASK_NOT_STARTED = 21;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%task}}';
    }
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner', 'assigned_to', 'status_id', 'create_date', 'due_date', 'last_updated','title'], 'required'],
            [['owner', 'assigned_to', 'status_id', 'deleted'], 'integer'],
            [['create_date', 'due_date', 'last_updated'], 'safe'],
            [['details'], 'string', 'max' => 255],
            [['assigned_to'], 'exist', 'skipOnError' => true, 'targetClass' => UserDb::className(), 'targetAttribute' => ['assigned_to' => 'id']],
            [['owner'], 'exist', 'skipOnError' => true, 'targetClass' => UserDb::className(), 'targetAttribute' => ['owner' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatusType::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'details' => 'Details',
            'owner' => 'Owner',
			'title' => 'Title',
            'assigned_to' => 'Assigned To',
            'status_id' => 'Task Status', 
            'create_date' => 'Create Date',
            'due_date' => 'Due Date',
            'last_updated' => 'Last Updated',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponentTasks()
    {
        return $this->hasMany(ComponentTask::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFolderTasks()
    {
        return $this->hasMany(FolderTask::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssignedTo()
    {
        return $this->hasOne(UserDb::className(), ['id' => 'assigned_to']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner0()
    {
        return $this->hasOne(UserDb::className(), ['id' => 'owner']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(StatusType::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskReminders()
    {
        return $this->hasMany(TaskReminder::className(), ['task_id' => 'id']);
    }
	
	public function fetchAllTask($user,$folderRef,$identity)
	{
		$folderTask = Foldertask::getAllFolderTask($folderRef);
		$folderTaskArray = [] ;
		$allTaskArray = [] ;
		
		foreach($folderTask as $key => $value){	
			 array_push($folderTaskArray,$value['task_id']);
		}
		
		$task = new Task;
		$fetchAllTask = $task->find()
			->where([$user => $identity])
			->all();
		
		foreach($fetchAllTask as $key => $value){
			 array_push($allTaskArray,$value['id']);
		}
		
		$result=array_intersect($allTaskArray,$folderTaskArray);
		$fetchTask = $task->findAll(array_values($result));
		return $fetchTask;
	}
	
	public static function fetchAllReminderTask($taskOwner, $folderRef, $taskAssignedTo = NULL)
	{	
		$task = new Task;
		$folderTask = FolderTask::getAllFolderTask($folderRef);
		$folderTaskArray = [] ;
		$allTaskArray = [] ;
		
		foreach($folderTask as $key => $value){	
			 array_push($folderTaskArray,$value['task_id']);
		}
		
		$fetchAllTask = $task->find()
			->where( [ 'or',['owner' => $taskOwner], ['assigned_to' => empty($taskAssignedTo) ? $taskOwner : $taskAssignedTo] ] )
			->all();
		
		foreach($fetchAllTask as $key => $value){
			 array_push($allTaskArray,$value['id']);
		}
		
		$result=array_intersect($allTaskArray,$folderTaskArray);
		$fetchTask = $task->findAll(array_values($result));
		return $fetchTask;
	}
	
	public static function selectActiveTask($presentTime,$userId,$usedId2)
	{
		$activeTask = Task::find()
			->andWhere(['>', 'due_date', $presentTime])
			->andWhere(['or', ['owner'=>$userId] , ['assigned_to'=>$usedId2]]) 
			->asArray()
			->all();
		return $activeTask;
	} 
	
	public function getTaskId($taskId)
	{
		$task = Task::find()->where(['id' => $taskId])->all();
		return $task;
	}
	
	public function getTaskUserids($reminderId)
	{
		$taskReminder = TaskReminder::getTaskId($reminderId);
		$task = Task :: getTaskId($taskReminder); // could be this->taskid
		return $task;
	}
	
	/***
	 *
	 */
	public static function selectTaskByStatus($user, $status = SELF::TASK_NOT_STARTED, $equateToStatus = true)
	{
		$symbol = $equateToStatus ? '=' : '!=';
		return SELF::find()
			->andWhere([$symbol, 'status_id', $status])
			->andWhere(['or', ['owner' => $user] , ['assigned_to' => $user]]) 
			->asArray()
			->all();
	}
	
	
}
