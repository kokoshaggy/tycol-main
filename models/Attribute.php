<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_attribute".
 *
 * @property int $id
 * @property string $attribute_name
 * @property string $value
 *
 * @property AssetAttribute[] $assetAttributes
 * @property ProductAttribute[] $productAttributes
 */
class Attribute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tm_attribute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute_name', 'value'], 'required'],
            [['attribute_name', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attribute_name' => 'Attribute Name',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetAttributes()
    {
        return $this->hasMany(AssetAttribute::className(), ['attribute_id' => 'id']);
    }


    public function getProducts()
        {
            return $this->hasMany(Product::className(), ['id' => 'product_id'])->via('productAttributes');
        }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributes()
    {
        return $this->hasMany(ProductAttribute::className(), ['attribute_id' => 'id']);
    }
}
