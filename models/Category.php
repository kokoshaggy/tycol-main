<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tm_category}}".
 *
 * @property integer $id
 * @property string $category_name
 *
 * @property TmCategoroyCorporation[] $tmCategoroyCorporations
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_name'], 'required'],
            [['category_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_name' => 'Category Name',
        ];
    }

    public function getCategoryId(){
       $categories = Category::find()->asArray()->indexBy('id')->all();
       return $categories;
    }

    public function getProducts(){
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryCorporations()
    {
        return $this->hasMany(CategoryCorporation::className(), ['category_id' => 'id']);
    }

     public static function getCategory()
    {
        $category =  Category::find()->asArray()->indexBy('id')->all();        
        return $category;
    }
}
