<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%project_order}}".
 *
 * @property string $tyc_ref
 * @property integer $order_number
 */
class ProjectOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tyc_ref', 'order_number'], 'required'],
            [['order_number'], 'integer'],
            [['tyc_ref'], 'string', 'max' => 16],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tyc_ref' => 'Tyc Ref',
            'order_number' => 'Order Number',
        ];
    }
}
