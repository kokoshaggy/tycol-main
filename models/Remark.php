<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "{{%remark}}".
 *
 * @property integer $id
 * @property string $folder_id
 * @property string $remark_type
 * @property string $remark_date
 * @property string $text
 */
class Remark extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%remark}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['remark_date','folder_id'], 'safe'],
            [['project_id'], 'integer'],
            [['remark_type', 'text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'folder_id' => Yii::t('remark', 'folder'),
            'remark_type' => Yii::t('remark', 'remark_type'),
            'remark_date' => Yii::t('remark', 'remark_date'),
            'project_id' => Yii::t('remark', 'project'),
            'text' => Yii::t('remark', 'text'),
        ];
    }
    
    public static function get_project_comment($project_id,$type)
	{
        $comments = Remark::find()
			->where(['project_id' => $project_id, 'remark_type' => $type,])
			->asArray()
    		->orderBy('id DESC')
        	->all();
        return $comments; 
    }
    
    public static function get_allproject_comment($project_id)
	{
         $comments = Remark::find()
    		->where(['project_id' => $project_id])
			->asArray()
    		->orderBy('id DESC')
        	->all();
        return $comments;
    }
	
    public function remarksdash()
	{
    	$comments = Remark::find()->asArray()
    		->orderBy('id DESC')->limit('5')
        	->all();
        return $comments;
    }
	
	public static function latestRemarks() 
	{
		return self::find()
				->orderBy('remark_date DESC')
				->limit(10) //limit should be obtained from user settings in the future. 
				->with('folder')
				->asArray()
				->all();
	}
	
	public function getProject()
    {
        return $this->hasOne(Project::className(), ['project_id' => 'project_id']);
    }
	
	public function getFolder(){
		return $this->hasOne(Folder::className(), ['id' => 'folder_id']);
	}

    
}

