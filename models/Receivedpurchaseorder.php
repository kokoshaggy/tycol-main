<?php

namespace app\models;


use Yii;
use yii\db\Expression;
use app\models\FolderComponent;
use app\models\Currency;
use app\boffins_vendor\classes\FolderSubComponentARModel;
use app\boffins_vendor\behaviors\DeleteUpdateBehavior;
use app\boffins_vendor\behaviors\DateBehavior;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "{{%tm_receivedpurchaseorder}}".
 *
 * @property string $receivedpurchaseorder_reference
 * @property integer $component_id
 * @property string $receivedpurchaseorder_type
 * @property string $description
 * @property integer $client_id
 * @property string $order_value
 * @property integer $order_currency
 * @property integer $supplier_id
 * @property string $issue_date
 * @property string $receivedpurchaseorder_duedate
 * @property string $final_delivery_date
 * @property string $last_updated
 * @property integer $deleted
 *
 * @property Component $component
 * @property ReceivedpurchaseorderComponent[] $ReceivedpurchaseorderComponents
 * @property Component[] $components
 */
class Receivedpurchaseorder extends FolderSubComponentARModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%receivedpurchaseorder}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['receivedpurchaseorder_reference', 'order_value','issue_date', 'receivedpurchaseorder_duedate', 'final_delivery_date' ], 'required'],
            [['client_id', 'order_currency', 'supplier_id'], 'integer'],
            [['order_value'], 'string'], //, 'numberPattern' => '/(^\d+\,\d+\.\d+$)|(^\d+\.\d+$)/'],
            [['issue_date', 'receivedpurchaseorder_duedate', 'final_delivery_date'], 'safe'],//'foldersList' removed by kingsley
            [['receivedpurchaseorder_reference', 'receivedpurchaseorder_type','description'], 'string', 'max' => 50],
            [['itemType', 'itemID'], 'safe'],
            //[['component_id'], 'exist', 'skipOnError' => true, 'targetClass' => Component::className(), 'targetAttribute' => ['component_id' => 'id']],
			//[['foldersList'], 'foldersValidation', 'skipOnEmpty' => true, 'skipOnError' => true],
			//[['foldersList'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'receivedpurchaseorder_reference' => 'RPO Reference',
            'component_id' => 'Component ID',
            'receivedpurchaseorder_type' => 'RPO Type',
            'description' => 'Description',
            'client_id' => 'Client',
            'order_value' => 'Order Value',
            'order_currency' => 'Order Currency',
            'supplier_id' => 'Supplier',
            'issue_date' => 'Issue Date',
            'receivedpurchaseorder_duedate' => 'RPO Due Date',
            'final_delivery_date' => 'Final/Actual Delivery Date',
            'last_updated' => 'Last Updated',
            'deleted' => 'Deleted',
			'clientNameString' => 'Client',
			'supplierNameString' => 'Supplier',
			//'foldersList' => 'Folder(s)', removed by kingsley
			'orderValueString' => 'Order Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponent()
    {
        return $this->hasOne(Component::className(), ['id' => 'component_id']);
    }

	public function getDescriptions()
    {
        return $this->description;
        
    }
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceivedpurchaseorderComponents()
    {
        return $this->hasMany(ReceivedpurchaseorderComponent::className(), ['receivedpurchaseorder_reference' => 'receivedpurchaseorder_reference']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubComponents()
    {
        return $this->hasMany(Component::className(), ['id' => 'component_id'])->viaTable('{{%receivedpurchaseorder_component}}', ['receivedpurchaseorder_reference' => 'receivedpurchaseorder_reference']);
    }
	
	/**
	 * ADDITIONS BY BOFFINS
	 */
	 
	//ADDED BY Anthony.
	
	const COMPONENT_TYPE = "received_purchase_order";
	private $_folderComponentItems = array();
	public $_foldersList = [];

	public function foldersValidation($attribute, $params, $validator) {
		if (empty($this->$attribute)) {
			$this->addError($attribute, "The RPO needs to be linked to at least one folder");
		} else {
			//$this->linkFolders($attribute);
			return true;
		}
	}
	
	/*public function beforeValidate() 
	{ 
		static $componentSet = false;
		if ($this->isNewRecord) {
			$this->deleted = 0;
			if (!$componentSet) { 
				$component = new Component;
				$component->component_type = self::COMPONENT_TYPE;
				if ($component->save(false)) {
					$this->component_id = $component->id;
					$componentSet = true;
					$this->_setFolderComponentItemsComponentId();
					return parent::beforeValidate();
				} else {
					return false;
				}
			} else {
				return parent::beforeValidate();
			}
		}
		return parent::beforeValidate();
	}*/
	
	public function init() {
		parent::init();
		$this->_junction = "app\models\ReceivedpurchaseorderComponent";
		$this->_junctionFK = "receivedpurchaseorder_reference";
	}
	
	public function getClient() 
	{
		return $this->hasOne(Client::className(), ['id' => 'client_id']);
	}
	
	public function getClientNameString() 
	{
		return $this->client->NameString;
	}
	
	public function getRPONameString()
	{
		return  $this->getClientNameString() . ' - ' . $this->description . ' (' . $this->getSupplierNameString() . ') ';
	}
	
	public function getSupplier() 
	{
		return $this->hasOne(Supplier::className(), ['id' => 'supplier_id']);
	}
	
	public function getSupplierNameString() 
	{
		return $this->supplier->NameString;
	}
	
	public function getFolderComponents() 
	{
		return $this->hasMany(FolderComponent::className(), ['component_id' => 'component_id']);
	}
	
	public function getFolders() 
	{
		return $this->hasMany(Folder::className(), ['tyc_ref' => 'folder_id' ])->via('folderComponents');
	}
		
	/*public function linkFolders($values) 
	{
		if (is_array($values)) {
			foreach ($values as $index => $folder) {
				$currentItem = $this->_folderComponentItems[] = new FolderComponent;
				$currentItem->component_id = (!$this->isNewRecord) ? $this->component_id : NULL;
				$currentItem->folder_id = $folder;
			}
		} else {
			$currentItem = $this->_folderComponentItems[] = new FolderComponent;
			$currentItem->component_id = (!$this->isNewRecord) ? $this->component_id : NULL;
			$currentItem->folder_id = $values;
		} 
	}*/
	
	/*public function afterSave($insert, $changedAttributes) 
	{
		foreach ($this->folderComponentItems as $item) {
			$item->save(false);
		}
		parent::afterSave($insert, $changedAttributes);
	}*/
	
	private function _setFolderComponentItemsComponentId() 
	{
		foreach ($this->folderComponentItems as $item) {
			$item->component_id = $this->component_id;
		}
	}
	
	public function getOrderCurrencySymbol() 
	{
		return Currency::findOne($this->order_currency)->symbol;
	}
	
	public function getOrderValueString() 
	{
		return $this->orderCurrencySymbol . " " . \Yii::$app->formatter->asDecimal($this->order_value);
	}
	
	public function getFoldersString() 
	{	
		$foldersString = '';
		foreach ($this->folders as $folder) {
			$foldersString .= $folder->tyc_ref . ' ,';
		}
		return substr_replace($foldersString, '.', -2, 2);
	}	
	
	public function myBehaviors() 
	{
		return [
			'deleteUpdateBehavior2' => [
					'class' => DeleteUpdateBehavior::className(),
					//'forceDelete' => true, 
					//'lastUpdatedAttribute' => 'NONE',
			],
			"dateValues" => [
				"class" => DateBehavior::className(),
				"AREvents" => [
						ActiveRecord::EVENT_BEFORE_VALIDATE => [ 'rules' => [
																			DateBehavior::DATE_CLASS_STAMP => [
																					'attributes' => ['last_updated'],
																					],
																			DateBehavior::DATE_CLASS_STANDARD => [
																					'attributes' => ['issue_date', 'receivedpurchaseorder_duedate', 'final_delivery_date']
																					]
																				] 
															],
						ActiveRecord::EVENT_AFTER_FIND => [ 'rules' => [
																			DateBehavior::DATE_CLASS_STANDARD => [
																					'attributes' => ['last_updated', 'issue_date', 'receivedpurchaseorder_duedate', 'final_delivery_date'],
																					]
																				] 
															],
					],
			],
		];
	}
}

	
	

