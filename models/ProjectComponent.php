<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_project_component".
 *
 * @property integer $project_id
 * @property integer $component_id
 */
class ProjectComponent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_component}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'component_id'], 'required'],
            [['project_id', 'component_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'project_id' => 'Project ID',
            'component_id' => 'Component ID',
        ];
    }
	
	
}
