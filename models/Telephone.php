<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%telephone}}".
 *
 * @property integer $id
 * @property string $telephone_number
 */
class Telephone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%telephone}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['telephone_number'], 'required'],
         
            [['telephone_number'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telephone_number' => 'Telephone Number',
        ];
    }
	
	public function getTelephoneNumber($telephoneid)
	{
		$telephoneId = Telephone::findOne(['id' => $telephoneid,]);
		return $telephoneId->telephone_number;
	}
	
	public function getNumberFromUser($userId)
	{
		$user = Userdb::getPersonId($userId);
		$person = Person::getPersonEntityId($user);
		$telephoneEntityId = TelephoneEntity::getEntityId($person);
		$telephone = Telephone::getTelephoneNumber($telephoneEntityId);
		return $telephone;
	}
}
