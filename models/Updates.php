<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_updates".
 *
 * @property integer $id
 * @property resource $update_contents
 * @property string $table_name
 * @property string $create_date
 */
class Updates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tm_updates';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_backup');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['update_contents', 'table_name', 'create_date'], 'required'],
            [['update_contents'], 'string'],
            [['create_date'], 'safe'],
            [['table_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'update_contents' => 'Update Contents',
            'table_name' => 'Table Name',
            'create_date' => 'Create Date',
        ];
    }
}
