<?php

namespace app\models;

use Yii;
use app\boffins_vendor\classes\FolderSubComponentARModel;
use app\boffins_vendor\behaviors\ComponentsBehavior;
use app\boffins_vendor\behaviors\DeleteUpdateBehavior;
use app\boffins_vendor\behaviors\DateBehavior;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "{{%product}}".
 *
 * @property integer $id
 * @property integer $product_category
 * @property string $product_name
 * @property string $main_attribute
 *
 * @property TmProductCorporation[] $tmProductCorporations
 */
class Product extends FolderSubComponentARModel
{   
    /**
     * @inheritdoc
     */
    public $attributes;

    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, png, jpeg'],
            [['product_name', 'main_attribute', 'product_category'], 'required'],
            [['product_category'], 'integer'],
            [['product_name', 'main_attribute'], 'string', 'max' => 255],
            [['product_category'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['product_category' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_category' => 'Category',
            'product_name' => 'Product Name',
            'main_attribute' => 'Main Attribute',
            'product_category' => 'Category',
            'image' => 'Upload Image',
        ];
    }
	
	public function getDescriptions()
    {
        return $this->product_name;
        
    }
  
    /**
     * @return \yii\db\ActiveQuery
     */

    public function getPAttributes(){
            return $this->hasMany(Attribute::className(), ['id' => 'attribute_id'])->via('productAttributes');
        }


     public function getProductAttributes()
    {
            return $this->hasMany(ProductAttribute::className(), ['product_id' => 'id']);
    }

    public function getTmProductCorporations(){
            return $this->hasOne(Supplier::className(), ['id' => 'supplier_id'])->via('productCorporations');
        }

    public function getProductCorporations()
    {
        return $this->hasOne(ProductCorporation::className(), ['product_id' => 'id']);
    }

    public function getDisplayProducts()
    {
        $product= $this->find()
            ->all();
        return $product;
    }

    public function getCategory(){
        return $this->hasOne(Category::className(), ['id' => 'product_category']);
    }

    public function getCategoryName(){
        return $this->category->category_name;
    }

    public function getCorporationName(){
        return $this->tmProductCorporations->name;
    }

    public function getAttributeName(){
        $attributes = [];
        $data = $this->pAttributes;
        foreach($data as $attr) {
            $attributes[] = $attr->attribute_name.": ".$attr->value;
        }
        return implode("<hr>", $attributes);
    }

}
