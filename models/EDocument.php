<?php

namespace app\models;

use Yii;
use \app\models\base\EDocument as BaseEDocument;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "tm_e_document".
 */
class EDocument extends BaseEDocument
{
	
    /*public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }*/
	const EDocumentLinking = 'doclinking';
	const EDocumentUpdate = 'docupdate';
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
              
      
            ]
        );
    }
	        
	public function scenarios()
    {
		$scenarios = parent::scenarios();
		$scenarios[self::EDocumentLinking] = ['reference'];
		$scenarios[self::EDocumentUpdate] = ['reference'];
        return $scenarios;
    }
	 public function uploadFile() {
        // get the uploaded file instance
        $image = UploadedFile::getInstance($this, 'upload_file');
 
        // if no image was uploaded abort the upload
		 
        if (empty($image)) {
            return false;
        }
 
        // generate random name for the file
        $this->file_location = time(). '.' . $image->extension;
 
        // the uploaded image instance
        return $image;
    }
 
    public function getUploadedFile() {
        // return a default image placeholder if your source avatar is not found
        $file = isset($this->file_location) ? $this->file_location : 'default.png';
        return Yii::$app->params['fileUploadUrl'] . $file;
    }
	
	public function getFileLocation() 
	{
		return $this->hasMany(EDocFileLocation::className(), ['e_document_id' => 'id'])->select(['file_location'])->asArray();
	}
	
	public function getFileLocationString() 
	{
		$filelocation = $this->fileLocation;
		$globalArray = [];
		foreach($filelocation as $key => $value){
			array_push($globalArray,$value['file_location']);
		}
		
		return implode(',',$globalArray);
	}
	
	
	
	
}
