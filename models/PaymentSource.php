<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%payment_source}}".
 *
 * @property integer $id
 * @property string $source_code
 * @property string $description
 */
class PaymentSource extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_source}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'source_code', 'description'], 'required'],
            [['id'], 'integer'],
            [['source_code'], 'string', 'max' => 4],
            [['description'], 'string', 'max' => 50],
            [['source_code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source_code' => 'Source Code',
            'description' => 'Description',
        ];
    }
	
	public static function displayPaymentSource()
	{
		return self::find()->asArray()->all();
	}
}
