<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_folder_component".
 *
 * @property string $folder_id
 * @property integer $componenet_id
 *
 * @property Folder $tycRef
 * @property Component $componenet
 */
class FolderComponent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%folder_component}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['folder_id', 'componenet_id'], 'required'],
            [['componenet_id'], 'integer'],
            [['folder_id'], 'string', 'max' => 16],
            [['folder_id'], 'exist', 'skipOnError' => true, 'targetClass' => Folder::className(), 'targetAttribute' => ['folder_id' => 'folder_id']],
            [['componenet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Component::className(), 'targetAttribute' => ['componenet_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'folder_id' => 'Folder',
            'componenet_id' => 'Componenet ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFolders()
    {
        return $this->hasMany(Folder::className(), ['id' => 'folder_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
	 
    public function getComponenet()
    {
        return $this->hasOne(Component::className(), ['id' => 'componenet_id']);
    }
	
	public static function countTotal($id)
	{
		return self::find()->where(['folder_id' => $id])->all();;
	}
	
	public function getListFolders()
	{
		$result = [];
		foreach ($this->folders as $folder) {
			$result[] = $folder->nameString;
		}
		return $result;
	}
}
