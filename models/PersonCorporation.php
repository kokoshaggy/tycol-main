<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%person_corporation}}".
 *
 * @property integer $person_id
 * @property integer $corporation_id
 *
 * @property TmPerson $person
 * @property TmCorporation $corporation
 */
class PersonCorporation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%person_corporation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'corporation_id'], 'required'],
            [['person_id', 'corporation_id'], 'integer'],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => TmPerson::className(), 'targetAttribute' => ['person_id' => 'id']],
            [['corporation_id'], 'exist', 'skipOnError' => true, 'targetClass' => TmCorporation::className(), 'targetAttribute' => ['corporation_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person_id' => 'Person ID',
            'corporation_id' => 'Corporation ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(TmPerson::className(), ['id' => 'person_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCorporation()
    {
        return $this->hasOne(TmCorporation::className(), ['id' => 'corporation_id']);
    }
}
