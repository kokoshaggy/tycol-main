<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_order_component".
 *
 * @property integer $order_number
 * @property integer $componenet_id
 *
 * @property TmOrder $orderNumber
 * @property TmComponent $componenet
 */
class OrderComponent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tm_order_component';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_number', 'componenet_id'], 'required'],
            [['order_number', 'componenet_id'], 'integer'],
            [['order_number'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_number' => 'order_number']],
            [['componenet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Component::className(), 'targetAttribute' => ['componenet_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_number' => 'Order Number',
            'componenet_id' => 'Componenet ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderNumber()
    {
        return $this->hasOne(Order::className(), ['order_number' => 'order_number']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponenet()
    {
        return $this->hasOne(Component::className(), ['id' => 'componenet_id']);
    }
}
