<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_component_task".
 *
 * @property integer $component_id
 * @property integer $task_id
 *
 * @property TmTask $task
 * @property TmComponent $component
 */
class ComponentTask extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%component_task}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['component_id', 'task_id'], 'required'],
            [['component_id', 'task_id'], 'integer'],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => TmTask::className(), 'targetAttribute' => ['task_id' => 'id']],
            [['component_id'], 'exist', 'skipOnError' => true, 'targetClass' => TmComponent::className(), 'targetAttribute' => ['component_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'component_id' => 'Component ID',
            'task_id' => 'Task ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(TmTask::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponent()
    {
        return $this->hasOne(TmComponent::className(), ['id' => 'component_id']);
    }
}
