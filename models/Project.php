<?php

namespace app\models;

use Yii;
use app\boffins_vendor\classes\FolderSubComponentARModel;
use app\boffins_vendor\behaviors\ComponentsBehavior;
use app\boffins_vendor\behaviors\DeleteUpdateBehavior;
use app\boffins_vendor\behaviors\DateBehavior;
/**
 * This is the model class for table "{{%project}}".
 *
 * @property string $tyc_ref
 * @property integer $project_id
 * @property integer $client_id
 * @property integer $supplier_id
 * @property string $client_reference
 * @property string $manufacturer_ref
 * @property string $project_status
 * @property string $pro_description
 *
 * @property Order[] $Orders
 * @property Client $client
 * @property Supplier $supplier
 */
class Project extends FolderSubComponentARModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'supplier_id','client_reference', 'manufacturer_ref','pro_description'], 'required'],
            [['client_id', 'supplier_id'], 'integer'],
            [['itemType', 'itemID'], 'safe'],
            [['client_reference', 'manufacturer_ref'], 'string', 'max' => 30],
            [['project_status'], 'string', 'max' => 255],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Supplier::className(), 'targetAttribute' => ['supplier_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tyc_ref' => 'Tyc Ref',
            'project_id' => 'Project',
            'client_id' => 'Client',
            'supplier_id' => 'Supplier',
            'client_reference' => 'Client Reference',
            'manufacturer_ref' => 'Manufacturer Ref',
            'project_status' => 'Project Status',
            'pro_description' => 'Description',
        ];
    }
	
	public function getDescriptions()
    {
        return $this->pro_description;
        
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['tyc_ref' => 'tyc_ref']);
    }
	
	public function getFolderComponent()
    {
        return $this->hasOne(FolderComponent::className(), ['component_id' => 'component_id']);
    }
	
	public function getTycref(){
		return !empty($this->folderComponent->folder_id)? $this->folderComponent->listFolders : 'Not in a folder yet';
	}
	
	public function getFolderDescription(){
		return $this->hasOne(Folder::className(), ['tyc_ref' => 'tycref']);
	}
	
	

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'supplier_id']);
    }
    
    //fetch the total number of projects added by kingsley of buffins 
    public function allrecords()
	{
        return Count($this->find()->all());
    }
    
    public static function get_project_comments($project_id,$type)
	{
        $remark=Remark::get_project_comment($project_id,$type);
        return $remark;
    }
        
    public static function get_allproject_comments($id)
	{
        $remark=Remark::get_allproject_comment($id);
        return $remark;
    }
	
    public function display_projects()
	{
		$project= $this->find()
    		->asArray()
			->all();
        return $project;
    }
	
	public function getId(){
		return $this->project_id;
	}
		
	public function init() 
	{
		parent::init();
		// $this->on(Components::EVENT_AFTER_SOFT_DELETES, [$this, 'test']);
		 
	}
	
	
	
    
}
