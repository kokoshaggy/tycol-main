<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%component_manager}}".
 *
 * @property integer $component_id
 * @property integer $user_id
 * @property string $role
 *
 * @property Component $component
 * @property User $user
 */
class ComponentManager extends \yii\db\ActiveRecord
{
	
	const CM_AUTHOR = 'author';
	const CM_USER = 'user';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%component_manager}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['component_id', 'user_id', 'role'], 'required'],
            [['component_id', 'user_id'], 'integer'],
            [['role'], 'string'],
            [['component_id'], 'exist', 'skipOnError' => true, 'targetClass' => Component::className(), 'targetAttribute' => ['component_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'component_id' => 'Component',
            'user_id' => 'User',
            'role' => 'Role',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponent()
    {
        return $this->hasOne(Component::className(), ['id' => 'component_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
