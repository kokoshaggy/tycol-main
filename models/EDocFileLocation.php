<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_e_doc_file_location".
 *
 * @property int $e_document_id
 * @property string $file_location
 */
class EDocFileLocation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tm_e_doc_file_location';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['e_document_id', 'file_location'], 'required'],
            [['e_document_id'], 'integer'],
            [['file_location'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'e_document_id' => 'E Document ID',
            'file_location' => 'File Location',
        ];
    }
}
