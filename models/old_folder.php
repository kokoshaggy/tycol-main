<?php

namespace app\models;

use Yii;
use app\models\Project;
use app\models\Folder;
/**
 * This is the model class for table "tm_folder".
 *
 * @property string $tyc_ref
 * @property string $type
 * @property string $description
 * @property string $notes
 *
 * @property Project[] $Projects
 * @property Remark[] $Remarks
 */
class Folder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%folder}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tyc_ref', 'type','description'], 'required'],
            [['type'], 'string'],
            [['tyc_ref'], 'string', 'max' => 16],
            [['description', 'notes'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tyc_ref' => 'Folder',
            'type' => 'Folder Type',
            'description' => 'Description',
            'notes' => 'Notes',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['tyc_ref' => 'tyc_ref']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemarks()
    {
        return $this->hasMany(Remark::className(), ['tyc_ref' => 'tyc_ref']);
    }
    
    public function allrecords()
	{
        return Count($this->find()->all());
    }
    
    public function selectone($tycref)
	{
        return Folder::findOne($tycref);
    }
    
    public function displayFolder()
	{
        $folders = $this->find()
    		->asArray()
    		->all();
        return $folders;
    }
    
    
    public function get_folder_project($id)
	{
		$project=Project::findAll(['tyc_ref' =>$id, ]);
        return $project;
    }
    
    
    public function get_folder_invoice($id)
	{
        $invoice=Invoice::findAll(['tyc_ref' =>$id,]);  
        return $invoice;
    }
    
	
	//Added by Anthony 
	
	public function getNameString() 

	{
		return $this->tyc_ref . " (" . substr($this->description, 0, 20) . "...)";
	}
	
	public function addSubComponent($component_id) 
	{

		if (!$this->isNewRecord){
			$folderComponent = new FolderComponent;
			$folderComponent->tyc_ref = $this->tyc_ref;
			$folderComponent->component_id = $component_id;
			return $folderComponent->save(false);
		} else {
			throw new \yii\base\Exception( "You can only link a subcomponent to a preexisting folder" );
		}
	}
	
	public function getTycDescription(){
		return '('.$this->tyc_ref.') '.$this->description;
	}
	
	public function getSubComponents($type = '') 
	{
		
	}
    
}
