<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%file}}".
 *
 * @property string $tyc_ref
 * @property string $type
 * @property string $description
 * @property string $notes
 */
class File extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tyc_ref', 'type'], 'required'],
            [['type'], 'string'],
            [['tyc_ref'], 'string', 'max' => 16],
            [['description', 'notes'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tyc_ref' => 'Tyc Ref',
            'type' => 'Type',
            'description' => 'Description',
            'notes' => 'Notes',
        ];
    }
}
