<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%receivedpurchaseorder_component}}".
 *
 * @property string $receivedpurchaseorder_reference
 * @property integer $component_id
 *
 * @property Receivedpurchaseorder $receivedpurchaseorderReference
 * @property Component $component
 */
class ReceivedpurchaseorderComponent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%receivedpurchaseorder_component}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['receivedpurchaseorder_reference', 'component_id'], 'required'],
            [['component_id'], 'integer'],
            [['receivedpurchaseorder_reference'], 'string', 'max' => 255],
            [['receivedpurchaseorder_reference'], 'exist', 'skipOnError' => true, 'targetClass' => Receivedpurchaseorder::className(), 'targetAttribute' => ['receivedpurchaseorder_reference' => 'receivedpurchaseorder_reference']],
            [['component_id'], 'exist', 'skipOnError' => true, 'targetClass' => Component::className(), 'targetAttribute' => ['component_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'receivedpurchaseorder_reference' => 'Receivedpurchaseorder Reference',
            'component_id' => 'Component ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceivedpurchaseorderReference()
    {
        return $this->hasOne(Receivedpurchaseorder::className(), ['receivedpurchaseorder_reference' => 'receivedpurchaseorder_reference']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponent()
    {
        return $this->hasOne(Component::className(), ['id' => 'component_id']);
    }
}
