<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%receivedpurchaseorder_type}}".
 *
 * @property integer $receivedpurchaseorder_type_code
 * @property string $receivedpurchaseorder_type_name
 */
class ReceivedpurchaseorderorderType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%receivedpurchaseorder_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['receivedpurchaseorder_type_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'receivedpurchaseorder_type_code' => 'Receivedpurchaseorder Type Code',
            'receivedpurchaseorder_type_name' => 'Receivedpurchaseorder Type Name',
        ];
    }
}
