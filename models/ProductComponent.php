<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_product_component".
 *
 * @property integer $product_id
 * @property integer $component_id
 */
class productComponent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_component}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'component_id'], 'required'],
            [['product_id', 'component_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'product ID',
            'component_id' => 'Component ID',
        ];
    }
    
    
}
