<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
	public $creatorName;
    public $creatorEmail;
    public $subject;
    public $body;
    public $bodyAdmin;
    public $verifyCode;
	public $to;
	public $assingedToEmail;
	public $assingedToname;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail()
    {
        return Yii::$app->mailer->compose()
            ->setTo($this->to)
            ->setFrom([$this->email => $this->name])
            ->setSubject($this->subject)
            ->setTextBody($this->body)
            ->send();
    }
	
	
	public function sendFeedbackEmail($supportEmail = 'admin@epsolun.com')
    {
        $adminSuccess = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'adminfeedback', 'text' => 'adminfeedbacktext'],
                [
                    'email' => $this->creatorEmail,
                    'body'  => $this->bodyAdmin,
                    'name'  => $this->creatorName,
                    'assingedToname'  => $this->assingedToname,
                    'supportEmail' => $supportEmail,
                ]
            )
            ->setFrom([$supportEmail => Yii::$app->params['siteName'] . ' bot'])
            ->setTo($this->creatorEmail)
            ->setSubject('You Created a New Task:' . $this->subject)
            ->send();
        $userSuccess = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'usersfeedback', 'text' => 'userfeedbacktext'],
                [
                    'email' => $this->assingedToEmail,
                    'body'  => $this->body,
                    'name'  => $this->creatorName,
					'assingedToname'  => $this->assingedToname,
                    'supportEmail' => $supportEmail,
                ]
            )
            ->setFrom([$supportEmail => Yii::$app->params['siteName'] . ' bot'])
            ->setTo($this->assingedToEmail)
            ->setSubject('You have just been assinged a new task ')
            ->send();
        return $userSuccess && $adminSuccess;
    }
}
