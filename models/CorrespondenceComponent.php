<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_correspondence_component".
 *
 * @property integer $corrrespondence_id
 * @property integer $component_id
 */
class CorrespondenceComponent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%correspondence_component}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['corrrespondence_id', 'component_id'], 'required'],
            [['corrrespondence_id', 'component_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'corrrespondence_id' => 'Corrrespondence ID',
            'component_id' => 'Component ID',
        ];
    }
}
