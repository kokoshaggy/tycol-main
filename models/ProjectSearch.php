<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Project;

/**
 * Tmprojectsearch represents the model behind the search form about `app\models\Tmproject`.
 */
class ProjectSearch extends project
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_reference', 'manufacturer_ref', 'project_status'], 'safe'],
            [['project_id', 'client_id', 'supplier_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Project::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'project_id' => $this->project_id,
            'client_id' => $this->client_id,
            'supplier_id' => $this->supplier_id,
        ]);

        $query->andFilterWhere(['like', 'client_reference', $this->client_reference])
            ->andFilterWhere(['like', 'manufacturer_ref', $this->manufacturer_ref])
            ->andFilterWhere(['like', 'project_status', $this->project_status]);

        return $dataProvider;
    }
	
	public function init() 
	{
		$this->stopComponentBehaviors = true;
	}
}
