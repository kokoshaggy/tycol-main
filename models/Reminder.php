<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_reminder".
 *
 * @property integer $id
 * @property string $reminder_time
 * @property string $notes
 * @property string $last_updated
 * @property integer $deleted
 *
 * @property TaskReminder[] $tmTaskReminders
 */
class Reminder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%reminder}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reminder_time', 'notes', 'last_updated'], 'required'],
            [['reminder_time', 'last_updated'], 'safe'],
            [['deleted'], 'integer'],
            [['notes'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reminder_time' => 'Reminder Time',
            'notes' => 'Notes',
            'last_updated' => 'Last Updated',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskReminders()
    {
        return $this->hasMany(TmTaskReminder::className(), ['reminder_id' => 'id']);
    }
	
	public static function getActiveTask($presentTime,$userId,$userId2)
	{
		return Task::selectActiveTask($presentTime, $userId, $userId2);
	} 
	
	/**
	 *  @brief retrieve all the reminders set on tasks on related to a specific folder
	 *  
	 *  @param $user int user id from UserDB - user 
	 *  @param $folderID int 
	 *  @param $identity Description for $identity
	 *  @return Return description
	 *  
	 *  @details More details
	 */
	
	public static function getReminder($user, $folderID, $identity = NULL)
	{
		$allTaskReminder = TaskReminder::fetchAllReminderTask($user, $folderID, $identity);
		$allTaskReminderArray = [];
		
		foreach($allTaskReminder as $key => $value){
			array_push($allTaskReminderArray, $value['reminder_id']);
		}
		
		$userReminders = Reminder::find()->andWhere( [ 'in', 'id', array_values($allTaskReminderArray) ] );
		return $userReminders;
	}
	
	public function checkForReminders($presentTime)
	{
		$reminderAlert = Reminder::find()->andWhere(['<=', 'reminder_time', $presentTime])
											->andWhere(['reminder_status' => 0]) 
											->all();
		return $reminderAlert;
	}
}
