<?php

namespace app\models;

use app\boffins_vendor\classes\FolderSubComponentARModel;
use app\boffins_vendor\behaviors\ComponentsBehavior;
use app\boffins_vendor\behaviors\DeleteUpdateBehavior;
use app\boffins_vendor\behaviors\DateBehavior;
use yii\db\ActiveRecord;


use Yii;

/**
 * This is the model class for table "{{%payment}}".
 *
 * @property integer $id
 * @property integer $receiver_corporation_id
 * @property integer $payment_source_id
 * @property string $value
 * @property integer $currency_id
 * @property string $payment_date
 *
 * @property Corporation $receiverCorporation
 * @property PaymentSource $paymentSource
 * @property Currency $currency
 */
class Payment extends FolderSubComponentARModel
{	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['receiver_corporation_id', 'payment_source_id', 'value', 'currency_id', 'payment_date'], 'safe'],
            [['id', 'receiver_corporation_id', 'payment_source_id', 'currency_id'], 'integer'],
            [['value'], 'string'],
            [['payment_date','itemType', 'itemID'], 'safe'],
            //[['tyc_ref'], 'string', 'max' => 16],
            [['receiver_corporation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Corporation::className(), 'targetAttribute' => ['receiver_corporation_id' => 'id']],
            [['payment_source_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentSource::className(), 'targetAttribute' => ['payment_source_id' => 'id']],
           /* [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
        */];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            //'tyc_ref' => 'Tyc Ref',
            'receiver_corporation_id' => 'Receiver Corporation ',
            'payment_source_id' => 'Payment Source ',
            'value' => 'Value',
            'currency_id' => 'Currency',
            'payment_date' => 'Payment Date',
        ];
    }	

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiverCorporation()
    {
        return $this->hasOne(Corporation::className(), ['id' => 'receiver_corporation_id']);
    }
	public function getReceiverName()
    {
        return $this->receiverCorporation->nameString;
    }
	
	public function getId(){
		return $this->id;
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentSource()
    {
        return $this->hasOne(PaymentSource::className(), ['id' => 'payment_source_id']);
    }
	
	public function getSubComponents() {
		return $this->hasMany(PaymentComponent::className(), ['payment_id' => 'id']);
	}

	public function getSourceCode(){
		return !empty($this->paymentSource->source_code)?$this->paymentSource->source_code:'no data to return';
	}
	
	public function getReceiver()
	{
		return $this->receiverCorporation->name;
	}
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }
	
	public function getValueAndSymbol(){
		return ($this->currency->symbol).' '.\Yii::$app->formatter->asDecimal($this->value);
	}
	
	public function getDescriptions()
    {
        return $this->value;
        
    }
	
	/*public function fetchFolderPayment($id)
    {
        return Payment::find()->where(['tyc_ref'=>$id])->asArray()->all();
    }
	
	
	public function getFoldercomptTyc()
    {
        return $this->hasOne(Foldercomponent::className(), ['component_id' => 'component_id']);
    }
	
	public function getComponentTycmain()
    {
        return $this->foldercomptTyc->component_id;
    }*/
	
	public function myBehaviors() {
		
		return [
				'componentBehavior' => ComponentsBehavior::className(),
				'deleteUpdateBehavior2' => [
						'class' => DeleteUpdateBehavior::className(),
						//'forceDelete' => true, 
						//'lastUpdatedAttribute' => 'NONE',
				],
				"dateValues" => 
				[
					"class" => DateBehavior::className(),
					"AREvents" => 
						[ActiveRecord::EVENT_BEFORE_VALIDATE => 
							[ 'rules' => 
								[
									DateBehavior::DATE_CLASS_STAMP => 
										[
											'attributes' => ['payment_date','last_updated'],
										],
									
								] 
							],
							ActiveRecord::EVENT_AFTER_FIND => 
								[ 'rules' => 
									[
										DateBehavior::DATE_CLASS_STANDARD => 
												[
													'attributes' => ['last_updated', 'payment_date'],
												]
									] 
								],
						],
				],
		];

		
	}
	
	
}
