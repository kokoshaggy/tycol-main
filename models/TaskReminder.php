<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_task_reminder".
 *
 * @property integer $reminder_id
 * @property integer $task_id
 *
 * @property TmTask $task
 * @property TmReminder $reminder
 */
class TaskReminder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%task_reminder}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reminder_id', 'task_id'], 'required'],
            [['reminder_id', 'task_id'], 'integer'],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
            [['reminder_id'], 'exist', 'skipOnError' => true, 'targetClass' => Reminder::className(), 'targetAttribute' => ['reminder_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reminder_id' => 'Reminder ID',
            'task_id' => 'Task ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(TmTask::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReminder()
    {
        return $this->hasOne(TmReminder::className(), ['id' => 'reminder_id']);
    }
	
	public static function fetchAllReminderTask($user, $folderRef, $identity)
	{
		$taskReminder = new Taskreminder;
		$folderReminders = Task::fetchAllReminderTask($user, $folderRef, $identity);
		$folderTaskArray = [] ;
		
		foreach($folderReminders as $key => $value){	
			 array_push($folderTaskArray, $value['id']);
		}
		
		$fetchTask = $taskReminder->find()->where(['reminder_id' => array_values($folderTaskArray)])->all();
		return $fetchTask;
	}
	
	public function getTaskId($reminderId)
	{
		$taskId = Taskreminder::find()->where(['reminder_id' => $reminderId,])->one();
		return !empty($taskId->task_id);
	}
	
}
