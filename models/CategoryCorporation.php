<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tm_categoroy_corporation}}".
 *
 * @property integer $productcategory_id
 * @property integer $corporation_id
 *
 * @property TmCategory $productcategory
 * @property TmCorporation $corporation
 */
class CategoryCorporation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category_corporation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['productcategory_id', 'corporation_id'], 'required'],
            [['productcategory_id', 'corporation_id'], 'integer'],
            [['productcategory_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['productcategory_id' => 'id']],
            [['corporation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Corporation::className(), 'targetAttribute' => ['corporation_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'productcategory_id' => 'Productcategory ID',
            'corporation_id' => 'Corporation ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductcategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'productcategory_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCorporation()
    {
        return $this->hasOne(Corporation::className(), ['id' => 'corporation_id']);
    }
}
