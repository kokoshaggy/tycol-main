<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_payment_component".
 *
 * @property integer $payment_id
 * @property integer $componenet_id
 *
 * @property TmPayment $payment
 * @property TmComponent $componenet
 */
class PaymentComponent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_component}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_id', 'componenet_id'], 'required'],
            [['payment_id', 'componenet_id'], 'integer'],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => TmPayment::className(), 'targetAttribute' => ['payment_id' => 'id']],
            [['componenet_id'], 'exist', 'skipOnError' => true, 'targetClass' => TmComponent::className(), 'targetAttribute' => ['componenet_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_id' => 'Payment ID',
            'componenet_id' => 'Componenet ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(TmPayment::className(), ['id' => 'payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponenet()
    {
        return $this->hasOne(TmComponent::className(), ['id' => 'componenet_id']);
    }
}
