<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%user_route_access}}".
 *
 * @property integer $user_id
 * @property string $route_name
 * @property integer $access_level
 */
class UserRouteAccess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_route_access}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'route_name', 'access_level'], 'required'],
            [['user_id', 'access_level'], 'integer'],
            [['route_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'route_name' => 'Route Name',
            'access_level' => 'Access Level',
        ];
    }
}
