<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_folder_task".
 *
 * @property string $folder_ref
 * @property integer $task_id
 *
 * @property TmTask $task
 * @property TmFolder $folderRef
 */
class FolderTask extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%folder_task}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['folder_id', 'task_id'], 'required'],
            [['task_id','folder_id'], 'integer'],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
            [['folder_id'], 'exist', 'skipOnError' => true, 'targetClass' => Folder::className(), 'targetAttribute' => ['folder_id' => 'tyc_ref']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'folder_id' => 'Folder Reference',
            'task_id' => 'Task',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFolderRef()
    {
        return $this->hasOne(Folder::className(), ['tyc_ref' => 'folder_id']);
    }
	
	public static function getAllFolderTask($folderID){
		$folderTask = new Foldertask;
		$folderTaskArray = $folderTask->findAll(['folder_id' => $folderID]) ;
		return $folderTaskArray;
	}
	
	public static function getUsername($id){
		return Userdb::findById($id) ;
	}
}
