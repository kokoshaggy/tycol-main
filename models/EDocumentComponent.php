<?php

namespace app\models;

use Yii;
use \app\models\base\EDocumentComponent as BaseEDocumentComponent;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tm_e_document_component".
 */
class EDocumentComponent extends BaseEDocumentComponent
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
