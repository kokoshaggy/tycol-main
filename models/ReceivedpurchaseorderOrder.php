<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%receivedpurchaseorder_order}}".
 *
 * @property string $receivedpurchaseorder_reference
 * @property integer $order_number
 */
class ReceivedpurchaseorderOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%receivedpurchaseorder_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['receivedpurchaseorder_reference', 'order_number'], 'required'],
            [['order_number'], 'integer'],
            [['receivedpurchaseorder_reference'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'receivedpurchaseorder_reference' => 'Receivedpurchaseorder Reference',
            'order_number' => 'Order Number',
        ];
    }
}
