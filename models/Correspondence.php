<?php

namespace app\models;

use Yii;
use app\boffins_vendor\classes\FolderSubComponentARModel;
use app\boffins_vendor\behaviors\ComponentsBehavior;
use app\boffins_vendor\behaviors\DeleteUpdateBehavior;
use app\boffins_vendor\behaviors\DateBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tm_correspondence".
 *
 * @property integer $id
 * @property string $tyc_ref
 * @property integer $component_id
 * @property string $particular_reference
 * @property integer $parent_id
 * @property string $title_description
 * @property resource $content
 * @property integer $corresponding_entity
 * @property integer $specific_corresponding_entity
 * @property integer $drafting_entity
 * @property integer $authorising_entity
 * @property string $create_date
 * @property string $notes
 */
class Correspondence extends FolderSubComponentARModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%correspondence}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['particular_reference', 'title_description', 'content', 'corresponding_entity', 'drafting_entity', 'authorising_entity'], 'required'],
            [['corresponding_entity', 'specific_corresponding_entity', 'drafting_entity', 'authorising_entity'], 'integer'],
            [['content', 'notes'], 'string'],
            [['create_date', 'parent_id', 'itemType', 'itemID'],  'safe'],

            [['title_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tyc_ref' => 'Tyc Ref',
            'component_id' => 'Component ID',
            'particular_reference' => 'Particular Reference',
            'parent_id' => 'Previous Correspondence',
            'title_description' => 'Title Description',
            'content' => 'Content',
            'corresponding_entity' => 'Addressee',
            'specific_corresponding_entity' => 'Contact Person',
            'drafting_entity' => 'Drafted By',
            'authorising_entity' => 'Authorised By',
            'create_date' => 'Date Created',
            'notes' => 'Note',
			'correspondingEntityNameString' => 'Address',
			'specificCorrespondingEntityName' => 'Contact Person',
			'draftingEntityPersonName' => 'Drafted By',
			'authorisingEntityPersonName' => 'Authorised By'
			
        ];
    }
	
	public static function fetchAllcorrespondence()
	{
		$details = Correspondence::find()->asArray()->all();
		return  $details ;
		
		 
	}
	
	public function getCorrespondingEntity()
    {
        return $this->hasOne(Corporation::className(), ['entity_id' => 'corresponding_entity']);
        
    }
	
	public function getDescriptions()
    {
        return $this->title_description;
        
    }
	
	public function getSpecificCorrespondingEntity()
    {
        return $this->hasOne(Person::className(), ['entity_id' => 'specific_corresponding_entity']);
		
        
    }
	public function getDraftingEntity()
    {
        return $this->hasOne(UserDb::className(), ['id' => 'drafting_entity']);
		
        
    }
	
	public function getDraftingEntityPersonName()
    {
        return $this->draftingEntity->person->personstring;
 
    }
	
	public function getAuthorisingEntity()
    {
        return $this->hasOne(UserDb::className(), ['id' => 'authorising_entity']);
 
    }
	public function getAuthorisingEntityPersonName()
    {
        return $this->authorisingEntity->person->personstring;
 
    }
	
	public function getSpecificCorrespondingEntityName()
    {
        
		return $this->specificCorrespondingEntity->personstring;
        
    }
	
	public function getCorrespondingEntityNameString(){
		return !empty($this->correspondingEntity->nameString)?$this->correspondingEntity->nameString:'Not Available';
	}
	
	public function myBehaviors() {
		
		return [
				'componentBehavior' => ComponentsBehavior::className(),
				'deleteUpdateBehavior2' => [
						'class' => DeleteUpdateBehavior::className(),
						//'forceDelete' => true, 
						//'lastUpdatedAttribute' => 'NONE',
				],
				"dateValues" => 
				[
					"class" => DateBehavior::className(),
					"AREvents" => 
						[ActiveRecord::EVENT_BEFORE_VALIDATE => 
							[ 'rules' => 
								[
									DateBehavior::DATE_CLASS_STAMP => 
										[
											'attributes' => ['create_date','last_updated'],
										],
									
								] 
							],
							ActiveRecord::EVENT_AFTER_FIND => 
								[ 'rules' => 
									[
										DateBehavior::DATE_CLASS_STANDARD => 
												[
													'attributes' => ['last_updated', 'create_date'],
												]
									] 
								],
						],
				],
		];

		
	}
}
