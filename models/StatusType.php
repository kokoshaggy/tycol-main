<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_status_type".
 *
 * @property integer $id
 * @property string $title
 * @property string $phase
 *
 * @property TmTask[] $tmTasks
 */
class StatusType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%status_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_title', 'status_group'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_title' => 'Title',
            'status_group' => 'group',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(TmTask::className(), ['status_id' => 'id']);
    }
	
	public static function fetchAllUsers($group)
	{
		$status = new StatusType;
		$result = $status->find()
			->where(['status_group'=>$group])
			->asArray()->indexBy('id')
			->all();
		return $result ;
	}
}
