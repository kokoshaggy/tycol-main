<?php

namespace app\models;

use Yii;
use app\boffins_vendor\classes\FolderSubComponentARModel;
use app\boffins_vendor\behaviors\ComponentsBehavior;
use app\boffins_vendor\behaviors\DeleteUpdateBehavior;
use app\boffins_vendor\behaviors\DateBehavior;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $order_number
 * @property string $folder_id
 * @property integer $component_id 
 * @property integer $supplier_id
 * @property string $supplier_ref
 * @property string $order_value
 * @property integer $order_currency
 * @property string $issue_date
 * @property string $supplier_completion_date
 * @property string $order_status
 * @property resource $order_file
 * @property string $last_updated 
 * @property integer $deleted 
 *
 * @property Project $tycRef
 * @property Corporation $supplier
 * @property Component $component
 * @property OrderComponent[] $OrderComponents
 */
 
class Order extends FolderSubComponentARModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() 
    {
        return [
            [['order_number', 'supplier_id', 'order_value', 'order_currency',], 'required'],
            [['order_number', 'supplier_id', 'order_currency'], 'integer'],
            [['order_value'], 'string'],
            [['issue_date', 'supplier_completion_date','itemType', 'itemID'], 'safe'],
            [['order_file'], 'string'],
            [[ 'supplier_ref'], 'string', 'max' => 16],
            [['order_status'], 'string', 'max' => 255],
            
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Corporation::className(), 'targetAttribute' => ['supplier_id' => 'id']],
			[['component_id'], 'exist', 'skipOnError' => true, 'targetClass' => Component::className(), 'targetAttribute' => ['component_id' => 'id']],
			[	['order_number'], 
				'unique',
				'message' => 'This order_number has already been taken.', 
				'filter' => function($query) {
								Yii::warning('this is run');
								if (! $this->isNewRecord ) { //order structure has to change. As Ajax validation works through new models???
									$query->andWhere(['NOT', ['order_number'=>$this->order_number]]); 
								}
							} 
			],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_number' => 'Order Number',
            'folder_id' => 'Main Tycol Reference',
            'supplier_id' => 'Supplier',
            'supplier_ref' => 'Supplier Reference',
            'order_value' => 'Order Value',
            'order_currency' => 'Order Currency',
            'issue_date' => 'Issue Date',
            'supplier_completion_date' => 'Supplier ETD',
            'order_status' => 'Order Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
	
	public function checkUniq($attribute, $params)
	{
		$uniq = self::find()->where(['order_number'=>$this->order_number])->one();
		if (count($uniq)==1){
			$this->addError('order_number', 'order number already exist.');
		}
	}
	
	public function clientValidateAttribute($model, $attribute, $view)
	{
		
		$uniq = self::find()->where(['order_number'=>$this->order_number])->one();
		if (count($uniq)==1){
			
			return <<<JS
			deferred.push(messages.push('test'));
JS;
	}
    
}
	
   /*  this method has to be revieved as tyc_ref nolonger exist 
   public function getTycRef()
    {
        return $this->hasOne(Project::className(), ['folder_id' => 'folder_id']);
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Corporation::className(), ['id' => 'supplier_id']);
    }
	
	public function getSuppliername()
    {
        return $this->supplier->name;
    }
	
	public function getStatustype()
    {
        return $this->hasOne(StatusType::className(), ['id' => 'order_status']);
    }
	
	public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'order_currency']);
    }
	
	public function getCurrencyAndAmount()
    {
        return '('.$this->currency->symbol .') '.\Yii::$app->formatter->asDecimal($this->order_value);
    }
    
	/**
	 * ADDED BY BOFFINS SYSTEMS
	 */
	//Added By Kingsley 
    public function allRecords() 
	{
        return Count($this->find()->all());
    }
	
	//Added by Anthony
	public function getComponent() 
	{ 
		return $this->hasOne(Component::className(), ['id' => 'component_id']);
	}
	
	public function getSubComponents() 
	{
		return $this->hasMany(OrderComponent::className(), ['order_number' => 'order_number']);
	}
	
	public function getDescriptions()
    {
        return $this->title_order_number;
        
    }

	/**
     * @inheritdoc
	 * See documentation for parent model FolderSubComponentARModel
	 */
	public function myBehaviors() 
	{
		
		return [
				'componentBehavior' => ComponentsBehavior::className(),
				'deleteUpdateBehavior2' => [
						'class' => DeleteUpdateBehavior::className(),
						//'forceDelete' => true, 
						//'lastUpdatedAttribute' => 'NONE',
				],
				"dateValues" => 
				[
					"class" => DateBehavior::className(),
					"AREvents" => 
						[ActiveRecord::EVENT_BEFORE_VALIDATE => 
							[ 'rules' => 
								[
									DateBehavior::DATE_CLASS_STAMP => 
										[
											'attributes' => ['last_updated'],
										],
									DateBehavior::DATE_CLASS_STANDARD => 
										[
											'attributes' => ['issue_date', 'supplier_completion_date']
										]
								] 
							],
							ActiveRecord::EVENT_AFTER_FIND => 
								[ 'rules' => 
									[
										DateBehavior::DATE_CLASS_STANDARD => 
												[
													'attributes' => ['last_updated', 'issue_date', 'supplier_completion_date'],
												]
									] 
								],
						],
				],
		];

		
	}
	
	
	
		
	
}
