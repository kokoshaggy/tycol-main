<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%folder}}".
 *
 * @property integer $id
 * @property string $tyc_ref
 * @property string $type
 * @property string $description
 * @property string $notes
 * @property string $last_updated
 * @property integer $deleted
 *
 * @property FolderComponent[] $folderComponents
 * @property Component[] $components
 * @property FolderManager[] $folderManagers
 * @property User[] $users
 * @property FolderTask[] $folderTasks
 * @property Task[] $tasks
 * @property Remark[] $remarks
 */
class Folder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%folder}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tyc_ref', 'type'], 'required'],
            [['deleted'], 'integer'],
            [['type'], 'string'],
            [['last_updated'], 'safe'],
            [['tyc_ref'], 'string', 'max' => 16],
            [['description', 'notes'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tyc_ref' => Yii::t('app', 'Folder'),
            'type' => Yii::t('app', 'Folder Type'),
            'description' => Yii::t('app', 'Description'),
            'notes' => Yii::t('app', 'Notes'),
            'last_updated' => Yii::t('app', 'Last Updated'),
            'deleted' => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFolderComponents()
    {
        return $this->hasMany(FolderComponent::className(), ['folder_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponents()
    {
        return $this->hasMany(Component::className(), ['id' => 'component_id'])->viaTable('{{%folder_component}}', ['folder_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFolderManagers()
    {
        return $this->hasMany(FolderManager::className(), ['folder_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('{{%folder_manager}}', ['folder_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFolderTasks()
    {
        return $this->hasMany(FolderTask::className(), ['folder_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['id' => 'task_id'])->viaTable('{{%folder_task}}', ['folder_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemarks()
    {
        return $this->hasMany(Remark::className(), ['folder_id' => 'id']);
    }
	
	//Added by Anthony 
	
	public function getNameString() 
	{
		return $this->tyc_ref . " (" . substr($this->description, 0, 20) . "...)";
	}
	
	public function addSubComponent($component_id) 
	{
		if (!$this->isNewRecord) {
			$folderComponent = new FolderComponent;
			$folderComponent->folder_id = $this->id;
			$folderComponent->component_id = $component_id;
			return $folderComponent->save(false);
		} else {
			throw new \yii\base\Exception( "You can only link a subcomponent to a preexisting folder" );
		}
	}
	
	public function getTycDescription()
	{
		return '(' . $this->tyc_ref . ') ' . $this->description;
	}
	
	public function getSubComponents($type = '') 
	{
		$componentsArray = [];
		foreach( $this->components as $component ) { //load this to a simple query with a where   filter where type is given. 
			if ( empty($type) ) {
				$componentsArray[$component->component_type] = $component->componentItem;
			}
			
			if ( !empty($type) && strtolower($type) == $component->component_type ) {
				$componentsArray[] = $component->componentItem;
			}
		}
		return $componentsArray;
	}
	
	public static function getDashboardItems($limit = 100) 
	{
		return SELF::find()
				->orderBy(['last_updated' => SORT_DESC])
				->limit($limit)->asArray()
				->all();
	}
	
	public function getIsEmpty() 
	{
		return empty( $this->components ) ? true: false;
	}
	
}
