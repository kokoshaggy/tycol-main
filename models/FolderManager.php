<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%folder_manager}}".
 *
 * @property string $tyc_ref
 * @property integer $user_id
 * @property string $role
 *
 * @property Folder $tycRef
 * @property User $user
 */
class FolderManager extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%folder_manager}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tyc_ref', 'user_id', 'role'], 'required'],
            [['user_id'], 'integer'],
            [['role'], 'string'],
            [['tyc_ref'], 'string', 'max' => 16],
            [['tyc_ref'], 'exist', 'skipOnError' => true, 'targetClass' => Folder::className(), 'targetAttribute' => ['tyc_ref' => 'tyc_ref']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tyc_ref' => 'Tyc Ref',
            'user_id' => 'User ID',
            'role' => 'Role',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTycRef()
    {
        return $this->hasOne(Folder::className(), ['tyc_ref' => 'tyc_ref']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
