<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Request */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
.info-text {
	font-size: 14px;
}

.info-text.big {
	font-size: 18px;
}
</style>
 
<section>
	<div id="file-inputs">
		<div class="info-text big">
			<?php echo "This is a tool for Top Idea to compare an excel sheet of contacts against your CRM list. This tool only accepts the following file formats. " ?>
		</div>
		<ul>
			<li>
				<?php echo "Excel 'xls', 'xml' or 'xlsx' files. "  ?>
			</li>
			<li>
				<?php echo "Comma separated files "  ?>
			</li>
		</ul>
		<div class="info-text">
			<?php echo "Also note that the CRM Full list needs to have the following columns as a minimum: " ?>
		</div>
		<ul>
			<li>
				<?php echo "code - which translates to the organisation's GT organisation code" ?>
			</li>
			<li>
				<?php echo "email - must be in email address format - '***@****.***' - or it would be ignored. " ?>
			</li>
			<li>
				<?php echo "contact - A full name of a contact for the organisation. When only a first name is stated, the surname is blank and not matchable " ?>
			</li>
			<li>
				<?php echo "name - corresponding to the organisations name " ?>
			</li>
		</ul>
		<div class="info-text">
			<?php echo "Finally note that the contacts list needs to have the following columns as a minimum: " ?>
		</div>
		<ul>
			<li>
				<?php echo "e-mail - Please note different spelling. This must be in email address format - '***@****.***' - or it would be ignored. " ?>
			</li>
			<li>
				<?php echo "surname - A full name of a contact for the organisation. When only a first name is stated, the surname is blank and not matchable " ?>
			</li>
			<li>
				<?php echo "name - corresponding to the organisations name " ?>
			</li>
		</ul>
		<h2 style="margin-bottom:25px;" >
			<?php echo "The tool will not work if the above formats are not met " ?>
		</h2>


		<?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data' ]  ]); ?>
		
		<?= $form->field($model, 'contacts_file')->fileInput(['multiple' => false]) ?>
		
		<?= $form->field($model, 'org_xml_file')->fileInput(['multiple' => false]) ?>
		
		<?= Html::submitButton( Yii::t('app', 'Compare'), ['class' => 'btn btn-primary']) ?>
		
		<?php ActiveForm::end(); ?>
	</div>

</section>