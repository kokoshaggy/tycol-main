<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Request */
/* @var $form yii\widgets\ActiveForm */
?>

<section>
	<div id="error">
		Sorry something went wrong. Please go back and try again. 
		
		<p>
		Please ensure that you have followed the guidelines and format.
		</p>
	</div>
</section>