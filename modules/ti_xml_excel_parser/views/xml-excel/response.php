<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Request */
/* @var $form yii\widgets\ActiveForm */
?>

<section>
	<div id="response">
		<h2>
			<?php echo "File Compare was sucessful. Results are as follows:" ?>
		</h2>
		<ul>
			<li>
				<?php echo "Successful Matches: " . $data['counts']['goodCount'] ?>
			</li>
			<li>
				<?php echo "Ambiguous Matches: " . $data['counts']['ambiguousCount'] ?>
			</li>
			<li>
				<?php echo "Unsuccessful Matches: " . $data['counts']['noMatchCount'] ?>
			</li>
		</ul>
		<p>
		<?php echo Html::a("File - " .  $data['export']['title'], $data['export']['fileLocation'], ['target'=>'_blank']); ?>
		</p>
		<p>
		<?php echo Html::a("File - " .  $data['export']['title2'], $data['export']['fileLocation2'], ['target'=>'_blank']); ?>
		</p>
	</div>
</section>