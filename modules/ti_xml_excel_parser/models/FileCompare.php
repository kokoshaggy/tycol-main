<?php 
namespace app\modules\ti_xml_excel_parser\models;

use yii\base\Model;
use yii\web\UploadedFile;

class FileCompare extends Model
{
    public $contacts_file;
    public $org_xml_file;
	private $fileLocations;
	private $orgColumns;
	private $_errorMessages = '';
	private $_error = false;
	
	public function attributeLabels()
    {
        return [
            'contacts_file' => 'Please provide Excel sheet(s) with contact details',
            'org_xml_file' => 'Please provide Excel or XML documents with full organisation details',
        ];
    }
	
	public function rules()
	{
		return [
			// both files are required
			[['contacts_file', 'org_xml_file'], 'safe'],
			[['contacts_file'], 'file', 'extensions' => 'xls, xml, xlsx', 'maxFiles' => 4],
			[['org_xml_file'], 'file', 'extensions' => 'xls, xml, xlsx']
		];
	}
	private function _uploadContacts() 	
	{
		if ( empty($this->contacts_file) ) {
			$this->addError('contacts_file', 'I need contacts to match against!');
			\Yii::trace('Contacts file is empty');
			return false;
		}
		
		\Yii::trace('Uploading Contacts');
		$fileName = 'ti_uploads/' . $this->contacts_file->baseName  . '_' . date('d_m_Y_H_i_s') . '.' . $this->contacts_file->extension;
		$this->contacts_file->saveAs($fileName);
		$this->fileLocations['contacts_file'] = $fileName;
		return true;
	}
	
	private function _uploadOrganisations()
	{
		if ( empty($this->org_xml_file) ) {
			$this->addError('org_xml_file', 'I need your full organisation list to match against!');
			\Yii::trace('Organisations file is empty');
			return false;
		}

		\Yii::trace('Uploading Organisations');
		$fileName = 'ti_uploads/' . $this->org_xml_file->baseName .  '_' . date('d_m_Y_H_i_s') . '.' . $this->org_xml_file->extension;
		$this->org_xml_file->saveAs($fileName);
		$this->fileLocations['org_xml_file'] = $fileName;
		return true;
	}
	
	public function upload()
	{
		return $this->_uploadOrganisations() && $this->_uploadContacts();
	}
	
	private function _readOrgXMLFile()
	{
		if ( empty($this->org_xml_file) ) {
			\Yii::trace('Organisations file is empty?');
			return false;
		}
		
		return $this->_readFile( $this->fileLocations['org_xml_file'], $this->org_xml_file->extension );
	}
	
	private function _readContactFile()
	{
		if ( empty($this->contacts_file) ) {
			\Yii::trace('Contact file is empty?');
			return false;
		}
		
		return $this->_readFile( $this->fileLocations['contacts_file'], $this->contacts_file->extension );
	}
	
	/*
	 * function to read an excel document (xls, xlsx, xml, csv)
	 * and return an array with the rows as array items. (each row is also an array with each column an item in the array)
	 */
	private function _readFile($fileLocation, $extension = 'xls') 
	{
		\Yii::trace("Reading file {$fileLocation}");
		switch ($extension) {
			case 'xls' :
				\Yii::trace('Uploading extension is xls');
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
				$sheet = $reader->load($fileLocation);
				$data = $sheet->getActiveSheet()->toArray();
				break;
			case 'xlsx' :
				\Yii::trace('Uploading extension is xlsx');
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				$sheet = $reader->load($fileLocation);
				$data = $sheet->getActiveSheet()->toArray();
				break;
			case 'xml' :
				\Yii::trace('Uploading extension is xml');
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xml();
				$sheet = $reader->load($fileLocation);
				$data = $sheet->getActiveSheet()->toArray();
				break;
			case 'csv' :
				\Yii::trace('Uploading extension is csv');
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
				$sheet = $reader->load($fileLocation);
				$data = $sheet->getActiveSheet()->toArray();
				break;
			default : 
				\Yii::trace('This file extension is unknown');
				$data = [];
		}
		return $data;
	}
	
	private function _getOrgKeyColumns() 
	{
		$organisationsArr = $this->_readOrgXMLFile();
		$orgArrKeys = $this->_matchColumnKeys( $organisationsArr[0], [
														'email' => 'email',
														'contact' => 'contact', 
														'name' => 'name',
														'code' => 'code'
														]);
		$orgNameColumn = array_flip( array_column( $organisationsArr, $orgArrKeys['name'], $orgArrKeys['code']  ) );
		$orgEmailColumn = array_flip( array_filter( array_column( $organisationsArr, $orgArrKeys['email'], $orgArrKeys['code'] ), [$this, '_filterEmail'] ) );
		
		$orgContactNames =  array_filter( array_column( $organisationsArr, $orgArrKeys['contact'], $orgArrKeys['code']  ) );
		$orgContactNamesMapped = array_map( array($this, '_splitNames'), $orgContactNames, array_keys($orgContactNames) );
		$orgContactNameColumn = [];
		array_walk_recursive($orgContactNamesMapped, function($v, $k) use (&$orgContactNameColumn) {
												 	array_key_exists($k, $orgContactNameColumn) ? array_push($orgContactNameColumn[$k], $v) : $orgContactNameColumn[$k][] = $v; 
												});
		return [ 
					'org_name' => $orgNameColumn, 
					'email' => $orgEmailColumn, 
					'contact_name_codes' => $orgContactNameColumn,
					'orgArrKeys' => $orgArrKeys,
				];
	}
	
	private function _splitNames($v, $k)
	{
		return array_fill_keys( explode(" ", $v), $k );
	}
	
	private function _filterEmail($v)
	{
		return !empty($v) && preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $v); //regex for emails. 
	}
		
	public function ti_compare() 
	{
		$contactsArr =  $this->_readContactFile();
		$contArrKeys = $this->_matchColumnKeys( $contactsArr[0], [
														'email' => 'e-mail',
														'contact' => 'surname', 
														'name' => 'organisation',
														]);
		$filteredOrgData = $this->_getOrgKeyColumns();
		$filteredOrgData['orgArrKeys']['org_code'] = count($filteredOrgData['orgArrKeys']) - 1;
		$filteredOrgData['orgArrKeys']['matching_key'] = count($filteredOrgData['orgArrKeys']) - 1;
		
		$noMatchCount = $ambiguousCount = $count = 0;
		$noMatch = $ambiguousContacts = $matchedContacts = [];
		foreach ( $contactsArr as $contact ) {
			$foundMatch = false;
			//test against Organisation Name first 
			if ( array_key_exists($contact[$contArrKeys['name']], $filteredOrgData['org_name']) ) {
				$foundMatch = true;
				$contact[$filteredOrgData['orgArrKeys']['org_code']] = $filteredOrgData['org_name'][$contact[$contArrKeys['name']]];
				$contact[$filteredOrgData['orgArrKeys']['matching_key']] = "Through Organisation Name";
				$matchedContacts[] = $contact;
				$count++;
			} elseif ( array_key_exists($contact[$contArrKeys['email']], $filteredOrgData['email']) ) {
				$foundMatch = true;
				$contact[$filteredOrgData['orgArrKeys']['org_code']] = $filteredOrgData['email'][$contact[$contArrKeys['email']]];
				$contact[$filteredOrgData['orgArrKeys']['matching_key']] = "Through Organisation Email";
				$matchedContacts[] = $contact;
				$count++;
			} elseif ( array_key_exists($contact[$contArrKeys['contact']], $filteredOrgData['contact_name_codes']) ) {
				if ( count($filteredOrgData['contact_name_codes'][$contact[$contArrKeys['contact']]]) == 1 ) {
					$foundMatch = true;
					$contact[$filteredOrgData['orgArrKeys']['org_code']] = implode( ", ", $filteredOrgData['contact_name_codes'][$contact[$contArrKeys['contact']]] );
					$contact[$filteredOrgData['orgArrKeys']['matching_key']] = "Through Organisation Surname (or First Name)";
					$matchedContacts[] = $contact;
					$count++;
				} else {
					$contact[$filteredOrgData['orgArrKeys']['org_code']] = implode( ", ", $filteredOrgData['contact_name_codes'][$contact[$contArrKeys['contact']]] );
					$contact[$filteredOrgData['orgArrKeys']['matching_key']] = empty($contact[$contArrKeys['contact']]) ? "No Surname provided" : "Multiple surnames matching";
					$ambiguousContacts[] = $contact;	
					$ambiguousCount++;
					/*foreach ( $filteredOrgData['contact_name_codes'][$contact[$contArrKeys['contact']]] ) {
						$contact[$filteredOrgData['orgArrKeys']['org_code']] = $filteredOrgData['contact_name_codes'][$contact[$contArrKeys['contact']]];
					}
					$contact[$filteredOrgData['orgArrKeys']['org_code']] = $filteredOrgData['email'][$contact[$contArrKeys['email']]];*/
				}
			} else {
				$contact['matching_key'] = "No match found";
				$noMatch[] = $contact;
				$noMatchCount++;
			}
		}
		
		$spreadSheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
		$spreadSheet->setActiveSheetIndex(0)
								->setTitle('Good Matches')
								->setCellValue('A1', 'Organisation')
								->setCellValue('B1', 'Code')
								->setCellValue('C1', 'First Name')
								->setCellValue('D1', 'Surname')
								->setCellValue('E1', 'E-Mail')
								->setCellValue('F1', 'Organisation Code')
								->setCellValue('G1', 'Matching Strategy');
		$spreadSheet->getActiveSheet()->fromArray($matchedContacts, null, 'A2');
		$spreadSheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$spreadSheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$spreadSheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$spreadSheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$spreadSheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$spreadSheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$spreadSheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		
		$spreadSheet2 = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
		$spreadSheet2->setActiveSheetIndex(0)
					->setTitle('Ambiguous Matches')
					->setCellValue('A1', 'Organisation')
					->setCellValue('B1', 'Code')
					->setCellValue('C1', 'First Name')
					->setCellValue('D1', 'Surname')
					->setCellValue('E1', 'E-Mail')
					->setCellValue('F1', 'Organisation Code')
					->setCellValue('G1', 'Matching Strategy');
		$spreadSheet2->getActiveSheet()->fromArray($ambiguousContacts, null, 'A2');
		$spreadSheet2->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$spreadSheet2->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$spreadSheet2->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$spreadSheet2->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$spreadSheet2->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$spreadSheet2->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$spreadSheet2->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		
		$spreadSheet2->createSheet();
		$spreadSheet2->setActiveSheetIndex(1)
					->setTitle('No Matches Found')
					->setCellValue('A1', 'Organisation')
					->setCellValue('B1', 'Code')
					->setCellValue('C1', 'First Name')
					->setCellValue('D1', 'Surname')
					->setCellValue('E1', 'E-Mail')
					->setCellValue('F1', 'Organisation Code')
					->setCellValue('G1', 'Matching Strategy');
		$spreadSheet2->getActiveSheet()->fromArray($noMatch, null, 'A2');
		$spreadSheet2->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$spreadSheet2->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$spreadSheet2->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$spreadSheet2->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$spreadSheet2->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$spreadSheet2->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$spreadSheet2->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xls($spreadSheet);
		$fileLocation = 'ti_uploads/' . $this->contacts_file->baseName . '_compared_' . date('d_m_Y_H_i_s') . '.xls';
		$writer->save($fileLocation);
		
		$writer2 = new \PhpOffice\PhpSpreadsheet\Writer\Xls($spreadSheet2);
		$fileLocation2 = 'ti_uploads/' . $this->contacts_file->baseName . '_poor_matches' . date('d_m_Y_H_i_s') . '.xls';
		$writer2->save($fileLocation2);
		return [ 
					'export' => [
						'fileLocation' => $fileLocation,
						'title' => $this->contacts_file->baseName . '_COMPARED',
						'fileLocation2' => $fileLocation2,
						'title2' => $this->contacts_file->baseName . '_POOR MATCHES',
					],
					'matched' => $matchedContacts, 
					'ambiguousMatch' => $ambiguousContacts, 
					'noMatch' => $noMatch,
					'counts' => [
								'noMatchCount' => $noMatchCount, 
								'ambiguousCount' => $ambiguousCount,
								'goodCount' => $count,
					],
				];
	}
	
	
	private function _matchColumnKeys($arr, $searchKeys) 
	{
		$result = [];
		foreach ($arr as $k => $v) {
			foreach ($searchKeys as $sk_index => $searchkey) {
				$found = false;
				if ( strpos( strtolower($v), $searchkey ) !== false ) {
					$found = true;
					$result[$sk_index] = $k;
					$searchKeys = array_diff( $searchKeys, [$searchkey] );
					break;
				}
			}
			
			if (!$found) {
				$result[$v] = $k;
			}
		}
		
		if ( !empty($searchKeys) ) {
			foreach ($searchKeys as $searchkey) {
				\Yii::warning("I could not find this key - '$searchkey'");
				$this->errorMessages = "I could not find this key - '$searchkey'";
			}
		}
		return $result;
	}
	
	public function getErrorMessages()
	{
		return $this->_errorMessages;
	}
	
	public function setErrorMessages($msg)
	{
		$this->_errorMessages .= "/r/n$msg ";
		$this->_error = true;
	}
	
	public function getHasError()
	{
		return $this->_error;
	}
	
}