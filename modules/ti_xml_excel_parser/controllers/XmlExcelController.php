<?php 
namespace app\modules\ti_xml_excel_parser\controllers;

use yii\web\Controller;
use yii\web\UploadedFile;
use app\modules\ti_xml_excel_parser\models\FileCompare;

class XmlExcelController extends Controller
{
    // ...
	public function actionBegin() 
	{
		\Yii::trace('Begin Action');
		$model = new FileCompare();
		if ($model->load(\Yii::$app->request->post())) {
			\Yii::trace('Post identified');
            $model->contacts_file = UploadedFile::getInstance($model, 'contacts_file');
            $model->org_xml_file = UploadedFile::getInstance($model, 'org_xml_file');
			\Yii::trace('Upload instanses set');
            if ($model->upload()) {
				\Yii::trace('Files uploaded?');
				try {
					$responseArr = $model->ti_compare();
				} catch (\Exception $e) {
					return $this->render('error');
				}
				return $this->render('response', ['data' => $responseArr]);
			} else {
				\Yii::trace('Something went wrong, unable to upload the files');
				return $this->render('error');
			}
		} else {
			\Yii::trace('Rendering form');
			return $this->render('files', ['model' => $model]);
		}
	}

}