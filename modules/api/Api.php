<?php

namespace app\modules\api;

/**
 * api module definition class
 */
class Api extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\api\controllers';

    /**
     * @inheritdoc
	 * root api controller where all the config is stored 
     */
    public function init()
	{
		parent::init();
		\Yii::$app->user->enableSession = false;// session is not needed for an api call 
		\Yii::$app->request->enableCookieValidation = false;
		\Yii::$app->request->parsers = [
        'application/json' => 'yii\web\JsonParser',]; // out put content in json 
		
	}
}
