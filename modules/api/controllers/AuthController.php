<?php

namespace app\modules\api\controllers;

/*class ProjectController extends \yii\web\Controller
{
    public function actionIndex()
    {
		echo 'this is test'; exit;
        //return $this->render('index');
    }

}

*/
class AuthController extends \yii\rest\Controller
{
	protected function verbs()
	{
	   return [
		   'login' => ['POST'],
	   ];
	}
	
	public static function allowedDomains()
	{
		return [
		   // '*',                    // star allows all domains
			'http://localhost:8100',
			'http://192.168.0.116:8100',
		];
	}  
	
	public function behaviors()
	{
		return array_merge(parent::behaviors(), [

			// For cross-domain AJAX request
			'corsFilter'  => [
				'class' => \yii\filters\Cors::className(),
				'cors'  => [
					// restrict access to domains:
					'Origin'                           => static::allowedDomains(),
					'Access-Control-Request-Method'    => ['POST'],
					'Access-Control-Allow-Credentials' => true,
					'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
				],
			],

		]);
	}
	
	public function actionLogin()
	{
    // init user login (username & password)
		$post_date = file_get_contents("php://input");
		$data = json_decode($post_date);
		
    	$username = $data->username;
		$password = $data->password ;
    	$response = [];
    	// validate input
		if(empty($username) || empty($password)){
      $response = [
        'status' => 'error',
        'message' => 'username & password is empty!',
        'data' => '',
      ];
    } else{
        // cari di database, ada nggak username dimaksud
        $user = \app\models\Userdb::findByUsername($username);
        // jika username ada maka
        	if(!empty($user)){
          // check, valid nggak passwordnya, jika valid maka bikin response success
          if($user->validatePassword($password)){
            $response = [
              'status' => 'success',
              'message' => 'login berhasil!',
              'data' => [
                  'id' => $user->id,
                  'username' => $user->username,
                  // token diambil dari field auth_key
                  'token' => $user->authKey,
              ]
            ];
          }
          // Jika password salah maka bikin response seperti ini
          else{
            $response = [
              'status' => 'error',
              'message' => 'password is not correct!',
              'data' => '',
            ];
          }
        }
        // Jika username tidak ditemukan bikin response kek gini
        	else{
          $response = [
            'status' => 'error',
            'message' => 'username is not correct!',
            'data' => '',
          ];
        }
    	}
		
	//Yii::$app->response->statusCode = 422;
    return $response;
	
	}
	
	public function actionFolder(){
		
	}
}

