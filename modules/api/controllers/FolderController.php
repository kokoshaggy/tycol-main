<?php

namespace app\modules\api\controllers;
use app\models\Folder;
use app\models\FolderTask;
use app\models\Foldersearch;
use app\models\Invoice;
use app\models\Receivedpurchaseorder;
use app\models\Correspondence;
use app\models\Order;
use app\models\Project;
use app\models\Payment;
use app\models\Paymentsource;
use app\models\Reminder;
use app\models\Corporation;
use app\models\Component as component;
use app\models\Foldercomponent as folderComponent;
use app\models\Task as task;
use app\models\Remark as Remarks;
/*class ProjectController extends \yii\web\Controller
{
    public function actionIndex()
    {
		echo 'this is test'; exit;
        //return $this->render('index');
    }

}

*/
class FolderController extends \yii\rest\Controller
{
	
	public static function allowedDomains()
	{
		return [
		    '*',                    // star allows all domains
			'http://localhost:8100',
			'http://192.168.0.116:8100',
		];
	}  
	
	public function behaviors()
	{
		return array_merge(parent::behaviors(), [

			// For cross-domain AJAX request
			'corsFilter'  => [
				'class' => \yii\filters\Cors::className(),
				'cors'  => [
					// restrict access to domains:
					'Origin'                           => static::allowedDomains(),
					'Access-Control-Request-Method'    => ['POST','GET'],
					'Access-Control-Allow-Credentials' => true,
					'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
				],
			],

		]);
	}
	
	public function actionIndex()
	{
		$folder = new Folder();
		$response = $folder->find()->all();
		if(!empty($response)){
			$data = ['status'=> 'success', 'data' => $response ];
		} else {
			
			$data = ['status'=> 'Error'];
		}
		
    	return $data;
	
	}
	
	
	public function actionView($id)
    {
		$folderComponent = folderComponent::countTotal($id);
		$remarks = new Remarks();
		$componetidarray = [];
		$remarksCont = $remarks->find()->where(['tyc_ref'=>$id])->count();
		
		foreach($folderComponent as $key => $value ){
			array_push($componetidarray, $value['component_id']);
			}
		
		$fetchComponents = component::findAll($componetidarray);
		$componetidArrayProjectSeperation = [];
		$componetidArrayInvoiceSeperation = [];
		$componetidArrayOrderSeperation = [];
		$componetidArrayRpoSeperation = [];
		$componetidArrayCorrespondenceSeperation = [];
		$componetidArrayPaymentSeperation =[];
		
		
		foreach($fetchComponents as $key => $value ){
			if($value['component_type'] == 'project'){
				array_push($componetidArrayProjectSeperation,$value['id']);
			} elseif($value['component_type'] == 'invoice'){
				array_push($componetidArrayInvoiceSeperation,$value['id']);
			} elseif($value['component_type'] == 'order'){
				array_push($componetidArrayOrderSeperation,$value['id']);
			} elseif($value['component_type'] == 'receivedpurchaseorder'){
				array_push($componetidArrayRpoSeperation,$value['id']);
			} elseif($value['component_type'] == 'correspondence'){
				array_push($componetidArrayCorrespondenceSeperation,$value['id']);
			} elseif($value['component_type'] == 'payment'){
				array_push($componetidArrayPaymentSeperation,$value['id']);
			}
		}
		// final output of all folder total components
		$realProject = Project::find()
			->where(['in','component_id',array_values($componetidArrayProjectSeperation)])
			->andWhere(['deleted'=>0])
			->all();
		
		$totalProject = count($realProject);
		$realProjectValue = [];
		foreach($realProject as $projectComponentId){
			array_push($realProjectValue,$projectComponentId['component_id']);
		}
		$totalInvoice = count(
			Invoice::find()
			->where(['in','component_id',array_values($componetidArrayInvoiceSeperation)])
			->andWhere(['deleted'=>0])
			->all());
		
		$totalOrder = count(
			Order::find()
			->where(['in','component_id',array_values($componetidArrayOrderSeperation)])
			->andWhere(['deleted'=>0])
			->all()) ;
		$totalRpo = count(
			Receivedpurchaseorder::find()
			->where(['in','component_id',array_values($componetidArrayRpoSeperation)])
			->andWhere(['deleted'=>0])
			->all()) ;
		
		$totalCorrespondence = count(
			Correspondence::find()
			->where(['in','component_id',array_values($componetidArrayCorrespondenceSeperation)])
			->andWhere(['deleted'=>0])
			->all()) ;
		
        //$thfolder = Folder::get_folder_project($id);
		$totalPayment = count(
			Payment::find()
			->where(['in','component_id',array_values($componetidArrayPaymentSeperation)])
			->andWhere(['deleted'=>0])
			->all());
		
		$totalPaymentSource = count(Paymentsource::find()->all());
		$data = [
				'status' => 'success',
				'data'=>[
					 ['componentName'=>'Project','componentTotal'=>$totalProject,'ComponentValue'=>serialize(array_values($realProjectValue))],
					
					 ['componentName'=>'Invoice','componentTotal'=>$totalProject,'ComponentValue'=>array_values($realProjectValue)],
					
				],
				
				];
		return $data;
       /* return $this->render('view', [
            //'projects' => $thfolder,
            'tyc_ref' => $this->findModel($id),
            'id' =>$id,
			'totalProject' => $totalProject,
			'totalInvoice' => $totalInvoice ,
			'totalRpo' => $totalRpo,
			'totalOrder' => $totalOrder,
			'remarks' => $remarksCont,
			'totalCorrespondence' => $totalCorrespondence,
			'totalPayment' => $totalPayment,
			'totalPaymentSource' => $totalPaymentSource,
			'invoiceIdValues' => array_values($componetidArrayInvoiceSeperation),
			'orderIdValues' => array_values($componetidArrayOrderSeperation),
			'projectIdValues' => array_values($componetidArrayProjectSeperation),
			'rpoIdValues' => array_values($componetidArrayRpoSeperation),
			'correspondenceIdValues' => array_values($componetidArrayCorrespondenceSeperation),
			'paymentIdValues' => array_values($componetidArrayPaymentSeperation),
			'fetchRemarks' => $remarks->findAll(['tyc_ref'=>$id]),
            
        ]);*/
    }
	
	public function actionGetReminder(){
		$model = new Reminder();
		$fetchData = $model->find()->where(['deleted'=>0])->all();
		$response = $fetchData;
		if(!empty($response)){
			$data = ['status'=> 'success', 'data' => $response ];
		} else {
			
			$data = ['status'=> 'Error'];
		}
		return $data;
	}
	
	public function actionUpdateReminder($id){
		$model = Reminder::findOne($id);
		$model->deleted = 1;
		if($model->save(false)){
			$data = ['status'=> 'success'];
		} else {
			$data = ['status'=> 'Error'];
		}
		
		return  $data;
		
	}
}

